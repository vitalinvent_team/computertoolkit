﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComputerToolkit
{
    public class Lists : List<Object>
    {

        //MAIN_LISTS***********************************************
        public Connections connections = null;
        public Files files = null;
        public FilesQuick filesQuick = null;
        public Hotkeys hotkeys = null;
        public Notes notes = null;
        public Plays plays = null;
        public Scripts scripts = null;
        public Sensors sensors = null;
        public Tasks tasks = null;
        public Universals universals = null;
        //universals
        //MAIN_LISTS***********************************************
        public Lists()
        {
            try
            {
                connections = new Connections();
            }
            catch (Exception ex) { Functions.Message(ex); }
            this.Add(connections);
            try
            {
                files = new Files();
            }
            catch (Exception ex) { Functions.Message(ex); }
            this.Add(files);
            try
            {
                filesQuick = new FilesQuick();
            }
            catch (Exception ex) { Functions.Message(ex); }
            this.Add(filesQuick);
            try
            {
                hotkeys = new Hotkeys();
            }
            catch (Exception ex) { Functions.Message(ex); }
            this.Add(hotkeys);
            try
            {
                notes = new Notes();
            }
            catch (Exception ex) { Functions.Message(ex); }
            this.Add(notes);
            try
            {
                plays = new Plays();
            }
            catch (Exception ex) { Functions.Message(ex); }
            this.Add(plays);
            try
            {
                scripts = new Scripts();
            }
            catch (Exception ex) { Functions.Message(ex); }
            this.Add(scripts);
            try
            {
                sensors = new Sensors();
            }
            catch (Exception ex) { Functions.Message(ex); }
            this.Add(sensors);
            try
            {
                tasks = new Tasks();
            }
            catch (Exception ex) { Functions.Message(ex); }
            this.Add(tasks);
            try
            {
                universals = new Universals();
            }
            catch (Exception ex) { Functions.Message(ex); }
            this.Add(universals);

        }
        public void SaveBase(Object obj)
        {
            try
            {
                ((ISavable)obj).SaveBase();
            }
            catch (Exception ex)
            { Functions.Message(ex); }
        }
        public void SaveBases()
        {
            List<Object> objects = this.ToList<Object>();
            foreach (Object obj in objects)
            {
                SaveBase(obj); ;
            }
        }
        public void ReadBase(Object obj)
        {
            try
            {
                ((ISavable)obj).ReadBase();
            }
            catch (Exception ex)
            { Functions.Message(ex); }
        }
        public void ReadBases()
        {
            List<Object> objects = this.ToList<Object>();
            foreach (Object obj in objects)
            {
                ReadBase(obj);
            }
        }

        internal void Encrypt()
        {
            foreach(ConnectionItem conn in connections)
            {
                if (!(conn.Adress == null))
                conn.Adress = ProcessIcon.Clean1(conn.Adress);
                if (!(conn.CommandLine == null))
                    conn.CommandLine = ProcessIcon.Clean1(conn.CommandLine);
                if (!(conn.Description == null))
                    conn.Description = ProcessIcon.Clean1(conn.Description);
                if (!(conn.Domain == null))
                    conn.Domain = ProcessIcon.Clean1(conn.Domain);
                if (!(conn.ExternalSettings == null))
                    conn.ExternalSettings = ProcessIcon.Clean1(conn.ExternalSettings);
                if (!(conn.HotString == null))
                    conn.HotString = ProcessIcon.Clean1(conn.HotString);
                if (!(conn.Password == null))
                    conn.Password = ProcessIcon.Clean1(conn.Password);
                if (!(conn.Port == null))
                    conn.Port = ProcessIcon.Clean1(conn.Port);
                if (!(conn.User == null))
                    conn.User = ProcessIcon.Clean1(conn.User);
            }
            connections.SaveBase();
            foreach (ScriptItem scr in scripts)
            {
                if (!(scr.Content == null))
                    scr.Content = ProcessIcon.Clean1(scr.Content);
            }
            scripts.SaveBase();
        }

        internal void Decrypt()
        {
            foreach (ConnectionItem conn in connections)
            {
                if (!(conn.Adress == null))
                    conn.Adress = ProcessIcon.Clean2(conn.Adress);
                if (!(conn.CommandLine == null))
                    conn.CommandLine = ProcessIcon.Clean2(conn.CommandLine);
                if (!(conn.Description == null))
                    conn.Description = ProcessIcon.Clean2(conn.Description);
                if (!(conn.Domain == null))
                    conn.Domain = ProcessIcon.Clean2(conn.Domain);
                if (!(conn.ExternalSettings == null))
                    conn.ExternalSettings = ProcessIcon.Clean2(conn.ExternalSettings);
                if (!(conn.HotString == null))
                    conn.HotString = ProcessIcon.Clean2(conn.HotString);
                if (!(conn.Password == null))
                    conn.Password = ProcessIcon.Clean2(conn.Password);
                if (!(conn.Port == null))
                    conn.Port = ProcessIcon.Clean2(conn.Port);
                if (!(conn.User == null))
                    conn.User = ProcessIcon.Clean2(conn.User);
            }
            connections.SaveBase();
            foreach (ScriptItem scr in scripts)
            {
                if (!(scr.Content == null))
                    scr.Content = ProcessIcon.Clean2(scr.Content);
            }
            scripts.SaveBase();
        }
    }
}
