﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class Plays : List<PlayItem>, ISavable
    {

        private string appPath;

        [XmlElement("listGroups")]
        public List<string> listGroups = new List<string>();
        public Plays()
        {
            appPath = Application.StartupPath.ToString();            
            ReadBase();
        }
        public void ReadBase()
        {
            string fileName = appPath + "\\" + this.GetType().Name + ".xml";
            try
            {
                
                string xmlString = File.ReadAllText(fileName);
                List<PlayItem> plays = xmlString.DeserializeFromStringXml<List<PlayItem>>();
                if (plays != null)
                {
                    this.Clear();
                    foreach (PlayItem play in plays)
                    {
                        this.Add(play);
                        if (play.Group == null) play.Group = "";
                        if (listGroups.FindAll(S => S.Equals(play.Group.ToString())).Count == 0)
                            listGroups.Add(play.Group);
                    }
                }
            }
            catch (Exception ex) {
                if (!File.Exists(fileName))
                    SaveBase();
                Functions.Message(ex);
            }
        }

        public void SaveBase()
        {
            try
            {
                string xmlString = EngineSerialization.SerializeToStringXml(this);
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                File.WriteAllText(fileName, xmlString);
            }
            catch (Exception ex) { Functions.Message(ex); }

        }
        //public void ReadBase()
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_PLAYS", "ComputerToolkitPlays.bin"), FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<PlayItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //}
        //public void ReadBase(string filePath)
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(filePath, FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<PlayItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //}
        //private void FillThisFromList(List<PlayItem> tempList)
        //{
        //    this.Clear();
        //    foreach (PlayItem play in tempList)
        //    {
        //        this.Add(play);
        //    }
        //}
        //public void SaveBase()
        //{
        //    using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_PLAYS", "ComputerToolkitPlays.bin"), FileMode.Create))
        //    {
        //        var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //        bformatter.Serialize(stream, this);
        //    }
        //    try
        //    {
        //        string xmlString = EngineSerialization.SerializeToStringXml(this);
        //        string fileName = appPath + "\\" + this.GetType().Name + ".xml";
        //        File.WriteAllText(fileName, xmlString);
        //    }
        //    catch (Exception ex) { Functions.Message(ex.Message); }
                
        //}
    }
}
