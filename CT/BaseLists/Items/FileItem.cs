﻿using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class FileItem : IItem
    {
        public bool Checked { get; set; }
        [XmlIgnore]  
        public Icon IconFile { get; set; }
        //public Icon IconFile
        //{
        //    get
        //    {
        //        return Functions.Base64ToIcon(IconFileString);
        //    }
        //    set
        //    {
        //        IconFileString = Functions.IconToBase64(value);
        //        IconFile = value;
        //    }
        //}
        private string _IconFileString;
        public string IconFileString
        {
            get
            {
                return _IconFileString;
            }
            set
            {
                IconFile = Functions.Base64ToIcon(value);
                _IconFileString = value;
            }
        }
        //public string IconFileString { get; set; }
        private String _Name;
        //public String Name { get { return ProcessIcon.DeClean2(_Name); } set { _Name = ProcessIcon.Clean1(value); } }
        public String Name { get { return _Name; } set { _Name = value; } }

        public Boolean Deleted { get; set; }


        private String _Group;

        public String Group { get; set; }

        public Boolean ZipAllFolder { get; set; }
        public Boolean ZipOneFile { get; set; }

        private String _PathFile;
        //public String PathFile { get { return ProcessIcon.DeClean2(_PathFile); } set { _PathFile = ProcessIcon.Clean1(value); } }
        public String PathFile { get { return _PathFile; } set { _PathFile = value; } }
        public bool UniquePath { get; set; }

        private String _PathFileFull;
        //public String PathFileFull { get { return ProcessIcon.DeClean2(_PathFileFull); } set { _PathFileFull = ProcessIcon.Clean1(value); } }
        public String PathFileFull { get { return _PathFileFull; } set { _PathFileFull = value; } }

        private String _Comments;
        //public String Comments { get { return ProcessIcon.DeClean2(_Comments); } set { _Comments = ProcessIcon.Clean1(value); } }
        public String Comments { get { return _Comments; } set { _Comments = value; } }

        private String _CompanyName;
        //public String CompanyName { get { return ProcessIcon.DeClean2(_CompanyName); } set { _CompanyName = ProcessIcon.Clean1(value); } }
        public String CompanyName { get { return _CompanyName; } set { _CompanyName = value; } }

        private String _FileDescription;
        //public String FileDescription { get { return ProcessIcon.DeClean2(_FileDescription); } set { _FileDescription = ProcessIcon.Clean1(value); } }
        public String FileDescription { get { return _FileDescription; } set { _FileDescription = value; } }

        private String _FileName;
        //public String FileName { get { return ProcessIcon.DeClean2(_FileName); } set { _FileName = ProcessIcon.Clean1(value); } }
        public String FileName { get { return _FileName; } set { _FileName = value; } }

        private String _FileVersion="1";
        //public String FileVersion { get { return ProcessIcon.DeClean2(_FileVersion); } set { _FileVersion = ProcessIcon.Clean1(value); } }
        public String FileVersion { get { return _FileVersion; } set { _FileVersion = value; } }

        private String _OriginalFilename;
        //public String OriginalFilename { get { return ProcessIcon.DeClean2(_OriginalFilename); } set { _OriginalFilename = ProcessIcon.Clean1(value); } }
        public String OriginalFilename { get { return _OriginalFilename; } set { _OriginalFilename = value; } }

        private String _ProductName;
        //public String ProductName { get { return ProcessIcon.DeClean2(_ProductName); } set { _ProductName = ProcessIcon.Clean1(value); } }
        public String ProductName { get { return _ProductName; } set { _ProductName = value; } }

        private String _Guid;
        public String Guid { get; set; }

        private String _UrlProgrammToUpdate;
        //public String UrlProgrammToUpdate { get { return ProcessIcon.DeClean2(_UrlProgrammToUpdate); } set { _UrlProgrammToUpdate = ProcessIcon.Clean1(value); } }
        public String UrlProgrammToUpdate { get { return _UrlProgrammToUpdate; } set { _UrlProgrammToUpdate = value; } }
        public String Date { get; set; }

        public FileItem()
        {
        }
        public FileItem(String guid, bool checkedVar, string namefile, string fileGroup, Icon icon, string pathFile, string pathFileFull, string сomments, string сompanyName, string fileDescription, string fileName, string fileVersion, string originalFilename, string productName,Boolean uniquePath)
        {
            this.Guid = guid;
            this.Checked = checkedVar;
            this.Group = fileGroup;
            this.Name = namefile;
            if (icon == null)
            {
                this.IconFile = Resources.file_icon16;
            } 
            else
            {
                this.IconFileString = Functions.IconToBase64(icon);
                this.IconFile = Functions.Base64ToIcon(IconFileString);
            }
            this.PathFile = pathFile;
            this.PathFileFull = pathFileFull;
            this.Comments = сomments;
            this.CompanyName = сompanyName;
            this.FileDescription = fileDescription;
            this.FileName = fileName;
            if (fileVersion.Length==0)
            {
                this.FileVersion = "1";
            }
            else
            {
                this.FileVersion = fileVersion;
            }
            this.OriginalFilename = originalFilename;
            this.ProductName = productName;
            this.Date = DateTime.Now.ToString();
            this.UniquePath = uniquePath;
        }

        public override string ToString()
        {
            return "Path:" + this.PathFile + System.Environment.NewLine + 
                   "Comments:" + this.Comments + System.Environment.NewLine + 
                   "CompanyName:" + this.CompanyName + System.Environment.NewLine +
                   "OriginalFilename:" + this.OriginalFilename + System.Environment.NewLine +
                   "ProductName:" + this.ProductName + System.Environment.NewLine +
                   "FileVersion:" + this.FileVersion + System.Environment.NewLine;
        }

    }
}

