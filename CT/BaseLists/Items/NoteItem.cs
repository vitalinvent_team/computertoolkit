﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComputerToolkit
{
    [Serializable]
    public class NoteItem : IItem
    {
        public Boolean Checked { get; set; }

        private String _Name;
        //public String Name { get { return ProcessIcon.DeClean2(_Name); } set { _Name = ProcessIcon.Clean1(value); } }
        public String Name { get { return _Name; } set { _Name = value; } }

        private String _Content;
        //public String Content { get { return ProcessIcon.DeClean2(_Content); } set { _Content = ProcessIcon.Clean1(value); } }
        public String Content { get { return _Content; } set { _Content = value; } }
        public String Date { get; set; }

        private String _Group;
        public String Group { get; set; }

        private String _Teg;
        //public String Teg { get { return ProcessIcon.DeClean2(_Teg); } set { _Teg = ProcessIcon.Clean1(value); } }
        public String Teg { get { return _Teg; } set { _Teg = value; } }
        public String Guid { get; set; }

        private String _User;
        //public String User { get { return ProcessIcon.DeClean2(_User); } set { _User = ProcessIcon.Clean1(value); } }
        public String User { get { return _User; } set { _User = value; } }
        public NoteItem() { }
        public NoteItem(String guid, String nameNote, String dateNote, String noteContent)
        {
            this.Guid = ProcessIcon.Clean2(guid);
            this.Name = ProcessIcon.Clean2(nameNote);
            this.Date = dateNote;
            this.Content = ProcessIcon.Clean2(noteContent);
            this.User = Settings.GetString("UserGuid");
            if (dateNote.Length == 0) Date = DateTime.Now.ToString();
        }
        public NoteItem(String guid, String nameNote, String dateNote, String noteContent, String group, String teg)
        {
            this.Guid = guid;
            this.Name = ProcessIcon.Clean2(nameNote);
            this.Date = dateNote;
            this.Content = ProcessIcon.Clean2(noteContent);
            this.Group = ProcessIcon.Clean2(group);
            this.Teg = ProcessIcon.Clean2(teg);
            this.User = Settings.GetString("UserGuid");
        }
        public override string ToString()
        {
            return this.Guid + " " + this.Date + " " + this.Content;
        }
    }
}
