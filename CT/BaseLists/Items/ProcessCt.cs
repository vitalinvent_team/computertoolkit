﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;

namespace ComputerToolkit
{
    //[Serializable]
    public class ProcessCt : Process
    {
        public String Path { get;private set; }
        public Image ScreenshootWindow { get; set; }

        public String Arguments { get; set; }
        public ProcessCt() { }
        public ProcessCt(string path)
        {
            Path = path;
        }
        public ProcessCt(string path,string arguments)
        {
            Path = path;
            Arguments = arguments;
        }
        public void StartProcess()
        {
            try
            {
                //Process.Start(Path);
                Process ffmpeg = new Process();
                ffmpeg.StartInfo.FileName = Path;
                ffmpeg.StartInfo.Arguments = Arguments;
                ffmpeg.Start();
            } catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        private Image GetScreenShootWindow(IntPtr handle)
        {
            ScreenCapture sc = new ScreenCapture();
            return sc.CaptureWindow(handle);
        }
    }
}
