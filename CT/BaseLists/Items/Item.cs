﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]

    [XmlRoot("Item")]
    public class Item
    {
        [XmlElement("name")]
        public String name { get; set; }

        [XmlElement("value")]
        public String value { get; set; }
        public Item() {}
    }
}
