﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComputerToolkit
{
    public class UserItem : IItem
    {
        public string Name { get; set; }
        public string Guid { get; set; }
        public string Password { get; set; }
        public UserItem() { }
        public UserItem(string name, string guid, string pass)
        {
            this.Guid = guid;
            this.Name = name;
            this.Password = pass;
        }
    }
}
