﻿using System;

namespace ComputerToolkit
{
    public interface Script
    {
        object Main();
    }
}
