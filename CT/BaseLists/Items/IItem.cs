﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComputerToolkit
{
    public interface IItem
    {
        String Guid { get; set; }
    }
}
