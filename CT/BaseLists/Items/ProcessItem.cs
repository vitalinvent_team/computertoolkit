﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class ProcessItem
    {
        public String Path { get;private set; }
                [XmlIgnore]  
        public Image ScreenshootWindow { get; set; }
       //public Image ScreenshootWindow
       // {
       //     get
       //     {
       //         return Functions.Base64ToImage(IconFileString);
       //     }
       //     set
       //     {
       //         IconFileString = Functions.ImageToBase64(value);
       //         ScreenshootWindow = value;
       //     }
       // }
        private string _ImageScreenshootWindow;
        public string ImageScreenshootWindow
        {
            get
            {
                return _ImageScreenshootWindow;
            }
            set
            {
                ScreenshootWindow = Functions.Base64ToImage(value);
                _ImageScreenshootWindow = value;
            }
        } 
        //public string IconFileString { get; set; }
        public ProcessItem() { }
        public ProcessItem(string path)
        {
            Path = path;
        }
        public void StartProcess()
        {
            Process.Start(Path);
        }
        private Image GetScreenShootWindow(IntPtr handle)
        {
            ScreenCapture sc = new ScreenCapture();
            return sc.CaptureWindow(handle);
        }

        public IntPtr MainWindowHandle { get; set; }

        public int Id { get; set; }
    }
}
