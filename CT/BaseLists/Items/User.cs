﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComputerToolkit
{
    [Serializable]
    public class User
    {
        public string Name { get; set; }
        public string Guid { get; set; }
        //public string _Password;
        //public string Password { get { return ProcessIcon.Clean2(_Password); } set { _Password = value; } }
        public string Password { get; set; }
        public string Email { get; set; }
        public User()
        {
            Name = Settings.Get("textBoxUser", "user");
            Password = Settings.Get("textBoxPassword", "");
            Email = Functions.CleanDefDecode(Settings.Get("textBoxUserEmail", ""));
            Guid = Settings.Get("UserGuid", "");
            if (Guid.Length == 0)
            {
                Guid = System.Guid.NewGuid().ToString();
                Settings.Set("UserGuid", "Guid");
            }
        }
        public User(string name,  string pass,string email,string guid)
        {
            this.Guid = guid;
            this.Name = name;
            this.Password = ProcessIcon.Clean1(pass);
            this.Email = email;
        }

    }
}
