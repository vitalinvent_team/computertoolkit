﻿using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ComputerToolkit
{
    [Serializable]
    public class UniversalItem: List<UniversalAtom>
    {
        public String Name {get;set;}
        public String Guid { get; set; }
        public String Description { get; set; }
        public String Group { get; set; }
        public String Date { get; set; }
        public UniversalItem() { }
        public UniversalItem(int value, String name, String group, String description,string date)
        {
            this.Name = name;
            this.Description = description;
            this.Guid = System.Guid.NewGuid().ToString();
            this.Add(new UniversalAtom(value));
            Date = date;
            if (date.Length == 0) Date = DateTime.Now.ToString();
        }

        public UniversalItem(Image value, String name, String group, String description, string date)
        {
            this.Name = name;
            this.Description = description;
            this.Guid = System.Guid.NewGuid().ToString();
            this.Add(new UniversalAtom(value));
            Date = date;
        }
        public UniversalItem(String value, String name, String group, String description, string date)
        {
            this.Name = name;
            this.Description = description;
            this.Guid = System.Guid.NewGuid().ToString();
            this.Add(new UniversalAtom(value));
            Date = date;
        }
        public UniversalItem(Boolean value, String name, String group, String description, string date)
        {
            this.Name = name;
            this.Description = description;
            this.Guid = System.Guid.NewGuid().ToString();
            this.Add(new UniversalAtom(value));
            Date = date;
        }
        public UniversalItem(Byte value, String name, String group, String description, string date)
        {
            this.Name = name;
            this.Description = description;
            this.Guid = System.Guid.NewGuid().ToString();
            this.Add(new UniversalAtom(value));
            Date = date;
        }
        public UniversalItem(Byte[] value, String name, String group, String description, string date)
        {
            this.Name = name;
            this.Description = description;
            this.Guid = System.Guid.NewGuid().ToString();
            this.Add(new UniversalAtom(value));
            Date = date;
        }
        public UniversalItem(string[] value, String name, String group, String description, string date)
        {
            this.Name = name;
            this.Description = description;
            this.Guid = System.Guid.NewGuid().ToString();
            this.Add(new UniversalAtom(value));
            Date = date;
        }
    }
}
