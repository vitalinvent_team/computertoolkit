﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComputerToolkit
{
    [Serializable]
    public class SensorItem : IItem
    {
        public Boolean Checked { get; set; }
        public Boolean Enabled { get; set; }
        public string Name { get; set; }
        public string Guid { get; set; }
        public string HotString { get; set; }
        public String Date { get; set; }
        public String Group { get; set; }
        public SensorItem() { }
        public SensorItem(string name,String hotString,string date)
        {
            this.Guid = System.Guid.NewGuid().ToString();
            this.Name = name;
            this.HotString = hotString;
            Date = date;
            if (date.Length == 0) Date = DateTime.Now.ToString();
            Group = "";
        }
    }
}
