﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class UniversalAtom : IItem
    {
        public string[] CSV { get; set; }
        public String Name { get; set; }
        public Boolean Checked { get; set; }
        public AtomType Type { get; set; }
                [XmlIgnore]  
        public Icon Icon
        {
            get
            {
                return Functions.Base64ToIcon(IconString);
            }
            set
            {
                IconString = Functions.IconToBase64(value);
                Icon = value;
            }
        }
        public string IconString { get; set; }
        public Image Image
        {
            get
            {
                return Functions.Base64ToImage(ImageString);
            }
            set
            {
                ImageString = Functions.ImageToBase64(value);
                Image = value;
            }
        }
        public string ImageString { get; set; }
        public String String { get; set; }
        public Byte[] ByteArray { get; set; }
        public Byte Byte { get; set; }
        public Boolean Boolean { get; set; }
        public int Integer { get; set; }
        public String Guid { get; set; }
        public UniversalAtom() { }
        public UniversalAtom(Image image)
        {
            this.Type = AtomType.Image;
            this.Image = image;
            this.Guid = System.Guid.NewGuid().ToString();
        }

        public UniversalAtom(string varString)
        {
            this.Type = AtomType.String;
            this.String = varString;
            this.Guid = System.Guid.NewGuid().ToString();
        }
        public UniversalAtom(Byte[] byteArray)
        {
            this.Type = AtomType.ByteArray;
            this.ByteArray = byteArray;
            this.Guid = System.Guid.NewGuid().ToString();
        }
        public UniversalAtom(Byte varByte)
        {
            this.Type = AtomType.Byte;
            this.Byte = varByte;
            this.Guid = System.Guid.NewGuid().ToString();
        }
        public UniversalAtom(Boolean boolean)
        {
            this.Type = AtomType.Boolean;
            this.Boolean = boolean;
            this.Guid = System.Guid.NewGuid().ToString();
        }
        public UniversalAtom(int integer)
        {
            this.Type = AtomType.Integer;
            this.Integer = integer;
            this.Guid = System.Guid.NewGuid().ToString();
        }

        public UniversalAtom(string[] value)
        {
            this.Type = AtomType.CSV;
            this.CSV = value;
            this.Guid = System.Guid.NewGuid().ToString();
        }
        public override string ToString()
        {
            switch (Type)
            {
                case AtomType.CSV:
                    return this.CSV.ToString();
                case AtomType.ByteArray:
                    return "Byte_array: "+this.ByteArray.Length+" bytes.";
                default:
                    return "Atom.";
                    break;
            }
        }
    }
}
