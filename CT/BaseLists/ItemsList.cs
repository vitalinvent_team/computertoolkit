﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]

    [XmlRoot("ItemsList")]
    public class ItemsList : List<Item>
    {
    }
}
