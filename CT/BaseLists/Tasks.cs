﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class Tasks : List<TaskItem>, ISavable
    {
        private string appPath;

        [XmlElement("listGroups")]
        public List<string> listGroups = new List<string>();
        public Tasks()
        {
            appPath = Application.StartupPath.ToString();            
            ReadBase();
        }
        public void ReadBase()
        {
            try
            {
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                string xmlString = File.ReadAllText(fileName);
                List<TaskItem> files = xmlString.DeserializeFromStringXml<List<TaskItem>>();
                if (files != null)
                {
                    this.Clear();
                    foreach (TaskItem file in files)
                    {
                        this.Add(file);
                        if (file.Group == null) file.Group = "";
                        if (listGroups.FindAll(S => S.Equals(file.Group.ToString())).Count == 0)
                            listGroups.Add(file.Group);
                    }
                }

            }
            catch (Exception ex) { Functions.Message(ex); }
        }

        public void SaveBase()
        {
            try
            {
                string xmlString = this.SerializeToStringXml();//EngineSerialization.SerializeToStringXml(this);
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                File.WriteAllText(fileName, xmlString);
            }
            catch (Exception ex) { Functions.Message(ex); }
        }

    }
}
