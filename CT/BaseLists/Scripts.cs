﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class Scripts : List<ScriptItem>, ISavable
    {
        private string appPath;

        [XmlElement("listGroups")]
        public List<string> listGroups = new List<string>();
        public Scripts()
        {
            appPath = Application.StartupPath.ToString();            
            ReadBase();
        }
        public void ReadBase()
        {
            string fileName = appPath + "\\" + this.GetType().Name + ".xml";
            string xmlString = File.ReadAllText(fileName);
            List<ScriptItem> scripts = xmlString.DeserializeFromStringXml<List<ScriptItem>>();
            if (scripts != null)
            {
                this.Clear();
                foreach (ScriptItem script in scripts)
                {
                    this.Add(script);
                    if (script.Group == null) script.Group = "";
                    if (listGroups.FindAll(S => S.Equals(script.Group.ToString())).Count == 0)
                        listGroups.Add(script.Group);
                }
            }
        }

        public void SaveBase()
        {
            try
            {
                string xmlString = EngineSerialization.SerializeToStringXml(this);
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                File.WriteAllText(fileName, xmlString);
            }
            catch (Exception ex) { Functions.Message(ex); }
        }



        //private Object GetAppMethods(String name)
        //{
        //    Object obj = null;
        //    switch (name)
        //    {
        //        case "ConnectionsList":
        //            obj = new Connections();
        //            break;
        //        case "NotesList":
        //            obj = new Notes();
        //            break;
        //        case "FilesList":
        //            obj = new Files();
        //            break;
        //        case "ScriptsList":
        //            obj = new Scripts();
        //            break;
        //    }
        //    return obj;
        //}
        //public void ReadBase()
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_SCRIPTS", "ComputerToolkitScripts.bin"), FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<ScriptItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //    FillGroups();
        //}
        //public void ReadBase(string filePath)
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(filePath, FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<ScriptItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //    FillGroups();
        //}
        //private void FillThisFromList(List<ScriptItem> tempList)
        //{
        //    this.Clear();
        //    foreach (ScriptItem script in tempList)
        //    {
        //        //string curUser = Settings.GetString("textBoxUser") + ";" + Settings.GetString("textBoxUserEmail");
        //        //connection.UserInfo = curUser;
        //        this.Add(script);
        //    }
        //    foreach (ScriptItem item in tempList)
        //    {
        //        if (item.Date == null)
        //        {
        //            item.Date = DateTime.Now.ToString();
        //        }
        //    }
        //    foreach (ScriptItem connection in tempList)
        //    {
        //        connection.IconScriptString = Functions.IconToBase64(connection.IconScript);
        //    }
        //}
        //public void SaveBase()
        //{
        //    CleanBase();
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_SCRIPTS", "ComputerToolkitScripts.bin"), FileMode.Create))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            bformatter.Serialize(stream, this);
        //        }
        //    }
        //    catch { }
        //    try
        //    {
        //        string xmlString = EngineSerialization.SerializeToStringXml(this);
        //        string fileName = appPath + "\\" + this.GetType().Name + ".xml";
        //        File.WriteAllText(fileName, xmlString);
        //    }
        //    catch (Exception ex) { Functions.Message(ex.Message); }
        //}
        //private void FillGroups()
        //{
        //    try
        //    {
        //        foreach (ScriptItem script in this)
        //        {
        //            if (listGroups.FindAll(S => S.Contains(script.Group.ToString())).Count == 0)
        //                listGroups.Add(script.Group);
        //        }
        //    }
        //    catch { }
        //}
        //private void CleanBase()
        //{
        //    try
        //    {
        //        foreach (ScriptItem script in this)
        //        {
        //            if (script.Group.IndexOf("\\") > -1)
        //            {
        //                script.Group = script.Group.Replace("\\", "");
        //            }

        //        }
        //    }
        //    catch { }
        //}
    }
}
