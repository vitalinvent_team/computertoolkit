﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit.Forms
{
    public partial class FormDebug : Form
    {
        public FormDebug()
        {
            InitializeComponent();
        }
        private void FormDebug_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == false)
            {
                SaveSettings();
                
            }
            else
            {
                LoadSettings();
            }
        }

        private void FormDebug_Load(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void FormDebug_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        }

        private void FormDebug_Activated(object sender, EventArgs e)
        {
        }

        private void label1_TextChanged(object sender, EventArgs e)
        {
            //dataGridView1.Rows.Add(label1.Text);
        }
        void LoadSettings()
        {
            this.Top = Settings.Get("fFormDebug_Top", 100);
            this.Left = Settings.Get("fFormDebug_Left", 100);
            this.Height = Settings.Get("fFormDebug_Height", 300);
            this.Width = Settings.Get("fFormDebug_Width", 700);
        }
        void SaveSettings()
        {
            Settings.Set("fFormDebug_Top", this.Top);
            Settings.Set("fFormDebug_Left", this.Left);
            Settings.Set("fFormDebug_Height", this.Height);
            Settings.Set("fFormDebug_Width", this.Width);
        }
        public void Print(string progress)
        {
            if (this.Visible == false)
            {
                this.Show();
                this.Visible = true;
                LoadSettings();
            }

            if (label1.InvokeRequired)
                label1.BeginInvoke(new Action(() => label1.Text = progress));
            else
                label1.Text = progress;
            if (label1.InvokeRequired)
                dataGridView1.BeginInvoke(new Action(() => dataGridView1.Rows.Add(progress)));
            else
                dataGridView1.Rows.Add(progress);
        }
    }
}
