﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit.Forms
{
    public partial class FormDebug2 : Form
    {
        public FormDebug2()
        {
            InitializeComponent();
        }
        private void FormProgress_Load(object sender, EventArgs e)
        {
            //LoadSettings();
        }
        private void FormProgress_FormClosing(object sender, FormClosingEventArgs e)
        {
            //SaveSettings();
            //this.Hide();
            //e.Cancel = true;
        }
        void LoadSettings()
        {
            this.Top = Settings.Get("fFormDebug_Top", 100);
            this.Left = Settings.Get("fFormDebug_Left", 100);
            this.Height = Settings.Get("fFormDebug_Height", 300);
            this.Width = Settings.Get("fFormDebug_Width", 700);
        }
        void SaveSettings()
        {
            Settings.Get("fFormDebug_Top", this.Top);
            Settings.Get("fFormDebug_Left", this.Left);
            Settings.Get("fFormDebug_Height", this.Height);
            Settings.Get("fFormDebug_Width", this.Width);
        }
        public void Print(string progress)
        {
            if (label1.InvokeRequired)
                label1.BeginInvoke(new Action(() => label1.Text = progress));
            else
                label1.Text = progress;

        }

        private void label1_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows.Add(label1.Text);
        }

        private void FormProgress_VisibleChanged(object sender, EventArgs e)
        {
            //if (this.Visible == false)
            //{
            //    LoadSettings();
            //} else
            //{
            //    SaveSettings();
            //}
        }

        private void FormProgress_Activated(object sender, EventArgs e)
        {
            LoadSettings();
        }
    }
}
