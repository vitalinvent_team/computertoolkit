﻿using AxMSTSCLib;

namespace ComputerToolkit
{
    partial class FormConnections
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConnections));
            this.tabControlConnections = new System.Windows.Forms.TabControl();
            this.axMsTscAxNotSafeForScripting1 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting2 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting3 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting4 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting5 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting6 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting7 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting8 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting9 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting10 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting11 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting12 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting13 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting14 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting15 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting16 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting17 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting18 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting19 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.axMsTscAxNotSafeForScripting20 = new AxMSTSCLib.AxMsTscAxNotSafeForScripting();
            this.AxMsRdpClient2NotSafeForScripting1 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.buttonMaximize = new System.Windows.Forms.Button();
            this.axMsRdpClient2NotSafeForScripting2 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting3 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting4 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting5 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting6 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting7 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting8 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting9 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting10 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting11 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting12 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting13 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting14 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting15 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting16 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting17 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting18 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting19 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            this.axMsRdpClient2NotSafeForScripting20 = new AxMSTSCLib.AxMsRdpClient2NotSafeForScripting();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMsRdpClient2NotSafeForScripting1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting20)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlConnections
            // 
            this.tabControlConnections.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlConnections.Location = new System.Drawing.Point(-7, -4);
            this.tabControlConnections.Name = "tabControlConnections";
            this.tabControlConnections.SelectedIndex = 0;
            this.tabControlConnections.Size = new System.Drawing.Size(925, 595);
            this.tabControlConnections.TabIndex = 0;
            this.tabControlConnections.SelectedIndexChanged += new System.EventHandler(this.tabControlConnections_SelectedIndexChanged);
            this.tabControlConnections.Click += new System.EventHandler(this.tabControlConnections_Click);
            this.tabControlConnections.DoubleClick += new System.EventHandler(this.tabControlConnections_DoubleClick);
            this.tabControlConnections.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabControlConnections_MouseClick_1);
            this.tabControlConnections.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tabControlConnections_MouseDown);
            this.tabControlConnections.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tabControlConnections_MouseMove);
            this.tabControlConnections.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tabControlConnections_MouseUp);
            // 
            // axMsTscAxNotSafeForScripting1
            // 
            this.axMsTscAxNotSafeForScripting1.Enabled = true;
            this.axMsTscAxNotSafeForScripting1.Location = new System.Drawing.Point(92, 42);
            this.axMsTscAxNotSafeForScripting1.Name = "axMsTscAxNotSafeForScripting1";
            this.axMsTscAxNotSafeForScripting1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting1.OcxState")));
            this.axMsTscAxNotSafeForScripting1.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting1.TabIndex = 7;
            this.axMsTscAxNotSafeForScripting1.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting2
            // 
            this.axMsTscAxNotSafeForScripting2.Enabled = true;
            this.axMsTscAxNotSafeForScripting2.Location = new System.Drawing.Point(358, 196);
            this.axMsTscAxNotSafeForScripting2.Name = "axMsTscAxNotSafeForScripting2";
            this.axMsTscAxNotSafeForScripting2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting2.OcxState")));
            this.axMsTscAxNotSafeForScripting2.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting2.TabIndex = 8;
            this.axMsTscAxNotSafeForScripting2.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting3
            // 
            this.axMsTscAxNotSafeForScripting3.Enabled = true;
            this.axMsTscAxNotSafeForScripting3.Location = new System.Drawing.Point(366, 204);
            this.axMsTscAxNotSafeForScripting3.Name = "axMsTscAxNotSafeForScripting3";
            this.axMsTscAxNotSafeForScripting3.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting3.OcxState")));
            this.axMsTscAxNotSafeForScripting3.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting3.TabIndex = 9;
            this.axMsTscAxNotSafeForScripting3.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting4
            // 
            this.axMsTscAxNotSafeForScripting4.Enabled = true;
            this.axMsTscAxNotSafeForScripting4.Location = new System.Drawing.Point(374, 212);
            this.axMsTscAxNotSafeForScripting4.Name = "axMsTscAxNotSafeForScripting4";
            this.axMsTscAxNotSafeForScripting4.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting4.OcxState")));
            this.axMsTscAxNotSafeForScripting4.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting4.TabIndex = 10;
            this.axMsTscAxNotSafeForScripting4.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting5
            // 
            this.axMsTscAxNotSafeForScripting5.Enabled = true;
            this.axMsTscAxNotSafeForScripting5.Location = new System.Drawing.Point(382, 220);
            this.axMsTscAxNotSafeForScripting5.Name = "axMsTscAxNotSafeForScripting5";
            this.axMsTscAxNotSafeForScripting5.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting5.OcxState")));
            this.axMsTscAxNotSafeForScripting5.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting5.TabIndex = 11;
            this.axMsTscAxNotSafeForScripting5.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting6
            // 
            this.axMsTscAxNotSafeForScripting6.Enabled = true;
            this.axMsTscAxNotSafeForScripting6.Location = new System.Drawing.Point(390, 228);
            this.axMsTscAxNotSafeForScripting6.Name = "axMsTscAxNotSafeForScripting6";
            this.axMsTscAxNotSafeForScripting6.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting6.OcxState")));
            this.axMsTscAxNotSafeForScripting6.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting6.TabIndex = 12;
            this.axMsTscAxNotSafeForScripting6.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting7
            // 
            this.axMsTscAxNotSafeForScripting7.Enabled = true;
            this.axMsTscAxNotSafeForScripting7.Location = new System.Drawing.Point(398, 236);
            this.axMsTscAxNotSafeForScripting7.Name = "axMsTscAxNotSafeForScripting7";
            this.axMsTscAxNotSafeForScripting7.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting7.OcxState")));
            this.axMsTscAxNotSafeForScripting7.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting7.TabIndex = 13;
            this.axMsTscAxNotSafeForScripting7.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting8
            // 
            this.axMsTscAxNotSafeForScripting8.Enabled = true;
            this.axMsTscAxNotSafeForScripting8.Location = new System.Drawing.Point(406, 244);
            this.axMsTscAxNotSafeForScripting8.Name = "axMsTscAxNotSafeForScripting8";
            this.axMsTscAxNotSafeForScripting8.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting8.OcxState")));
            this.axMsTscAxNotSafeForScripting8.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting8.TabIndex = 14;
            this.axMsTscAxNotSafeForScripting8.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting9
            // 
            this.axMsTscAxNotSafeForScripting9.Enabled = true;
            this.axMsTscAxNotSafeForScripting9.Location = new System.Drawing.Point(414, 252);
            this.axMsTscAxNotSafeForScripting9.Name = "axMsTscAxNotSafeForScripting9";
            this.axMsTscAxNotSafeForScripting9.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting9.OcxState")));
            this.axMsTscAxNotSafeForScripting9.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting9.TabIndex = 15;
            this.axMsTscAxNotSafeForScripting9.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting10
            // 
            this.axMsTscAxNotSafeForScripting10.Enabled = true;
            this.axMsTscAxNotSafeForScripting10.Location = new System.Drawing.Point(422, 260);
            this.axMsTscAxNotSafeForScripting10.Name = "axMsTscAxNotSafeForScripting10";
            this.axMsTscAxNotSafeForScripting10.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting10.OcxState")));
            this.axMsTscAxNotSafeForScripting10.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting10.TabIndex = 16;
            this.axMsTscAxNotSafeForScripting10.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting11
            // 
            this.axMsTscAxNotSafeForScripting11.Enabled = true;
            this.axMsTscAxNotSafeForScripting11.Location = new System.Drawing.Point(430, 268);
            this.axMsTscAxNotSafeForScripting11.Name = "axMsTscAxNotSafeForScripting11";
            this.axMsTscAxNotSafeForScripting11.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting11.OcxState")));
            this.axMsTscAxNotSafeForScripting11.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting11.TabIndex = 17;
            this.axMsTscAxNotSafeForScripting11.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting12
            // 
            this.axMsTscAxNotSafeForScripting12.Enabled = true;
            this.axMsTscAxNotSafeForScripting12.Location = new System.Drawing.Point(438, 276);
            this.axMsTscAxNotSafeForScripting12.Name = "axMsTscAxNotSafeForScripting12";
            this.axMsTscAxNotSafeForScripting12.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting12.OcxState")));
            this.axMsTscAxNotSafeForScripting12.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting12.TabIndex = 18;
            this.axMsTscAxNotSafeForScripting12.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting13
            // 
            this.axMsTscAxNotSafeForScripting13.Enabled = true;
            this.axMsTscAxNotSafeForScripting13.Location = new System.Drawing.Point(446, 284);
            this.axMsTscAxNotSafeForScripting13.Name = "axMsTscAxNotSafeForScripting13";
            this.axMsTscAxNotSafeForScripting13.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting13.OcxState")));
            this.axMsTscAxNotSafeForScripting13.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting13.TabIndex = 19;
            this.axMsTscAxNotSafeForScripting13.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting14
            // 
            this.axMsTscAxNotSafeForScripting14.Enabled = true;
            this.axMsTscAxNotSafeForScripting14.Location = new System.Drawing.Point(454, 292);
            this.axMsTscAxNotSafeForScripting14.Name = "axMsTscAxNotSafeForScripting14";
            this.axMsTscAxNotSafeForScripting14.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting14.OcxState")));
            this.axMsTscAxNotSafeForScripting14.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting14.TabIndex = 20;
            this.axMsTscAxNotSafeForScripting14.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting15
            // 
            this.axMsTscAxNotSafeForScripting15.Enabled = true;
            this.axMsTscAxNotSafeForScripting15.Location = new System.Drawing.Point(462, 300);
            this.axMsTscAxNotSafeForScripting15.Name = "axMsTscAxNotSafeForScripting15";
            this.axMsTscAxNotSafeForScripting15.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting15.OcxState")));
            this.axMsTscAxNotSafeForScripting15.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting15.TabIndex = 21;
            this.axMsTscAxNotSafeForScripting15.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting16
            // 
            this.axMsTscAxNotSafeForScripting16.Enabled = true;
            this.axMsTscAxNotSafeForScripting16.Location = new System.Drawing.Point(470, 308);
            this.axMsTscAxNotSafeForScripting16.Name = "axMsTscAxNotSafeForScripting16";
            this.axMsTscAxNotSafeForScripting16.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting16.OcxState")));
            this.axMsTscAxNotSafeForScripting16.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting16.TabIndex = 22;
            this.axMsTscAxNotSafeForScripting16.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting17
            // 
            this.axMsTscAxNotSafeForScripting17.Enabled = true;
            this.axMsTscAxNotSafeForScripting17.Location = new System.Drawing.Point(478, 316);
            this.axMsTscAxNotSafeForScripting17.Name = "axMsTscAxNotSafeForScripting17";
            this.axMsTscAxNotSafeForScripting17.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting17.OcxState")));
            this.axMsTscAxNotSafeForScripting17.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting17.TabIndex = 23;
            this.axMsTscAxNotSafeForScripting17.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting18
            // 
            this.axMsTscAxNotSafeForScripting18.Enabled = true;
            this.axMsTscAxNotSafeForScripting18.Location = new System.Drawing.Point(486, 324);
            this.axMsTscAxNotSafeForScripting18.Name = "axMsTscAxNotSafeForScripting18";
            this.axMsTscAxNotSafeForScripting18.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting18.OcxState")));
            this.axMsTscAxNotSafeForScripting18.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting18.TabIndex = 24;
            this.axMsTscAxNotSafeForScripting18.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting19
            // 
            this.axMsTscAxNotSafeForScripting19.Enabled = true;
            this.axMsTscAxNotSafeForScripting19.Location = new System.Drawing.Point(494, 332);
            this.axMsTscAxNotSafeForScripting19.Name = "axMsTscAxNotSafeForScripting19";
            this.axMsTscAxNotSafeForScripting19.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting19.OcxState")));
            this.axMsTscAxNotSafeForScripting19.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting19.TabIndex = 25;
            this.axMsTscAxNotSafeForScripting19.Visible = false;
            // 
            // axMsTscAxNotSafeForScripting20
            // 
            this.axMsTscAxNotSafeForScripting20.Enabled = true;
            this.axMsTscAxNotSafeForScripting20.Location = new System.Drawing.Point(502, 340);
            this.axMsTscAxNotSafeForScripting20.Name = "axMsTscAxNotSafeForScripting20";
            this.axMsTscAxNotSafeForScripting20.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsTscAxNotSafeForScripting20.OcxState")));
            this.axMsTscAxNotSafeForScripting20.Size = new System.Drawing.Size(192, 192);
            this.axMsTscAxNotSafeForScripting20.TabIndex = 26;
            this.axMsTscAxNotSafeForScripting20.Visible = false;
            // 
            // AxMsRdpClient2NotSafeForScripting1
            // 
            this.AxMsRdpClient2NotSafeForScripting1.Enabled = true;
            this.AxMsRdpClient2NotSafeForScripting1.Location = new System.Drawing.Point(532, 12);
            this.AxMsRdpClient2NotSafeForScripting1.Name = "AxMsRdpClient2NotSafeForScripting1";
            this.AxMsRdpClient2NotSafeForScripting1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxMsRdpClient2NotSafeForScripting1.OcxState")));
            this.AxMsRdpClient2NotSafeForScripting1.Size = new System.Drawing.Size(192, 192);
            this.AxMsRdpClient2NotSafeForScripting1.TabIndex = 26;
            this.AxMsRdpClient2NotSafeForScripting1.Visible = false;
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.Image = global::ComputerToolkit.Properties.Resources.minus;
            this.buttonMinimize.Location = new System.Drawing.Point(883, -1);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(18, 18);
            this.buttonMinimize.TabIndex = 27;
            this.buttonMinimize.UseVisualStyleBackColor = true;
            this.buttonMinimize.Click += new System.EventHandler(this.buttonMinimize_Click);
            // 
            // buttonMaximize
            // 
            this.buttonMaximize.Image = global::ComputerToolkit.Properties.Resources.upload;
            this.buttonMaximize.Location = new System.Drawing.Point(901, -1);
            this.buttonMaximize.Name = "buttonMaximize";
            this.buttonMaximize.Size = new System.Drawing.Size(18, 18);
            this.buttonMaximize.TabIndex = 28;
            this.buttonMaximize.UseVisualStyleBackColor = true;
            this.buttonMaximize.Click += new System.EventHandler(this.buttonMaximize_Click);
            // 
            // axMsRdpClient2NotSafeForScripting2
            // 
            this.axMsRdpClient2NotSafeForScripting2.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting2.Location = new System.Drawing.Point(543, 22);
            this.axMsRdpClient2NotSafeForScripting2.Name = "axMsRdpClient2NotSafeForScripting2";
            this.axMsRdpClient2NotSafeForScripting2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting2.OcxState")));
            this.axMsRdpClient2NotSafeForScripting2.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting2.TabIndex = 29;
            this.axMsRdpClient2NotSafeForScripting2.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting3
            // 
            this.axMsRdpClient2NotSafeForScripting3.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting3.Location = new System.Drawing.Point(556, 30);
            this.axMsRdpClient2NotSafeForScripting3.Name = "axMsRdpClient2NotSafeForScripting3";
            this.axMsRdpClient2NotSafeForScripting3.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting3.OcxState")));
            this.axMsRdpClient2NotSafeForScripting3.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting3.TabIndex = 30;
            this.axMsRdpClient2NotSafeForScripting3.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting4
            // 
            this.axMsRdpClient2NotSafeForScripting4.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting4.Location = new System.Drawing.Point(564, 38);
            this.axMsRdpClient2NotSafeForScripting4.Name = "axMsRdpClient2NotSafeForScripting4";
            this.axMsRdpClient2NotSafeForScripting4.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting4.OcxState")));
            this.axMsRdpClient2NotSafeForScripting4.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting4.TabIndex = 31;
            this.axMsRdpClient2NotSafeForScripting4.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting5
            // 
            this.axMsRdpClient2NotSafeForScripting5.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting5.Location = new System.Drawing.Point(572, 46);
            this.axMsRdpClient2NotSafeForScripting5.Name = "axMsRdpClient2NotSafeForScripting5";
            this.axMsRdpClient2NotSafeForScripting5.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting5.OcxState")));
            this.axMsRdpClient2NotSafeForScripting5.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting5.TabIndex = 32;
            this.axMsRdpClient2NotSafeForScripting5.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting6
            // 
            this.axMsRdpClient2NotSafeForScripting6.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting6.Location = new System.Drawing.Point(580, 54);
            this.axMsRdpClient2NotSafeForScripting6.Name = "axMsRdpClient2NotSafeForScripting6";
            this.axMsRdpClient2NotSafeForScripting6.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting6.OcxState")));
            this.axMsRdpClient2NotSafeForScripting6.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting6.TabIndex = 33;
            this.axMsRdpClient2NotSafeForScripting6.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting7
            // 
            this.axMsRdpClient2NotSafeForScripting7.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting7.Location = new System.Drawing.Point(588, 62);
            this.axMsRdpClient2NotSafeForScripting7.Name = "axMsRdpClient2NotSafeForScripting7";
            this.axMsRdpClient2NotSafeForScripting7.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting7.OcxState")));
            this.axMsRdpClient2NotSafeForScripting7.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting7.TabIndex = 34;
            this.axMsRdpClient2NotSafeForScripting7.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting8
            // 
            this.axMsRdpClient2NotSafeForScripting8.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting8.Location = new System.Drawing.Point(596, 70);
            this.axMsRdpClient2NotSafeForScripting8.Name = "axMsRdpClient2NotSafeForScripting8";
            this.axMsRdpClient2NotSafeForScripting8.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting8.OcxState")));
            this.axMsRdpClient2NotSafeForScripting8.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting8.TabIndex = 35;
            this.axMsRdpClient2NotSafeForScripting8.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting9
            // 
            this.axMsRdpClient2NotSafeForScripting9.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting9.Location = new System.Drawing.Point(604, 78);
            this.axMsRdpClient2NotSafeForScripting9.Name = "axMsRdpClient2NotSafeForScripting9";
            this.axMsRdpClient2NotSafeForScripting9.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting9.OcxState")));
            this.axMsRdpClient2NotSafeForScripting9.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting9.TabIndex = 36;
            this.axMsRdpClient2NotSafeForScripting9.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting10
            // 
            this.axMsRdpClient2NotSafeForScripting10.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting10.Location = new System.Drawing.Point(612, 86);
            this.axMsRdpClient2NotSafeForScripting10.Name = "axMsRdpClient2NotSafeForScripting10";
            this.axMsRdpClient2NotSafeForScripting10.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting10.OcxState")));
            this.axMsRdpClient2NotSafeForScripting10.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting10.TabIndex = 37;
            this.axMsRdpClient2NotSafeForScripting10.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting11
            // 
            this.axMsRdpClient2NotSafeForScripting11.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting11.Location = new System.Drawing.Point(620, 94);
            this.axMsRdpClient2NotSafeForScripting11.Name = "axMsRdpClient2NotSafeForScripting11";
            this.axMsRdpClient2NotSafeForScripting11.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting11.OcxState")));
            this.axMsRdpClient2NotSafeForScripting11.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting11.TabIndex = 38;
            this.axMsRdpClient2NotSafeForScripting11.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting12
            // 
            this.axMsRdpClient2NotSafeForScripting12.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting12.Location = new System.Drawing.Point(628, 102);
            this.axMsRdpClient2NotSafeForScripting12.Name = "axMsRdpClient2NotSafeForScripting12";
            this.axMsRdpClient2NotSafeForScripting12.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting12.OcxState")));
            this.axMsRdpClient2NotSafeForScripting12.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting12.TabIndex = 39;
            this.axMsRdpClient2NotSafeForScripting12.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting13
            // 
            this.axMsRdpClient2NotSafeForScripting13.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting13.Location = new System.Drawing.Point(636, 110);
            this.axMsRdpClient2NotSafeForScripting13.Name = "axMsRdpClient2NotSafeForScripting13";
            this.axMsRdpClient2NotSafeForScripting13.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting13.OcxState")));
            this.axMsRdpClient2NotSafeForScripting13.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting13.TabIndex = 40;
            this.axMsRdpClient2NotSafeForScripting13.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting14
            // 
            this.axMsRdpClient2NotSafeForScripting14.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting14.Location = new System.Drawing.Point(644, 118);
            this.axMsRdpClient2NotSafeForScripting14.Name = "axMsRdpClient2NotSafeForScripting14";
            this.axMsRdpClient2NotSafeForScripting14.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting14.OcxState")));
            this.axMsRdpClient2NotSafeForScripting14.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting14.TabIndex = 41;
            this.axMsRdpClient2NotSafeForScripting14.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting15
            // 
            this.axMsRdpClient2NotSafeForScripting15.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting15.Location = new System.Drawing.Point(652, 126);
            this.axMsRdpClient2NotSafeForScripting15.Name = "axMsRdpClient2NotSafeForScripting15";
            this.axMsRdpClient2NotSafeForScripting15.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting15.OcxState")));
            this.axMsRdpClient2NotSafeForScripting15.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting15.TabIndex = 42;
            this.axMsRdpClient2NotSafeForScripting15.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting16
            // 
            this.axMsRdpClient2NotSafeForScripting16.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting16.Location = new System.Drawing.Point(660, 134);
            this.axMsRdpClient2NotSafeForScripting16.Name = "axMsRdpClient2NotSafeForScripting16";
            this.axMsRdpClient2NotSafeForScripting16.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting16.OcxState")));
            this.axMsRdpClient2NotSafeForScripting16.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting16.TabIndex = 43;
            this.axMsRdpClient2NotSafeForScripting16.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting17
            // 
            this.axMsRdpClient2NotSafeForScripting17.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting17.Location = new System.Drawing.Point(668, 142);
            this.axMsRdpClient2NotSafeForScripting17.Name = "axMsRdpClient2NotSafeForScripting17";
            this.axMsRdpClient2NotSafeForScripting17.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting17.OcxState")));
            this.axMsRdpClient2NotSafeForScripting17.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting17.TabIndex = 44;
            this.axMsRdpClient2NotSafeForScripting17.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting18
            // 
            this.axMsRdpClient2NotSafeForScripting18.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting18.Location = new System.Drawing.Point(676, 154);
            this.axMsRdpClient2NotSafeForScripting18.Name = "axMsRdpClient2NotSafeForScripting18";
            this.axMsRdpClient2NotSafeForScripting18.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting18.OcxState")));
            this.axMsRdpClient2NotSafeForScripting18.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting18.TabIndex = 45;
            this.axMsRdpClient2NotSafeForScripting18.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting19
            // 
            this.axMsRdpClient2NotSafeForScripting19.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting19.Location = new System.Drawing.Point(684, 164);
            this.axMsRdpClient2NotSafeForScripting19.Name = "axMsRdpClient2NotSafeForScripting19";
            this.axMsRdpClient2NotSafeForScripting19.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting19.OcxState")));
            this.axMsRdpClient2NotSafeForScripting19.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting19.TabIndex = 46;
            this.axMsRdpClient2NotSafeForScripting19.Visible = false;
            // 
            // axMsRdpClient2NotSafeForScripting20
            // 
            this.axMsRdpClient2NotSafeForScripting20.Enabled = true;
            this.axMsRdpClient2NotSafeForScripting20.Location = new System.Drawing.Point(692, 174);
            this.axMsRdpClient2NotSafeForScripting20.Name = "axMsRdpClient2NotSafeForScripting20";
            this.axMsRdpClient2NotSafeForScripting20.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient2NotSafeForScripting20.OcxState")));
            this.axMsRdpClient2NotSafeForScripting20.Size = new System.Drawing.Size(192, 192);
            this.axMsRdpClient2NotSafeForScripting20.TabIndex = 47;
            this.axMsRdpClient2NotSafeForScripting20.Visible = false;
            // 
            // FormConnections
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 584);
            this.ControlBox = false;
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting20);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting19);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting18);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting17);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting16);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting15);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting14);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting13);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting12);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting11);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting10);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting9);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting8);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting7);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting6);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting5);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting4);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting3);
            this.Controls.Add(this.axMsRdpClient2NotSafeForScripting2);
            this.Controls.Add(this.buttonMinimize);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting20);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting19);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting18);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting17);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting16);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting15);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting14);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting13);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting12);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting11);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting10);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting9);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting8);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting7);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting6);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting5);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting4);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting3);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting2);
            this.Controls.Add(this.AxMsRdpClient2NotSafeForScripting1);
            this.Controls.Add(this.buttonMaximize);
            this.Controls.Add(this.tabControlConnections);
            this.Controls.Add(this.axMsTscAxNotSafeForScripting1);
            this.Name = "FormConnections";
            this.ShowIcon = false;
            this.Activated += new System.EventHandler(this.FormConnections_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormConnections_FormClosing);
            this.Load += new System.EventHandler(this.FormConnections_Load);
            this.DoubleClick += new System.EventHandler(this.FormConnections_DoubleClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormConnections_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FormConnections_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FormConnections_MouseUp);
            this.Resize += new System.EventHandler(this.FormConnections_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsTscAxNotSafeForScripting20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AxMsRdpClient2NotSafeForScripting1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient2NotSafeForScripting20)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlConnections;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting1;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting2;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting3;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting4;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting5;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting6;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting7;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting8;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting9;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting10;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting11;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting12;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting13;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting14;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting15;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting16;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting17;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting18;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting19;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting20;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.Button buttonMaximize;

        private AxMsRdpClient2NotSafeForScripting AxMsRdpClient2NotSafeForScripting1;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting2;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting3;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting4;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting5;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting6;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting7;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting8;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting9;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting10;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting11;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting12;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting13;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting14;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting15;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting16;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting17;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting18;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting19;
        private AxMsRdpClient2NotSafeForScripting axMsRdpClient2NotSafeForScripting20;
    }
}