﻿using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ThreadState = System.Threading.ThreadState;

namespace ComputerToolkit
{
    public partial class FormSensMon : Form
    {
        String GridHeaderswithdataGridViewSensorsValues="";
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        String[] name;
        String[] value;
        //Sensors sensors = new Sensors();
        private Thread threadUpdateSensors;
        private int senscount;
        private Point firstPoint;
        private bool mouseIsDown;
        Logs logs = new Logs("sensors");

        public FormSensMon()
        {
            InitializeComponent();
            timer.Tick+=timer_Tick;
            timer.Enabled = false;
            senscount = ProcessIcon.lists.sensors.Count;
            name = new string[senscount];
            value = new string[senscount];
            //threadUpdateSensors = new Thread(GetValuesSensors);
        }

        private void buttonTimer_click(object sender, EventArgs e)
        {
        }
        private void SetTimer()
        {
            try
            {
            int value = int.Parse(Settings.Get("SensorsTimeRefresh", "30"));
            //if (value < 30)
            //{
            //    timer.Interval=30000;
            //    Settings.Set("SensorsTimeRefresh", "30");
            //}
            if (value > 0)
            {
                timer.Interval = value*1000;
                timer.Enabled = true;
            }
            }
            catch { }
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            UpdateSensors();
        }

        private delegate void SetSensValue(string val);

        private void GetValuesSensors()
        {
            try
            {
                int idx = 0;
                foreach (SensorItem sensor in ProcessIcon.lists.sensors)
                {
                    if (sensor.Enabled)
                    {
                        value[idx] = Functions.HotstringExecute(sensor.HotString).ToString();
                        //value[idx] = Functions.ExecuteScript(sensor.HotString);
                        name[idx] = sensor.Name;
                        idx++;
                    }
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }

        }
        private void UpdateSensors()
        {
            try
            {
                //if (threadUpdateSensors.ThreadState == ThreadState.Running)
                //{

                    threadUpdateSensors = new Thread(GetValuesSensors);
                    threadUpdateSensors.Start();
                //}
                //else
                //{
                //    threadUpdateSensors.Start();
                //}
                int idx = 0;
                dataGridViewSensorsValues.Rows.Clear();
                //for (int i = 0; i < senscount; i++)
                foreach (SensorItem sensor in ProcessIcon.lists.sensors)
                {
                    if (sensor.Enabled)
                    {
                        dataGridViewSensorsValues.Rows.Add(name[idx], value[idx]);
                        if (sensor.Enabled)
                        {
                            logs.Add(name[idx] + " - " + value[idx]);
                        }
                        idx++;
                    }
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }


        }

        private void ShellCommandLine(String cmd,int currentLine)
        {
            Process p = new Process();
            p.StartInfo.RedirectStandardOutput = true;
            //p.WaitForExit();
            //StringBuilder value = new StringBuilder();
            while ( ! p.HasExited ) {
                //value.Append(p.StandardOutput.ReadToEnd());
                value[currentLine] = p.StandardOutput.ReadToEnd();
            }
        }


        private void notifyIcon1_Click(object sender, System.EventArgs e)
        {
            this.Show();
        }
        private void FormInfo_Load(object sender, EventArgs e)
        {
            checkBoxPin.Checked = true;
            SetTimer();
            GetSettings();
        }
        private void GetSettings()
        {
            try
            {
                SetTimer();
                this.Left = Settings.Get("FormInfoFormLeft", 0);
                this.Top = Settings.Get("FormInfoFormTop", 0);
                this.Width = Settings.Get("FormInfoFormWidth", 312);
                this.Height = Settings.Get("FormInfoFormHeight", 167);
                GridHeaderswithdataGridViewSensorsValues = Settings.Get("GridHeaderswithdataGridViewSensorsValues", "");
                SetGridHeadersWidth(GridHeaderswithdataGridViewSensorsValues, dataGridViewSensorsValues);
            }
            catch
            {
            }
        }
        private void SaveSettings()
        {
            try
            {
                Settings.Set("FormInfoFormLeft", this.Left);
                Settings.Set("FormInfoFormTop", this.Top);
                 Settings.Set("FormInfoFormWidth", this.Width);
                 Settings.Set("FormInfoFormHeight", this.Height);
                GridHeaderswithdataGridViewSensorsValues = GetGridHeadersWidth(dataGridViewSensorsValues);
                Settings.Set("GridHeaderswithdataGridViewSensorsValues", GridHeaderswithdataGridViewSensorsValues);
                Settings.Set("SensorsTimeRefresh", (int)timer.Interval/1000);
            }
            catch { }
        }

    private void FormInfo_FormClosing(object sender, FormClosingEventArgs e)
    {
        SaveSettings();
        timer.Enabled = false;
    }
    private String GetGridHeadersWidth(DataGridView DGW)
    {
        string retVal = "";
        foreach (DataGridViewColumn GH in DGW.Columns)
        {
            retVal += GH.Width.ToString() + ";";
        }
        return retVal;
    }
    private void SetGridHeadersWidth(string param, DataGridView DGW)
    {
        try
        {
            string[] width = param.Split(';');
            int indx = 0;
            foreach (DataGridViewColumn GH in DGW.Columns)
            {
                GH.Width = int.Parse(width[indx]);
                indx++;
            }
        }
        catch { }
    }

    private void FormSensMon_MouseDown(object sender, MouseEventArgs e)
    {
        firstPoint = e.Location;
        mouseIsDown = true;

    }

    private void FormSensMon_MouseMove(object sender, MouseEventArgs e)
    {
        if (mouseIsDown)
        {
            // Get the difference between the two points
            int xDiff = firstPoint.X - e.Location.X;
            int yDiff = firstPoint.Y - e.Location.Y;

            // Set the new point
            int x = this.Location.X - xDiff;
            int y = this.Location.Y - yDiff;
            this.Location = new Point(x, y);
        }

    }

    private void FormSensMon_MouseUp(object sender, MouseEventArgs e)
    {
        mouseIsDown = false;

    }

    private void checkBoxPin_CheckedChanged(object sender, EventArgs e)
    {
        if (checkBoxPin.Checked)
        {
            checkBoxPin.Image = ComputerToolkit.Properties.Resources.pin_small_pressed;
            this.TopMost = true;
        }
        else
        {
            checkBoxPin.Image = ComputerToolkit.Properties.Resources.pin_small;
            this.TopMost = false;
        }

    }

    private void button1_Click(object sender, EventArgs e)
    {
        timer.Enabled = false;
        this.Hide();
    }

    private void buttonTimer_Click_1(object sender, EventArgs e)
    {
        string value = Settings.Get("SensorsTimeRefresh", "30");
        if (EngineDialogs.InputBox("Time to refresh from 30 secods. (обновление от 30 сек)", "Please input time in seconds.", ref value) == DialogResult.OK)
        {
            //if (int.Parse(value) < 30)
            //{
            //    value = "30";
            //}
            Settings.Set("SensorsTimeRefresh", value);
        }
        SetTimer();

    }

    private void dataGridViewSensorsValues_MouseDown(object sender, MouseEventArgs e)
    {
        firstPoint = e.Location;
        mouseIsDown = true;
    }

    private void dataGridViewSensorsValues_MouseUp(object sender, MouseEventArgs e)
    {
        mouseIsDown = false;
    }

    private void dataGridViewSensorsValues_MouseMove(object sender, MouseEventArgs e)
    {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

    private void buttonLogs_Click(object sender, EventArgs e)
    {
        if (listBoxLog.Visible)
        {
            listBoxLog.Visible = false;
        }
        else
        {
                listBoxLog.Visible = true;
                try
                {
                    listBoxLog.Items.Clear();
                }
                catch { };
                string[] lgs = logs.GetAllLogs();
                try
                {
                foreach (String log in lgs)
                {
                    listBoxLog.Items.Add(log);
                }
                }
                catch { };
        }
    }

    private void buttonClearLogs_Click(object sender, EventArgs e)
    {
            logs.ClearAllLogs();
            try
            {
                listBoxLog.Items.Clear();
            }
            catch { };
            //listBoxLog.DataSource = logs.GetAllLogs();
    }

        private void listBoxLog_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void listBoxLog_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void listBoxLog_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }
    }
}
