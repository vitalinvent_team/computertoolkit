﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit
{
    public partial class FormQuickMagnetickChooser : Form
    {
        private enum sideMagnet
        {
            Top,
            Left,
            Right
        }
        int height = 0;
        int width = 0;
        Timer timer = new Timer();
        private sideMagnet currentSide;
        private Point firstPoint;
        private bool mouseIsDown;
        //Files files= new Files();
        //FilesQuick filesQuick = new FilesQuick();
        //Connections connections = new Connections();
        List<QuickItem> QuickList= new List<QuickItem>();
        private bool AllreadyRefreshed=false;
        //private StickyWindow stickyWindow;
        //private System.ComponentModel.Container components = null;
        public FormQuickMagnetickChooser()
        {
            InitializeComponent();
            //stickyWindow = new StickyWindow(this);
        }
        private void CheckForOut()
        {
            //if (this.Left > this.Left + this.Width)
            //{
            //    this.Left = SystemInformation.WorkingArea.Width - (this.Width - 6);
            //}
            //if (this.Top < 0 - this.Height)
            //{
            //    this.Top = 0 - (this.Height - 6);
            //}

        }
        private void FormQuickMagnetickChooser_Load(object sender, EventArgs e)
        {
            this.Left=Settings.Get("FormQuickMagnetickChooserLeft", 100);
            this.Top = Settings.Get("FormQuickMagnetickChooserTop", 0);
            switch (Settings.Get("FormQuickMagnetickChooserSide", "Top"))
            {
                case "Top":
                    currentSide = sideMagnet.Top;
                    break;
                case "Left":
                    currentSide = sideMagnet.Left;
                    break;
                case "Right":
                    currentSide = sideMagnet.Right;
                    break;
            }


            timer.Interval = 1000;
            timer.Tick += timer_Tick;
            //stickyWindow.StickToScreen = true;
            foreach (FileItem file in ProcessIcon.lists.files)
            {
                QuickList.Add(new QuickItem(file.Name,file));
            }
            foreach (FileItemQuick file in ProcessIcon.lists.filesQuick)
            {
                QuickList.Add(new QuickItem(file.Name, file));
            }
            foreach (ConnectionItem conn in ProcessIcon.lists.connections)
            {
                QuickList.Add(new QuickItem(conn.Name, conn));
            }
            FillTreeView();


            FindSide();
            timer.Enabled = false;
            this.Height = 300;
            switch (currentSide)
            {
                case sideMagnet.Left:
                    this.Left = 0 - (this.Width - 6);
                    break;
                case sideMagnet.Right:
                    this.Left = SystemInformation.VirtualScreen.Width - 6;
                    break;
                case sideMagnet.Top:
                    this.Top = 0 - (this.Height - 6);
                    break;
            }

        }

        void timer_Tick(object sender, EventArgs e)
        {
            FindSide();
            timer.Enabled = false;
            this.Height = 300;
            switch (currentSide)
            {
                case sideMagnet.Left:
                    this.Left = 0 - (this.Width - 6);
                    break;
                case sideMagnet.Right:
                    this.Left = SystemInformation.VirtualScreen.Width - 6;
                    break;
                case sideMagnet.Top:
                    this.Top = 0 - (this.Height - 6);
                    break;
            }
            AllreadyRefreshed = false;
            SaveSettings();

        }
        private void FillTreeView()
        {
            treeViewQuckChooser.Nodes.Clear();
            TreeNode filesRoot = treeViewQuckChooser.Nodes.Add("Files", "Files",3);
            TreeNode filesQickRoot = treeViewQuckChooser.Nodes.Add("Quick files", "Quick files",3);
            TreeNode connectionsRoot = treeViewQuckChooser.Nodes.Add("Connections", "Connections", 2);
            foreach (QuickItem item in QuickList)
            {
                if (item.obj.GetType() == typeof(FileItem))
                    filesRoot.Nodes.Add(item.Name, item.Name,1);
                if (item.obj.GetType() == typeof(FileItemQuick))
                    filesQickRoot.Nodes.Add(item.Name, item.Name, 1);
                if (item.obj.GetType() == typeof(ConnectionItem))
                    if (ProcessIcon.formConnection.FindTab(item.Name))
                    {
                        connectionsRoot.Nodes.Add(item.Name, item.Name, 6);
                    }
                    else
                    {
                        connectionsRoot.Nodes.Add(item.Name, item.Name, 0);
                    }
            }
        }
        private class QuickItem
        {
            public String Name {get;set;}
            public Object obj { get; set; }
            public QuickItem(String name,Object obje)
            {
                this.Name = name;
                this.obj = obje;
            }
        }
        private void FindSide()
        {
            if (this.Top <= 0)
                currentSide = sideMagnet.Top;
            if (this.Left <= 0)
                currentSide = sideMagnet.Left;
            if (this.Left + this.Width >= SystemInformation.VirtualScreen.Width)
                currentSide = sideMagnet.Right;
        }

        private void FormQuickMagnetickChooser_Move(object sender, EventArgs e)
        {
            FindSide();
            //stickyWindow.StickToScreen =false;

        }

        private void FormQuickMagnetickChooser_Leave(object sender, EventArgs e)
        {
            timer.Enabled = false;
        }

        private void treeViewQuckChooser_Leave(object sender, EventArgs e)
        {
            timer.Enabled = false;
        }

        private void FormQuickMagnetickChooser_MouseMove(object sender, MouseEventArgs e)
        {
            timer.Enabled = true;
            this.Height = 520;
        }

        private void treeViewQuckChooser_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                timer.Enabled = false;
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
            else
            {
                this.Height = 520;
                switch (currentSide)
                {
                    case sideMagnet.Left:
                        this.Left = 0;
                        break;
                    case sideMagnet.Right:
                        this.Left = SystemInformation.VirtualScreen.Width - this.Width;
                        break;
                    case sideMagnet.Top:
                        this.Top = 0;
                        break;
                }
                if (!AllreadyRefreshed)
                RefreshConnections();
                AllreadyRefreshed = true;
            }
        }

        private void treeViewQuckChooser_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void treeViewQuckChooser_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void FormQuickMagnetickChooser_Activated(object sender, EventArgs e)
        {
        }

        private void treeViewQuckChooser_MouseLeave(object sender, EventArgs e)
        {
            timer.Enabled = true;
        }

        private void treeViewQuckChooser_MouseHover(object sender, EventArgs e)
        {
            timer.Enabled = false;
        }

        private void treeViewQuckChooser_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void FormQuickMagnetickChooser_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        }
        private void SaveSettings()
        {
            Settings.Set("FormQuickMagnetickChooserLeft", this.Left);
            Settings.Set("FormQuickMagnetickChooserTop", this.Top);
            Settings.Set("FormQuickMagnetickChooserSide", currentSide.ToString());
        }

        private void treeViewQuckChooser_DoubleClick(object sender, EventArgs e)
        {

        }

        private void treeViewQuckChooser_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeView tw = (TreeView)sender;
            QuickItem fi = QuickList.Find(x => x.Name == tw.SelectedNode.Name);
            if (fi != null)
            {
                Functions.OpenObject(fi.obj);
            }
            RefreshConnections();
        }

        private void treeViewQuckChooser_Layout(object sender, LayoutEventArgs e)
        {

        }
        private void RefreshConnections()
        {
            foreach (TreeNode node in treeViewQuckChooser.Nodes)
            {
                if (node.Name.Contains("Connections"))
                {
                    foreach (TreeNode subnode in node.Nodes)
                    {

                        if (ProcessIcon.formConnection.FindTab(subnode.Name))
                        {
                            subnode.ImageIndex = 6;
                            //node = new TreeNode(string.Empty, 12, 12);
                        }
                        else
                        {
                            subnode.ImageIndex = 0;
                        }
                    }
                }
            }
        }
    }
}
