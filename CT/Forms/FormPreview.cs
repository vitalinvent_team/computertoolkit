﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit
{
    public partial class FormPreview : Form
    {
        public FormPreview(Image img)
        {
            InitializeComponent();
            pictureBoxScreenshot.Image = img;
        }

        private void pictureBoxScreenshot_Click(object sender, EventArgs e)
        {

        }
        public void SetPreviewImage(Image img)
        {
            pictureBoxScreenshot.Image = img;
        }
    }
}
