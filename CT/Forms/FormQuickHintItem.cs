﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit.Forms
{
    public partial class FormQuickHintItem : Form
    {
        private Object CurrentObject  {get;set;}
        public FormQuickHintItem(object obj)
        {
            InitializeComponent();
            ToolTip yourToolTip = new ToolTip();
            yourToolTip.ToolTipIcon = ToolTipIcon.Info;
            yourToolTip.IsBalloon = true;
            yourToolTip.ShowAlways = true;
            if (obj.GetType() == typeof(String))
            {
                String str = (String)obj;
                linkLabel.Text = str;
                this.BackColor = Color.White;
            }
            if (obj.GetType() == typeof(NoteItem))
            {
                NoteItem ni = (NoteItem)obj;
                linkLabel.Text ="Note: "+ ni.Name;
                linkLabel.Tag = ni.Guid;
                yourToolTip.SetToolTip(linkLabel, ni.Content);
                this.BackColor = Color.Yellow;
                CurrentObject = ni;
            }
            if (obj.GetType() == typeof(ScriptItem))
            {
                ScriptItem ni = (ScriptItem)obj;
                linkLabel.Text = "Script: " + ni.Name;
                linkLabel.Tag = ni.Guid;
                yourToolTip.SetToolTip(linkLabel, ProcessIcon.Clean2(ni.Content));
                this.BackColor = Color.Gray;
                CurrentObject = ni;
            }
            if (obj.GetType() == typeof(FileItem))
            {
                FileItem ni = (FileItem)obj;
                linkLabel.Text = "File: " + ni.Name;
                linkLabel.Tag = ni.Guid;
                yourToolTip.SetToolTip(linkLabel, ni.PathFileFull);
                this.BackColor = Color.Green;
                CurrentObject = ni;
            }
            if (obj.GetType() == typeof(FileItemQuick))
            {
                FileItemQuick ni = (FileItemQuick)obj;
                linkLabel.Text = "FileQ: " + ni.Name;
                linkLabel.Tag = ni.Guid;
                yourToolTip.SetToolTip(linkLabel, ni.PathFile);
                this.BackColor = Color.GreenYellow;
                CurrentObject = ni;
            }
            if (obj.GetType() == typeof(ConnectionItem))
            {
                ConnectionItem ni = (ConnectionItem)obj;
                linkLabel.Text = "Connection: " + ni.Name;
                linkLabel.Tag = ni.Guid;
                yourToolTip.SetToolTip(linkLabel, ProcessIcon.Clean2(ni.Adress));
                this.BackColor = Color.FromArgb(128,128,255);
                CurrentObject = ni;
            }
            if (obj.GetType() == typeof(UniversalItem))
            {
                UniversalItem ni = (UniversalItem)obj;
                linkLabel.Text = "Universal: " + ni.Name;
                linkLabel.Tag = ni.Guid;
                this.BackColor = Color.Magenta;
                CurrentObject = ni;
            }
            if (obj.GetType() == typeof(TaskItem))
            {
                TaskItem ni = (TaskItem)obj;
                linkLabel.Text = "Task: " + ni.Name;
                linkLabel.Tag = ni.Guid;
                yourToolTip.SetToolTip(linkLabel, ni.HotString);
                this.BackColor = Color.Orange;
                CurrentObject = ni;
            }
            
        }
        private void FormQuickHintItem_Load(object sender, EventArgs e)
        {

        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Functions.OpenObject(CurrentObject);
        }
    }
}
