﻿namespace ComputerToolkit
{
    partial class FormMethodsHint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMethodsHint));
            this.richTextBoxMainMethods = new System.Windows.Forms.RichTextBox();
            this.richTextBoxGlobalMethods = new System.Windows.Forms.RichTextBox();
            this.buttonReloadConnections = new System.Windows.Forms.Button();
            this.comboBoxMembers = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxSearchMain = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBoxMainMethods
            // 
            this.richTextBoxMainMethods.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxMainMethods.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxMainMethods.Location = new System.Drawing.Point(3, 22);
            this.richTextBoxMainMethods.Name = "richTextBoxMainMethods";
            this.richTextBoxMainMethods.Size = new System.Drawing.Size(390, 415);
            this.richTextBoxMainMethods.TabIndex = 1;
            this.richTextBoxMainMethods.Text = "";
            this.richTextBoxMainMethods.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.richTextBoxMainMethods_KeyPress);
            // 
            // richTextBoxGlobalMethods
            // 
            this.richTextBoxGlobalMethods.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxGlobalMethods.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxGlobalMethods.Location = new System.Drawing.Point(0, 22);
            this.richTextBoxGlobalMethods.Name = "richTextBoxGlobalMethods";
            this.richTextBoxGlobalMethods.Size = new System.Drawing.Size(601, 415);
            this.richTextBoxGlobalMethods.TabIndex = 3;
            this.richTextBoxGlobalMethods.Text = "";
            this.richTextBoxGlobalMethods.TextChanged += new System.EventHandler(this.richTextBox2_TextChanged);
            this.richTextBoxGlobalMethods.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.richTextBoxGlobalMethods_KeyPress);
            // 
            // buttonReloadConnections
            // 
            this.buttonReloadConnections.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReloadConnections.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonReloadConnections.Image = global::ComputerToolkit.Properties.Resources.reload;
            this.buttonReloadConnections.Location = new System.Drawing.Point(369, 0);
            this.buttonReloadConnections.Name = "buttonReloadConnections";
            this.buttonReloadConnections.Size = new System.Drawing.Size(21, 21);
            this.buttonReloadConnections.TabIndex = 48;
            this.toolTip1.SetToolTip(this.buttonReloadConnections, "Обновить списки");
            this.buttonReloadConnections.UseVisualStyleBackColor = true;
            this.buttonReloadConnections.Click += new System.EventHandler(this.buttonReloadConnections_Click);
            // 
            // comboBoxMembers
            // 
            this.comboBoxMembers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMembers.FormattingEnabled = true;
            this.comboBoxMembers.Location = new System.Drawing.Point(3, 1);
            this.comboBoxMembers.Name = "comboBoxMembers";
            this.comboBoxMembers.Size = new System.Drawing.Size(595, 21);
            this.comboBoxMembers.TabIndex = 49;
            this.comboBoxMembers.SelectedIndexChanged += new System.EventHandler(this.comboBoxMembers_SelectedIndexChanged);
            this.comboBoxMembers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxMembers_KeyPress);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxSearchMain);
            this.panel1.Controls.Add(this.buttonReloadConnections);
            this.panel1.Controls.Add(this.richTextBoxMainMethods);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(393, 438);
            this.panel1.TabIndex = 51;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Поиск:";
            // 
            // textBoxSearchMain
            // 
            this.textBoxSearchMain.Location = new System.Drawing.Point(60, 1);
            this.textBoxSearchMain.Name = "textBoxSearchMain";
            this.textBoxSearchMain.Size = new System.Drawing.Size(303, 20);
            this.textBoxSearchMain.TabIndex = 2;
            this.textBoxSearchMain.TextChanged += new System.EventHandler(this.textBoxSearchMain_TextChanged);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(393, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 438);
            this.splitter1.TabIndex = 52;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.richTextBoxGlobalMethods);
            this.panel2.Controls.Add(this.comboBoxMembers);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(396, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(601, 438);
            this.panel2.TabIndex = 53;
            // 
            // FormMethodsHint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 438);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMethodsHint";
            this.Text = "Список методов и переменных";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMethodsHint_FormClosing);
            this.Load += new System.EventHandler(this.FormMethodsHint_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FormMethodsHint_KeyPress);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RichTextBox richTextBoxMainMethods;
        private System.Windows.Forms.RichTextBox richTextBoxGlobalMethods;
        private System.Windows.Forms.Button buttonReloadConnections;
        private System.Windows.Forms.ComboBox comboBoxMembers;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxSearchMain;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}