﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ComputerToolkit.Items;
using System.Threading;
using System.Globalization;

namespace ComputerToolkit
{
    public partial class FormMethodsHint : Form
    {
        private Methods methods = new Methods();
        public FormMethodsHint()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormMethodsHint_Load(object sender, EventArgs e)
        {

            comboBoxMembers.Text=Settings.GetString("comboBoxMembers");
            this.Top = Settings.Get("FormMethodsHintTop", 0);
            this.Left = Settings.Get("FormMethodsHintLeft", 0);
            this.Height=Settings.Get("FormMethodsHintHeight",429);
            this.Width=Settings.Get("FormMethodsHintWidth",1011);
            //methods.ReadGlobalMethodsBase();
            //methods.ReadMainMethodsBase();
            methods.RefrshAllMethods();
            methods.Members.Sort();
            foreach (string member in methods.Members)
            {
                comboBoxMembers.Items.Add(member);
            }

            FillFromLists();
            if (Top < 0) Top = 0;
            if (Left < 0) Left = 0;
            if ((Left + Width) > Screen.PrimaryScreen.WorkingArea.Width) Left = 0;
            if ((Top + Height) > Screen.PrimaryScreen.WorkingArea.Height) Top = 0;

        }

        void FillFromLists()
        {
            richTextBoxMainMethods.Text = "";
            foreach (Method method in methods.mainList)
            {
                if (((method.ToString().ToLower().Contains(textBoxSearchMain.Text.ToLower()))) || (textBoxSearchMain.Text.Length < 2))
                {
                    AppendTextMain(method.Member + ".", Color.Gray);
                    AppendTextMain(method.Name, Color.Black);
                    AppendTextMain("( ", Color.Black);
                    foreach (Parameter param in method.Parameters)
                    {
                        AppendTextMain(param.Type + " ", Color.Blue);
                        AppendTextMain(param.Name + " ", Color.Black);
                        //comboBoxMethodsApp.Items.Add(param);
                    }
                    AppendTextMain(" )", Color.Black);
                    AppendTextMain(method.ReturnType + "\n", Color.Green);
                }
            }
            richTextBoxGlobalMethods.Text = "";
            foreach (Method method in methods.globalList)
            {
                if (method.Member == comboBoxMembers.Text)
                //if (((method.ToString().ToLower().Contains(textBoxSearchMain.Text.ToLower()))) || (textBoxSearchMain.Text.Length < 2))
                {
                    if (method.ToString().ToLower().Contains(textBoxSearchMain.Text.ToLower()) || textBoxSearchMain.Text.Length < 2)
                    {
                        AppendTextGlobal(method.Member + ".", Color.Gray);
                        AppendTextGlobal(method.Name, Color.Black);
                        AppendTextGlobal("( ", Color.Black);
                        foreach (Parameter param in method.Parameters)
                        {
                            AppendTextGlobal(param.Type + " ", Color.Blue);
                            AppendTextGlobal(param.Name + " ", Color.Black);
                            //comboBoxMethodsApp.Items.Add(param);
                        }
                        AppendTextGlobal(" )", Color.Black);
                        AppendTextGlobal(method.ReturnType + "\n", Color.Green);
                    }
                }
            }
 
        }

        private void FillFromListGlobals()
        {
            richTextBoxGlobalMethods.Text = "";
            foreach (Method method in methods.globalList)
            {
                if (method.Member == comboBoxMembers.Text | (method.Member.Contains(textBoxSearchMain.Text) && textBoxSearchMain.Text.Length > 0))
                {
                    AppendTextGlobal(method.Member + ".", Color.Gray);
                    AppendTextGlobal(method.Name, Color.Black);
                    AppendTextGlobal("( ", Color.Black);
                    foreach (Parameter param in method.Parameters)
                    {
                        AppendTextGlobal(param.Type + " ", Color.Blue);
                        AppendTextGlobal(param.Name + " ", Color.Black);
                        //comboBoxMethodsApp.Items.Add(param);
                    }
                    AppendTextGlobal(" )", Color.Black);
                    AppendTextGlobal(method.ReturnType + "\n", Color.Green);
                }
            }
 

        }
        void AppendTextMain(string text, Color color)
        {
            int start = richTextBoxMainMethods.TextLength;
            richTextBoxMainMethods.AppendText(text);
            int end = richTextBoxMainMethods.TextLength;

            // Textbox may transform chars, so (end-start) != text.Length
            richTextBoxMainMethods.Select(start, end - start);
            {
                richTextBoxMainMethods.SelectionColor = color;
                // could set box.SelectionBackColor, box.SelectionFont too.
            }
            richTextBoxMainMethods.SelectionLength = 0; // clear
        }
        void AppendTextGlobal(string text, Color color)
        {
            int start = richTextBoxGlobalMethods.TextLength;
            richTextBoxGlobalMethods.AppendText(text);
            int end = richTextBoxGlobalMethods.TextLength;

            // Textbox may transform chars, so (end-start) != text.Length
            richTextBoxGlobalMethods.Select(start, end - start);
            {
                richTextBoxGlobalMethods.SelectionColor = color;
                // could set box.SelectionBackColor, box.SelectionFont too.
            }
            richTextBoxGlobalMethods.SelectionLength = 0; // clear
        }

        private void buttonReloadConnections_Click(object sender, EventArgs e)
        {
            //methods.RefrshAllMethods();
            //methods.SaveGlobalMethodsBase();
            //methods.SaveMainMethodsBase();
            FillFromLists();
        }

        private void comboBoxMembers_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Set("comboBoxMembers", comboBoxMembers.Text);
            FillFromListGlobals();
        }

        private void FormMethodsHint_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Set("FormMethodsHintTop", this.Top);
            Settings.Set("FormMethodsHintLeft", this.Left);
            Settings.Set("FormMethodsHintHeight", this.Height);
            Settings.Set("FormMethodsHintWidth", this.Width);
        }

        private void FormMethodsHint_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Hide();
            }

        }

        private void richTextBoxMainMethods_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Hide();
            }

        }

        private void comboBoxMembers_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Hide();
            }

        }

        private void richTextBoxGlobalMethods_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Hide();
            }

        }

        private void textBoxSearchMain_TextChanged(object sender, EventArgs e)
        {
             
        }

        private void textBoxSearchGlobal_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
