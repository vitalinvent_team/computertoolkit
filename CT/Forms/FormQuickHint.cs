﻿using ComputerToolkit.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit.Forms
{
    public partial class FormQuickHint : Form
    {
        public Timer timerSetFocusOnTextBox;

        public FormQuickHint()
        {
            InitializeComponent();
        }

        private void FormQuickHint_Load(object sender, EventArgs e)
        {
            timerSetFocusOnTextBox = new Timer();
            timerSetFocusOnTextBox.Interval = 1000;
            timerSetFocusOnTextBox.Tick +=timerSetFocusOnTextBox_Tick;
            textBoxQuickSearch.Focus();
        }
        private void timerSetFocusOnTextBox_Tick(object sender, EventArgs e)
        {
            this.Focus();
            textBoxQuickSearch.Focus();
            timerSetFocusOnTextBox.Enabled = false;
        }
        private void textBoxQuickSearch_MouseHover(object sender, EventArgs e)
        {
            ProcessIcon.timerQuickSearshHide.Enabled = false;
        }

        private void textBoxQuickSearch_MouseLeave(object sender, EventArgs e)
        {
            if (textBoxQuickSearch.Text.Length == 0)
            {
                ProcessIcon.timerQuickSearshHide.Enabled = true;
                ProcessIcon.HideQuickHintFormsList();
                ProcessIcon.listFormQuickHintItem.Clear();
                ProcessIcon.currentQuickIntem = 0;
            }
            else
            {
                ProcessIcon.timerQuickSearshHide.Enabled = false;
            }
        }

        private void textBoxQuickSearch_TextChanged(object sender, EventArgs e)
        {
            if (textBoxQuickSearch.Text.Length == 0)
            {
                ProcessIcon.timerQuickSearshHide.Enabled = true;
                ProcessIcon.HideQuickHintFormsList();
                ProcessIcon.listFormQuickHintItem.Clear();
            }
            else
            {
                ProcessIcon.timerQuickSearshHide.Enabled = false;
                SearchObjectsAddQuickHintItems();
            }
        }
        private void SearchObjectsAddQuickHintItems()
        {
            if (textBoxQuickSearch.Text.Length > 1)
            {
                ProcessIcon.HideQuickHintFormsList();
                ProcessIcon.listFormQuickHintItem.Clear();
                ProcessIcon.currentQuickIntem = 0;
                List<Object> listObjects = Functions.FindObjectByGuid(textBoxQuickSearch.Text);
                foreach (Object obj in listObjects)
                {
                    FormQuickHintItem formQuickHintItem = new FormQuickHintItem(obj);
                    ProcessIcon.listFormQuickHintItem.Add(formQuickHintItem);
                }
                ProcessIcon.ShowQuickHintFormsList();
                ProcessIcon.formQuickHint.Focus();
            }

        }
        private void textBoxQuickSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Hide();
                textBoxQuickSearch.Text = "";
                ProcessIcon.HideQuickHintFormsList();
                ProcessIcon.listFormQuickHintItem.Clear();
                ProcessIcon.currentQuickIntem = 0;
            }
            if (e.KeyChar == (char)Keys.Tab)
            {

            }
        }
        private void textBoxQuickSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                ProcessIcon.listFormQuickHintItem[ProcessIcon.currentQuickIntem].Activate();
                ProcessIcon.currentQuickIntem++;
                if (ProcessIcon.currentQuickIntem > ProcessIcon.listFormQuickHintItem.Count) ProcessIcon.currentQuickIntem = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            textBoxQuickSearch.Text = "";
            ProcessIcon.HideQuickHintFormsList();
            ProcessIcon.listFormQuickHintItem.Clear();
            ProcessIcon.currentQuickIntem = 0;
        }

        private void FormQuickHint_Activated(object sender, EventArgs e)
        {
            textBoxQuickSearch.Focus();
        }


    }
}
