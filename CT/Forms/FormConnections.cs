﻿using MSTSCLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Runtime.CompilerServices;
using ComputerToolkit.Properties;
//using VncSharp;

namespace ComputerToolkit
{

    public partial class FormConnections : Form
    {
        private bool mouseIsDown = false;
        private Point firstPoint;
        public AxMSTSCLib.AxMsTscAxNotSafeForScripting RdpClient;
        private AxMSTSCLib.AxMsTscAxNotSafeForScripting axMsTscAxNotSafeForScripting;
        public Object RdpClientCopy;
        private int CurrentClient=1;
        TabPage senderClickMousePage;
        #region ConnectionsMenu
        public ContextMenuStrip CreateConnectionTabMenu()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            //ToolStripMenuItem mnuSubItemConnMenu = new ToolStripMenuItem();
            //mnuSubItemConnMenu.Text = "Основные";
            //mnuSubItemConnMenu.Name = "Основные";
            //mnuSubItemConnMenu.Tag = "Основные";
            //mnuSubItemConnMenu.Click += new System.EventHandler(MenuConnMenu_Click);
            //itemConnMenu.DropDownItems.Add(mnuSubItemConnMenu);
            ToolStripSeparator sep;
            ToolStripMenuItem itemConnMenuClose = new ToolStripMenuItem();
            itemConnMenuClose.Text = "Закрыть " + tabControlConnections.SelectedTab.Name;
            itemConnMenuClose.Image = Resources.del.ToBitmap();
            itemConnMenuClose.Click += new System.EventHandler(MenuConnMenuClose_Click);
            menu.Items.Add(itemConnMenuClose);
            sep = new ToolStripSeparator();
            menu.Items.Add(sep);
            ToolStripMenuItem itemConnMenuBottomBookmarks = new ToolStripMenuItem();
            itemConnMenuBottomBookmarks.Text = "Закладки верх/низ" + tabControlConnections.SelectedTab.Name;
            itemConnMenuBottomBookmarks.Image = Resources.group.ToBitmap();
            itemConnMenuBottomBookmarks.Click += new System.EventHandler(MenuConnMenuTopBottom_Click);
            menu.Items.Add(itemConnMenuBottomBookmarks);
            ToolStripMenuItem itemConnMenuLeftRightBookmarks = new ToolStripMenuItem();
            itemConnMenuLeftRightBookmarks.Text = "Закладки лево/право" + tabControlConnections.SelectedTab.Name;
            itemConnMenuLeftRightBookmarks.Image = Resources.group.ToBitmap();
            itemConnMenuLeftRightBookmarks.Click += new System.EventHandler(MenuConnMenuLeftRight_Click);
            menu.Items.Add(itemConnMenuLeftRightBookmarks);
            return menu;
        }
        void MenuConnMenuClose_Click(object sender, EventArgs e)
        {
            CloseSelectedTab();
        }
        private void CloseSelectedTab()
        {
            tabControlConnections.TabPages.Remove(tabControlConnections.SelectedTab);
            if (tabControlConnections.TabPages.Count == 0)
            {
                SaveProp();
                this.Hide();
            }
        }
        void MenuConnMenuTopBottom_Click(object sender, EventArgs e)
        {
            if (tabControlConnections.Alignment == System.Windows.Forms.TabAlignment.Bottom)
            {
                tabControlConnections.Alignment = System.Windows.Forms.TabAlignment.Top;
            }
            else
            {
                tabControlConnections.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            }
        }
        void MenuConnMenuLeftRight_Click(object sender, EventArgs e)
        {
            if (tabControlConnections.Alignment == System.Windows.Forms.TabAlignment.Left)
            {
                tabControlConnections.Alignment = System.Windows.Forms.TabAlignment.Right;
            }
            else
            {
                tabControlConnections.Alignment = System.Windows.Forms.TabAlignment.Left;
            }
        } 
        #endregion
        #region Events
        private void tabControlConnections_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void tabControlConnections_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void tabControlConnections_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void FormConnections_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void FormConnections_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void FormConnections_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void tabControlConnections_DoubleClick(object sender, EventArgs e)
        {
            CloseSelectedTab();
        }

        private void FormConnections_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void FormConnections_Resize(object sender, EventArgs e)
        {
        }

        private void tabControlConnections_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void tabControlConnections_Click(object sender, EventArgs e)
        {

        }

        private void tabControlConnections_MouseClick_1(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenuStrip cmtmnu = CreateConnectionTabMenu();
                cmtmnu.Show(Cursor.Position);
            }
        }

        private void FormConnections_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveProp();
        }
        private void FormConnections_Load(object sender, EventArgs e)
        {

            this.Visible = true;
            LoadProp();
        }
        #endregion //events
        public FormConnections()
        {
            InitializeComponent();
        }
        public bool FindTab(String name)
        {
            bool found = false;
            foreach (TabPage tab in tabControlConnections.TabPages)
            {
                if (name.Equals(tab.Text))
                {
                    return true;
                }
            }
            return false;
        }
        public void SelectTab(String name)
        {
            bool found = false;
            foreach (TabPage tab in tabControlConnections.TabPages)
            {
                if (name.Equals(tab.Text))
                {
                    tabControlConnections.SelectedTab = tab;
                }
            }
        }
        public class MyRDP : AxMSTSCLib.AxMsRdpClient2NotSafeForScripting
        {
            public MyRDP()
                : base()
            {
            }

            protected override void WndProc(ref System.Windows.Forms.Message m)
            {
                // Fix for the missing focus issue on the rdp client component
                if (m.Msg == 0x0021) // WM_MOUSEACTIVATE
                {
                    if (!this.ContainsFocus)
                    {
                        this.Focus();
                    }
                }

                base.WndProc(ref m);
            }
        }
        //public void AddTab(String serverName, String domain, String user, String password, ConnectionProtocol connProt, String name,String port)
        public void AddTab(ConnectionItem connection)
        {
            if (FindTab(connection.Name))
            {
                SelectTab(connection.Name);
            }
            else
            {
                if (CurrentClient < 20)
                {
                    switch (connection.Protocol)
                    {
                        case ConnectionProtocol.RDP:
                            try
                            {
                                TabPage myTabPage = new TabPage(connection.Name);

                                AxMSTSCLib.AxMsRdpClient2NotSafeForScripting RdpClientLocal = (AxMSTSCLib.AxMsRdpClient2NotSafeForScripting)this.Controls["AxMsRdpClient2NotSafeForScripting" + CurrentClient.ToString()];
                                RdpClientLocal.CreateControl();
                                ((System.ComponentModel.ISupportInitialize)(RdpClientLocal)).BeginInit();
                                myTabPage.Name = ProcessIcon.Clean2(connection.Adress);
                                myTabPage.ToolTipText = connection.Description;
                                RdpClientLocal.Enabled = true;
                                RdpClientLocal.OnConnecting += RdpClientLocal_OnConnecting;
                                RdpClientLocal.OnConnected += RdpClientLocal_OnConnected;
                                RdpClientLocal.OnDisconnected += RdpClientLocal_OnDisconnected;
                                RdpClientLocal.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
                                if (!(connection.Domain == null))
                                {
                                    RdpClientLocal.Domain = connection.Domain;
                                }
                                RdpClientLocal.Name = ProcessIcon.Clean2(connection.Adress);
                                if (ProcessIcon.Clean2(connection.Adress).IndexOf(':') > 0)
                                {
                                    RdpClientLocal.Server = ProcessIcon.Clean2(connection.Adress).Split(':')[0];
                                    RdpClientLocal.AdvancedSettings2.RDPPort = int.Parse(ProcessIcon.Clean2(connection.Adress).Split(':')[1]);
                                }
                                else
                                {
                                    RdpClientLocal.Server = ProcessIcon.Clean2(connection.Adress);
                                    RdpClientLocal.AdvancedSettings2.RDPPort = int.Parse(ProcessIcon.Clean2(connection.Port));
                                }
                                //RdpClientLocal.AdvancedSettings2.RDPPort = 3389;
                                if (!(connection.User== null))
                                    RdpClientLocal.UserName = ProcessIcon.Clean2(connection.User);
                                if (!(connection.Password == null))
                                    RdpClientLocal.AdvancedSettings2.ClearTextPassword = ProcessIcon.Clean2(connection.Password);
                                if (!(connection.Password == null))
                                {
                                    IMsTscNonScriptable secured = (IMsTscNonScriptable)RdpClientLocal.GetOcx();
                                    secured.ClearTextPassword = ProcessIcon.Clean2(connection.Password);
                                }
                                RdpClientLocal.FullScreenTitle = ProcessIcon.Clean2(connection.Adress);
                                RdpClientLocal.Location = new System.Drawing.Point(0, 0);
                                myTabPage.Controls.Add(RdpClientLocal);
                                tabControlConnections.TabPages.Add(myTabPage);
                                RdpClientLocal.Parent = myTabPage;
                                RdpClientLocal.DesktopWidth = tabControlConnections.Width - 12;
                                RdpClientLocal.DesktopHeight = tabControlConnections.Height - 26;
                                RdpClientLocal.Width = RdpClientLocal.Parent.Width;
                                RdpClientLocal.Height = RdpClientLocal.Parent.Height;
                                RdpClientLocal.Visible = true;
                                RdpClientLocal.BringToFront();
                                ((System.ComponentModel.ISupportInitialize)(RdpClientLocal)).EndInit();
                                RdpClientLocal.Connect();

                                tabControlConnections.SelectedTab = myTabPage;
                                CurrentClient++;
                            }
                            catch (Exception ex) {
                                Functions.Message(ex);
                            }
                            break;
                        case ConnectionProtocol.VNC:
                            //RemoteDesktop newVNC = new RemoteDesktop();
                            //TabPage TabPageVNC = new TabPage(name);
                            //newVNC.Name = "vnc " + name;
                            //newVNC.Parent = TabPageVNC;
                            //newVNC.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
                            //TabPageVNC.Controls.Add(newVNC);
                            //tabControlConnections.TabPages.Add(TabPageVNC);
                            //newVNC.Width = newVNC.Parent.Width;
                            //newVNC.Height = newVNC.Parent.Height;
                            //if (port == null)
                            //{
                            //    newVNC.VncPort = 5900;
                            //}
                            //else if (port.Length == 0)
                            //{
                            //    newVNC.VncPort = 5900;
                            //}
                            //else
                            //{
                            //    newVNC.VncPort = int.Parse(port);
                            //}
                            ////CONNECT
                            //try
                            //{
                            //    newVNC.Connect(serverName);
                            //}
                            //catch (Exception)
                            //{
                            //}
                            break;
                    }
                }
            }
        }

        private void RdpClientLocal_OnDisconnected(object sender, AxMSTSCLib.IMsTscAxEvents_OnDisconnectedEvent e)
        {
            throw new NotImplementedException();
        }

        private void RdpClientLocal_OnConnected(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void RdpClientLocal_OnConnecting(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void SaveProp()
        {
            Settings.Set("FormConnectionsWindowState", this.WindowState.ToString());
            switch (tabControlConnections.Alignment)
            {
                case System.Windows.Forms.TabAlignment.Bottom:
                    Settings.Set("FormConnectionsTabOrientation", "Bottom");
                    break;
                case System.Windows.Forms.TabAlignment.Top:
                    Settings.Set("FormConnectionsTabOrientation", "Top");
                    break;
                case System.Windows.Forms.TabAlignment.Left:
                    Settings.Set("FormConnectionsTabOrientation", "Left");
                    break;
                case System.Windows.Forms.TabAlignment.Right:
                    Settings.Set("FormConnectionsTabOrientation", "Right");
                    break;
                default:
                    Settings.Set("FormConnectionsTabOrientation", "Bottom");
                    break;
            }
            Settings.Set("FormConnectionsLeft", this.Left);
            Settings.Set("FormConnectionsTop", this.Top);
            Settings.Set("FormConnectionsWidth", this.Width);
            Settings.Set("FormConnectionsHeight", this.Height);
        }

        private void LoadProp()
        {
            //this.TopMost = true;
            this.TopMost = Settings.Get("checkBoxConnectionsFormTopmost", false);
            this.Left = Settings.Get("FormConnectionsLeft", 0);
            this.Top = Settings.Get("FormConnectionsTop", 0);
            this.Width=Settings.Get("FormConnectionsWidth", 900);
            this.Height=Settings.Get("FormConnectionsHeight", 600);
            string ws = Settings.Get("FormConnectionsWindowState", "Normal");
            switch (ws)
            {
                case "Normal":
                    this.WindowState = FormWindowState.Normal;
                    break;
                case "Maximized":
                    this.WindowState = FormWindowState.Maximized;
                    break;
                //default:
                //    this.WindowState = FormWindowState.Normal;
                //    break;
            }
            switch (Settings.Get("FormConnectionsTabOrientation", "Bottom"))
            {
                case "Bottom":
                    tabControlConnections.Alignment = System.Windows.Forms.TabAlignment.Bottom;
                    break;
                case "Top":
                    tabControlConnections.Alignment = System.Windows.Forms.TabAlignment.Top;
                    break;
                case "Left":
                    tabControlConnections.Alignment = System.Windows.Forms.TabAlignment.Left;
                    break;
                case "Right":
                    tabControlConnections.Alignment = System.Windows.Forms.TabAlignment.Right;
                    break;
                default:
                    tabControlConnections.Alignment = System.Windows.Forms.TabAlignment.Bottom;
                    break;
            }

        }
        private void tabControlConnections_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void FormConnections_Activated(object sender, EventArgs e)
        {
            if (Top < 0) Top = 0;
            if (Left < 0) Left = 0;
            //if ((Left + Width) > Screen.PrimaryScreen.WorkingArea.Width) Left = 0;
            //if ((Top + Height) > Screen.PrimaryScreen.WorkingArea.Height) Top = 0;
            if (Width < 100) Width = 500;
            if (Height < 100) Height = 400;
        }

        private void buttonMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void buttonMaximize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
