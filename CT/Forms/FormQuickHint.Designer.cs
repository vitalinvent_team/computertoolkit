﻿namespace ComputerToolkit.Forms
{
    partial class FormQuickHint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormQuickHint));
            this.textBoxQuickSearch = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxQuickSearch
            // 
            this.textBoxQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxQuickSearch.Location = new System.Drawing.Point(0, 0);
            this.textBoxQuickSearch.Name = "textBoxQuickSearch";
            this.textBoxQuickSearch.Size = new System.Drawing.Size(254, 20);
            this.textBoxQuickSearch.TabIndex = 0;
            this.textBoxQuickSearch.TextChanged += new System.EventHandler(this.textBoxQuickSearch_TextChanged);
            this.textBoxQuickSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxQuickSearch_KeyPress);
            this.textBoxQuickSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxQuickSearch_KeyUp);
            this.textBoxQuickSearch.MouseLeave += new System.EventHandler(this.textBoxQuickSearch_MouseLeave);
            this.textBoxQuickSearch.MouseHover += new System.EventHandler(this.textBoxQuickSearch_MouseHover);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Image = global::ComputerToolkit.Properties.Resources.close_icon;
            this.button1.Location = new System.Drawing.Point(255, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(19, 19);
            this.button1.TabIndex = 13;
            this.button1.TabStop = false;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormQuickHint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 20);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxQuickSearch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormQuickHint";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormQuickHint_Activated);
            this.Load += new System.EventHandler(this.FormQuickHint_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxQuickSearch;
        private System.Windows.Forms.Button button1;
    }
}