﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit
{
    public partial class FormTaskReminder : Form
    {
        protected TaskItem taskItem = null;
        private bool timeChanged=false;
        void SetSettings()
        {
            Settings.Set("FormTaskReminder_Top", this.Top);
            Settings.Set("FormTaskReminder_Left", this.Left);
        }
        void GetSettings()
        {
            this.Top = Settings.Get("FormTaskReminder_Top", 100);
            this.Left = Settings.Get("FormTaskReminder_Left", 100);
        }
        public FormTaskReminder()
        {
            InitializeComponent();
            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
        }
        public FormTaskReminder(String taskGuid = "")
        {
            InitializeComponent();
            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
        }
        public FormTaskReminder(TaskItem _taskItem=null)
        {
            taskItem = _taskItem;
            InitializeComponent();
            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
        }

        private void textBoxaskReminderLaterTime_TextChanged(object sender, EventArgs e)
        {
            Settings.Set("textBoxaskReminderLaterTime_TextChanged", textBoxaskReminderLaterTime.Text);
        }

        private void FormTaskReminder_Load(object sender, EventArgs e)
        {
            LoadTask();
            if (taskItem != null)
                taskItem.LastTimeToRun = DateTime.Now.ToString();
            //dateTimePicker1.Format = DateTimePickerFormat.Custom;
            //dateTimePicker1.CustomFormat = "dd.mm.yyyy HH:mm:ss";
            GetSettings();
        }

        private void buttonMoveLaterOneDay_Click(object sender, EventArgs e)
        {
            MoveLaterTask(1440);
            this.Close();
        }

        private void buttonMoveLater15Min_Click(object sender, EventArgs e)
        {
            MoveLaterTask(15);
            this.Close();
        }

        private void buttonMoveLaterOneHour_Click(object sender, EventArgs e)
        {
            MoveLaterTask(60);
            this.Close();
        }
        private void buttonEndTask_Click(object sender, EventArgs e)
        {
            EndTask();
            this.Close();
        }
        private void LoadTask(String guid="")
        {
            if (taskItem == null)
            {
                taskItem = new TaskItem("Напоминание_" + DateTime.Now.ToString(), DateTime.Now.ToString(), "","", DateTime.Now.ToString());
                SetFormFields();
            }
            else
            {
                TaskItem findedTask = ProcessIcon.lists.tasks.Find(x => x.Guid == taskItem.Guid);
                taskItem = findedTask;
                SetFormFields();
            }
        }
        protected void SetFormFields()
        {
            textBoxDescription.Text = taskItem.Description;
            richTextBoxHotstring.Text = taskItem.HotString;
            this.Text = taskItem.Name;
            dateTimePicker1.Value = DateTime.Parse(taskItem.TimeToRun);
            textBoxaskReminderLaterTime.Text = Settings.Get("textBoxaskReminderLaterTime_TextChanged", "60");
        }
        protected void GetFormFields()
        {
            taskItem.Description= textBoxDescription.Text;
            taskItem.HotString = richTextBoxHotstring.Text;
            taskItem.Name= this.Text;
            taskItem.TimeToRun = dateTimePicker1.Value.ToString();
            taskItem.isReminder = true;
            Settings.Set("textBoxaskReminderLaterTime_TextChanged", textBoxaskReminderLaterTime.Text);
        }
        void SaveTask()
        {
            TaskItem findedTask = null;
            findedTask = ProcessIcon.lists.tasks.Find(x => x.Guid == taskItem.Guid);
            if (!(findedTask == null))
            {
                GetFormFields();
                findedTask = taskItem;
                findedTask.Date = DateTime.Now.ToString();
                ProcessIcon.lists.tasks.SaveBase();
            }
            else
            {
                if ((textBoxDescription.Text.Length) > 0)
                {
                    GetFormFields();
                    taskItem.Enabled = true;
                    taskItem.Date = DateTime.Now.ToString();
                    taskItem.Group = "reminder";
                    ProcessIcon.lists.tasks.Add(taskItem);
                    ProcessIcon.lists.tasks.SaveBase();
                }
            }
        }
        void EndTask()
        {
            taskItem.Enabled = false;
            taskItem.Date = DateTime.Now.ToString();
            SaveTask();
        }
        void MoveLaterTask(int minuses=60)
        {
            TaskItem findedTask = null;
            findedTask = ProcessIcon.lists.tasks.Find(x => x.Guid == taskItem.Guid);
            if (!(findedTask == null))
            {
                if (!(timeChanged) || (minuses>0))
                {
                    //int timeTolate = Settings.Get("textBoxaskReminderLaterTime_TextChanged", 1);
                    dateTimePicker1.Value = dateTimePicker1.Value.AddMinutes(minuses);//new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, dateTimePicker1.Value.Hour+ timeTolate, dateTimePicker1.Value.Minute, dateTimePicker1.Value.Second);
                    SaveTask();

                }
                SaveTask();
                ProcessIcon.lists.tasks.ReadBase();
            }
            else
            {
                SaveTask();
                ProcessIcon.lists.tasks.ReadBase();
            }
        }
        private void textBoxDescription_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormTaskReminder_FormClosing(object sender, FormClosingEventArgs e)
        {
            MoveLaterTask();
            SetSettings();
        }

        private void FormTaskReminder_Activated(object sender, EventArgs e)
        {
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            timeChanged = true;
        }

        private void dateTimePicker1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dateTimePicker1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode ==Keys.Enter)
            {
                MoveLaterTask();
                this.Close();
            }

        }

    }
}
