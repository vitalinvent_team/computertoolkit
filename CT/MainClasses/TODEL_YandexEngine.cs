﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Collections.ObjectModel;
//using System.Windows.Forms;
//using System.ComponentModel;
//using System.Runtime.CompilerServices;
//using System.Threading.Tasks;
//using Disk.SDK;
//using Disk.SDK.Utils;
//using System.IO;
//using System.Diagnostics;
//using ComputerToolkit.Properties;
//using Disk.SDK.Provider;

//namespace ComputerToolkit
//{
//    public class YandexEngine : INotifyPropertyChanged
//    {
//        public State state;
//        public event PropertyChangedEventHandler PropertyChanged;
//        public ObservableCollection<DiskItemInfo> FolderItems;
//        private IDiskSdkClient sdk;
//        private ObservableCollection<DiskItemInfo> folderItems;
//        public List<DiskItemInfo> filesList;
//        private readonly ICollection<DiskItemInfo> selectedItems = new Collection<DiskItemInfo>();
//        private readonly ICollection<DiskItemInfo> cutItems = new Collection<DiskItemInfo>();
//        private readonly ICollection<DiskItemInfo> copyItems = new Collection<DiskItemInfo>();
//        //private LoginWindow loginWindow;
//        private string currentPath, previousPath, downloadFileName;
//        private bool isLaunch;

//        /// <summary>
//        /// Gets or sets the access token.
//        /// </summary>
//        /// <value>The access token.</value>
//        public static string AccessToken { get; set; }

//        public YandexEngine()
//        {
//            try
//            {
//                currentPath = "/" + VariablesClass.ONLINE_FOLDER + "/";
//                if (VariablesClass.YandexToken.Length == 0)
//                {
//                    LoginYandexDisk loginYandexDisk = new LoginYandexDisk(sdk);
//                    loginYandexDisk.AuthCompleted += this.SdkOnAuthorizeCompleted;
//                    loginYandexDisk.ShowDialog();
//                    AccessToken = VariablesClass.YandexToken;
//                }
//                CreateSdkClient();
//                state = State.ready;
//            }
//            catch (Exception Ex)
//            {
//                Functions.Message(Ex);
//            }
//        }

//        private void CreateSdkClient()
//        {
//            this.sdk = new DiskSdkClient(VariablesClass.YandexToken);
//            filesList =new  List<DiskItemInfo>();
//            this.AddCompletedHandlers();
//        }
//        internal void Refresh()
//        {
//            currentPath = "/"+VariablesClass.ONLINE_FOLDER+"/";
//            sdk.GetListAsync("/" + VariablesClass.ONLINE_FOLDER + "/");
//            state = State.inProgress;
//            WaitForReady();
//        }
//        private void WaitForReady()
//        {
//            while (state == State.inProgress)
//            {
//            }
//        }
//        internal void MakeDir(string name)
//        {
//            try
//            {
//                //DiskItemInfo fFolder = FolderItems.First(x => x.DisplayName == name);
//                //if (fFolder == null)
//                sdk.MakeDirectoryAsync(name);
//                state = State.inProgress;
//                WaitForReady();
//            }
//            catch(Exception ex)
//            {
//                Functions.Message(ex);
//                state = State.error;
//            }
//        }
//        private string GetUniqueFileName(string sourceItem)
//        {
//            var currentFileName = Path.GetFileName(sourceItem);
//            if (this.folderItems.Any(item => item.OriginalDisplayName == currentFileName))
//            {
//                var fileName = Path.GetFileNameWithoutExtension(currentFileName);
//                var extension = Path.GetExtension(currentFileName);
//                var uniqueName = string.Format("{0} - Copy{1}", fileName, extension);
//                const string FILE_NAME_FORMAT = "{0} - Copy ({1}){2}";
//                int uniqueIndex = 1;
//                while (this.folderItems.Any(item => item.OriginalDisplayName == uniqueName))
//                {
//                    uniqueIndex++;
//                    uniqueName = string.Format(FILE_NAME_FORMAT, fileName, uniqueIndex, extension);
//                }

//                return uniqueName;
//            }

//            return currentFileName;
//        }
//        private void UpdateProgress(ulong current, ulong total)
//        {
//            VariablesClass.progressBarCurrent = current;
//            VariablesClass.progressBarTotal = total;
//        }
//        internal void UploadFileToFolder(string filePath)
//        {
//            Stream stream = File.OpenRead(filePath);
//            var filePathOnline = this.currentPath + Functions.GetFileNameInPath(filePath);
//            this.sdk.UploadFileAsync(filePathOnline, stream, new AsyncProgress(this.UpdateProgress), this.SdkOnUploadCompleted);
//            state = State.inProgress;
//            WaitForReady();
//        }

//        internal void UpdateFile(string filePath)
//        {
//            Stream stream = File.OpenRead(filePath);
//            var filePathOnline = this.currentPath + Functions.GetFileNameInPath(filePath);
//            this.sdk.UploadFileAsync(filePathOnline, stream, new AsyncProgress(this.UpdateProgress), this.SdkOnUploadCompleted);
//            state = State.inProgress;
//            WaitForReady();
//        }

//        internal string DownloadFile(string filePath)
//        {
//            string fileDownloaded = Application.StartupPath.ToString() + "\\" + VariablesClass.TEMP_FOLDER + "\\" + filePath;
//            var fileStream = File.OpenWrite(fileDownloaded);
//            this.sdk.DownloadFileAsync(currentPath+filePath, fileStream, new AsyncProgress(this.NullProgress), this.SdkOnDownloadCompleted);
//            state = State.inProgress;
//            WaitForReady();
//            return fileDownloaded;
//        }
//        private void NullProgress(ulong current, ulong total)
//        {
//        }
//        private void AddCompletedHandlers()
//        {
//            this.sdk.CopyCompleted += this.SdkOnCopyCompleted;
//            this.sdk.GetListCompleted += this.SdkOnGetListCompleted;
//            this.sdk.MakeFolderCompleted += this.SdkOnMakeFolderCompleted;
//            this.sdk.MoveCompleted += this.SdkOnMoveCompleted;
//            this.sdk.PublishCompleted += this.SdkOnPublishCompleted;
//            this.sdk.RemoveCompleted += this.SdkOnRemoveCompleted;
//            this.sdk.TrashCompleted += this.SdkOnTrashCompleted;
//            this.sdk.UnpublishCompleted += this.SdkOnUnpublishCompleted;
//        }

//        private void SdkOnDownloadCompleted(object sender, SdkEventArgs e)
//        {
//            if (e.Error == null)
//            {
//                var fileName = Path.GetFileName(this.downloadFileName);
//                var filePath = this.GetFilePath(fileName);
//                state = State.ready;
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }

//        }

//        private void SdkOnUploadCompleted(object sender, SdkEventArgs e)
//        {
//            if (e.Error == null)
//            {
//                this.InitFolder(this.currentPath);
//                state = State.ready;
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }

//        }

//        private void SdkOnUnpublishCompleted(object sender, SdkEventArgs e)
//        {
//            if (e.Error == null)
//            {
//                this.InitFolder(this.currentPath);
//                state = State.ready;
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }
//        }

//        private void SdkOnPublishCompleted(object sender, GenericSdkEventArgs<string> e)
//        {
//            if (e.Error == null)
//            {
//                //MessageBox.Show(e.Result);
//                //Clipboard.SetText(e.Result);
//                state = State.ready;
//                this.InitFolder(this.currentPath);
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }

//        }

//        private void SdkOnTrashCompleted(object sender, SdkEventArgs e)
//        {
//            if (e.Error == null)
//            {
//                this.InitFolder(this.currentPath);
//                state = State.ready;
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }

//        }

//        private void SdkOnRemoveCompleted(object sender, SdkEventArgs e)
//        {
//            if (e.Error == null)
//            {
//                this.InitFolder(this.currentPath);
//                state = State.ready;
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }

//        }

//        private void SdkOnMoveCompleted(object sender, SdkEventArgs e)
//        {
//            if (e.Error == null)
//            {
//                this.InitFolder(this.currentPath);
//                state = State.ready;
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }

//        }

//        private void SdkOnMakeFolderCompleted(object sender, SdkEventArgs e)
//        {
//            if (e.Error == null)
//            {
//                this.InitFolder(this.currentPath);
//                state = State.ready;
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }

//        }

//        private void SdkOnGetListCompleted(object sender, GenericSdkEventArgs<IEnumerable<DiskItemInfo>> e)
//        {
//            if (e.Error == null)
//            {
//                FolderItems = new ObservableCollection<DiskItemInfo>(e.Result);
//                filesList.Clear();
//                foreach (DiskItemInfo file in FolderItems)
//                {
//                    filesList.Add(file);
//                }
//                state = State.ready;
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }

//        }

//        private void SdkOnCopyCompleted(object sender, SdkEventArgs e)
//        {
//            if (e.Error == null)
//            {
//                this.InitFolder(this.currentPath);
//                state = State.ready;
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }

//        }

//        private void SdkOnAuthorizeCompleted(object sender, GenericSdkEventArgs<string> e)
//        {
//            if (e.Error == null)
//            {
//                AccessToken = e.Result;
//                VariablesClass.YandexToken = e.Result;
//                Settings.Set("YandexToken", VariablesClass.YandexToken);
//                this.CreateSdkClient();
//                    this.OnPropertyChanged("IsLoggedIn");
//                    this.OnPropertyChanged("IsLoggedOut");
//                    this.InitFolder("/");
//                    state = State.ready;
//            }
//            else
//            {
//                Functions.Message(e.Error);
//                state = State.error;
//            }
//        }

//        [NotifyPropertyChangedInvocator]
//        protected virtual void OnPropertyChanged(string propertyName)
//        {
//            PropertyChangedEventHandler handler = this.PropertyChanged;
//            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
//        }
//        private string GetFilePath(string fileName)
//        {
//            return Application.StartupPath.ToString()+"\\"+VariablesClass.TEMP_FOLDER +"\\"+ fileName;
//        }
//        private void LaunchFile(string filePath)
//        {
//            //Process.Start(filePath);
//        }
//        private void ProcessError(SdkException ex)
//        {
//            MessageBox.Show("SDK error: " + ex.Message);            
//        }
//        private void InitFolder(string path)
//        {
//            if (!string.IsNullOrEmpty(AccessToken))
//            {
//                this.CurrentPath = path;
//                this.sdk.GetListAsync(path);
//            }
//        }
//        /// <summary>
//        /// Gets or sets the current path.
//        /// </summary>
//        /// <value>The current path.</value>
//        public string CurrentPath
//        {
//            get
//            {
//                return this.currentPath != null
//                           ? Uri.UnescapeDataString(this.currentPath)
//                           : string.Empty;
//            }
//            set
//            {
//                if (this.currentPath != value)
//                {
//                    this.currentPath = value;
//                    this.OnPropertyChanged("CurrentPath");
//                    this.OnPropertyChanged("WindowTitle");
//                }
//            }
//        }


//    }

//}
