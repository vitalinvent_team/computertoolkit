﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ComputerToolkit
{
    class EngineScript
    {
        public static String contentScriptToExcute = "";
        private static string appPath = Application.StartupPath.ToString();
        //private static string guidScriptToExecute;
        private static string miniScriptContent;
        public static String doevents = "System.Windows.Forms.Application.DoEvents();if (ProcessIcon.StopScript) { break; }";
        public static String doeventsvoid = "System.Windows.Forms.Application.DoEvents(); if (ProcessIcon.StopScript) { break; }";
        Dictionary<string, string> providerOptions = new Dictionary<string, string>
        {
          {"CompilerVersion", "v4.0"}
        };

        public EngineScript(ScriptLanguage scriptLanguage)
        {
        }

        public void AddParameters()
        {

        }

        private static void DeleteOldScripts()
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(Application.StartupPath.ToString());
            FileInfo[] fis = di.GetFiles("script_*.dll");

            foreach (FileInfo file in fis)
            {
                file.Delete();
            }
        }
        public static void ExecuteScript(String guid)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + "" + guid);
            }
            catch { }

            EngineScript scriptEngine;
            String value = "";
            scriptEngine = new EngineScript(ScriptLanguage.CSharp);
            //Scripts scripts = new Scripts();
            //ProcessIcon.lists.scripts.ReadBase();
            ScriptItem script = ProcessIcon.lists.scripts.Find(x => x.Guid == guid.Trim());
            scriptEngine.ExecuteCode(script);
            //value =  .ToString();
            //return value;
        }
        public static String ExecuteScriptWait(String guid)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + "" + guid);
            }
            catch { }

            EngineScript scriptEngine;
            String value = "";
            scriptEngine = new EngineScript(ScriptLanguage.CSharp);
            //Scripts scripts = new Scripts();
            //ProcessIcon.lists.scripts.ReadBase();
            ScriptItem script = ProcessIcon.lists.scripts.Find(x => x.Guid == guid.Trim());
            scriptEngine.ExecuteCodeWait(script);
            return ProcessIcon.outputScript.ToString();
        }
        public static String ExecuteScript(ScriptItem script)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + "" + script.ToString());
            }
            catch { }

            EngineScript scriptEngine;
            String value = "";
            scriptEngine = new EngineScript(ScriptLanguage.VBasic);
            scriptEngine.ExecuteCode(script);
            if (ProcessIcon.outputScript == null)
            {
                return "";
                //return ProcessIcon.outputScript.ToString();
            } else
            {
                return null;
            }
        }
        public static String ExecuteScriptWait(ScriptItem script)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + "" + script.ToString());
            }
            catch { }

            EngineScript scriptEngine;
            String value = "";
            scriptEngine = new EngineScript(ScriptLanguage.VBasic);
            //value = scriptEngine.ExecuteCode(script).ToString();
            //return value;
            scriptEngine.ExecuteCodeWait(script);
            return ProcessIcon.outputScript.ToString();
        }
        public void ExecuteCode(ScriptItem script)
        {
            contentScriptToExcute = script.Content;
            //Thread thread = new Thread(ExecuteCodeThread);
            //ProcessIcon.threads.Add(thread);
            //thread.Start();
            ExecuteCodeThread();
        }
        public void ExecuteCodeWait(ScriptItem script)
        {
            contentScriptToExcute = script.Content;
            //Thread thread = new Thread(ExecuteCodeThread);
            //ProcessIcon.threads.Add(thread);
            //thread.Start();
            //thread.Join();
            ExecuteCodeThread();
        }

        private static void ExecuteCodeThread()
        {
            ProcessIcon.StopScript = false;
            DeleteOldScripts();
            object output = "";
            string folderToexecute = Application.StartupPath.ToString(); //+ "\\temp";
            if (!(Directory.Exists(folderToexecute)))
                Directory.CreateDirectory(folderToexecute);
            string fileToExecute = folderToexecute + "\\script_" + Functions.GetRandomString(8) + ".dll";
            //string fileToExecute = folderToexecute + "\\TestClass.dll";
            //Functions.DeleteFiles(folderToexecute, "*.dll");
            CSharpCodeProvider provider = new CSharpCodeProvider(new Dictionary<string, string>() { { "CompilerVersion", "v3.5" } });
            //CSharpCodeProvider provider = new CSharpCodeProvider();
            ICodeCompiler cc = provider.CreateCompiler();
            CompilerParameters compilerParams = new CompilerParameters();

            if (!Directory.Exists(folderToexecute))
            {
                Directory.CreateDirectory(folderToexecute);
            }
            compilerParams.OutputAssembly = fileToExecute;
            compilerParams.ReferencedAssemblies.Add("System.dll");
            compilerParams.ReferencedAssemblies.Add("System.Core.dll");
            compilerParams.ReferencedAssemblies.Add("System.Data.dll");
            compilerParams.ReferencedAssemblies.Add("System.Xml.dll");
            compilerParams.ReferencedAssemblies.Add("System.Xml.Linq.dll");
            compilerParams.ReferencedAssemblies.Add("System.Drawing.dll");
            //compilerParams.ReferencedAssemblies.Add("System.Net.dll");
            compilerParams.ReferencedAssemblies.Add("mscorlib.dll");
            compilerParams.ReferencedAssemblies.Add("System.Windows.Forms.dll");
            foreach (String library in Updates.Libaries)
            {
                compilerParams.ReferencedAssemblies.Add(Application.StartupPath.ToString() + "\\" + library);
            }
            //compilerParams.ReferencedAssemblies.Add(Application.StartupPath.ToString() + "\\WebCam_Capture.dll");            
            //compilerParams.ReferencedAssemblies.Add(Application.StartupPath.ToString() + "\\PJLControls.dll");
            //compilerParams.ReferencedAssemblies.Add("characteristics.dll");            
            compilerParams.ReferencedAssemblies.Add(Application.StartupPath.ToString() + "\\ComputerToolkit.exe");
            compilerParams.WarningLevel = 3;
            compilerParams.CompilerOptions = "/target:library /optimize";
            compilerParams.GenerateExecutable = false;
            compilerParams.GenerateInMemory = true;
            System.CodeDom.Compiler.TempFileCollection tfc = new TempFileCollection(folderToexecute, false);
            CompilerResults cr = new CompilerResults(tfc);
            do { } while (cr == null);
            do { } while (tfc == null);
            try
            {
                //ScriptItem scr = ProcessIcon.lists.scripts.Find(x => x.Guid == guidScriptToExecute);
                //if (scr == null) return;
                //contentScriptToExcute = ProcessIcon.Clean2(scr.Content);
                contentScriptToExcute = Functions.Clean2(contentScriptToExcute);
                contentScriptToExcute = contentScriptToExcute.Replace("DoEvents();", doevents);
                contentScriptToExcute = contentScriptToExcute.Replace("DoEventsVoid();", doeventsvoid);
                cr = cc.CompileAssemblyFromSource(compilerParams, contentScriptToExcute);
            } catch (Exception ex)
            {
                Functions.Message(ex);
            }
            string err = "";
            if (cr.Errors.Count > 0)
            {
                foreach (CompilerError ce in cr.Errors)
                {
                    err += (ce.ErrorNumber.ToString() + ": " + ce.ErrorText);
                }
                Functions.Message(err);
                ProcessIcon.logs.Add(err+Environment.NewLine+ contentScriptToExcute);
            }
            else
            {
                //System.Collections.Specialized.StringCollection sc = cr.Output;
                //foreach (string s in sc)
                //{
                //    output += s;
                //}
                //Functions.Message(output);
                //EXECUTE

                AppDomainSetup ads = new AppDomainSetup();
                ads.ShadowCopyFiles = "true";
                AppDomain.CurrentDomain.SetShadowCopyFiles();

                AppDomain newDomain = AppDomain.CreateDomain("newDomain");

                byte[] rawAssembly = loadFile(fileToExecute);
                Assembly assembly = newDomain.Load(rawAssembly, null);

                Script testClass = (Script)assembly.CreateInstance("ComputerToolkit.MainClass");
                //Thread th = new Thread();
                output = testClass.Main();

                testClass = null;
                assembly = null;

                AppDomain.Unload(newDomain);
                newDomain = null;
                if (File.Exists(fileToExecute))
                    File.Delete(fileToExecute);
            }
            ProcessIcon.outputScript =  output;

        }
        public static void ExecuteMiniScript(String script)
        {
            miniScriptContent = script;
            //Thread thread = new Thread(ExecuteMiniScriptThread);
            //thread.Start();
            ExecuteMiniScriptThread();
        }
        public static String ExecuteMiniScriptWait(String script)
        {
            miniScriptContent = script;
            //Thread thread = new Thread(ExecuteMiniScriptThread);
            //thread.Start();
            //thread.Join();
            //return ProcessIcon.outputScript.ToString();
            ExecuteMiniScriptThread();
            return ProcessIcon.outputScript.ToString();
        }
        public static void ExecuteMiniScriptThread()
        {
            ProcessIcon.StopScript = false;
            String output = null;
            bool returnExist = false;
            string lastUsing = "";
            string afterUsings = "";
            try
            {
                miniScriptContent = ProcessIcon.Clean2(miniScriptContent);
                ScriptItem scriptMini = new ScriptItem();
                EngineScript scriptEngine = new EngineScript(ScriptLanguage.CSharp);
                string content = "";
                string[] splittedscript = miniScriptContent.Split(';');
                content += "using System;" + System.Environment.NewLine;
                //content += "System.Windows.Forms.dll;" + System.Environment.NewLine;
                if (miniScriptContent.Contains("using"))
                {
                    foreach (string line in splittedscript)
                    {
                        try
                        {
                            if (line.Substring(0, 5).ToLower().Contains("using"))
                            {
                                content += line + ";" + System.Environment.NewLine;
                                lastUsing = line;
                            }
                        }
                        catch { }
                    }
                    lastUsing = lastUsing.Replace("using", "").Trim();
                    int lenUsings = miniScriptContent.IndexOf(lastUsing) + lastUsing.Length + 1;
                    afterUsings = miniScriptContent.Substring(lenUsings, miniScriptContent.Length - lenUsings);
                }
                else
                {
                    afterUsings = miniScriptContent;
                }
                content += "namespace ComputerToolkit" + System.Environment.NewLine;
                content += "{" + System.Environment.NewLine;
                content += "    public class MainClass : ComputerToolkit.Script" + System.Environment.NewLine;
                content += "    {" + System.Environment.NewLine;
                content += "        public MainClass(){}" + System.Environment.NewLine;
                content += "        public object Main()" + System.Environment.NewLine;
                content += "        {" + System.Environment.NewLine;
                content += afterUsings + System.Environment.NewLine;
                if (afterUsings.ToLower().Contains("return"))
                    returnExist = true;
                //foreach (string line in splittedscript)
                //{
                //    try
                //    {
                //        if (!(line.Substring(0, 5).ToLower().Contains("using")))
                //        {
                //            content += line + ";" + System.Environment.NewLine;
                //            if (line.ToLower().Contains("return"))
                //                returnExist = true;
                //        }
                //    }
                //    catch 
                //    {
                //        content += line  + System.Environment.NewLine;
                //    }
                //}
                if (returnExist == false)
                    content += "            return \"\";" + System.Environment.NewLine;
                content += "        }" + System.Environment.NewLine;
                content += "    }" + System.Environment.NewLine;
                content += "}" + System.Environment.NewLine;


                //scriptMini.Guid = Guid.NewGuid().ToString();
                //scriptMini.Name = "newscript";
                //scriptMini.Content = ProcessIcon.Clean2(content);
                //if (Settings.Get("debug", false))
                //    File.WriteAllText("script.txt", content);
                //scriptEngine.ExecuteCode(content);
                contentScriptToExcute = content;
                ExecuteCodeThread();
            }
            catch (Exception Ex)
            {
                Functions.Message(Ex);
            }
        }
        private static byte[] loadFile(string filename)
        {
            try
            {
                FileStream fs = new FileStream(filename, FileMode.Open);
                byte[] buffer = new byte[(int)fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();
                fs = null;
                return buffer;
            }
            catch (Exception ex) { Functions.Message(ex);return null; }
        }
 
    }
}
