﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit
{
    class CommandLine : List<string>
    {
        public Boolean ExitCode=false;
        public Boolean Invisible = false;
        public CommandLine()
        {
            GetHelp();
        }
        public CommandLine(string[] args)
        {
            GetHelp();
            Settings.Set("debug", false);
            if (args.Length > 0)
            {
                bool finded = false;
                foreach (string arg in args)
                {
                    if (arg.Contains("/dt"))
                    {
                        Settings.Set("TasksEnabled", false);
                        finded = true;
                    }
                    if (arg.Contains("/debug"))
                    {
                        Settings.Set("debug", true);
                        finded = true;
                    }
                    if (arg.Contains("/proxy"))
                    {
                        Settings.Set("textBoxProxy", arg.Substring(6, arg.Length - 6));
                        finded = true;
                    }
                    if (arg.Contains("/i"))
                    {
                        this.Invisible = true;
                        finded = true;
                    }
                }
                if (finded == false)
                {
                    MessageBox.Show(GetHelpBox());
                    this.ExitCode = true;
                }
            }
        }
        private void GetHelp()
        {
            this.Add("/dt - disable tasks (выключить назначенные задания)");
            this.Add("/? - show help (подсказка по командам)");
            this.Add("/i - invisible (запустить невидимым)");
        }
        private string GetHelpBox()
        {
            string retval="";
            foreach (string hlp in this)
            {
                retval += hlp + "\n";
            }
            return retval;
        }
    }
}
