﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZedGraph;
using System.Drawing;
using System.Windows.Forms;

namespace ComputerToolkit
{
    public class EngineCharts
    {
        public Form formDiagrams = new Form();
        public ZedGraphControl zedGraphControl = new ZedGraphControl();
        public StatusStrip statusStrip = new StatusStrip();
        public  ToolStripStatusLabel statusLabel1 = new ToolStripStatusLabel();
        public ToolStripStatusLabel statusLabel2 = new ToolStripStatusLabel();
        public  ToolStripStatusLabel statusLabel3 = new ToolStripStatusLabel();
        public EngineCharts()
        {
            zedGraphControl.Dock = DockStyle.Fill;
            formDiagrams.Controls.Add(zedGraphControl);
            statusStrip.Dock = DockStyle.Bottom;
            formDiagrams.Controls.Add(statusStrip);
            statusLabel1.Spring = true;
            statusStrip.Items.Add(statusLabel1);
            statusStrip.Items.Add(statusLabel2);
            statusStrip.Items.Add(statusLabel3);
        }
        //public void ShowLineChart(List<string[]> listData,string title="Graphics",string xTitle="X Value",string yTitle="Y Value")
        public void ShowLineChart()
        {
            PointPairList list = new PointPairList();
            for (double x = 0; x < 36; x++)
            {
                double y = Math.Sin(x * Math.PI / 15.0);

                list.Add(x, y);
            }
        }
        public void ShowLineChart(PointPairList list)
        {
            GraphPane myPane = zedGraphControl.GraphPane;

            // Set the titles and axis labels
            myPane.Title.Text = "My Test Graph";
            myPane.XAxis.Title.Text = "X Value";
            myPane.YAxis.Title.Text = "My Y Axis";

            // Make up some data points from the Sine function

            // Generate a blue curve with circle symbols, and "My Curve 2" in the legend
            LineItem myCurve = myPane.AddCurve("My Curve", list, Color.Blue,
                                    SymbolType.Circle);
            // Fill the area under the curve with a white-red gradient at 45 degrees
            myCurve.Line.Fill = new Fill(Color.White, Color.Red, 45F);
            // Make the symbols opaque by filling them with white
            myCurve.Symbol.Fill = new Fill(Color.White);

            // Fill the axis background with a color gradient
            myPane.Chart.Fill = new Fill(Color.White, Color.LightGoldenrodYellow, 45F);

            // Fill the pane background with a color gradient
            myPane.Fill = new Fill(Color.White, Color.FromArgb(220, 220, 255), 45F);

            // Calculate the Axis Scale Ranges
            zedGraphControl.AxisChange();
            //ZedGraph.ZedGraphControl zedGraph = formCharts.Controls.Find("zg1", false);
            formDiagrams.Show();
        }
    }
}
