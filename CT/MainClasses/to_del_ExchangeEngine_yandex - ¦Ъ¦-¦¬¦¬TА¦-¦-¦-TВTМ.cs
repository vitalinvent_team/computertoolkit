﻿//using System.Threading;
//using ComputerToolkit.Items;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Net;
//using System.Text;
//using System.Text.RegularExpressions;
//using System.Web;
//using System.Windows.Forms;
//using System.Runtime.Serialization;

//namespace ComputerToolkit
//{
//    public class ExchangeEngine_Yandex
//    {
//        private User user = new User();
//        private string appPath;
//        public ExchangeEngine_Yandex()
//        {
//            appPath = Application.StartupPath.ToString();
//        }
//        public void AddUser(User user)
//        {
//            //googleEngine.Refresh();
//            //Google.Apis.Drive.v2.Data.File googleFile = googleEngine.listFiles.Find(x => x.Title == user.Name + ".bin");
//            if (yandexEngine != null)
//            {
//                ProcessIcon.notifyIcon.BalloonTipText = "User allready exist.";
//                ProcessIcon.notifyIcon.ShowBalloonTip(3);
//            }
//            else
//            {
//                string filePath = SerializeObjToFile(user);
//                //googleEngine.UploadFileToFolder(filePath, user.Name + ".bin", "users");
//                ProcessIcon.notifyIcon.BalloonTipText = "Succesfully added.";
//                ProcessIcon.notifyIcon.ShowBalloonTip(3);
//            }
//        }
//        public void VerifyUser(string user, string password)
//        {
//            yandexEngine.Refresh();
//            string filePath = yandexEngine.DownloadFile(user + ".bin");
//            User userG = (User)DeserializeObjFromFile(filePath);
//            System.IO.File.Delete(filePath);
//            if (userG.Password == password)
//            {
//                ProcessIcon.notifyIcon.BalloonTipText = "Succesfully connected.";
//                ProcessIcon.notifyIcon.ShowBalloonTip(3);
//                VariablesClass.user = userG;
//                Settings.Set("UserGuid", userG.Guid);
//            }
//            else
//            {
//                ProcessIcon.notifyIcon.BalloonTipText = "Error in name or password.";
//                ProcessIcon.notifyIcon.ShowBalloonTip(3);
//            }
//        }
//        public void UploadObject(Object obj)
//        {
//            try
//            {
//                if (obj.GetType() == typeof(Notes))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_NOTES", "ComputerToolkitNotes.bin") + ";" + user.Guid;
//                    yandexEngine.UploadFileToFolder(filePath, fileTitle, "usersdata");
//                }
//                if (obj.GetType() == typeof(Scripts))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_SCRIPTS", "ComputerToolkitScripts.bin") + ";" + user.Guid;
//                    yandexEngine.UploadFileToFolder(filePath, fileTitle, "usersdata");
//                }
//                if (obj.GetType() == typeof(Connections))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_CONNECTIONS", "ComputerToolkitConnections.bin") + ";" + user.Guid;
//                    yandexEngine.UploadFileToFolder(filePath, fileTitle, "usersdata");
//                }
//                if (obj.GetType() == typeof(Universals))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_UNIVERSALS", "ComputerToolkitUniversals.bin") + ";" + user.Guid;
//                    yandexEngine.UploadFileToFolder(filePath, fileTitle, "usersdata");
//                }
//                if (obj.GetType() == typeof(Tasks))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_TASKS", "ComputerToolkitTasks.bin") + ";" + user.Guid;
//                    yandexEngine.UploadFileToFolder(filePath, fileTitle, "usersdata");
//                }
//                if (obj.GetType() == typeof(Hotkeys))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_HOTKEYS", "ComputerToolkitHotkeys.bin") + ";" + user.Guid;
//                    yandexEngine.UploadFileToFolder(filePath, fileTitle, "usersdata");
//                }
//                if (obj.GetType() == typeof(Scripts))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_SCRIPTS", "ComputerToolkitScripts.bin") + ";" + user.Guid;
//                    yandexEngine.UploadFileToFolder(filePath, fileTitle, "usersdata");
//                }
//                if (obj.GetType() == typeof(Sensors))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_SENSORS", "ComputerToolkitSensors.bin") + ";" + user.Guid;
//                    yandexEngine.UploadFileToFolder(filePath, fileTitle, "usersdata");
//                }
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message);
//            }
//        }
//        public void UpdateObject(Object obj)
//        {
//            try
//            {
//                if (obj.GetType() == typeof(Notes))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_NOTES", "ComputerToolkitNotes.bin") + ";" + user.Guid;
//                    yandexEngine.UpdateFile(filePath, fileTitle);
//                }
//                if (obj.GetType() == typeof(Scripts))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_SCRIPTS", "ComputerToolkitScripts.bin") + ";" + user.Guid;
//                    yandexEngine.UpdateFile(filePath, fileTitle);
//                }
//                if (obj.GetType() == typeof(Connections))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_CONNECTIONS", "ComputerToolkitConnections.bin") + ";" + user.Guid;
//                    yandexEngine.UpdateFile(filePath, fileTitle);
//                }
//                if (obj.GetType() == typeof(Universals))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_UNIVERSALS", "ComputerToolkitUniversals.bin") + ";" + user.Guid;
//                    yandexEngine.UpdateFile(filePath, fileTitle);
//                }
//                if (obj.GetType() == typeof(Tasks))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_TASKS", "ComputerToolkitTasks.bin") + ";" + user.Guid;
//                    yandexEngine.UpdateFile(filePath, fileTitle);
//                }
//                if (obj.GetType() == typeof(Hotkeys))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_HOTKEYS", "ComputerToolkitHotkeys.bin") + ";" + user.Guid;
//                    yandexEngine.UpdateFile(filePath, fileTitle);
//                }
//                if (obj.GetType() == typeof(Scripts))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_SCRIPTS", "ComputerToolkitScripts.bin") + ";" + user.Guid;
//                    yandexEngine.UpdateFile(filePath, fileTitle);
//                }
//                if (obj.GetType() == typeof(Sensors))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_SENSORS", "ComputerToolkitSensors.bin") + ";" + user.Guid;
//                    yandexEngine.UpdateFile(filePath, fileTitle);
//                }
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message);
//            }
//        }
//        public string DowloadObject(Object obj)
//        {
//            try
//            {
//                if (obj.GetType() == typeof(Notes))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_NOTES", "ComputerToolkitNotes.bin") + ";" + user.Guid;
//                    return yandexEngine.DownloadFile(fileTitle);
//                }
//                if (obj.GetType() == typeof(Scripts))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_SCRIPTS", "ComputerToolkitScripts.bin") + ";" + user.Guid;
//                    return yandexEngine.DownloadFile(fileTitle);
//                }
//                if (obj.GetType() == typeof(Connections))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_CONNECTIONS", "ComputerToolkitConnections.bin") + ";" + user.Guid;
//                    return yandexEngine.DownloadFile(fileTitle);
//                }
//                if (obj.GetType() == typeof(Universals))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_UNIVERSALS", "ComputerToolkitUniversals.bin") + ";" + user.Guid;
//                    return yandexEngine.DownloadFile(fileTitle);
//                }
//                if (obj.GetType() == typeof(Tasks))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_TASKS", "ComputerToolkitTasks.bin") + ";" + user.Guid;
//                    return yandexEngine.DownloadFile(fileTitle);
//                }
//                if (obj.GetType() == typeof(Hotkeys))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_HOTKEYS", "ComputerToolkitHotkeys.bin") + ";" + user.Guid;
//                    return yandexEngine.DownloadFile(fileTitle);
//                }
//                if (obj.GetType() == typeof(Scripts))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_SCRIPTS", "ComputerToolkitScripts.bin") + ";" + user.Guid;
//                    return yandexEngine.DownloadFile(fileTitle);
//                }
//                if (obj.GetType() == typeof(Sensors))
//                {
//                    string filePath = SerializeObjToFile(obj);
//                    string fileTitle = Settings.Get("FILE_SENSORS", "ComputerToolkitSensors.bin") + ";" + user.Guid;
//                    return yandexEngine.DownloadFile(fileTitle);
//                }
//                return null;
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message);
//                return null;
//            }
//        }
//        public void SynchronizeObject(Object obj)
//        {
//            try
//            {
//                string path = DowloadObject(obj);
//                {
//                    if (obj.GetType() == typeof(Notes))
//                    {
//                        Notes notesOnline = new Notes();
//                        Notes notesOffline = new Notes();
//                        if (path != null)
//                        {
//                            notesOnline.ReadBase(path);
//                            foreach (NoteItem noteOnline in notesOnline)
//                            {
//                                NoteItem findedItem = notesOffline.Find(x => x.Guid == noteOnline.Guid);
//                                if (findedItem != null)
//                                {
//                                    if (DateTime.Parse(noteOnline.Date) > DateTime.Parse(findedItem.Date))
//                                    {
//                                        findedItem = noteOnline;
//                                    }
//                                }
//                                else
//                                {
//                                    notesOffline.Add(noteOnline);
//                                }
//                            }
//                            notesOffline.SaveBase();
//                            UpdateObject(notesOffline);
//                        }
//                        else
//                        {
//                            UploadObject(notesOffline);
//                        }

//                    }
//                    if (obj.GetType() == typeof(Scripts))
//                    {
//                        Scripts scriptsOnline = new Scripts();
//                        Scripts scriptsOffline = new Scripts();
//                        if (path != null)
//                        {
//                            scriptsOnline.ReadBase(path);
//                            foreach (ScriptItem scriptOnline in scriptsOnline)
//                            {
//                                ScriptItem findedItem = scriptsOffline.Find(x => x.Guid == scriptOnline.Guid);
//                                if (findedItem != null)
//                                {
//                                    if (DateTime.Parse(scriptOnline.Date) > DateTime.Parse(findedItem.Date))
//                                    {
//                                        findedItem = scriptOnline;
//                                    }
//                                }
//                                else
//                                {
//                                    scriptsOffline.Add(scriptOnline);
//                                }
//                            }
//                            scriptsOffline.SaveBase();
//                            UpdateObject(scriptsOffline);
//                        }
//                        else
//                        {
//                            UploadObject(scriptsOffline);
//                        }

//                    }
//                    if (obj.GetType() == typeof(Connections))
//                    {
//                        Connections connectionsOnline = new Connections();
//                        Connections connectionsOffline = new Connections();
//                        if (path != null)
//                        {
//                            connectionsOnline.ReadBase(path);
//                            foreach (ConnectionItem connectionOnline in connectionsOnline)
//                            {
//                                ConnectionItem findedItem = connectionsOffline.Find(x => x.Guid == connectionOnline.Guid);
//                                if (findedItem != null)
//                                {
//                                    if (DateTime.Parse(connectionOnline.Date) > DateTime.Parse(findedItem.Date))
//                                    {
//                                        findedItem = connectionOnline;
//                                    }
//                                }
//                                else
//                                {
//                                    connectionsOffline.Add(connectionOnline);
//                                }
//                            }
//                            connectionsOffline.SaveBase();
//                            UpdateObject(connectionsOffline);
//                        }
//                        else
//                        {
//                            UploadObject(connectionsOffline);
//                        }
//                    }
//                    if (obj.GetType() == typeof(Universals))
//                    {
//                        Universals universalsOnline = new Universals();
//                        Universals universalsOffline = new Universals();
//                        if (path != null)
//                        {
//                            universalsOnline.ReadBase(path);
//                            foreach (UniversalItem universalOnline in universalsOnline)
//                            {
//                                UniversalItem findedItem = universalsOffline.Find(x => x.Guid == universalOnline.Guid);
//                                if (findedItem != null)
//                                {
//                                    if (DateTime.Parse(universalOnline.Date) > DateTime.Parse(findedItem.Date))
//                                    {
//                                        findedItem = universalOnline;
//                                    }
//                                }
//                                else
//                                {
//                                    universalsOffline.Add(universalOnline);
//                                }
//                            }
//                            universalsOffline.SaveBase();
//                            UpdateObject(universalsOffline);
//                        }
//                        else
//                        {
//                            UploadObject(universalsOffline);
//                        }

//                    }
//                    if (obj.GetType() == typeof(Tasks))
//                    {
//                        Tasks tasksOnline = new Tasks();
//                        Tasks tasksOffline = new Tasks();
//                        if (path != null)
//                        {
//                            tasksOnline.ReadBase(path);
//                            foreach (TaskItem taskOnline in tasksOnline)
//                            {
//                                TaskItem findedItem = tasksOffline.Find(x => x.Guid == taskOnline.Guid);
//                                if (findedItem != null)
//                                {
//                                    if (DateTime.Parse(taskOnline.Date) > DateTime.Parse(findedItem.Date))
//                                    {
//                                        findedItem = taskOnline;
//                                    }
//                                }
//                                else
//                                {
//                                    tasksOffline.Add(taskOnline);
//                                }
//                            }
//                            tasksOffline.SaveBase();
//                            UpdateObject(tasksOffline);
//                        }
//                        else
//                        {
//                            UploadObject(tasksOffline);
//                        }

//                    }
//                    if (obj.GetType() == typeof(Hotkeys))
//                    {
//                        Hotkeys hotkeysOnline = new Hotkeys();
//                        Hotkeys hotkeysOffline = new Hotkeys();
//                        if (path != null)
//                        {
//                            hotkeysOnline.ReadBase(path);
//                            foreach (HotkeyItem hotkeyOnline in hotkeysOnline)
//                            {
//                                HotkeyItem findedItem = hotkeysOffline.Find(x => x.Guid == hotkeyOnline.Guid);
//                                if (findedItem != null)
//                                {
//                                    if (DateTime.Parse(hotkeyOnline.Date) > DateTime.Parse(findedItem.Date))
//                                    {
//                                        findedItem = hotkeyOnline;
//                                    }
//                                }
//                                else
//                                {
//                                    hotkeysOffline.Add(hotkeyOnline);
//                                }
//                            }
//                            hotkeysOffline.SaveBase();
//                            UpdateObject(hotkeysOffline);
//                        }
//                        else
//                        {
//                            UploadObject(hotkeysOffline);
//                        }
//                    }
//                    if (obj.GetType() == typeof(Sensors))
//                    {
//                        Sensors sensorsOnline = new Sensors();
//                        Sensors sensorsOffline = new Sensors();
//                        if (path != null)
//                        {
//                            sensorsOnline.ReadBase(path);
//                            foreach (SensorItem sensorOnline in sensorsOnline)
//                            {
//                                SensorItem findedItem = sensorsOffline.Find(x => x.Guid == sensorOnline.Guid);
//                                if (findedItem != null)
//                                {
//                                    if (DateTime.Parse(sensorOnline.Date) > DateTime.Parse(findedItem.Date))
//                                    {
//                                        findedItem = sensorOnline;
//                                    }
//                                }
//                                else
//                                {
//                                    sensorsOffline.Add(sensorOnline);
//                                }
//                            }
//                            sensorsOffline.SaveBase();
//                            UpdateObject(sensorsOffline);
//                        }
//                        else
//                        {
//                            UploadObject(sensorsOffline);
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message);
//            }
//        }
//        private Object DeserializeObjFromFile(string fileName)
//        {
//            try
//            {
//                Object obj;
//                using (Stream stream = File.Open(fileName, FileMode.Open))
//                {
//                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

//                    obj = (Object)bformatter.Deserialize(stream);
//                }
//                return obj;
//            }
//            catch (SerializationException e)
//            {
//                MessageBox.Show(e.Message);
//                return null;
//            }

//        }
//        private string SerializeObjToFile(Object obj)
//        {
//            try
//            {
//                string fname = System.Guid.NewGuid().ToString() + ".bin";
//                using (Stream stream = File.Open(appPath + "\\" + VariablesClass.TEMP_FOLDER + "\\" + fname, FileMode.Create))
//                {
//                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
//                    bformatter.Serialize(stream, obj);
//                }

//                return appPath + "\\" + VariablesClass.TEMP_FOLDER + "\\" + fname;
//            }
//            catch (SerializationException e)
//            {
//                MessageBox.Show(e.Message);
//                return null;
//            }
//        }
//        private string StreamToString(MemoryStream stream)
//        {
//            stream.Position = 0;
//            using (StreamReader reader = new StreamReader(stream))
//            {
//                return reader.ReadToEnd();
//            }
//        }
//        private Stream StringToStream(string src)
//        {
//            byte[] byteArray = Encoding.UTF8.GetBytes(src);
//            return new MemoryStream(byteArray);
//        }


//    }
//}

