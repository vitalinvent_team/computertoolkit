﻿using CG.Web.MegaApiClient;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ComputerToolkit.MainClasses
{
    public class EngineFileCloud
    {
        Thread threadArchiveFiles = null;
        private static bool isUploadFilesRunning = false;
        private static bool isDownloadFilesRunning = false;
        string rootPath = null;
        private User user = new User();
        private static int _processCounterOneFile = 0;
        public static int processCounterOneFile { get { return _processCounterOneFile; } }

        private static int _processCounterFolder = 0;
        public static int processCounterFolder { get { return _processCounterFolder; } }
        public static int processCounter { get { return _processCounterOneFile + _processCounterFolder; } }

        private string appPath;
        private Label label1;
        public static Boolean isBusy = false;
        //private Logs logs = new Logs("exchangeCloudFilesEngine");
        void Progress()
        {
            //ProcessIcon.mainForm.
        }
        public EngineFileCloud()
        {


            appPath = Application.StartupPath.ToString();
            if (!(Directory.Exists("upload")))
                Directory.CreateDirectory("upload");
            if (!(Directory.Exists("download")))
                Directory.CreateDirectory("download");
            rootPath = Settings.GetString("textBoxRootDir");
        }
        internal List<String> GetFilesCloudWithoutExtensions()
        {
            try
            {
                MegaApiClient mega = new MegaApiClient();

                mega.Login(user.Email, ProcessIcon.Clean2(user.Password));
                var nodes = mega.GetNodes();
                INode root = nodes.Single(n => n.Type == NodeType.Root);
                List<String> files = new List<string>();
                foreach(INode inode in nodes)
                {
                    if (!(inode == null))
                        if (!(inode.Name == null))
                            files.Add(inode.Name.Replace(".bin",""));
                }
                return files;
            }
            catch (Exception ex) { Functions.Message(ex); return null; }
        }
        internal void UploadSelectedToFilesCloudRewrite()
        {
            try
            {
                MegaApiClient mega = new MegaApiClient();
                mega.Login(user.Email, ProcessIcon.Clean2(user.Password));
                var nodes = mega.GetNodes();
                INode root = nodes.Single(n => n.Type == NodeType.Root);
                String[] files = Directory.GetFiles(appPath + "\\upload", "*.bin");
                while (isBusy) { }
                int idx = 1;
                foreach (string file in files)
                {
                    mega.Delete(FindNode(Path.GetFileName(file), mega), false);
                    //ProcessIcon.Debug.Print("All-" + files.Length.ToString() + " archiving-" + idx.ToString() + " " + file + " " + Functions.StrFormatFileSizeBytesMega(file));
                    INode myFile = mega.UploadFile(file, root);
                    Application.DoEvents();
                    idx++;
                }
                //ProcessIcon.Debug.Print("Upload complete");
                Functions.Message("Upload complete");
            }
            catch (Exception ex) { Functions.Message(ex); }
        }
        internal void DeleteSelectedFiles()
        {
            try
            {
                MegaApiClient mega = new MegaApiClient();
                mega.Login(user.Email, ProcessIcon.Clean2(user.Password));
                var nodes = mega.GetNodes();
                foreach (FileItem file in ProcessIcon.lists.files)
                {
                    if ((file.Checked) && (file.Deleted))
                        mega.Delete(FindNode(Path.GetFileName(CreateName(file)), mega), false);
                }
            }
            catch (Exception ex) { Functions.Message(ex); }
        }
        internal void UploadSelectedToFilesCloud()
        {
            try
            {
                MegaApiClient mega = new MegaApiClient();
                mega.Login(user.Email, ProcessIcon.Clean2(user.Password));
                var nodes = mega.GetNodes();
                INode root = nodes.Single(n => n.Type == NodeType.Root);
                String[] files = Directory.GetFiles(appPath + "\\upload", "*.bin");
                while (isBusy) { }
                int idx = 1;
                foreach (string file in files)
                {
                    //ProcessIcon.Debug.Print("All-" + files.Length.ToString() + " archiving-" + idx.ToString() + " " + file + " " + Functions.StrFormatFileSizeBytesMega(file));
                    INode myFile = mega.UploadFile(file, root);
                    Application.DoEvents();
                    idx++;
                }
                //ProcessIcon.Debug.Print("Upload complete");
                Functions.Message("Upload complete");
            }
            catch (Exception ex) { Functions.Message(ex); }
        }
        private INode FindNode(String fileName, MegaApiClient mega)
        {
            IEnumerable<INode> nodesList = mega.GetNodes();
            foreach (INode node in nodesList)
            {
                if (node.Name == fileName)
                    return node;
            }
            return null;
        }
        public void CreateArchivesFromSelectedFiles()
        {
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(appPath + "\\upload");

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                isBusy = true;
                foreach (FileItem file in ProcessIcon.lists.files)
                {
                    try
                    {
                        if ((file.Checked) && (file.ZipAllFolder))
                            _processCounterFolder++;
                        if ((file.Checked) && (file.ZipOneFile))
                            _processCounterOneFile++;
                    }
                    catch (Exception ex) { Functions.Message(ex,file); }
                }
                int idx = 1;
                foreach (FileItem file in ProcessIcon.lists.files)
                {
                    try
                    {
                    if ((file.Checked) && (file.ZipAllFolder))
                    {
                        CreateFolderArchive(file, idx);
                        //logs.Add("All-" + processCounter.ToString() + " archiving-" + idx.ToString() + " " + file);

                    }
                    if ((file.Checked) && (file.ZipOneFile))
                    {
                        CreateOneFileArchive(file, idx);
                        //logs.Add("All-" + processCounter.ToString() + " archiving-" + idx.ToString() + " " + file);
                    }
                    //Program.processIcon.SetState("All-"+processCounter.ToString()+" archiving-"+ idx.ToString());
                    //Program.processIcon.Invoke(Program.processIcon.updateTextTitle, "All-" + processCounter.ToString() + " archiving-" + idx.ToString());
                    idx++;
                    Application.DoEvents();
                    }
                    catch (Exception ex) { Functions.Message(ex,file); }
                }
            }
            catch (Exception ex) { Functions.Message(ex); }
            //ProcessIcon.Debug.Print("Archiving complete");
            Functions.Message("Archiving complete");
            isBusy = false;
        }
        private void CreateFolderArchive(FileItem file,int idx)
        {
            try
            {
                String fileNameZip = appPath + "\\upload\\" + CreateName(file) ;
                if (!(File.Exists(fileNameZip)))
                {
                    string path;
                    if (rootPath.Length > 0)
                    {
                        path = rootPath + "\\" + ProcessIcon.lists.files.Find(x => x.Guid == file.Guid).PathFile;
                    }
                    else
                    {
                        path = Application.StartupPath.ToString() + "\\bin" + ProcessIcon.lists.files.Find(x => x.Guid == file.Guid).PathFile;
                    }
                    if (file.UniquePath)
                        path = file.PathFile;
                    ICSharpCode.SharpZipLib.Zip.FastZip zip = new ICSharpCode.SharpZipLib.Zip.FastZip();
                    zip.Password = Program.processIcon.HeightList2();//Settings.Get("NameList", "");
                    zip.CreateEmptyDirectories = true;
                    //ProcessIcon.Debug.Print("All-" + processCounter.ToString() + " archiving-" + idx.ToString() + " " + file+Environment.NewLine+ fileNameZip);
                    zip.CreateZip(fileNameZip, GetFileFolder(file), true, "");
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex,file);
            }
        }
        public static String CreateName(FileItem file)
        {
            String retVal = "";
            retVal = file.Name.Trim() + "_" + Path.GetFileNameWithoutExtension(file.PathFileFull.Trim()) + "_" + file.FileDescription.Trim();
            if (retVal.Length > 150) retVal = retVal.Substring(0, 150);
            retVal = retVal.Replace("_", "ISUNDERLINE");
            retVal = CleanString(retVal);
            retVal = retVal.Replace("ISUNDERLINE", "_");
            retVal += "_fileversion_" + CleanString(file.FileVersion.Trim()) + ".bin";
            return retVal;
        }
        public static String CreateName(DataGridViewRow row)
        {
            String retVal = "";
            String name = row.Cells["Name"].Value.ToString();
            retVal = row.Cells["Name"].Value.ToString().Trim() + "_" + System.IO.Path.GetFileNameWithoutExtension(row.Cells["PathFileFull"].Value.ToString().Trim()) + "_" + row.Cells["FileDescription"].Value.ToString().Trim();
            if (retVal.Length > 150) retVal = retVal.Substring(0, 150);
            retVal = retVal.Replace("_", "ISUNDERLINE");
            retVal = CleanString(retVal);
            retVal = retVal.Replace("ISUNDERLINE", "_");
            retVal += "_fileversion_" + CleanString(row.Cells["FileVersion"].Value.ToString().Trim()) + ".bin";
            return retVal;
        }
        private static String CleanString(String retVal)
        {
            foreach (char ch in " ()-_*&^%$#@,/;:'[]{}|`~".ToCharArray())
            {
                retVal = retVal.Replace(ch.ToString(), "-");
            }
            return retVal;
        }
        private String GetFileFolder(FileItem file)
        {
            String retVal = "";
            if (file.UniquePath)
            {
                retVal =Path.GetDirectoryName(file.PathFile);

            }
            else
            {
                String[] arrPath = file.PathFile.Split('\\');
                retVal = Application.StartupPath.ToString() + arrPath[0] + "\\bin";
                for (int idx = 0; idx < arrPath.Length - 1; idx++)
                {
                    retVal += arrPath[idx] + "\\";
                }
            }
            return retVal;
        }
        private void CreateOneFileArchive(FileItem file,int idx)
        {
            try
            {
            String fileNameZip = appPath + "\\upload\\" + CreateName(file);
            if (!(File.Exists(fileNameZip)))
            {
                string path;

                if (rootPath.Length > 0)
                {
                    path = rootPath + "\\" + ProcessIcon.lists.files.Find(x => x.Guid == file.Guid).PathFile;
                }
                else
                {
                    path = Application.StartupPath.ToString() + "\\bin" + ProcessIcon.lists.files.Find(x => x.Guid == file.Guid).PathFile;
                }
                    if (file.UniquePath)
                        path = file.PathFile;
                    //ProcessIcon.Debug.Print("All-" + processCounter.ToString() + " archiving-" + idx.ToString() + " " + file + Environment.NewLine + fileNameZip);
                if (File.Exists(path))
                {
                    FileStream fileStreamIn = new FileStream(path, FileMode.Open, FileAccess.Read);
                    FileStream fileStreamOut = new FileStream(fileNameZip, FileMode.Create, FileAccess.Write);

                    ZipOutputStream zipOutStream = new ZipOutputStream(fileStreamOut);

                    byte[] buffer = new byte[fileStreamIn.Length];

                    ZipEntry entry = new ZipEntry(Path.GetFileName(path));
                    zipOutStream.Password = Program.processIcon.HeightList2();//Settings.Get("NameList", "");
                        zipOutStream.PutNextEntry(entry);

                    int size;
                    do
                    {
                        size = fileStreamIn.Read(buffer, 0, buffer.Length);
                        zipOutStream.Write(buffer, 0, size);
                    } while (size > 0);

                    zipOutStream.Close();
                    fileStreamOut.Close();
                    fileStreamIn.Close();
                }
            }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }

        internal void DownloadFilesFromCloud()
        {
            try
            {
                //CreateArchivesFromSelectedFiles();
                MegaApiClient mega = new MegaApiClient();
                mega.Login(user.Email, ProcessIcon.Clean2(user.Password));
                var nodes = mega.GetNodes();
                INode root = nodes.Single(n => n.Type == NodeType.Root);
                IEnumerable<INode> nodesList = mega.GetNodes();
                foreach (INode node in nodesList)
                {
                    foreach (FileItem file in ProcessIcon.lists.files)
                    {
                        if (file.Checked)
                        {
                            String fileNameMust = CreateName(file);
                            String fileNameToDownload = appPath + "\\download\\" + node.Name;
                            if (fileNameMust == node.Name)
                            {
                                if (!(File.Exists(fileNameToDownload)))
                                    mega.DownloadFile(node, fileNameToDownload);
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { Functions.Message(ex); }
            //ProcessIcon.Debug.Print("Download complete");
            Functions.Message("Download complete");
            UnpackArchives();
        }
        private void UnpackArchives()
        {
            try
            {
                String[] filesArc = Directory.GetFiles(appPath + "\\download", "*.bin");
                foreach (String fileArc in filesArc)
                {
                    foreach (FileItem fileItem in ProcessIcon.lists.files)
                    {
                        try
                        {
                            String fileName = CreateName(fileItem);
                            if (fileItem.Checked)
                            {
                                if (fileName == Path.GetFileName(fileArc))
                                {
                                    string path;
                                    if (rootPath.Length > 0)
                                    {
                                        path = rootPath + "\\" + ProcessIcon.lists.files.Find(x => x.Guid == fileItem.Guid).PathFile;
                                    }
                                    else
                                    {
                                        path = Application.StartupPath.ToString() + "\\bin" + ProcessIcon.lists.files.Find(x => x.Guid == fileItem.Guid).PathFile;
                                    }
                                    if (fileItem.UniquePath)
                                        path = fileItem.PathFile;
                                    if (!(Directory.Exists(Path.GetDirectoryName(path))))
                                    {
                                        Directory.CreateDirectory(Path.GetDirectoryName(path));
                                    }
                                    ICSharpCode.SharpZipLib.Zip.FastZip zip = new FastZip();
                                    zip.Password = Program.processIcon.HeightList2();//Settings.Get("NameList", "");
                                    if (fileItem.ZipAllFolder)
                                    {
                                        zip.ExtractZip(fileArc, Path.GetDirectoryName(path), "");
                                    }
                                    if (fileItem.ZipOneFile)
                                    {
                                        zip.ExtractZip(fileArc, Path.GetDirectoryName(path), "");
                                    }

                                    //zip.ExtractAll(Path.GetDirectoryName(path));
                                }
                            }
                        }
                        catch (Exception ex) { Functions.Message(ex); }
                    }
                }
            }
            catch (Exception ex) { Functions.Message(ex); }
            //ProcessIcon.Debug.Print("Unpack complete");
            Functions.Message("Unpack complete");
        }
        private bool CheckNode(String fileName, MegaApiClient mega)
        {
            IEnumerable<INode> nodesList = mega.GetNodes();
            foreach (INode node in nodesList)
            {
                if (node.Name == fileName)
                    return true;
            }
            return false;
        }
        private void CheckAndDeleteNode(String fileName, MegaApiClient mega)
        {
            IEnumerable<INode> nodesList = mega.GetNodes();
            foreach (INode node in nodesList)
            {
                if (node.Name==fileName)
                    mega.Delete(node, false);
            }
        }

        //private void InitializeComponent()
        //{
        //    this.label1 = new System.Windows.Forms.Label();
        //    this.SuspendLayout();
        //    // 
        //    // label1
        //    // 
        //    this.label1.Location = new System.Drawing.Point(12, 9);
        //    this.label1.Name = "label1";
        //    this.label1.Size = new System.Drawing.Size(726, 125);
        //    this.label1.TabIndex = 0;
        //    this.label1.Text = "label1";
        //    // 
        //    // EngineFileCloud
        //    // 
        //    this.ClientSize = new System.Drawing.Size(750, 143);
        //    this.Controls.Add(this.label1);
        //    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
        //    this.Name = "EngineFileCloud";
        //    this.Load += new System.EventHandler(this.EngineFileCloud_Load);
        //    this.ResumeLayout(false);

        //}

        private void EngineFileCloud_Load(object sender, EventArgs e)
        {

        }
    }
}
