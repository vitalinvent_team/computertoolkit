﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ComputerToolkit
{
    public class Updates
    {
        public static List<String> Libaries = new List<string>() { "PJLControls.dll", "WebCam_Capture.dll" , "ICSharpCode.SharpZipLib.dll" };
        private string PROXY_DEF = "";
        private int Major = Assembly.GetEntryAssembly().GetName().Version.Major;
        private int Minor = Assembly.GetEntryAssembly().GetName().Version.Minor;
        private int Build = Assembly.GetEntryAssembly().GetName().Version.Build;
        private int Revision = Assembly.GetEntryAssembly().GetName().Version.Revision;
        //YandexEngine yandexEngine = new YandexEngine();
        private string URL_VERSION = "http://www.vitalinvent.com/ct/bin/version";
        private string URL_FILES = "http://www.vitalinvent.com/ct/bin/files";
        private string URL_UPDATE = "http://www.vitalinvent.com/ct/bin/";
        public static bool resultVerify = false;
        public Updates()
        {
        }
        public void GetUpdate()
        {
        }
        public void VerifyUpdate()
        {
            VerifyUpdate(true);
        }
        public bool Update(bool showInfo = true)
        {
            UpdateLibraries();
            try
            {
                string result = "";
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    result = wc.UploadString(VariablesClass.API_URL + VariablesClass.API_VARIABLE_SELECT.Replace(VariablesClass.API_VARIABLE_NAME_REPLACE, VariablesClass.API_VAL_EXE_VER), "");
                }
                string[] resArr = result.Split('.');
                int MajorRecieve = int.Parse(resArr[0]);
                int MinorRecieve = int.Parse(resArr[1]);
                int BuildRecieve = int.Parse(resArr[2]);
                int RevisionRecieve = int.Parse(resArr[3]);
                bool enableUpdate = false;
                if (Major < MajorRecieve) enableUpdate = true;
                if (Minor < MinorRecieve) enableUpdate = true;
                if (Build < BuildRecieve) enableUpdate = true;
                if (Revision < RevisionRecieve) enableUpdate = true;
                if (enableUpdate)
                {
                    DownloadFiles();
                }
                else
                {
                    if (showInfo)
                    {
                        if (showInfo)
                        {
                            ProcessIcon.notifyIcon.BalloonTipText = "No updates available.";
                            ProcessIcon.notifyIcon.ShowBalloonTip(3);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (showInfo)
                {
                    ProcessIcon.notifyIcon.BalloonTipText = ex.Message.ToString();
                    ProcessIcon.notifyIcon.ShowBalloonTip(3);
                }
            }
            return resultVerify;
        }
        public void UpdateLibraries(bool showInfo = true)
        {
            foreach(String library in Libaries)
            {
                if (!(File.Exists(Application.StartupPath.ToString() + "\\" + library)))
                {
                    string result = "";
                    using (WebClient wc = new WebClient())
                    {
                        try
                        {
                            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                            String url = VariablesClass.API_URL + VariablesClass.API_VARIABLE_SELECT_REPLACE.Replace(VariablesClass.API_VARIABLE_NAME_REPLACE, library).Replace(VariablesClass.API_VAL_REPLACE,"modules");
                            Logs logs = new Logs("updates");
                            logs.Add(url);
                            result = wc.UploadString(url, "");
                            String path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\" + library;
                            Functions.Base64ToFile(result, path);
                        } catch (Exception ex) { Functions.Message(ex); }
                    }
                }
            }
        }
        public bool VerifyUpdate(bool showInfo=true)
        {

            try
            {
                string result = "";
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    result = wc.UploadString(VariablesClass.API_URL + VariablesClass.API_VARIABLE_SELECT.Replace(VariablesClass.API_VARIABLE_NAME_REPLACE, VariablesClass.API_VAL_EXE_VER), "");
                }
                string[] resArr = result.Split('.');
                int MajorRecieve = int.Parse(resArr[0]);
                int MinorRecieve = int.Parse(resArr[1]);
                int BuildRecieve = int.Parse(resArr[2]);
                int RevisionRecieve = int.Parse(resArr[3]);
                bool enableUpdate = false;
                if (Major < MajorRecieve) enableUpdate = true;
                if (Minor < MinorRecieve) enableUpdate = true;
                if (Build < BuildRecieve) enableUpdate = true;
                if (Revision < RevisionRecieve) enableUpdate = true;
                if ((enableUpdate) & !(resultVerify))
                {
                    DialogResult res = MessageBox.Show("Enable new version " + MajorRecieve + "."
                                                       + MinorRecieve + "." + BuildRecieve + "." + RevisionRecieve
                                                       + " do you wish to upgrade?", "Updates",
                        MessageBoxButtons.OKCancel);
                    resultVerify = true;
                    if (res == DialogResult.OK)
                    {
                        DownloadFiles();
                    }
                }
                else
                {
                    if (showInfo)
                    {
                        ProcessIcon.notifyIcon.BalloonTipText = "No updates available.";
                        ProcessIcon.notifyIcon.ShowBalloonTip(3);
                    }
                }
            }
            catch (Exception ex)
            {
                if (showInfo)
                {
                    ProcessIcon.notifyIcon.BalloonTipText = ex.Message.ToString();
                    ProcessIcon.notifyIcon.ShowBalloonTip(3);
                }
            }
            return resultVerify;
        }

        public void DownloadFiles()
        {
                DowloadUpdate();
                string exeName = Path.GetFileName(System.Reflection.Assembly.GetEntryAssembly().Location);
                StreamWriter sw = new StreamWriter("update.cmd");
                sw.WriteLine(Environment.SystemDirectory+"\\ping -n 3 127.0.0.1 > NUL");
                //sw.WriteLine("del /q " + exeName);
                sw.WriteLine("copy _" + exeName + " " + exeName + " /Y");
                sw.WriteLine("start "+ exeName);
                sw.WriteLine("del _"+ exeName);
                sw.WriteLine("exit");
                sw.Close();
                System.Diagnostics.Process.Start("update.cmd");
                Application.Exit();
        }
        public static void DowloadUpdate()
        {
            string result = "";
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                result = wc.UploadString(VariablesClass.API_URL + VariablesClass.API_VARIABLE_SELECT.Replace(VariablesClass.API_VARIABLE_NAME_REPLACE, VariablesClass.API_VAL_EXE_FILE), "");
                String path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\_" + Path.GetFileName(System.Reflection.Assembly.GetEntryAssembly().Location);
                Functions.Base64ToFile(result, path);
            }
        }
        private void ArchiveDlls()
        {

        }
        internal void xDoDevelopUpload()
        {
            String path = System.Reflection.Assembly.GetEntryAssembly().Location;
            System.Net.WebClient Client = new System.Net.WebClient();
            Client.Headers.Add("Content-Type", "binary/octet-stream");
            string URI = VariablesClass.API_URL+ VariablesClass.API_INSERTUPDATEFILE.Replace(VariablesClass.API_VARIABLE_NAME_REPLACE, VariablesClass.API_VAL_EXE_FILE).Replace(VariablesClass.API_VARIABLE_VALUE_REPLACE, "VARIABLE_VALUE");
            byte[] result = Client.UploadFile(URI, "POST", path);
            String s = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
            URI = VariablesClass.API_URL + VariablesClass.API_VARIABLE_INSERTUPD.Replace(VariablesClass.API_VARIABLE_NAME_REPLACE, VariablesClass.API_VAL_EXE_VER).Replace(VariablesClass.API_VARIABLE_VALUE_REPLACE, Assembly.GetEntryAssembly().GetName().Version.ToString());
            string myParameters = "";
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                string HtmlResult = wc.UploadString(URI, myParameters);
            }
            Functions.Message(s);
        }
        internal void BackupLists()
        {
            try
            {
                String path = CreateArchiveLists();
                System.Net.WebClient Client = new System.Net.WebClient();
                Client.Headers.Add("Content-Type", "binary/octet-stream");
                string URI = VariablesClass.API_URL + VariablesClass.API_INSERTUPDATEFILE.Replace(VariablesClass.API_VARIABLE_NAME_REPLACE, Path.GetFileNameWithoutExtension(path)).Replace(VariablesClass.API_VARIABLE_VALUE_REPLACE, "VARIABLE_VALUE");
                byte[] result = Client.UploadFile(URI, "POST", path);
                String s = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
                URI = VariablesClass.API_URL + VariablesClass.API_VARIABLE_INSERTUPD.Replace(VariablesClass.API_VARIABLE_NAME_REPLACE, VariablesClass.API_VAL_EXE_VER).Replace(VariablesClass.API_VARIABLE_VALUE_REPLACE, Assembly.GetEntryAssembly().GetName().Version.ToString());
                string myParameters = "";
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string HtmlResult = wc.UploadString(URI, myParameters);
                }
                Functions.Message(s);
            }
            catch (Exception ex) { Functions.Message(ex); }
        }
        private String CreateArchiveLists()
        {
            try
            {
                String fileNameZip = Application.StartupPath.ToString() + "\\upload\\" + "bkpSettings_"+DateTime.Now.ToString().Replace("-","_").Replace(" ", "_").Replace(":", "_").Replace(".", "_") + ".bin";
                ICSharpCode.SharpZipLib.Zip.FastZip zip = new ICSharpCode.SharpZipLib.Zip.FastZip();
                zip.Password = Settings.Get("NameList", "");
                zip.CreateEmptyDirectories = true;
                zip.CreateZip(fileNameZip, Application.StartupPath.ToString(), false, @"\.xml$");
                return fileNameZip;
            }
            catch (Exception ex) { Functions.Message(ex); return null; }
        }
        #region ParseJson
        private void wc_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            int i = 1;
        }
        private List<Dictionary<string, string>> ParseJSONToListDict(string json)
        {
            //List<Dictionary<string, string>> listDictStr = new List<Dictionary<string, string>>();
            Dictionary<string, string> dictionaryOut = new Dictionary<string, string>(); ;
            List<Dictionary<string, string>> listDict = new List<Dictionary<string, string>>();

            Dictionary<string, object> dictionary = ParseJSON(json);
            foreach (string key in dictionary.Keys)
            {
                List<object> listObj = (List<object>)dictionary[key];
                foreach (Dictionary<string, object> subKey in listObj)
                {
                    Dictionary<string, object> distObj = (Dictionary<string, object>)subKey;
                    listDict.Add(dictionaryOut);
                    foreach (KeyValuePair<string, object> subItem in distObj)
                    {
                        Dictionary<string, object> subDict = (Dictionary<string, object>)subItem.Value;
                        dictionaryOut = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, object> itemDict in subDict)
                        {
                            try
                            {
                                dictionaryOut.Add(itemDict.Key, itemDict.Value.ToString());
                            }
                            catch
                            { }
                            //formMain.StatusInfo = "";
                        }
                    }
                }
            }
            //foreach (Dictionary<string, object> dict in listDict)
            //{
            //    foreach (KeyValuePair<string, object> subItem in dict)
            //    {
            //        Dictionary<string, string> subDict = new Dictionary<string, string>();
            //        subDict.Add(subItem.Key, subItem.Value.ToString());
            //        listDictStr.Add(subDict);
            //    }
            //}
            return listDict;
        }
        private Dictionary<string, string> ParseJSONToDict(string json)
        {
            Dictionary<string, string> dictionaryOut = new Dictionary<string, string>();
            Dictionary<string, object> dictionary = ParseJSON(json);
            foreach (string key in dictionary.Keys)
            {
                List<object> listObj = (List<object>)dictionary[key];
                foreach (Dictionary<string, object> subKey in listObj)
                {
                    Dictionary<string, object> distObj = (Dictionary<string, object>)subKey;
                    foreach (KeyValuePair<string, object> subItem in distObj)
                    {
                        Dictionary<string, object> subDict = (Dictionary<string, object>)subItem.Value;
                        foreach (KeyValuePair<string, object> itemDict in subDict)
                        {
                            try
                            {
                                dictionaryOut.Add(itemDict.Key, itemDict.Value.ToString());
                            }
                            catch
                            { }
                            //formMain.StatusInfo = "";
                        }
                    }
                }
            }
            return dictionaryOut;
        }
        private Dictionary<string, object> ParseJSON(string json)
        {
            int end;
            return ParseJSON(json, 0, out end);
        }
        private Dictionary<string, object> ParseJSON(string json, int start, out int end)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            bool escbegin = false;
            bool escend = false;
            bool inquotes = false;
            string key = null;
            int cend;
            StringBuilder sb = new StringBuilder();
            Dictionary<string, object> child = null;
            List<object> arraylist = null;
            Regex regex = new Regex(@"\\u([0-9a-z]{4})", RegexOptions.IgnoreCase);
            int autoKey = 0;
            for (int i = start; i < json.Length; i++)
            {
                char c = json[i];
                if (c == '\\') escbegin = !escbegin;
                if (!escbegin)
                {
                    if (c == '"')
                    {
                        inquotes = !inquotes;
                        if (!inquotes && arraylist != null)
                        {
                            arraylist.Add(DecodeString(regex, sb.ToString()));
                            sb.Length = 0;
                        }
                        continue;
                    }
                    if (!inquotes)
                    {
                        switch (c)
                        {
                            case '{':
                                if (i != start)
                                {
                                    child = ParseJSON(json, i, out cend);
                                    if (arraylist != null) arraylist.Add(child);
                                    else
                                    {
                                        dict.Add(key, child);
                                        key = null;
                                    }
                                    i = cend;
                                }
                                continue;
                            case '}':
                                end = i;
                                if (key != null)
                                {
                                    if (arraylist != null) dict.Add(key, arraylist);
                                    else dict.Add(key, DecodeString(regex, sb.ToString()));
                                }
                                return dict;
                            case '[':
                                arraylist = new List<object>();
                                continue;
                            case ']':
                                if (key == null)
                                {
                                    key = "array" + autoKey.ToString();
                                    autoKey++;
                                }
                                if (arraylist != null && sb.Length > 0)
                                {
                                    arraylist.Add(sb.ToString());
                                    sb.Length = 0;
                                }
                                dict.Add(key, arraylist);
                                arraylist = null;
                                key = null;
                                continue;
                            case ',':
                                if (arraylist == null && key != null)
                                {
                                    dict.Add(key, DecodeString(regex, sb.ToString()));
                                    key = null;
                                    sb.Length = 0;
                                }
                                if (arraylist != null && sb.Length > 0)
                                {
                                    arraylist.Add(sb.ToString());
                                    sb.Length = 0;
                                }
                                continue;
                            case ':':
                                key = DecodeString(regex, sb.ToString());
                                sb.Length = 0;
                                continue;
                        }
                    }
                }
                sb.Append(c);
                if (escend) escbegin = false;
                if (escbegin) escend = true;
                else escend = false;
            }
            end = json.Length - 1;
            return dict; //theoretically shouldn't ever get here
        }
        private string DecodeString(Regex regex, string str)
        {
            return Regex.Unescape(regex.Replace(str, match => char.ConvertFromUtf32(Int32.Parse(match.Groups[1].Value, System.Globalization.NumberStyles.HexNumber))));
        }


        #endregion ParseJson
    }
}
