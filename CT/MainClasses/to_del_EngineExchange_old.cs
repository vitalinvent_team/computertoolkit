﻿using System.Threading;
using ComputerToolkit.Items;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Windows.Forms;

namespace ComputerToolkit
{
    public class ExchangeEngine
    {
        private string PROXY_DEF = "";
        private string SITEURL_TO_SEND_NOTES = "http://www.vitalinvent.com/ct/notes/sendn.php";
        private string SITEURL_TO_UPDATE_NOTES = "http://www.vitalinvent.com/ct/notes/updn.php";
        private string SITEURL_TO_UPDATE_NOTES_PART = "http://www.vitalinvent.com/ct/notes/updnpart.php";
        private string SITEURL_TO_RESEIVE_NOTES = "http://www.vitalinvent.com/ct/notes/getn.php";
        private string SITEURL_TO_RESEIVE_NOTES_GUID_DATE = "http://www.vitalinvent.com/ct/notes/getngd.php";

        private string SITEURL_TO_ADD_USER = "http://www.vitalinvent.com/ct/users/addu.php";
        private string SITEURL_TO_RECEIVE_USER = "http://www.vitalinvent.com/ct/users/getu.php";
        private string SITEURL_TO_VERIFY_USER = "http://www.vitalinvent.com/ct/users/getu.php";

        private string SITEURL_TO_SEND_SCRIPTS = "http://www.vitalinvent.com/ct/scripts/sends.php";
        private string SITEURL_TO_UPDATE_SCRIPTS = "http://www.vitalinvent.com/ct/scripts/upds.php";
        private string SITEURL_TO_RESEIVE_SCRIPTS = "http://www.vitalinvent.com/ct/scripts/gets.php";
        private string SITEURL_TO_RESEIVE_SCRIPTS_GUID_DATE = "http://www.vitalinvent.com/ct/scripts/getsgd.php";

        private FormMain formMain; //calling form definition to send status
        #region ParseJson
        private void wc_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            int i = 1;
        }
        Dictionary<string, string> dictionaryOut = new Dictionary<string, string>();
        private List<Dictionary<string, string>> ParseJSONToListDict(string json)
        {
            List<Dictionary<string, string>> listDict = new List<Dictionary<string, string>>();
            try
            {
                Dictionary<string, object> dictionary = ParseJSON(json);
                foreach (string key in dictionary.Keys)
                {
                    List<object> listObj = (List<object>)dictionary[key];
                    foreach (Dictionary<string, object> subKey in listObj)
                    {
                        Dictionary<string, object> distObj = (Dictionary<string, object>)subKey;
                        foreach (KeyValuePair<string, object> subItem in distObj)
                        {
                            Dictionary<string, object> subDict = (Dictionary<string, object>)subItem.Value;
                            dictionaryOut = new Dictionary<string, string>();
                            foreach (KeyValuePair<string, object> itemDict in subDict)
                            {
                                try
                                {
                                    dictionaryOut.Add(itemDict.Key, itemDict.Value.ToString());
                                }
                                catch
                                {
                                }
                            }
                        }
                        listDict.Add(dictionaryOut);
                    }
                }
            }
            catch (Exception Ex)
            {
                Functions.Message(Ex);
            }
            return listDict;
        }
        private Dictionary<string, string> ParseJSONToDict(string json)
        {
            Dictionary<string, string> dictionaryOut = new Dictionary<string, string>();
            try
            {
                Dictionary<string, object> dictionary = ParseJSON(json);
                foreach (string key in dictionary.Keys)
                {
                    List<object> listObj = (List<object>)dictionary[key];
                    foreach (Dictionary<string, object> subKey in listObj)
                    {
                        Dictionary<string, object> distObj = (Dictionary<string, object>)subKey;
                        foreach (KeyValuePair<string, object> subItem in distObj)
                        {
                            Dictionary<string, object> subDict = (Dictionary<string, object>)subItem.Value;
                            foreach (KeyValuePair<string, object> itemDict in subDict)
                            {
                                try
                                {
                                    dictionaryOut.Add(itemDict.Key, itemDict.Value.ToString());
                                }
                                catch
                                {
                                }
                                //formMain.StatusInfo = "";
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Functions.Message(Ex);
            }
            return dictionaryOut;
        }
        private Dictionary<string, object> ParseJSON(string json)
        {
            int end;
            return ParseJSON(json, 0, out end);
        }
        private Dictionary<string, object> ParseJSON(string json, int start, out int end)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                bool escbegin = false;
                bool escend = false;
                bool inquotes = false;
                string key = null;
                int cend;
                StringBuilder sb = new StringBuilder();
                Dictionary<string, object> child = null;
                List<object> arraylist = null;
                Regex regex = new Regex(@"\\u([0-9a-z]{4})", RegexOptions.IgnoreCase);
                int autoKey = 0;
                for (int i = start; i < json.Length; i++)
                {
                    char c = json[i];
                    if (c == '\\') escbegin = !escbegin;
                    if (!escbegin)
                    {
                        if (c == '"')
                        {
                            inquotes = !inquotes;
                            if (!inquotes && arraylist != null)
                            {
                                arraylist.Add(DecodeString(regex, sb.ToString()));
                                sb.Length = 0;
                            }
                            continue;
                        }
                        if (!inquotes)
                        {
                            switch (c)
                            {
                                case '{':
                                    if (i != start)
                                    {
                                        child = ParseJSON(json, i, out cend);
                                        if (arraylist != null) arraylist.Add(child);
                                        else
                                        {
                                            dict.Add(key, child);
                                            key = null;
                                        }
                                        i = cend;
                                    }
                                    continue;
                                case '}':
                                    end = i;
                                    if (key != null)
                                    {
                                        if (arraylist != null) dict.Add(key, arraylist);
                                        else dict.Add(key, DecodeString(regex, sb.ToString()));
                                    }
                                    return dict;
                                case '[':
                                    arraylist = new List<object>();
                                    continue;
                                case ']':
                                    if (key == null)
                                    {
                                        key = "array" + autoKey.ToString();
                                        autoKey++;
                                    }
                                    if (arraylist != null && sb.Length > 0)
                                    {
                                        arraylist.Add(sb.ToString());
                                        sb.Length = 0;
                                    }
                                    dict.Add(key, arraylist);
                                    arraylist = null;
                                    key = null;
                                    continue;
                                case ',':
                                    if (arraylist == null && key != null)
                                    {
                                        dict.Add(key, DecodeString(regex, sb.ToString()));
                                        key = null;
                                        sb.Length = 0;
                                    }
                                    if (arraylist != null && sb.Length > 0)
                                    {
                                        arraylist.Add(sb.ToString());
                                        sb.Length = 0;
                                    }
                                    continue;
                                case ':':
                                    key = DecodeString(regex, sb.ToString());
                                    sb.Length = 0;
                                    continue;
                            }
                        }
                    }
                    sb.Append(c);
                    if (escend) escbegin = false;
                    if (escbegin) escend = true;
                    else escend = false;
                }
                end = json.Length - 1;
                return dict; //theoretically shouldn't ever get here
            }
            catch (Exception Ex)
            {
                Functions.Message(Ex);
            }

            end = 0; return dict;

        }
        private string DecodeString(Regex regex, string str)
        {
            return Regex.Unescape(regex.Replace(str, match => char.ConvertFromUtf32(Int32.Parse(match.Groups[1].Value, System.Globalization.NumberStyles.HexNumber))));
        }
        #endregion ParseJson
        #region Notes
        public void SynchronizeNotes()
        {
            string strToUrl = SITEURL_TO_RESEIVE_NOTES_GUID_DATE + "?User=" + Functions.Base64Encode(Settings.GetString("UserGuid"));
            var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
            string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
            if (proxAddr.Length > 0)
            {
                WebProxy mp = new WebProxy(proxAddr);
                mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.Proxy = mp;
            }

            request.BeginGetResponse(ResponseSynchronizeNotes, request);
        }
        private void ResponseSynchronizeNotes(IAsyncResult ar)
        {
            try
            {
                //Notes notes = new Notes();
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                List<string> onlineNotesListGuids = new List<string>();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                List<Dictionary<string, string>> resultList = ParseJSONToListDict(result);
                if (resultList.Count != 0)
                {
                    foreach (Dictionary<string, string> dict in resultList)
                    {
                        if (dict.Count != 0)
                        {
                            //check online notes
                            string GuidReceived = Functions.Base64Decode(dict["Guid"]);
                            string DateReceived = Functions.Base64Decode(dict["Date"]);
                            onlineNotesListGuids.Add(GuidReceived);
                            NoteItem note = ProcessIcon.lists.notes.Find(x => x.Guid == GuidReceived);
                            if (note == null)
                            {
                                DownloadNewNote(GuidReceived);
                            }
                            else
                            {
                                if (DateTime.Parse(note.Date) < DateTime.Parse(DateReceived))
                                {
                                    DownloadNote(note);
                                }
                                if (DateTime.Parse(note.Date) > DateTime.Parse(DateReceived))
                                {
                                    UpdateNote(note);
                                }
                            }
                        }
                    }
                }
                //check offline notes
                foreach (NoteItem localNote in ProcessIcon.lists.notes)
                {
                    string findedOnlineNote = onlineNotesListGuids.Find(x => x == localNote.Guid);
                    if (findedOnlineNote == null)
                    {
                        UploadNote(localNote);
                    }
                }

            }
            catch (Exception Ex)
            {
                Functions.Message(Ex);
            }

        }
        public void DownloadNewNote(String noteGuid)
        {
            string strToUrl = SITEURL_TO_RESEIVE_NOTES + "?Guid=" + Functions.Base64Encode(noteGuid);
            var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
            string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
            if (proxAddr.Length > 0)
            {
                WebProxy mp = new WebProxy(proxAddr);
                mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.Proxy = mp;
            }
            request.BeginGetResponse(ResponseDownloadNewNote, request);
        }
        private void ResponseDownloadNewNote(IAsyncResult ar)
        {
            try
            {
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                Dictionary<string, string> resultDict = ParseJSONToDict(result);
                if (resultDict.Count != 0)
                {
                    string Guid = Functions.Base64Decode(resultDict["Guid"]);
                    string Content = Functions.Base64Decode(resultDict["Content"]);
                    string Date = Functions.Base64Decode(resultDict["Date"]);
                    string Group = Functions.Base64Decode(resultDict["Group"]);
                    string Name = Functions.Base64Decode(resultDict["Name"]);
                    string Teg = Functions.Base64Decode(resultDict["Teg"]);
                    //Notes notes = new Notes();
                    ProcessIcon.lists.notes.Add(new NoteItem(Guid, Name, Date, Content, Group, Teg));
                    ProcessIcon.lists.notes.SaveBase();
                }

            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }


        public void DownloadNote(NoteItem note)
        {
            string strToUrl = SITEURL_TO_RESEIVE_NOTES + "?Guid=" + Functions.Base64Encode(note.Guid);
            var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
            string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
            if (proxAddr.Length > 0)
            {
                WebProxy mp = new WebProxy(proxAddr);
                mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.Proxy = mp;
            }
            request.BeginGetResponse(ResponseDownloadNote, request);
        }
        private void ResponseDownloadNote(IAsyncResult ar)
        {
            try
            {
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                Dictionary<string, string> resultDict = ParseJSONToDict(result);
                if (resultDict.Count != 0)
                {
                    //Notes notes = new Notes();
                    string Guid = Functions.Base64Decode(resultDict["Guid"]);
                    NoteItem note = ProcessIcon.lists.notes.Find(x => x.Guid == Guid);
                    note.Content = Functions.Base64Decode(resultDict["Content"]);
                    note.Date = Functions.Base64Decode(resultDict["Date"]);
                    note.Group = Functions.Base64Decode(resultDict["Group"]);
                    note.Name = Functions.Base64Decode(resultDict["Name"]);
                    note.Teg = Functions.Base64Decode(resultDict["Teg"]);
                    ProcessIcon.lists.notes.SaveBase();
                }

            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }

        public void UploadNote(NoteItem note)
        {
            if (Functions.Base64Encode(note.Content).Length > 1000)
            {
                List<string> listPart = GetPartContent(Functions.Base64Encode(note.Content));
                String json = "{\"User\":\"" + Functions.Base64Encode(note.User) + "\","
                + "\"Guid\":\"" + Functions.Base64Encode(note.Guid) + "\","
                + "\"Name\":\"" + Functions.Base64Encode(note.Name) + "\","
                + "\"Content\":\"" + listPart[0] + "\","
                + "\"Date\":\"" + Functions.Base64Encode(note.Date) + "\","
                + "\"Teg\":\"" + Functions.Base64Encode(note.Teg) + "\"}";
                string strToUrl = SITEURL_TO_SEND_NOTES + "?json=" + json;
                var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
                request.ContentType = "text/json";
                request.Method = "POST";
                string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
                if (proxAddr.Length > 0)
                {
                    WebProxy mp = new WebProxy(proxAddr);
                    mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    request.Proxy = mp;
                }
                request.BeginGetResponse(ResponseUpdateNote, request);

                for (int i = 1; i < listPart.Count; i++)
                {
                    json = "{\"User\":\"" + Functions.Base64Encode(note.User) + "\","
                    + "\"Guid\":\"" + Functions.Base64Encode(note.Guid) + "\","
                    + "\"Name\":\"" + Functions.Base64Encode(note.Name) + "\","
                    + "\"Content\":\"" + listPart[i] + "\","
                    + "\"Date\":\"" + Functions.Base64Encode(note.Date) + "\","
                    + "\"Teg\":\"" + Functions.Base64Encode(note.Teg) + "\"}";
                    strToUrl = SITEURL_TO_UPDATE_NOTES_PART + "?json=" + json;
                    request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
                    request.ContentType = "text/json";
                    request.Method = "POST";
                    proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
                    if (proxAddr.Length > 0)
                    {
                        WebProxy mp = new WebProxy(proxAddr);
                        mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                        request.Proxy = mp;
                    }
                    request.BeginGetResponse(ResponseUpdateNote, request);
                }
            }
            else
            {
                String json = "{\"User\":\"" + Functions.Base64Encode(note.User) + "\","
                    + "\"Guid\":\"" + Functions.Base64Encode(note.Guid) + "\","
                    + "\"Name\":\"" + Functions.Base64Encode(note.Name) + "\","
                    + "\"Content\":\"" + Functions.Base64Encode(note.Content) + "\","
                    + "\"Date\":\"" + Functions.Base64Encode(note.Date) + "\","
                    + "\"Teg\":\"" + Functions.Base64Encode(note.Teg) + "\"}";
                string strToUrl = SITEURL_TO_SEND_NOTES + "?json=" + json;
                //string strToUrl2 = SITEURL_TO_SEND_NOTES + "?json=";
                var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
                request.ContentType = "application/json; charset=UTF-8";
                request.Accept = "application/json";
                request.Method = "POST";
                request.ContentLength = strToUrl.Length;

                string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
                if (proxAddr.Length > 0)
                {
                    WebProxy mp = new WebProxy(proxAddr);
                    mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    request.Proxy = mp;
                }
                request.BeginGetResponse(ResponseUploadNote, request);
            }



            // create a request
            //        String json = "?json=" + "{\"User\":\"" + Functions.Base64Encode(note.User) + "\","
            //+ "\"Guid\":\"" + Functions.Base64Encode(note.Guid) + "\","
            //+ "\"Name\":\"" + Functions.Base64Encode(note.Name) + "\","
            //+ "\"Content\":\"" + Functions.Base64Encode(note.Content) + "\","
            //+ "\"Date\":\"" + Functions.Base64Encode(note.Date) + "\","
            //+ "\"Teg\":\"" + Functions.Base64Encode(note.Teg) + "\"}";
            //        HttpWebRequest request = (HttpWebRequest)
            //        WebRequest.Create(SITEURL_TO_SEND_NOTES); request.KeepAlive = false;
            //        request.ProtocolVersion = HttpVersion.Version10;
            //        request.Method = "POST";


            //        // turn our request string into a byte stream
            //        byte[] postBytes = Encoding.UTF8.GetBytes(json);

            //        // this is important - make sure you specify type this way
            //        request.ContentType = "application/json; charset=UTF-8";
            //        request.Accept = "application/json";
            //        request.ContentLength = postBytes.Length;
            //        Stream requestStream = request.GetRequestStream();

            //        // now send it
            //        requestStream.Write(postBytes, 0, postBytes.Length);
            //        requestStream.Close();

            //        // grab te response and print it out to the console along with the status code
            //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //        string result;
            //        using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            //        {
            //            result = rdr.ReadToEnd();
            //            ProcessIcon.notifyIcon.BalloonTipText = result;
            //            ProcessIcon.notifyIcon.ShowBalloonTip(3);
            //        }

        }
        private void ResponseUploadNote(IAsyncResult ar)
        {
            try
            {
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }

        private List<String> GetPartContent(String cont)
        {
            List<String> listPart = new List<string>();
            for (int i = 0; i < cont.Length; i += 1000)
            {
                if ((i + 1000) > cont.Length)
                {
                    listPart.Add(cont.Substring(i, cont.Length - i));
                }
                else
                {
                    listPart.Add(cont.Substring(i, 1000));
                }
            }
            return listPart;
        }
        public void UpdateNote(NoteItem note)
        {
            if (Functions.Base64Encode(note.Content).Length > 1000)
            {
                List<string> listPart = GetPartContent(Functions.Base64Encode(note.Content));
                String json = "{\"User\":\"" + Functions.Base64Encode(note.User) + "\","
                + "\"Guid\":\"" + Functions.Base64Encode(note.Guid) + "\","
                + "\"Name\":\"" + Functions.Base64Encode(note.Name) + "\","
                + "\"Content\":\"" + listPart[0] + "\","
                + "\"Date\":\"" + Functions.Base64Encode(note.Date) + "\","
                + "\"Teg\":\"" + Functions.Base64Encode(note.Teg) + "\"}";
                string strToUrl = SITEURL_TO_UPDATE_NOTES + "?json=" + json;
                var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
                request.ContentType = "text/json";
                request.Method = "POST";
                string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
                if (proxAddr.Length > 0)
                {
                    WebProxy mp = new WebProxy(proxAddr);
                    mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    request.Proxy = mp;
                }
                request.BeginGetResponse(ResponseUpdateNote, request);
                Thread.Sleep(250);
                for (int i = 1; i < listPart.Count; i++)
                {
                    json = "{\"User\":\"" + Functions.Base64Encode(note.User) + "\","
                    + "\"Guid\":\"" + Functions.Base64Encode(note.Guid) + "\","
                    + "\"Name\":\"" + Functions.Base64Encode(note.Name) + "\","
                    + "\"Content\":\"" + listPart[i] + "\","
                    + "\"Date\":\"" + Functions.Base64Encode(note.Date) + "\","
                    + "\"Teg\":\"" + Functions.Base64Encode(note.Teg) + "\"}";
                    strToUrl = SITEURL_TO_UPDATE_NOTES_PART + "?json=" + json;
                    request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
                    request.ContentType = "text/json";
                    request.Method = "POST";
                    proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
                    if (proxAddr.Length > 0)
                    {
                        WebProxy mp = new WebProxy(proxAddr);
                        mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                        request.Proxy = mp;
                    }
                    request.BeginGetResponse(ResponseUpdateNote, request);
                    Thread.Sleep(250);
                }
            }
            else
            {
                String json = "{\"User\":\"" + Functions.Base64Encode(note.User) + "\","
                    + "\"Guid\":\"" + Functions.Base64Encode(note.Guid) + "\","
                    + "\"Name\":\"" + Functions.Base64Encode(note.Name) + "\","
                    + "\"Content\":\"" + Functions.Base64Encode(note.Content) + "\","
                    + "\"Date\":\"" + Functions.Base64Encode(note.Date) + "\","
                    + "\"Teg\":\"" + Functions.Base64Encode(note.Teg) + "\"}";
                string strToUrl = SITEURL_TO_UPDATE_NOTES + "?json=" + json;
                var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
                request.ContentType = "text/json";
                request.Method = "POST";
                //request.ContentLength = strToUrl.Length;
                string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
                if (proxAddr.Length > 0)
                {
                    WebProxy mp = new WebProxy(proxAddr);
                    mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    request.Proxy = mp;
                }
                request.BeginGetResponse(ResponseUpdateNote, request);

            }
        }
        private void ResponseUpdateNote(IAsyncResult ar)
        {
            try
            {
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }

            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        #endregion Notes
        #region Users
        public void AddUser(string name, string password, string email, string guid)
        {
            try
            {
                string strToUrl = SITEURL_TO_ADD_USER + "?Name=" + name + "&Password=" + password + "&Email=" + email +
                                  "&Guid=" + guid;
                var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
                string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
                if (proxAddr.Length > 0)
                {
                    WebProxy mp = new WebProxy(proxAddr);
                    mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    request.Proxy = mp;
                }
                request.BeginGetResponse(ResponseUserAddData, request);
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        private void ResponseUserAddData(IAsyncResult ar)
        {
            try
            {
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        public void VerifyUser(string userName, string password)
        {
            try
            {
                string strToUrl = SITEURL_TO_VERIFY_USER + "?Name=" + userName + "&Password=" + password;
                var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
                string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
                if (proxAddr.Length > 0)
                {
                    WebProxy mp = new WebProxy(proxAddr);
                    mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    request.Proxy = mp;
                }

                request.BeginGetResponse(ResponseUserData, request);
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        private void ResponseUserData(IAsyncResult ar)
        {
            try
            {
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                Dictionary<string, string> resultDict = ParseJSONToDict(result);
                if (resultDict.Count != 0)
                {
                    string user = Settings.GetString("textBoxClientId");
                    string password = Settings.GetString("textBoxClientSecret");
                    string email = Functions.CleanDefDecode(Settings.GetString("textBoxUserEmail"));
                    string UserGuid = resultDict["Guid"];
                    if ((resultDict["Password"] == password))
                    {
                        Settings.Set("UserGuid", UserGuid);
                        Functions.Message("Succesfully connected.");
                    }
                    else
                    {
                        Functions.Message("Password error.");
                    }
                }
                else
                {
                    Functions.Message("User does not exist.");
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        #endregion Users
        #region Scripts
        public void SynchronizeScripts()
        {
            try
            {
                string strToUrl = SITEURL_TO_RESEIVE_SCRIPTS_GUID_DATE + "?User=" +
                                  Functions.Base64Encode(Settings.GetString("UserGuid"));
                var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
                string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
                if (proxAddr.Length > 0)
                {
                    WebProxy mp = new WebProxy(proxAddr);
                    mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    request.Proxy = mp;
                }

                request.BeginGetResponse(ResponseSynchronizeScripts, request);
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        private void ResponseSynchronizeScripts(IAsyncResult ar)
        {
            try
            {
                //Scripts scripts = new Scripts();
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                List<string> onlineNotesListGuids = new List<string>();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                List<Dictionary<string, string>> resultList = ParseJSONToListDict(result);
                if (resultList.Count != 0)
                {
                    foreach (Dictionary<string, string> dict in resultList)
                    {
                        if (dict.Count != 0)
                        {
                            //check online notes
                            string GuidReceived = Functions.Base64Decode(dict["Guid"]);
                            string DateReceived = Functions.Base64Decode(dict["Date"]);
                            onlineNotesListGuids.Add(GuidReceived);
                            ScriptItem script = ProcessIcon.lists.scripts.Find(x => x.Guid == GuidReceived);
                            if (script == null)
                            {
                                DownloadNewScript(GuidReceived);
                            }
                            else
                            {
                                if (DateTime.Parse(script.Date) < DateTime.Parse(DateReceived))
                                {
                                    DownloadScript(script);
                                }
                                if (DateTime.Parse(script.Date) > DateTime.Parse(DateReceived))
                                {
                                    UpdateScript(script);
                                }
                            }
                        }
                    }
                }
                //check offline notes
                foreach (ScriptItem localScript in ProcessIcon.lists.scripts)
                {
                    string findedOnlineNote = onlineNotesListGuids.Find(x => x == localScript.Guid);
                    if (findedOnlineNote == null)
                    {
                        UploadScript(localScript);
                    }
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        public void DownloadNewScript(String noteGuid)
        {
            string strToUrl = SITEURL_TO_RESEIVE_SCRIPTS + "?Guid=" + Functions.Base64Encode(noteGuid);
            var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
            string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
            if (proxAddr.Length > 0)
            {
                WebProxy mp = new WebProxy(proxAddr);
                mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.Proxy = mp;
            }

            request.BeginGetResponse(ResponseDownloadNewScript, request);
        }
        private void ResponseDownloadNewScript(IAsyncResult ar)
        {
            try
            {
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                Dictionary<string, string> resultDict = ParseJSONToDict(result);
                if (resultDict.Count != 0)
                {
                    string Guid = Functions.Base64Decode(resultDict["Guid"]);
                    string Content = Functions.Base64Decode(resultDict["Content"]);
                    string Date = Functions.Base64Decode(resultDict["Date"]);
                    string Group = Functions.Base64Decode(resultDict["Group"]);
                    string Name = Functions.Base64Decode(resultDict["Name"]);
                    string ScriptLang = Functions.Base64Decode(resultDict["ScriptLang"]);
                    string UserInfo = Functions.Base64Decode(resultDict["UserInfo"]);
                    string Teg = Functions.Base64Decode(resultDict["Teg"]);
                    string User = Functions.Base64Decode(resultDict["User"]);
                    //Scripts scripts = new Scripts();
                    ScriptLanguage sl = ScriptLanguage.CSharp;
                    switch (ScriptLang)
                    {
                        case "CSharp":
                            sl = ScriptLanguage.CSharp;
                            break;
                        case "VBasic":
                            sl = ScriptLanguage.VBasic;
                            break;
                    }
                    ProcessIcon.lists.scripts.Add(new ScriptItem(Guid, Name, Date, Content, Group, sl, UserInfo));
                    ProcessIcon.lists.scripts.SaveBase();
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }


        public void DownloadScript(ScriptItem script)
        {
            string strToUrl = SITEURL_TO_RESEIVE_SCRIPTS + "?Guid=" + Functions.Base64Encode(script.Guid);
            var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
            string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
            if (proxAddr.Length > 0)
            {
                WebProxy mp = new WebProxy(proxAddr);
                mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.Proxy = mp;
            }

            request.BeginGetResponse(ResponseDownloadScript, request);
        }
        private void ResponseDownloadScript(IAsyncResult ar)
        {
            try
            {
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                Dictionary<string, string> resultDict = ParseJSONToDict(result);
                if (resultDict.Count != 0)
                {
                    //Scripts scripts = new Scripts();
                    string Guid = Functions.Base64Decode(resultDict["Guid"]);
                    ScriptItem script = ProcessIcon.lists.scripts.Find(x => x.Guid == Guid);
                    script.Content = ProcessIcon.Clean2(Functions.Base64Decode(resultDict["Content"]));
                    script.Date = Functions.Base64Decode(resultDict["Date"]);
                    script.Group = Functions.Base64Decode(resultDict["Group"]);
                    script.Name = Functions.Base64Decode(resultDict["Name"]);
                    ScriptLanguage sl = ScriptLanguage.CSharp;
                    switch (Functions.Base64Decode(resultDict["ScriptLang"]))
                    {
                        case "CSharp":
                            sl = ScriptLanguage.CSharp;
                            break;
                        case "VBasic":
                            sl = ScriptLanguage.VBasic;
                            break;
                    }
                    script.ScriptLang = sl;
                    //script.Teg = Functions.Base64Decode(resultDict["Teg"]);
                    script.UserInfo = Functions.Base64Decode(resultDict["UserInfo"]);
                    script.User = Functions.Base64Decode(resultDict["User"]);
                    ProcessIcon.lists.scripts.SaveBase();
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        public void UpdateScript(ScriptItem script)
        {
            String json = "{\"User\":\"" + Functions.Base64Encode(script.User) + "\","
                + "\"Guid\":\"" + Functions.Base64Encode(script.Guid) + "\","
                + "\"Name\":\"" + Functions.Base64Encode(script.Name) + "\","
                + "\"Content\":\"" + Functions.Base64Encode(script.Content) + "\","
                + "\"Date\":\"" + Functions.Base64Encode(script.Date) + "\","
                + "\"Group\":\"" + Functions.Base64Encode(script.Group) + "\","
                + "\"ScriptLang\":\"" + Functions.Base64Encode(script.ScriptLang.ToString()) + "\","
                //+ "\"Teg\":\"" + Functions.Base64Encode(script.Teg) + "\","
                + "\"UserInfo\":\"" + Functions.Base64Encode(script.UserInfo) + "\"}";
            string strToUrl = SITEURL_TO_UPDATE_SCRIPTS + "?json=" + json;
            var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
            string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
            if (proxAddr.Length > 0)
            {
                WebProxy mp = new WebProxy(proxAddr);
                mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.Proxy = mp;
            }
            request.BeginGetResponse(ResponseUpdateScript, request);
        }
        private void ResponseUpdateScript(IAsyncResult ar)
        {
            try
            {
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                //ProcessIcon.notifyIcon.BalloonTipText = result.ToString();
                //ProcessIcon.notifyIcon.ShowBalloonTip(3);
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        public void UploadScript(ScriptItem script)
        {
            String json = "{\"User\":\"" + Functions.Base64Encode(script.User) + "\","
                + "\"Guid\":\"" + Functions.Base64Encode(script.Guid) + "\","
                + "\"Name\":\"" + Functions.Base64Encode(script.Name) + "\","
                + "\"Content\":\"" + Functions.Base64Encode(script.Content) + "\","
                + "\"Date\":\"" + Functions.Base64Encode(script.Date) + "\","
                + "\"Group\":\"" + Functions.Base64Encode(script.Group) + "\","
                + "\"ScriptLang\":\"" + Functions.Base64Encode(script.ScriptLang.ToString()) + "\","
                //+ "\"Teg\":\"" + Functions.Base64Encode(script.Teg) + "\","
                + "\"UserInfo\":\"" + Functions.Base64Encode(script.UserInfo) + "\"}";
            string strToUrl = SITEURL_TO_SEND_SCRIPTS + "?json=" + json;
            var request = WebRequest.Create(new Uri(strToUrl)) as HttpWebRequest;
            string proxAddr = Settings.Get("textBoxProxy", PROXY_DEF);
            if (proxAddr.Length > 0)
            {
                WebProxy mp = new WebProxy(proxAddr);
                mp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.Proxy = mp;
            }

            request.BeginGetResponse(ResponseUploadScript, request);
        }
        private void ResponseUploadScript(IAsyncResult ar)
        {
            try
            {
                var request = ar.AsyncState as HttpWebRequest;
                var response = request.EndGetResponse(ar) as HttpWebResponse;
                string result = "";
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                //ProcessIcon.notifyIcon.BalloonTipText = result.ToString();
                //ProcessIcon.notifyIcon.ShowBalloonTip(3);
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }
        #endregion Scripts

    }
}

