﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ComputerToolkit.MainClasses
{
    public static class Localization
    {
        public static Dictionary<string, string> GetLocalization(string str)
        {
            Dictionary<string, string> locDict = new Dictionary<string, string>();
            try
            {
                List<string> readedLines = new List<string>();
                string lineR;
                using (StreamReader sr = new StreamReader(str, Encoding.UTF8))
                {
                    while ((lineR = sr.ReadLine()) != null)
                    {
                        readedLines.Add(lineR);
                    }
                }
                foreach (string line in readedLines)
                {
                    string[] locStrArr = line.Split(';');
                    locDict.Add(locStrArr[0], locStrArr[1]);
                }
            }
            catch { }
            return locDict;
        }
        public static List<string> GetLocalizations()
        {
            List<string> listFiles = new List<string>();
            string[] filePaths = Directory.GetFiles(Application.StartupPath.ToString(),"*.loc");
            foreach (string file in filePaths)
            {
                listFiles.Add(Functions.GetFileNameInPath(file));
            }
            return listFiles;
        }


    }
}
