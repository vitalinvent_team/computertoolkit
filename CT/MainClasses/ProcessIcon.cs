﻿using System.Threading;
using ComputerToolkit.Forms;
using ComputerToolkit.MainClasses;
using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;
using WebCam_Capture;

namespace ComputerToolkit
{
    public class ProcessIcon
    {
        //public static ComputerToolkit.Debug Debug = new ComputerToolkit.Debug(); 
        //public static FormDebug Debug = new FormDebug();
        public static FormDebug Debug = new FormDebug();

        public static Boolean StopScript = false;
        public static List<Thread> threads = new List<Thread>();
        public static Object outputScript=null;
        Updates updates = new Updates();
        //public static WebCamCapture WebCamCapture = new WebCamCapture();
        public static List<Timer> timers = new List<Timer>();
        public static Timer timerFirstSync = new Timer();
        public static Lists lists = new Lists();
        public static FormQuickHint formQuickHint = new FormQuickHint();
        //public static FormQuickHintItem formQuickHintItem = new FormQuickHintItem();
        public static List<FormQuickHintItem> listFormQuickHintItem = new List<FormQuickHintItem>();
        public static int currentQuickIntem = 0;
        public static NotifyIcon notifyIcon;
        private ProcessesClass processesClass = new ProcessesClass();
        ContextMenuStrip menu = new ContextMenus().CreateMOnouseOverMenu();
        //private List<Timer> timersList = new List<Timer>();
        public static Timer timerTasks = new Timer();
        public static Timer timerQuickSearshHide = new Timer();
        public Timer timerCheckUpdates = new Timer();
        private Timer timerSynchronize = new Timer();
        //Tasks tasks = new Tasks();
        private bool updatesEnable = false;
        private bool googleEnable = false;
        private Thread googleThread;
        public static ContextMenuStrip cmtmnu = new ContextMenus().CreateMainMenu();
        public static FormConnections formConnection = new FormConnections();
        public static EngineExchange exchangeEngine = new EngineExchange();
        public static Logs logs = new Logs("logs");
        //private static String NamesToClean = Settings.Get("NameList", "");
        private static String NamesToClean = null;
        private static String slt = "!" + "@" + "#" + "$";
        public Form mainForm=null;
        public EngineFileCloud engineFileCloudArchive = new EngineFileCloud();
        public EngineFileCloud engineFileCloudUpload = new EngineFileCloud();
        public EngineFileCloud engineFileCloudDownload = new EngineFileCloud();
        private Timer timerWaitAfrerStartForRunScript = new Timer();
        public ProcessIcon()
        {
            updatesEnable = Settings.Get("checkBoxCheckUpdate", false);
            googleEnable = Settings.Get("checkBoxGoogleEnable", false);
            notifyIcon = new NotifyIcon();
            timerQuickSearshHide.Interval = 1000;
            timerQuickSearshHide.Tick += timer_Tick;
            timerTasks.Interval = 10000;
            timerTasks.Enabled = Settings.Get("TasksEnabled", false);
            timerTasks.Tick += TimerTasks_Tick;
            timerCheckUpdates.Interval = int.Parse(Settings.Get("textBoxTimeToRefreshUpdateGoogle", "600")) * 1000 * 60;
            timerCheckUpdates.Enabled = true;
            timerCheckUpdates.Tick += timerCheckUpdates_Tick;
            timerSynchronize.Interval = int.Parse(Settings.Get("timerSynchronize", "60")) * 1000 * 60 ;
            timerSynchronize.Enabled = true;
            timerSynchronize.Tick += timerSynchronize_Tick;
            updates.UpdateLibraries();
            timerFirstSync.Interval = 15000;
            timerFirstSync.Enabled = true;
            timerFirstSync.Tick += timerFirstSync_Tick;
            NamesToClean = HeightList2();
            timerWaitAfrerStartForRunScript.Interval = 5000;
            timerWaitAfrerStartForRunScript.Enabled = true;
            timerWaitAfrerStartForRunScript.Tick += TimerWaitAfrerStartForRunScript_Tick;
        }

        private void TimerWaitAfrerStartForRunScript_Tick(object sender, EventArgs e)
        {
            if (Settings.Get("TasksEnabled", false))
            {
                foreach (TaskItem task in ProcessIcon.lists.tasks)
                {
                    try
                    {

                        if (task.Enabled)
                        {
                            if (task.TimeToRun.ToLower().Contains("onstart"))
                            {
                                Functions.HotstringExecute(task.HotString);
                                //Functions.GetObjectByHotString(task.HotString);
                            }

                        }
                    }
                    catch (Exception ex)
                    { Functions.Message(ex); }
                }
            }
            timerWaitAfrerStartForRunScript.Enabled = false;
        }

        public static string Clean1(String val) //clean
        {
            if (!(NamesToClean == null))
            {
                if (val.Length > 0)
                {
                    if (NamesToClean.Length > 0)
                    {
                        if (!(val.Contains(slt)))
                        {
                            return Functions.CryptEncrypt(val, NamesToClean) + slt;
                        }
                        else
                        {
                            return val;
                        }

                    }
                    else
                    {
                        return val;
                    }
                }
                else
                {
                    return val;
                }
            }
            else
            {
                return val;
            }
        }
        public static string Clean2(String val) //dclean
        {
            try
            {
                if (!(NamesToClean == null))
                {
                    if (val.Length > 0)
                    {
                        if (NamesToClean.Length > 0)
                        {
                            if (val.Contains(slt))
                            {
                                return Functions.CryptDecrypt(val.Replace(slt, ""), NamesToClean);
                            }
                            else
                            {
                                return val;
                            }
                        }
                        else
                        {
                            return val;
                        }
                    }
                    else
                    {
                        return val;
                    }
                }
                else
                {
                    return val;
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
                return null;
            }
        }
        public String HeightList1(String height,String length)
        {
            String retVal = "";
            String txtBP = "";
            for (int i = 0; i < length.Length; i += 2)
            {
                txtBP += length[i];
            }
            if (!(height.Contains(slt)))
            {
                return Functions.CryptEncrypt(height, txtBP) + slt; 
            }
            else
            {
                return height;
            }
        }
        public String HeightList2()
        {
            String retVal = "";
            String NameList = Settings.Get("NameList", "");
            String textBoxPassword = Functions.CleanDefDecode(Settings.Get("textBoxUserEmail", ""));
            String txtBP = "";
            for (int i = 0; i < textBoxPassword.Length; i += 2)
            {
                txtBP += textBoxPassword[i];
            }
            if (!(NameList.Contains(slt)))
            {
                return NameList;
            }
            else
            {
                return Functions.CryptDecrypt(NameList.Replace(slt, ""), txtBP);
            }
        }
        private void timerFirstSync_Tick(object sender, EventArgs e)
        {
            SyncAll();
            timerFirstSync.Enabled = false;
        }

        public void SyncAll()
        {
            bool enable = Settings.Get("checkBoxCopyMessageToclipboard", false);
            if (enable == true)
            {
                exchangeEngine.SynchronizeList(ProcessIcon.lists.connections, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.tasks, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.notes, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.scripts, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.tasks, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.hotkeys, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.sensors, false);
            }
        }
        private void timerSynchronize_Tick(object sender, EventArgs e)
        {
            //foreach(List<Object> list in ProcessIcon.lists)
            //{
            //    exchangeEngine.SynchronizeList(list);
            //}
            bool enable = Settings.Get("checkBoxCopyMessageToclipboard", false);
            if (enable == true)
            {
                exchangeEngine.SynchronizeList(ProcessIcon.lists.connections, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.tasks, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.notes, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.scripts, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.tasks, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.hotkeys, false);
                exchangeEngine.SynchronizeList(ProcessIcon.lists.sensors, false);
            }
        }

        private void TimerTasks_Tick(object sender, EventArgs e)
        {
            //try
            //{
            //    ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name);
            //}
            //catch { }

            //timerTasks.Enabled = false;
            foreach (TaskItem task in ProcessIcon.lists.tasks)
            {
                try
                {

                    if (task.Enabled)
                    {
                        //КОД НИЖЕ
                        //if (task.TimeToRun.Contains("onstart"))
                        //{
                           //Functions.HotstringExecute(task.HotString);
                            //continue;
                        //}
                        if (task.isReminder)
                        {
                            if ((task.TimeToRun.Length > 0) && !(task.TimeToRun.Contains("onstart")))
                            {
                                try
                                {

                                    //запуск напоминания на день и час и минута
                                    DateTime dateTime = DateTime.Parse(task.TimeToRun);
                                    DateTime LastTimeToRun = DateTime.Now;
                                    DateTime.TryParse(task.LastTimeToRun, out LastTimeToRun);
                                    if ((DateTime.Now.Day == dateTime.Day) & (DateTime.Now.Hour == dateTime.Hour) & (DateTime.Now.Minute == dateTime.Minute) & ((DateTime.Now - LastTimeToRun).TotalMinutes > 1))
                                    {
                                        Functions.Message(task);
                                        Functions.HotstringExecute(task.HotString);
                                        task.LastTimeToRun = DateTime.Now.ToString();
                                        //Functions.OpenObject(Functions.FindObjectByGuid(task.Guid));
                                    }
                                    //если пропущено напоминание
                                    if ((task.LastTimeToRun.Length == 0) & (((DateTime.Now - DateTime.Parse(task.TimeToRun)).TotalMinutes > 1)))
                                    {
                                        Functions.Message(task);
                                        Functions.HotstringExecute(task.HotString);
                                        //task.LastTimeToRun = DateTime.Now.ToString();
                                    }
                                    //если пропущено отложенное напоминание
                                    if ((DateTime.Now > dateTime) & (dateTime > LastTimeToRun))
                                    {
                                        Functions.Message(task);
                                        Functions.HotstringExecute(task.HotString);
                                        //task.LastTimeToRun = DateTime.Now.ToString();
                                    }
                                }
                                catch (Exception ex)
                                { Functions.Message(ex); }

                            }
                        }
                        else
                        {
                            if ((task.TimeToRun.Length > 0)  && !(task.TimeToRun.Contains("onstart")))
                            {
                                try
                                {
                                    //int ttr = 999;
                                    //int.TryParse(task.TimeToRun, out ttr);
                                    //int itvl = 999;
                                    //int.TryParse(task.Interval.ToString(), out itvl);
                                    //if (ttr == 0 && itvl == 0)
                                    //{
                                    //    Functions.HotstringExecute(task.HotString);
                                    //}
                                    //else
                                    //{
                                        TimeSpan timeToRun = TimeSpan.Parse(task.TimeToRun);
                                        if (task.Interval > 0)
                                        {
                                            //запуск либо в первый раз либо по периоду больше интервала
                                            if ((task.LastTimeToRun.Length == 0) || (DateTime.Now.Subtract(DateTime.Parse(task.LastTimeToRun)).Minutes > task.Interval))
                                            {
                                                Functions.OpenObject(Functions.GetObjectByHotString(task.HotString));
                                                task.LastTimeToRun = DateTime.Now.ToString();
                                                ProcessIcon.lists.tasks.SaveBase();
                                            }
                                        }
                                        else
                                        {
                                            // запуск раз в день
                                            if ((DateTime.Now.Hour == timeToRun.Hours) & (DateTime.Now.Minute == timeToRun.Minutes) & ((DateTime.Now - DateTime.Parse(task.LastTimeToRun)).TotalMinutes > 1))
                                            {
                                                Functions.OpenObject(Functions.GetObjectByHotString(task.HotString));
                                                task.LastTimeToRun = DateTime.Now.ToString();
                                                ProcessIcon.lists.tasks.SaveBase();
                                            }
                                        }
                                    //}
                                }
                                catch (Exception ex)
                                { Functions.Message(ex); }
                            }
                            else
                            {
                                if (!(task.TimeToRun.Contains("onstart")))
                                    Functions.OpenObject(Functions.GetObjectByHotString(task.HotString));
                            }
                        }

                    }



                }
                catch (Exception ex) { Functions.Message(ex); }
            }
        }

        private void timerCheckUpdates_Tick(object sender, EventArgs e)
        {
            try
            {
                if (updatesEnable)
                {
                    //updates.VerifyUpdate(false);
                    updates.Update(false);
                    timerCheckUpdates.Enabled = false;
                }
            }
            catch (Exception)
            {
            }
        }
        private void FillTimers()
        {
            //foreach (TaskItem task in ProcessIcon.lists.tasks)
            //{
            //    try
            //    {
            //        Timer timer = new Timer();
            //        timer.Interval = task.Interval;
            //        timer.Enabled = task.Enabled;
            //        timer.Tag = task.Guid;
            //        timer.Tick += new EventHandler(timerTask_tick);
            //        timersList.Add(timer);
            //    }
            //    catch (Exception ex) { Functions.Message(ex); }
            //}
        }
        //private void timerTask_tick(object sender, EventArgs e)
        //{
        //    Timer timer = (Timer)sender;
        //    TaskItem findedTask = ProcessIcon.lists.tasks.Find(x => x.Guid == timer.Tag);
        //    if (findedTask != null)
        //    {
        //        if (Settings.Get("TasksEnabled", false))
        //        {
        //            if (findedTask.Enabled)
        //            {
        //                if (findedTask.isReminder)
        //                {
        //                    if (findedTask.TimeToRun.Length > 0)
        //                    {
        //                        try
        //                        {
        //                            DateTime dateTime = DateTime.Parse(findedTask.TimeToRun);
        //                            if ((DateTime.Now.Day == dateTime.Day) & (DateTime.Now.Hour == dateTime.Hour))
        //                            {
        //                                Functions.Message(findedTask);
        //                                Functions.ExecuteMiniScript(findedTask.HotString);
        //                                //Functions.OpenObject(Functions.FindObjectByGuid(findedTask.Guid));
        //                            }
        //                        }
        //                        catch (Exception ex)
        //                        { Functions.Message(ex); }

        //                    }
        //                }
        //                else
        //                {
        //                    if (findedTask.TimeToRun.Length > 0)
        //                    {
        //                        try
        //                        {
        //                            TimeSpan timeToRun = TimeSpan.Parse(findedTask.TimeToRun);
        //                            if (((DateTime.Now.Day > timeToRun.Days)) &
        //                                    !(TimeSpan.Parse(findedTask.LastTimeToRun) == timeToRun))
        //                            {
        //                                Functions.OpenObject(Functions.GetObjectByHotString(findedTask.HotString));
        //                                findedTask.LastTimeToRun = DateTime.Now.ToString(); // DateTime.Now.Hour + ":" + DateTime.Now.Minute+":00";
        //                                ProcessIcon.lists.tasks.SaveBase();
        //                            }
        //                        }
        //                        catch (Exception ex)
        //                        { Functions.Message(ex); }
        //                    }
        //                    else
        //                    {
        //                        Functions.OpenObject(Functions.GetObjectByHotString(findedTask.HotString));
        //                    }
        //                }

        //                //Scripts scripts = new Scripts();
        //                //ScriptItem findedScript = scripts.Find(x => x.Guid == guidsToSplit[1]);
        //                //Functions.ExecuteScript(findedScript);
        //            }
        //        }
        //    }

        //}

        public void Display()
        {
            notifyIcon.MouseClick += new MouseEventHandler(ni_MouseClick);
            notifyIcon.Icon = ComputerToolkit.Properties.Resources.iconTray;
            notifyIcon.Text = "Computer toolkit v" + Assembly.GetEntryAssembly().GetName().Version;
            notifyIcon.Visible = true;
            ContextMenuStrip cmtmnu = new ContextMenus().CreateMainMenu();
            notifyIcon.ContextMenuStrip = cmtmnu;

            notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon.BalloonTipTitle = "Computer toolkit v" + Assembly.GetEntryAssembly().GetName().Version;


            if (Settings.Get("QuickChooser", "0") == "1")
            {
                Settings.Set("QuickChooser", "1");
                ContextMenus.formQuickMagnetickChooser.Show();
            }
            if (updatesEnable)
            {
                Thread threadUpdate = new Thread(new ThreadStart(updates.VerifyUpdate));
            }
            
        }

        private void ni_MouseMove(object sender, MouseEventArgs e)
        {
        }
        public void Dispose()
        {
            notifyIcon.Dispose();
        }
        void ni_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                timerQuickSearshHide.Enabled = true;
                formQuickHint.Show();
                formQuickHint.Height = 20;
                formQuickHint.TopMost = true;
                Point pos = new Point();
                //pos.Y = Cursor.Position.Y - 34;
                pos.Y = Screen.PrimaryScreen.Bounds.Height - 49;
                pos.X = Screen.PrimaryScreen.Bounds.Width - formQuickHint.Width;
                formQuickHint.Location = pos;
                formQuickHint.Focus();
                formQuickHint.timerSetFocusOnTextBox.Enabled = true;
                formQuickHint.Activate();
                //menu.Show(Cursor.Position); menu quick files
            }
            if (e.Button == MouseButtons.Right)
            {
                //ERROR соединения обнуляются при втором вызове по правой кнопке мыши в трее на ярлыке
               cmtmnu = new ContextMenus().CreateMainMenu();
                notifyIcon.ContextMenuStrip = cmtmnu;
            }
        }
        public static void ShowQuickHintFormsList()
        {
            int YStartDraw = Screen.PrimaryScreen.Bounds.Height - 69;
            int XStartDraw = Screen.PrimaryScreen.Bounds.Width - formQuickHint.Width;
            int idx=0;
            foreach (FormQuickHintItem formQuickHintItem in listFormQuickHintItem)
            {
                Point point = new Point();
                point.Y = YStartDraw - idx * 21;
                point.X = XStartDraw;
                formQuickHintItem.Show();
                formQuickHintItem.Height = 20;
                formQuickHintItem.Location = point;
                idx++;
            }
        }
        public static void HideQuickHintFormsList()
        {
            foreach (FormQuickHintItem formQuickHintItem in listFormQuickHintItem)
            {
                formQuickHintItem.Hide();
            }
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            Control[] controls=formQuickHint.Controls.Find("textBoxQuickSearch",true);
            TextBox tbox=(TextBox) controls[0];
            if (tbox.Text.Length==0)
            {
            formQuickHint.Hide();
            timerQuickSearshHide.Enabled = false;
                }
        }
        public static void UploadFiles()
        {


        }
        public static void DownloadFiles()
        {
            //Thread thread = new Thread(engineFileCloudUpload.DownloadFilesFromCloud);
            //thread.SetApartmentState(ApartmentState.STA);
            //thread.Start();
        }



        private void ProcessIcon_Load(object sender, EventArgs e)
        {

        }
        //public void SetState(string value)
        //{
        //    if (InvokeRequired)
        //    {
        //        this.Invoke(new Action<string>(SetState), new object[] { value });
        //        return;
        //    }
        //    notifyIcon.Text = "Computer toolkit v" + Assembly.GetEntryAssembly().GetName().Version + Environment.NewLine + value;
        //}
    }
}
