﻿using ComputerToolkit.Forms;
using ComputerToolkit.Properties;
using ComputerToolkit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ComputerToolkit
{
    class ContextMenus
    {
        public Process proc;
        private string appPath;
        private ProcessesClass processesClass = new ProcessesClass();
        bool isAboutLoaded = false;
        public static FormQuickMagnetickChooser formQuickMagnetickChooser = new FormQuickMagnetickChooser();
        public ContextMenus()
        {
            appPath = Application.StartupPath.ToString();
       }
        #region MainMenu
        public ContextMenuStrip CreateMainMenu()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            try
            {
                ToolStripMenuItem item;
                ToolStripMenuItem itemGroup;
                ToolStripSeparator sep;
                ProcessIcon.lists.files.listGroups.Sort();
                foreach (String fGr in ProcessIcon.lists.files.listGroups)
                {
                    item = new ToolStripMenuItem();
                    item.Text = fGr;
                    ProcessIcon.lists.files.Sort((a, b) => a.FileName.CompareTo(b.FileName));
                    foreach (FileItem fi in ProcessIcon.lists.files)
                    {
                        if (fGr == fi.Group)
                        {
                            ToolStripMenuItem mnuSubItem = new ToolStripMenuItem();
                            mnuSubItem.Text = GetNameMenu(fi);
                            mnuSubItem.Name = fi.Name+" "+fi.Comments+" "+fi.FileDescription;
                            if (fi.IconFile == null)
                            {
                                mnuSubItem.Image = Resources.Start.ToBitmap();
                            }
                            else
                            {
                                mnuSubItem.Image = fi.IconFile.ToBitmap();//yoyo                    
                            }
                            mnuSubItem.Tag = fi.Guid;
                            mnuSubItem.MouseDown += mnuSubItem_MouseDown;
                            item.DropDownItems.Add(mnuSubItem);
                        }
                    }
                    item.Tag = "menuDropdown";
                    menu.Items.Add(item);
                }
                sep = new ToolStripSeparator();
                menu.Items.Add(sep);

                ToolStripMenuItem itemConnections = new ToolStripMenuItem();
                itemConnections.Image = Resources.connections17;
                itemConnections.Text = "Соединения";
                bool checkBoxShowGroupsInConnections = Settings.Get("checkBoxShowGroupsInConnections", false);
                try
                {
                    ProcessIcon.lists.connections.Sort((a, b) => a.Group.CompareTo(b.Group));
                }
                catch (Exception ex) { Functions.Message(ex); }
                try
                {
                    ProcessIcon.lists.connections.Sort((a, b) => a.Name.CompareTo(b.Name));
                }
                catch (Exception ex) { Functions.Message(ex); }
                if (Settings.Get("checkBoxMakeSubmenuInConnections", false))
                {
                    foreach (String connGr in ProcessIcon.lists.connections.listGroups)
                    {
                        itemGroup = new ToolStripMenuItem();
                        itemGroup.Text = connGr;
                        foreach (ConnectionItem fi in ProcessIcon.lists.connections)
                        {
                            if (fi.Enabled)
                            if (connGr == fi.Group)
                            {
                                try
                                {
                                    ToolStripMenuItem mnuSubItem = new ToolStripMenuItem();
                                    if (checkBoxShowGroupsInConnections)
                                    {
                                        mnuSubItem.Text = fi.Group + "/" + fi.Name;
                                    }
                                    else
                                    {
                                        mnuSubItem.Text = fi.Name;
                                    }
                                    mnuSubItem.Name = fi.Name;
                                    if (ProcessIcon.formConnection.FindTab(fi.Name))
                                    {
                                        mnuSubItem.Image = Resources.Start.ToBitmap();
                                    }
                                    else
                                    {
                                        if (fi.IconConnection == null)
                                        {
                                            mnuSubItem.Image = Resources.Start.ToBitmap();
                                        }
                                        else
                                        {
                                                switch(fi.Protocol)//yoyo                    
                                                {
                                                    case ConnectionProtocol.AMMYY:
                                                        mnuSubItem.Image = Resources.ammy.ToBitmap();
                                                        break;
                                                    case ConnectionProtocol.RDP:
                                                        mnuSubItem.Image = Resources.rdp.ToBitmap();
                                                        break;
                                                    case ConnectionProtocol.TEAMVIEWER:
                                                        mnuSubItem.Image = Resources.teamviewer.ToBitmap();
                                                        break;
                                                    case ConnectionProtocol.VNC:
                                                        mnuSubItem.Image = Resources.vnc.ToBitmap();
                                                        break;
                                                }
                                        }
                                    }
                                    mnuSubItem.Tag = fi.Guid;
                                    mnuSubItem.MouseDown += MenuConnection_MouseDown;
                                    itemGroup.DropDownItems.Add(mnuSubItem);
                                }
                                catch (Exception ex) { }
                            }
                        }
                        itemConnections.DropDownItems.Add(itemGroup);
                    }

                }
                else
                {
                    foreach (ConnectionItem fi in ProcessIcon.lists.connections)
                    {
                        ToolStripMenuItem mnuSubItem = new ToolStripMenuItem();
                        if (checkBoxShowGroupsInConnections)
                        {
                            mnuSubItem.Text = fi.Group + "/" + fi.Name;
                        }
                        else
                        {
                            mnuSubItem.Text = fi.Name;
                        }
                        mnuSubItem.Name = fi.Name;
                        if (ProcessIcon.formConnection.FindTab(fi.Name))
                        {
                            mnuSubItem.Image = Resources.Start.ToBitmap();
                        }
                        else
                        {
                            if (fi.IconConnection == null)
                            {
                                mnuSubItem.Image = Resources.Start.ToBitmap();
                            }
                            else
                            {
                                mnuSubItem.Image = fi.IconConnection.ToBitmap();//yoyo                    
                            }
                        }
                        mnuSubItem.Tag = fi.Guid;
                        //mnuSubItem.ToolTipText = fi.ToString();
                        itemConnections.DropDownItems.Add(mnuSubItem);
                        mnuSubItem.Click += new System.EventHandler(MenuConnection_Click);
                        mnuSubItem.MouseDown += MenuConnection_MouseDown;


                    }
                }
                sep = new ToolStripSeparator();
                itemConnections.DropDownItems.Add(sep);
                ToolStripMenuItem mnuSubItemMaximizeConnections = new ToolStripMenuItem();
                mnuSubItemMaximizeConnections.Text = "Показать соединения";
                mnuSubItemMaximizeConnections.Name = "Показать соединения";
                mnuSubItemMaximizeConnections.Tag = "Показать соединения";
                mnuSubItemMaximizeConnections.Image = Resources.Window_FullScreen.ToBitmap();
                mnuSubItemMaximizeConnections.Click += new System.EventHandler(MenuShowConn_Click);
                itemConnections.DropDownItems.Add(mnuSubItemMaximizeConnections);

                menu.Items.Add(itemConnections);



                sep = new ToolStripSeparator();
                menu.Items.Add(sep);



                ToolStripMenuItem mnuItemScripts = new ToolStripMenuItem();
                mnuItemScripts.Text = "Скрипты";
                mnuItemScripts.Name = "Скрипты";
                mnuItemScripts.Tag = "Скрипты";
                mnuItemScripts.Image = Resources.Window_Cascade;
                //mnuItemScripts.Click += new System.EventHandler(MenuScripts_Click);


                ProcessIcon.lists.scripts.Sort((a, b) => a.Name.CompareTo(b.Name));
                foreach (ScriptItem script in ProcessIcon.lists.scripts)
                {
                    if (script.UseInMenu)
                    {
                        ToolStripMenuItem mnuSubItem = new ToolStripMenuItem();
                        mnuSubItem.Text = script.Name;
                        mnuSubItem.Name = script.Name;
                        mnuSubItem.Image = Resources.Window;
                        mnuSubItem.Tag = script.Guid;
                        //mnuSubItem.ToolTipText = script.ToString();
                        mnuSubItem.Click += new System.EventHandler(MenuScripts_Click);
                        mnuItemScripts.DropDownItems.Add(mnuSubItem);
                    }
                }

                menu.Items.Add(mnuItemScripts);

                sep = new ToolStripSeparator();
                menu.Items.Add(sep);

                ToolStripMenuItem itemPreferences = new ToolStripMenuItem();
                itemPreferences.Text = "Прочее";
                itemPreferences.Image = Resources.more17;


                ToolStripMenuItem mnuSubItemRefreshMenu = new ToolStripMenuItem();
                mnuSubItemRefreshMenu.Text = "Обновить меню";
                mnuSubItemRefreshMenu.Name = "Обновить меню";
                mnuSubItemRefreshMenu.Tag = "Обновить меню";
                mnuSubItemRefreshMenu.Image = Resources.refresh;
                mnuSubItemRefreshMenu.Click += new System.EventHandler(MenumnuSubItemRefreshMenu_Click);
                itemPreferences.DropDownItems.Add(mnuSubItemRefreshMenu);

                ToolStripMenuItem mnuSubItemSysMonitor = new ToolStripMenuItem();
                mnuSubItemSysMonitor.Text = "Системный монитор";
                mnuSubItemSysMonitor.Name = "Системный монитор";
                mnuSubItemSysMonitor.Tag = "Системный монитор";
                mnuSubItemSysMonitor.Image = Resources.sysmon.ToBitmap();
                mnuSubItemSysMonitor.Click += new System.EventHandler(MenuSysMonitor_Click);
                itemPreferences.DropDownItems.Add(mnuSubItemSysMonitor);

                ToolStripMenuItem mnuSubItemSensorsMonitor = new ToolStripMenuItem();
                mnuSubItemSensorsMonitor.Text = "Сенсорный монитор";
                mnuSubItemSensorsMonitor.Name = "Сенсорный монитор";
                mnuSubItemSensorsMonitor.Tag = "Сенсорный монитор";
                mnuSubItemSensorsMonitor.Image = Resources.sensorsmon.ToBitmap();
                mnuSubItemSensorsMonitor.Click += new System.EventHandler(MenuSensorsMonitor_Click);
                itemPreferences.DropDownItems.Add(mnuSubItemSensorsMonitor);
                ToolStripMenuItem mnuSubItemQuickChooser = new ToolStripMenuItem();
                mnuSubItemQuickChooser.Text = "Быстрый выбор";
                mnuSubItemQuickChooser.Name = "Быстрый выбор";
                mnuSubItemQuickChooser.Tag = "Быстрый выбор";
                mnuSubItemQuickChooser.Image = Resources.QuickMagnetic.ToBitmap();
                mnuSubItemQuickChooser.Click += new System.EventHandler(MenuQuickChooser_Click);
                itemPreferences.DropDownItems.Add(mnuSubItemQuickChooser);

                ToolStripMenuItem mnuSubItemPlayer = new ToolStripMenuItem();
                mnuSubItemPlayer.Text = "Аудио плеер";
                mnuSubItemPlayer.Name = "Аудио плеер";
                mnuSubItemPlayer.Tag = "Аудио плеер";
                mnuSubItemPlayer.Image = Resources.QuickMagnetic.ToBitmap();
                mnuSubItemPlayer.Click += new System.EventHandler(MenuPlayer_Click);
                itemPreferences.DropDownItems.Add(mnuSubItemPlayer);

                ToolStripMenuItem mnuCharts = new ToolStripMenuItem();
                mnuCharts.Text = "Графики";
                mnuCharts.Name = "Графики";
                mnuCharts.Tag = "Графики";
                mnuCharts.Image = Resources.Folder_Network;
                mnuCharts.Click += new System.EventHandler(MenuCharts_Click);
                itemPreferences.DropDownItems.Add(mnuCharts);



                menu.Items.Add(itemPreferences);



                ToolStripMenuItem mnuItemPreferences = new ToolStripMenuItem();
                mnuItemPreferences.Text = "Настройки";
                mnuItemPreferences.Name = "Настройки";
                mnuItemPreferences.Tag = "Настройки";
                mnuItemPreferences.Image = Resources.settings17;
                mnuItemPreferences.Click += new System.EventHandler(MenuPreferences_Click);
                menu.Items.Add(mnuItemPreferences);

                ToolStripMenuItem itemTaskRemindwer = new ToolStripMenuItem();
                itemTaskRemindwer.Text = "Напоминание";
                itemTaskRemindwer.Name = "Напоминание";
                itemTaskRemindwer.Tag = "Напоминание";
                itemTaskRemindwer.Image = Resources.clock1;
                itemTaskRemindwer.Click += new System.EventHandler(MenuitemTaskRemindwer_Click);
                menu.Items.Add(itemTaskRemindwer);


                ToolStripMenuItem mnuSubItemRestart = new ToolStripMenuItem();
                mnuSubItemRestart.Text = "Перезапустить";
                mnuSubItemRestart.Name = "Перезапустить";
                mnuSubItemRestart.Tag = "Перезапустить";
                mnuSubItemRestart.Image = Resources.reload;
                mnuSubItemRestart.Click += new System.EventHandler(MenuRestart_Click);
                menu.Items.Add(mnuSubItemRestart);


                ToolStripMenuItem mnuSubItemExit = new ToolStripMenuItem();
                mnuSubItemExit.Text = "Закрыть";
                mnuSubItemExit.Name = "Закрыть";
                mnuSubItemExit.Tag = "Закрыть";
                mnuSubItemExit.Image = Resources.close_icon;
                mnuSubItemExit.Click += new System.EventHandler(MenuExit_Click);
                menu.Items.Add(mnuSubItemExit);

                return menu;

            }catch  (Exception ex)
            {
                Functions.Message(ex);
                return menu;
            }
        }

        private void MenuRestart_Click(object sender, EventArgs e)
        {
            Functions.Restart();
        }

        private void MenuitemTaskRemindwer_Click(object sender, EventArgs e)
        {
            FormTaskReminder formTaskReminder = new FormTaskReminder();
            formTaskReminder.Show();
        }

        private void MenumnuSubItemRefreshMenu_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.connections.ReadBase();
            ProcessIcon.lists.files.ReadBase();
            ProcessIcon.lists.filesQuick.ReadBase();
            ProcessIcon.lists.hotkeys.ReadBase();
            ProcessIcon.lists.notes.ReadBase();
            ProcessIcon.lists.scripts.ReadBase();
            ProcessIcon.lists.sensors.ReadBase();
            ProcessIcon.lists.tasks.ReadBase();
        }

        private void MenuCharts_Click(object sender, EventArgs e)
        {
            EngineCharts engineCharts = new EngineCharts();
            engineCharts.ShowLineChart();
        }

        private void MenuConnection_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ToolStripDropDownItem menuItem = (ToolStripDropDownItem)sender;
                bool checkBoxShowGroupsInConnections = Settings.Get("checkBoxShowGroupsInConnections", false);
                ConnectionItem findedItem;
                if (checkBoxShowGroupsInConnections)
                {
                    findedItem = ProcessIcon.lists.connections.Find(x => x.Guid == menuItem.Tag.ToString());
                }
                else
                {
                    findedItem = ProcessIcon.lists.connections.Find(x => x.Guid == menuItem.Tag.ToString());
                }
                switch (findedItem.Protocol)
                {
                    case ConnectionProtocol.RDP:
                        if ((findedItem.HotString == null) || (findedItem.HotString.Length == 0)) // не выбираются изза то нуль то пустая строка
                        {
                            if ((findedItem.ExternalSettings == null) || (findedItem.ExternalSettings.Length == 0)) // не выбираются изза то нуль то пустая строка
                            {
                                if (findedItem.Guid.Length > 0)
                                {
                                    ProcessIcon.formConnection.Show();
                                    ProcessIcon.formConnection.AddTab(findedItem);
                                }
                            }
                            else
                            {
                                string nameFileRdp = System.IO.Path.GetTempPath() + findedItem.Name.Replace("/", "").Replace("\\", "") + ".rdp";
                                using (StreamWriter sw = new StreamWriter(nameFileRdp))
                                {
                                    String exsettings = findedItem.ExternalSettings.Replace("#ADRESS#", ProcessIcon.Clean2(findedItem.Adress)).Replace("#USERNAME#", findedItem.User);
                                    sw.Write(ProcessIcon.Clean2(exsettings));
                                }
                                ShellMenuConnection(nameFileRdp);
                                ProcessStartInfo psi = new ProcessStartInfo(nameFileRdp);
                                //psi.UseShellExecute = true;
                                //Process.Start(psi);
                                //File.Delete(nameFileRdp);
                            }
                        }
                        else
                        {
                            Functions.GetObjectByHotString(findedItem.HotString);
                        }
                        break;
                    case ConnectionProtocol.TEAMVIEWER:
                        Functions.OpenTeamViewerConnection(findedItem);
                        break;
                    case ConnectionProtocol.AMMYY:
                        Functions.OpenAMMYConnection(findedItem);
                        break;
                    case ConnectionProtocol.VNC:
                        object obj = null;
                        if (findedItem.HotString.Length == 0)
                        {
                        }
                        else
                        {
                            obj = Functions.GetObjectByHotString(findedItem.HotString);
                        }
                        //if (!(findedItem.ExternalSettings == null))
                        if (findedItem.ExternalSettings.Length == 0)
                        {
                            if (findedItem.Guid.Length > 0)
                            {
                                if (!(obj == null))
                                {
                                    //formConnection.Show();
                                    //formConnection.AddTab(findedItem.Adress, findedItem.Domain, findedItem.User, "", findedItem.Protocol,
                                    //    findedItem.Name, findedItem.Port);
                                    FileItem vncexe = (FileItem)obj;
                                    if (obj.GetType() == typeof(FileItem))
                                    {
                                        Functions.ShellMenuProgramm(vncexe.Guid, findedItem.CommandLine);
                                    }

                                }
                            }
                        }
                        else
                        {
                            string nameFileVnc = System.IO.Path.GetTempPath() + findedItem.Name + ".vnc";
                            using (StreamWriter sw = new StreamWriter(nameFileVnc))
                            {
                                sw.Write(findedItem.ExternalSettings);
                            }
                            if (!(obj == null))
                            {
                                FileItem vncexe = (FileItem)obj;
                                if (obj.GetType() == typeof(FileItem))
                                {
                                    Functions.ShellMenuProgramm(vncexe.Guid, findedItem.CommandLine + " " + nameFileVnc);
                                }
                            }
                        }
                        break;
                    //case ConnectionProtocol.AMMY:
                    //    object obj2 = null;
                    //    if (findedItem.HotString.Length == 0)
                    //    {
                    //    }
                    //    else
                    //    {
                    //        obj2 = Functions.GetObjectByHotString(findedItem.HotString);
                    //    }
                    //    if (findedItem.ExternalSettings == null)//(findedItem.ExternalSettings.Length == 0)
                    //    {
                    //        if (findedItem.Guid.Length > 0)
                    //        {
                    //            if (!(obj2 == null))
                    //            {
                    //                //formConnection.Show();
                    //                //formConnection.AddTab(findedItem.Adress, findedItem.Domain, findedItem.User, "", findedItem.Protocol,
                    //                //    findedItem.Name, findedItem.Port);
                    //                FileItem vncexe = (FileItem)obj2;
                    //                if (obj2.GetType() == typeof(FileItem))
                    //                {
                    //                    Functions.ShellMenuProgramm(vncexe.Guid, findedItem.CommandLine);
                    //                }

                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        string nameFileVnc = System.IO.Path.GetTempPath() + findedItem.Name + ".ammy";
                    //        using (StreamWriter sw = new StreamWriter(nameFileVnc))
                    //        {
                    //            sw.Write(findedItem.ExternalSettings);
                    //        }
                    //        if (!(obj2 == null))
                    //        {
                    //            FileItem vncexe = (FileItem)obj2;
                    //            if (obj2.GetType() == typeof(FileItem))
                    //            {
                    //                Functions.ShellMenuProgramm(vncexe.Guid, findedItem.CommandLine + " " + nameFileVnc);
                    //            }
                    //        }
                    //    }
                    //    break;
                }


            }
            if (e.Button == MouseButtons.Right)
            {
                ToolStripMenuItem mnu = (ToolStripMenuItem)sender;
                Functions.Message(ProcessIcon.lists.connections.Find(fi => fi.Guid == mnu.Tag).ToString());
            }
        }

        void mnuSubItem_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ToolStripMenuItem mnu = (ToolStripMenuItem)sender;
                Functions.ShellMenuProgramm(mnu.Tag.ToString());
            }
            if (e.Button == MouseButtons.Right)
            {
                ToolStripMenuItem mnu = (ToolStripMenuItem)sender;
                Functions.Message(ProcessIcon.lists.files.Find(fi => fi.Guid == mnu.Tag).ToString());
            }
        }
        private void MenuScripts_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            try
            {
                ScriptItem findedScript = ProcessIcon.lists.scripts.Find(x => x.Guid == tsmi.Tag.ToString().Trim());
                if (findedScript == null)
                {
                    ProcessIcon.lists.scripts.ReadBase();
                    findedScript = ProcessIcon.lists.scripts.Find(x => x.Guid == tsmi.Tag.ToString().Trim());
                }
                EngineScript.ExecuteScript(findedScript);

            }
            catch (Exception ex)
            {
                Functions.Message(ex);

            }
        }



        private void MenuPlayer_Click(object sender, EventArgs e)
        {
        }

        private void MenuShowConn_Click(object sender, EventArgs e)
        {
            ProcessIcon.formConnection.WindowState = FormWindowState.Normal;
        }

        private void MenuQuickChooser_Click(object sender, EventArgs e)
        {
            if (Settings.Get("QuickChooser", "0") == "1")
            {
                Settings.Set("QuickChooser", "0");
                formQuickMagnetickChooser.Hide();
            }
            else
            {
                Settings.Set("QuickChooser", "1");
                formQuickMagnetickChooser.Show();
            }
        }

        private void MenuSensorsMonitor_Click(object sender, EventArgs e)
        {
            FormSensMon sensmon = new FormSensMon();
            sensmon.Show();
            if (sensmon.Left > Screen.PrimaryScreen.WorkingArea.Width) sensmon.Left = 0;
            if (sensmon.Top > Screen.PrimaryScreen.WorkingArea.Height) sensmon.Top = 0;
            sensmon.Visible = true;
        }

        private void MenuSysMonitor_Click(object sender, EventArgs e)
        {
            FormSysMon sysmon = new FormSysMon();
            sysmon.Show();
        }

        private void MenuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        public ContextMenuStrip CreateMOnouseOverMenu()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripSeparator sep;
            sep = new ToolStripSeparator();
            ToolStripMenuItem mnuItemFileQuick;
            foreach (FileItemQuick fi in ProcessIcon.lists.filesQuick)
            {
                mnuItemFileQuick = new ToolStripMenuItem();
                mnuItemFileQuick.Text = fi.Name;
                mnuItemFileQuick.Name = fi.Name;
                try
                {
                    mnuItemFileQuick.Image = fi.IconFile.ToBitmap();//yoyo                    
                }
                catch
                {
                    mnuItemFileQuick.Image = Resources.file17;//oyoyo                    
                }
                mnuItemFileQuick.Tag = fi.Guid;
                mnuItemFileQuick.Click += new System.EventHandler(MenuFileQuick_Click);
                menu.Items.Add(mnuItemFileQuick);
            }
            menu.Items.Add(sep);
            sep = new ToolStripSeparator();           
            ToolStripMenuItem itemQuickNote = new ToolStripMenuItem();
            itemQuickNote.Text = "Быстрая запись";
            itemQuickNote.Name = "Быстрая запись";
            itemQuickNote.Tag = "Быстрая запись";
            itemQuickNote.Image = Resources.note1.ToBitmap();
            itemQuickNote.Click += new System.EventHandler(MenuQuickNote_Click);
            menu.Items.Add(itemQuickNote);
            menu.Items.Add(sep);
            sep = new ToolStripSeparator();
            ToolStripMenuItem itemQuickHint = new ToolStripMenuItem();
            itemQuickHint.Text = "Быстрая подсказка";
            itemQuickHint.Name = "Быстрая подсказка";
            itemQuickHint.Tag = "Быстрая подсказка";
            itemQuickHint.Image = Resources.note1.ToBitmap();
            itemQuickHint.Click += new System.EventHandler(MenuQuickHint_Click);
            menu.Items.Add(itemQuickHint);
            menu.Items.Add(sep);
            ToolStripMenuItem itemEvernote = new ToolStripMenuItem();
            itemEvernote.Text = "Evernote запись";
            itemEvernote.Name = "Evernote запись";
            itemEvernote.Tag = "Evernote запись";
            itemEvernote.Image = Resources.evernote.ToBitmap();
            itemEvernote.Click += new System.EventHandler(MenuEvernote_Click);
            menu.Items.Add(itemEvernote);
            sep = new ToolStripSeparator();
            menu.Items.Add(itemEvernote);

            return menu;
        }


        private void MenuQuickHint_Click(object sender, EventArgs e)
        {
            FormQuickHint formQuickHint = new FormQuickHint();
            formQuickHint.Show();
        }

        private void MenuFileQuick_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mnu = (ToolStripMenuItem)sender;
            ShellMenuProgrammQuick(mnu.Tag.ToString());
        }

        private void MenuEvernote_Click(object sender, EventArgs e)
        {
            
        }

        private void MenuQuickNote_Click(object sender, EventArgs e)
        {

            //formNotepad.NewNote();
        }

        private String GetNameMenu(FileItem file)
        {
            return file.Name+" "+file.FileName;
        }
        void MenuConnection_Click(object sender, EventArgs e)
        {



        }
        void MenuProcesses_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mnu = (ToolStripMenuItem)sender;
            processesClass.ShowProcessByHandle(mnu.Name);
        }
        void MenuPreferences_Click(object sender, EventArgs e)
        {
            Program.processIcon.mainForm = new FormMain();
            Program.processIcon.mainForm.Show();
        }

        void MenuPrograms_Click(object sender, EventArgs e)        
        {
            ToolStripMenuItem mnu = (ToolStripMenuItem)sender;
            Functions.ShellMenuProgramm(mnu.Tag.ToString());
        }

        //private void ShellMenuProgramm(String guid)
        //{
        //    string path;
        //    string rootPath=Settings.GetString("textBoxRootDir");
        //    if (rootPath.Length > 0)
        //    {
        //        path = rootPath+"\\"+files.Find(x => x.Guid == guid).PathFile;
        //    } 
        //    else
        //    {
        //        path = Application.StartupPath.ToString()+"\\bin\\"+files.Find(x => x.Guid == guid).PathFile;
        //    }
        //    try
        //    {
        //        ProcessCt process = new ProcessCt(path);
        //        Thread th = new Thread(new ThreadStart(process.StartProcess));
        //        th.Start();
        //    }
        //    catch (Exception ex) { Functions.Message(ex.Message + " " + path); }
        //    //Process.Start(files.Find(delegate(FileItem file) { return file.Guid == guid; }).PathFile);
        //    //ProcessItem proc = new ProcessItem(Process.Start(files.Find(delegate(FileItem file) { return file.Guid == guid; }).PathFile));
        //    //processesClass.listProcesses.Add(proc);
        //    //processesClass.SaveProcesses();
        //}
        private void ShellMenuConnection(String path)
        {
            ProcessCt process = new ProcessCt(path);
            Thread th = new Thread(new ThreadStart(process.StartProcess));
            th.Start();
        }
        private void ShellMenuProgrammQuick(String guid)
        {
            string path = ProcessIcon.lists.filesQuick.Find(x => x.Guid == guid).PathFile;
            Process.Start(path);
            //ProcessItem proc = new ProcessItem(Process.Start(filesQuick.Find(delegate(FileItemQuick file) { return file.Guid == guid; }).PathFile));
            //processesClass.listProcesses.Add(proc);
            //processesClass.SaveProcesses();
        }
        void Exit_Click(object sender, EventArgs e)
        {
            // Quit without further ado.
            Application.Exit();
        }
        #endregion
        //#region ConnectionsMenu
        //public ContextMenuStrip CreateConnectionTabMenu()
        //{
        //    ContextMenuStrip menu = new ContextMenuStrip();
        //    ToolStripMenuItem itemConnMenu = new ToolStripMenuItem();
        //    //ToolStripMenuItem mnuSubItemConnMenu = new ToolStripMenuItem();
        //    //mnuSubItemConnMenu.Text = "Основные";
        //    //mnuSubItemConnMenu.Name = "Основные";
        //    //mnuSubItemConnMenu.Tag = "Основные";
        //    //mnuSubItemConnMenu.Click += new System.EventHandler(MenuConnMenu_Click);
        //    //itemConnMenu.DropDownItems.Add(mnuSubItemConnMenu);
        //    ToolStripSeparator sep;
        //    itemConnMenu.Text = "Закрыть";
        //    itemConnMenu.Image = Resources.del.ToBitmap();
        //    itemConnMenu.Click += new System.EventHandler(MenuConnMenuClose_Click);
        //    menu.Items.Add(itemConnMenu);
        //    return menu;
        //}
        //void MenuConnMenuClose_Click(object sender, EventArgs e)
        //{
        //    TabControl tabControl = (TabControl)sender;
        //    tabControl.TabPages.Remove(tabControl.SelectedTab);
        //}
        //#endregion
    }
}
