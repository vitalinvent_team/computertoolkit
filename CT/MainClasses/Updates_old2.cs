﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Net;
//using System.Reflection;
//using System.Text;
//using System.Text.RegularExpressions;
//using System.Windows.Forms;
////using Disk.SDK;
//namespace ComputerToolkit
//{
//    public class Updates
//    {
//        private string PROXY_DEF = "";
//        private int Major = Assembly.GetEntryAssembly().GetName().Version.Major;
//        private int Minor = Assembly.GetEntryAssembly().GetName().Version.Minor;
//        private int Build = Assembly.GetEntryAssembly().GetName().Version.Build;
//        private int Revision = Assembly.GetEntryAssembly().GetName().Version.Revision;
//        //YandexEngine yandexEngine = new YandexEngine();
//        private string VERSION_PATH = "";
//        private string appPath;
//        private string exeName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".exe";
//        public Updates()
//        {
//            appPath = Application.StartupPath.ToString();
//            VERSION_PATH = appPath + "\\" + VariablesClass.TEMP_FOLDER + "\\" + "version";
//        }
//        public void VerifyUpdateNoMessages()
//        {
//            VerifyUpdate(false);
//        }
//        public void VerifyUpdate(bool showInfo)
//        {
//            try
//            {
//                yandexEngine.Refresh();
//                //DiskItemInfo findedItem = yandexEngine.filesList.Find(x => x.OriginalDisplayName == "version");
//                string downloadedFile = VERSION_PATH;
//                if (findedItem != null)
//                {
//                    downloadedFile = yandexEngine.DownloadFile("version");
//                    string[] resArr = ReadVersion(downloadedFile);
//                    int MajorRecieve = int.Parse(resArr[0]);
//                    int MinorRecieve = int.Parse(resArr[1]);
//                    int BuildRecieve = int.Parse(resArr[2]);
//                    int RevisionRecieve = int.Parse(resArr[3]);
//                    bool enableUpdate = false;
//                    if (Major < MajorRecieve) enableUpdate = true;
//                    if (Minor < MinorRecieve) enableUpdate = true;
//                    if (Build < BuildRecieve) enableUpdate = true;
//                    if (Revision < RevisionRecieve) enableUpdate = true;
//                    if (enableUpdate)
//                    {
//                        DialogResult res = MessageBox.Show("Enable new version " + MajorRecieve + "."
//                                                           + MinorRecieve + "." + BuildRecieve + "." + RevisionRecieve
//                                                           + " do you wish to upgrade?", "Updates",
//                            MessageBoxButtons.OKCancel);
//                        if (res == DialogResult.OK)
//                        {
//                            using (WebClient Client = new WebClient())
//                            {
//                                //DiskItemInfo findedItem = yandexEngine.filesList.Find(x => x.OriginalDisplayName == exeName);
//                                //DownloadFiles();
//                            }
//                        }
//                    }
//                    else
//                    {
//                        //DoDevelopUpload();
//                        if (showInfo)
//                            Functions.Message("No updates available.");
//                    }
//                }
//                else
//                {
//                    xDoDevelopUpload();
//                }

//            }
//            catch (Exception ex)
//            {
//                Functions.Message(ex);
//            }

//        }
//        public void xDoDevelopUpload()
//        {
//            if (Settings.Get("debug", false))
//            {
//                SaveVersion();
//                //UploadVersion(); yandexengine
//                UploadExe();
//                //Functions.Message("Develop uploaded");
//            }
//        }
//        public void DownloadFiles()
//        {
//            //yandexEngine.DownloadFile(exeName);
//            //StreamWriter sw = new StreamWriter("update.cmd");
//            //sw.WriteLine("ping -n 3 127.0.0.1 > NUL");
//            //sw.WriteLine("del /q " + exeName);
//            //sw.WriteLine("copy /temp/" + exeName + " " + exeName+" /Y");
//            //sw.WriteLine("start ComputerToolkit.exe");
//            //sw.WriteLine("exit");
//            //sw.Close();
//            //System.Diagnostics.Process.Start("update.cmd");
//            //Application.Exit();
//        }
//        private string SaveVersion()
//        {
//            try
//            {
//                using (StreamWriter stream = new StreamWriter(VERSION_PATH))
//                {
//                    stream.WriteLine(Major.ToString() + "." + Minor.ToString() + "." + Build.ToString() + "." + Revision.ToString());
//                }

//                return VERSION_PATH;
//            }
//            catch (Exception e)
//            {
//                Functions.Message(e);
//                return null;
//            }
//        }
//        private string[] ReadVersion(string path)
//        {
//            try
//            {
//                string result;
//                using (var reader = new StreamReader(path))
//                {
//                    result = reader.ReadToEnd();
//                }
//                string[] resArr = result.Split('.');
//                return resArr;
//            }
//            catch (Exception ex)
//            { Functions.Message(ex); return null; }
//        }
//        public void UploadExe()
//        {
//            //try
//            //{
//            //    yandexEngine.UploadFileToFolder(appPath + "\\VIRTUAL\\" + exeName);
//            //}
//            //catch (Exception ex)
//            //{
//            //    Functions.Message(ex);
//            //}
//        }
//        public void UploadVersion()
//        {
//            //try
//            //{
//            //    yandexEngine.UploadFileToFolder(VERSION_PATH);
//            //}
//            //catch (Exception ex)
//            //{
//            //    Functions.Message(ex);
//            //}
//        }
//    }
//}
