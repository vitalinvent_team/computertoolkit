﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace ComputerToolkit
{
    public static class Program
    {
        public static ProcessIcon processIcon = null;

        [STAThread]

        static void Main(string[] args)
        {
            try
            {
                //if (Functions.isRunning())
                //{
                //    return;
                //}
                //GlobalKeyboardHook gHook = new GlobalKeyboardHook(); // Create a new GlobalKeyboardHook
                //gHook.KeyDown += new KeyEventHandler(Open_HOtkey_object);
                //gHook.hook();

                //Application.EnableVisualStyles();
                //Application.SetCompatibleTextRenderingDefault(false);

                CommandLine commandLine = new CommandLine(args);
                EmailClass mail = new EmailClass();
                ProcessIcon pi = new ProcessIcon();//WILL CRASH EXPLORER !! HA HA HA! ... UPS SECOND CALL FOR WHAT
                //processIcon = new ProcessIcon();
                processIcon = pi;
                //using (ProcessIcon pi = new ProcessIcon())
                //{
                //Settings.ReloadSettings();
                try
                {
                    var exists = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1;
                    if ((!commandLine.ExitCode) | (!exists))
                    {
                        KeyboardHook hook = new KeyboardHook();
                        hook.KeyPressed += new EventHandler<KeyPressedEventArgs>(hook_KeyPressed);
                        // register the control + alt + f12 combination as hot key.
                        //hook.RegisterHotKey(ComputerToolkit.ModifierKeys.Control | ComputerToolkit.ModifierKeys.Alt, Keys.F12);
                        foreach (HotkeyItem hotkey in ProcessIcon.lists.hotkeys)
                        {
                            hook.RegisterHotKey(hotkey.Keystring);
                        }
                        //hook.RegisterHotKey(ComputerToolkit.ModifierKeys.Control | ComputerToolkit.ModifierKeys.Alt | ModifierKeys.Win, Keys.F12);


                        if (!commandLine.Invisible)
                        {
                            pi.Display();
                        }


                        //var keyHandler = new Action<string, Keys>((text, keyInfo) =>
                        //{
                        //    HookedKey(keyInfo);
                        //});
                        //HookManager.StartMonitoring(keyHandler);
                        Application.Run();
                    }

                }
                catch (Exception ex)
                {
                    //ProcessIcon.notifyIcon.BalloonTipText = ex.Message;
                    //ProcessIcon.notifyIcon.ShowBalloonTip(3);
                    MessageBox.Show(ex.Message);
                    //mail.SendErrorOverEmail(ex.GetBaseException() + " " + ex.ToString());
                }
                //    }
            } catch (Exception ex)
            { Functions.Message(ex); }
        }

        private static void hook_KeyPressed(object sender, KeyPressedEventArgs e)
        {
            try
            {
                //FormMain.StatusInfo = e.Modifier.ToString() + "+" + e.Key.ToString();
                //MessageBox.Show(e.Modifier.ToString() + "+" + e.Key.ToString());
                //CurrentKey = e.Modifier.ToString() + "+" + e.Key.ToString();

                string[] modArr = e.Modifier.ToString().Replace(" ", "").Split(',');
                foreach (HotkeyItem hotkey in ProcessIcon.lists.hotkeys)
                {
                    string[] str = hotkey.Keystring.Split(',');
                    bool finalnotFindedKey = true;
                    if (str.Count() == modArr.Count() + 1)
                    {
                        for (int i = 0; i < modArr.Count(); i++)
                        {
                            if (hotkey.Keystring.Contains(modArr[i]))
                            {
                                finalnotFindedKey = false;
                            }
                            else
                            {
                                finalnotFindedKey = true;
                                break;
                            }
                        }
                    }
                    if (!finalnotFindedKey)
                    {
                        string cleanStr = hotkey.Keystring.Replace(",", "");
                        cleanStr = cleanStr.Replace(" ", "");
                        cleanStr = cleanStr.Replace("Alt", "");
                        cleanStr = cleanStr.Replace("Control", "");
                        cleanStr = cleanStr.Replace("Shift", "");
                        if (cleanStr == e.Key.ToString())
                        {
                            Functions.OpenObject(Functions.GetObjectByHotString(hotkey.HotString));

                        }
                    }
                }
            } catch (Exception ex)
            {
                Functions.Message(ex);
            }
            }

    }
}
