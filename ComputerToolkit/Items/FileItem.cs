﻿using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class FileItem : IItem
    {
        public bool Checked { get; set; }
        [XmlIgnore]  
        public Icon IconFile { get; set; }
        //public Icon IconFile
        //{
        //    get
        //    {
        //        return Functions.Base64ToIcon(IconFileString);
        //    }
        //    set
        //    {
        //        IconFileString = Functions.IconToBase64(value);
        //        IconFile = value;
        //    }
        //}
        private string _IconFileString;
        public string IconFileString
        {
            get
            {
                return _IconFileString;
            }
            set
            {
                IconFile = Functions.Base64ToIcon(value);
                _IconFileString = value;
            }
        } 
        //public string IconFileString { get; set; }
        public String Name { get; set; }
        public String Group { get; set; }
        public String PathFile { get; set; }
        public String PathFileFull { get; set; }
        public String Comments { get; set; }
        public String CompanyName { get; set; }
        public String FileDescription { get; set; }
        public String FileName { get; set; }
        public String FileVersion { get; set; }
        public String OriginalFilename { get; set; }
        public String ProductName { get; set; }
        public String Guid { get; set; }
        public String UrlProgrammToUpdate { get; set; }
        public String Date { get; set; }
        public FileItem()
        {
        }
        public FileItem(String guid, bool checkedVar, string namefile, string fileGroup, Icon icon, string pathFile, string pathFileFull, string сomments, string сompanyName, string fileDescription, string fileName, string fileVersion, string originalFilename, string productName)
        {
            this.Guid = guid;
            this.Checked = checkedVar;
            this.Group = fileGroup;
            this.Name = namefile;
            if (icon == null)
            {
                this.IconFile = Resources.file_icon16;
            } 
            else
            {
                this.IconFileString = Functions.IconToBase64(icon);
                this.IconFile = Functions.Base64ToIcon(IconFileString);
            }
            this.PathFile = pathFile;
            this.PathFileFull = pathFileFull;
            this.Comments = сomments;
            this.CompanyName = сompanyName;
            this.FileDescription = fileDescription;
            this.FileName = fileName;
            this.FileVersion = fileVersion;
            this.OriginalFilename = originalFilename;
            this.ProductName = productName;
        }

        public override string ToString()
        {
            return "Path:" + this.PathFile + System.Environment.NewLine + 
                   "Comments:" + this.Comments + System.Environment.NewLine + 
                   "CompanyName:" + this.CompanyName + System.Environment.NewLine +
                   "OriginalFilename:" + this.OriginalFilename + System.Environment.NewLine +
                   "ProductName:" + this.ProductName + System.Environment.NewLine +
                   "FileVersion:" + this.FileVersion + System.Environment.NewLine;
        }

    }
}

