﻿using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ComputerToolkit
{
    [Serializable]
    public class HotkeyItem : IItem
    {
        public bool Checked { get; set; }
        public String Keystring { get; set; }
        public String Name { get; set; }
        public String Guid { get; set; }
        public String HotString { get; set; }
        public String Date { get; set; }
        public String Group { get; set; }
        public HotkeyItem() { }
        public HotkeyItem(string name, string keycode,string date)
        {
            this.Guid = System.Guid.NewGuid().ToString();
            this.Name = name;
            this.Keystring = keycode;
            Date = date;
            if (date.Length == 0) Date = DateTime.Now.ToString();
            Group = "";
        }
        public override string ToString()
        {
            return "Name:" + this.Name + System.Environment.NewLine+
                   "Keycode:" + this.Keystring;
        }

    }
}

