﻿using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class FileItemQuick : IItem
    {
        public bool Checked { get; set; }

                [XmlIgnore]  
        public Icon IconFile { get; set; }
        //public Icon IconFile { 
        //    get{
        //        return Functions.Base64ToIcon(IconFileString);} 
        //    set{
        //        IconFileString = Functions.IconToBase64(value);
        //        IconFile = value;
        //    }
        //}

        //public string IconFileString { get; set; }

        private string _IconFileString;
        public string IconFileString
        {
            get
            {
                return _IconFileString;
            }
            set
            {
                IconFile = Functions.Base64ToIcon(value);
                _IconFileString = value;
            }
        } 
        public String Name { get; set; }
        public String Group { get; set; }
        public String PathFile { get; set; }
        public String Guid { get; set; }
        public String Date { get; set; }
        public FileItemQuick() { }
        public FileItemQuick(String guid, bool checkedVar, string namefile, string pathFile)
        {
            this.Guid = guid;
            this.Checked = checkedVar;
            this.Name = namefile;
            this.PathFile = pathFile;
            try
            {
                this.IconFile = System.Drawing.Icon.ExtractAssociatedIcon(pathFile);
            }
            catch 
            {
                this.IconFile = Resources.file_icon16;
            }
        }
        public override string ToString()
        {
            return "Path:" + this.PathFile + System.Environment.NewLine;
        }

    }
}

