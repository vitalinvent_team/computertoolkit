﻿using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{
   [Serializable]

   [XmlRoot("ScriptItem")]
    public class ScriptItem : IItem
    {
        //[XmlElement("IconScript")]
        [XmlIgnore]  
       public Icon IconScript { get; set; }
        //public Icon IconScript
        //{
        //    get
        //    {
        //        return Functions.Base64ToIcon(IconScriptString);
        //    }
        //    set
        //    {
        //        IconScriptString = Functions.IconToBase64(value);
        //        IconScript = value;
        //    }
        //}
        [XmlElement("IconScriptString")]

        private string _IconScriptString;
        public string IconScriptString
        {
            get
            {
                return _IconScriptString;
            }
            set
            {
                IconScript = Functions.Base64ToIcon(value);
                _IconScriptString = value;
            }
        } 
        //public string IconScriptString { get; set; }
        [XmlElement("UseInMenu")]
        public bool UseInMenu { get; set; }
        [XmlElement("Name")]
        public String Name { get; set; }
        [XmlElement("Content")]
        public String Content { get; set; }
        [XmlElement("ScriptLang")]
        public ScriptLanguage ScriptLang { get; set; }
        [XmlElement("Date")]
        public String Date { get; set; }
        [XmlElement("Group")]
        public String Group { get; set; }
        [XmlElement("Guid")]
        public String Guid { get; set; }
        [XmlElement("UserInfo")]
        public String UserInfo { get; set; }
        [XmlElement("User")]
        public String User { get; set; }
        [XmlElement("Global")]
        public bool Global { get; set; }

       [XmlElement("CompilerVerison")]
        public CompilerVersion CompilerVerison { get; set; }
        //public ScriptItem(String guid, string nameScript, string groupScript, string scriptContent, ScriptLanguage scriptLanguage, string userInfo)
        public ScriptItem() { }
        public ScriptItem(String guid, string name, string date, string content, string group, ScriptLanguage scriptLanguage, string userInfo)
        {
            this.Guid = guid;
            this.Name = name;
            this.Date = date;
            if (date.Length == 0) Date = DateTime.Now.ToString();
            this.Content = content;
            this.Group = group;
            this.ScriptLang = scriptLanguage;
            this.UserInfo = userInfo;
            switch (scriptLanguage)
            {
                case ScriptLanguage.CSharp:
                    this.IconScript = Resources.script_icon_sharp;
                    break;
                case ScriptLanguage.VBasic:
                    this.IconScript = Resources.script_icon_basic;
                    break;
            }
            this.User = Settings.GetString("UserGuid"); 
        }
        public override string ToString()
        {
            return "Name:" + this.Name + System.Environment.NewLine+
                    "Group:" + this.Group + System.Environment.NewLine +
                    "Script:" + this.Content.Substring(0,50) + System.Environment.NewLine;
        }

    }
    public enum CompilerVersion
    {
        v2_0,
        v3_5,
        v4_0
    }
}

