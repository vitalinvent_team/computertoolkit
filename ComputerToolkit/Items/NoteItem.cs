﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComputerToolkit
{
    [Serializable]
    public class NoteItem : IItem
    {
        public Boolean Checked { get; set; }
        public String Name { get; set; }
        public String Content { get; set; }
        public String Date { get; set; }
        public String Group { get; set; }
        public String Teg { get; set; }
        public String Guid { get; set; }
        public String User { get; set; }
        public NoteItem() { }
        public NoteItem(String guid, String nameNote, String dateNote, String noteContent)
        {
            this.Guid = guid;
            this.Name = nameNote;
            this.Date = dateNote;
            this.Content = noteContent;
            this.User = Settings.GetString("UserGuid");
            if (dateNote.Length == 0) Date = DateTime.Now.ToString();
        }
        public NoteItem(String guid, String nameNote, String dateNote, String noteContent, String group, String teg)
        {
            this.Guid = guid;
            this.Name = nameNote;
            this.Date = dateNote;
            this.Content = noteContent;
            this.Group = group;
            this.Teg = teg;
            this.User = Settings.GetString("UserGuid");
        }
        public override string ToString()
        {
            return this.Guid + " " + this.Date + " " + this.Content;
        }
    }
}
