﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]

    [XmlRoot("TaskItem")]
    public class TaskItem : IItem
    {
        [XmlElement("Checked")]
        public Boolean Checked { get; set; }
        [XmlElement("Enabled")]
        public Boolean Enabled { get; set; }
        [XmlElement("Name")]
        public Boolean isReminder { get; set; }
        [XmlElement("isReminder")]
        public String Name { get; set; }
        [XmlElement("Description")]
        public String Description { get; set; }
        [XmlElement("TimeToRun")]
        public String TimeToRun { get; set; }
        [XmlElement("LastTimeToRun")]
        public String LastTimeToRun { get; set; }
        [XmlElement("Interval")]
        public int Interval { get; set; }
        [XmlElement("Group")]
        public String Group { get; set; }
        [XmlElement("Guid")]
        public String Guid { get; set; }
        [XmlElement("HotString")]
        public String HotString { get; set; }

        [XmlElement("Date")]
        public String Date { get; set; }

        public TaskItem() { }
        public TaskItem(String name, String dateTimeToRun, String description,string hotString)
        {
            this.Enabled = false;
            this.isReminder = false;
            this.LastTimeToRun = "";
            this.Interval = 60000;
            this.Group = "";
            this.Date = DateTime.Now.ToString();
            this.Guid = System.Guid.NewGuid().ToString();
            this.Name = name;
            this.TimeToRun = dateTimeToRun;
            this.Description = description;
            this.HotString = hotString;
        }

        public override string ToString()
        {
            return "Name:" + this.Name + " Time:" + this.TimeToRun + " Description:" + this.Description;
        }
    }
}
