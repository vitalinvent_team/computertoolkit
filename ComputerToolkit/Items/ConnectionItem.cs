﻿using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{

    [Serializable]
    public class ConnectionItem : IItem
    {
        public Boolean Checked { get; set; }
        public String Name { get; set; }
        public String Group { get; set; }
        public String Adress { get; set; }
        public String User { get; set; }
        public String Password { get; set; }
        public String Domain { get; set; }
        public String Description { get; set; }

        public Boolean Enabled { get; set; }

        [XmlIgnore]  
        public Icon IconConnection { get; set; }
        //public Icon IconConnection
        //{
        //    get
        //    {
        //        return Functions.Base64ToIcon(IconFileString);
        //    }
        //    set
        //    {
        //        IconFileString = Functions.IconToBase64(value);
        //        IconConnection = value;
        //    }
        //}
        private string _IconConnectionString;
        public string IconConnectionString
        {
            get
            {
                return _IconConnectionString;
            }
            set
            {
                IconConnection = Functions.Base64ToIcon(value);
                _IconConnectionString = value;
            }
        } 
        //public string IconConnectionString { get; set; }
        public String Guid { get; set; }
        public String Port { get; set; }
        public ConnectionProtocol Protocol { get; set; }
        public String HotString { get; set; }
        public String ExternalSettings { get; set; }
        public String CommandLine { get; set; }
        public String Date { get; set; }
        public ConnectionItem()
        {
        }
        public ConnectionItem(String nameConnection, String groupConnection, String adress, String user, String password,ConnectionProtocol protocol, string port,string date)
        {
            this.Name = nameConnection;
            this.Group = groupConnection;
            this.Adress = adress;
            this.User = user;
            this.Password = password;
            this.Protocol = protocol;
            Date = date;
            if (date.Length == 0) Date = DateTime.Now.ToString();
            if (port == null)
            {
                this.Port = "5900";
            } else if (port.Length == 0)
            {
                this.Port = "5900";
            }
            else
            {
                this.Port = port;
            }
            this.Guid = System.Guid.NewGuid().ToString();

            switch (Protocol)
            {
                case ConnectionProtocol.RDP:
                    this.IconConnection = Resources.rdp;
                    break;
                case ConnectionProtocol.VNC:
                    this.IconConnection = Resources.vnc;
                    break;
                case ConnectionProtocol.TEAMVIEWER:
                    this.IconConnection = Resources.teamviewer;
                    break;
                case ConnectionProtocol.AMMY:
                    this.IconConnection = Resources.ammy;
                    break;
            }
            CommandLine = "";
            Description = "";
            Domain = "";
            ExternalSettings = "";
            HotString = "";
            _IconConnectionString = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAimSURBVFhHzVYJUFRXFn2OijsIKpZLGMwkGolBU44Yo05SbjHGJY7o1MRSISoTo46jxuCW6ERQoiIIoo6sArI10LSytw10QwsNNA3dNHvTICDQgCwKNJtnbuOfqtSMEarGmppT9ar5//Puue/c89677P8OHq7X30sQBNumJgd9mJYUOiNbmjiK+/S/QfwD3mX/u4k4fy4Ufv5JfTH8xCfpaYk5xarUqPra9JvVGukppUL6VXmR7A9VmoL3tJoiE27qm0GNRuxr8+ltMPYPMOMHGDlbhOmLpLBeq8Sa7eW46FWB5LQKxCRXQJBcBKFY2ZGRVVCeK1eml5aoI+trNNfadNoj/T1ttl3dPcsLCwtHc6GHh5JCcarVkluUQDDYlHiwqZlgpkVgE+vo3XP86QTQ1A6kFwCJsgHEPupCXFYnjWeIk+ogym6AKLcJ6soONDXUgc/nu3Ohhwdpuqhi0kxPIgsEm/4AbJYY7Ld5YG9XUBKNWO/Qi75+oL0TaOkAmp8BOvqtbwOqmoHyeiBNDTxU6PFYWwwPD49ALvTQ6NG3Tk5KEPZOm3MTzCgAzDwGbGYKmEUO2JwSsGl1mLaiC7lFQK0OKK4CSh7TqCXiJ4C2AWhoBYroWV01gBJ1Hm7cuHGMCz809N1NH966zcPqjaEwtgwDmxQBNkMI9lYWmGUh2OwqUqIV/nzgMZHlFAO5JYC8jEYFkF8JlBL5QyqPTN0JRXaqQYHtXPihMdDf9OUJR1/Y7oyBzSo+lcGbK0MalUFBSWjIEzoccxlAsRZIyQFS5SS5AhATqUQFZJcD4RlAprIZj8Rx8PT0/JgLPzT69HVHHb65g3WbebDdRcSjyQumZEbzaFLiISkhA5ugxOd7dRDn9CNGNAC+6AVi0gCBBHhAxEnZgE8sKaOqR+pDgaEEFlz4odHUUOSxZdt1LFkZhF37YmFiQQqMoTGVymHwg6EcE8SYv7oIQfwOBEQ9h09EF3wje+Af3YeAmBcIJvJrAS8gV1QhIY7X7OXlNfxtWKTKEixbeQXWNnexeQcP7yzyBxtFKpjdIwPyqBxxYCYpmGIth5OnDu7+rbh8pw1XfJ7D1a8LbgF6Igcu3OyEPE+N6KiQQi708JApTcu1XuyKuQt9YLPCF/MX+2KEMSVg7EMqhFISUVSSOIy2TIfDSS3OuTXiexcdTl5uwamrrTjr1kH+6MHfPVugUuYiNCRIyIUeGkD3yJjoB82z3rkMi7m3YDnPk349MHaaO9h4gxdIDUMpzCIpkXis36nEgdNa2B2vgv2JWux1fAKH043YdrAVP12vQ2lRJgIC/D248EMD6LBwdqa9z87BaPI1TDK/gvFmlzDK+BIdQNdIBTobTOm7GSkxmYeFqyTY+rUSG3YV4os9JdhkX44v91diyZYanHMtR6k6FUKh8DAXfmgM9D9ddea0K962vITJ5lcpkQs0fqRxlsZ5sN9cIQN6kAeoHMZBmL0wFss2SPD7dVIs+VyGpRvlWL61AFMXqXDuihJPqjNx4cKFFVz4ocGP4tsF3L6JeGEQ/O/6wsX5Hv52NBifbbiN9xd5YNI0N9qWXpSM4Z6gy2pcIKbMjcKMBQJYLk7A7z4SYfSsZFIoAbd80ukYzsHFSy6iA98e8rO33/eDldWC158H111dzf568PDaXbt3OF50cuQJeN5lakUCygpTIctIQoB3MM7/4AP7r32wdn0ArD8Kw4x5Aoyafp92CpmT3cWCZVGIT85HR3s92tvpgvgFGnXNcL/ulWJuPv0DjvL1MDU1HfG+lZXVujVrttlu3exw5KDd1Ts3zhdGhVwFP8wdEcHeuBckgLevBPsOxMLhcAra2+gmGgJSWTbenTtvDUczPMycOdNy7969p+hUU7i7u+PM6dP48exJeLr9BF6IO/JzYyh090sGDgMDA9D36KHv6gG6gLsltyCoCgF66fhOy4CJyeTZXPhfx+rVq8c5OTlFZWZmQiaTIS4uDhEREfD28YHLzz/j0OHDsNuzG0olXQL/Br1ej+5OSor4w1T+mJDE8IV0IepK69DV0YPvTjgmcDS/jpycnKS8vDyIxWJIJBJkZ2dDoVDA8E4qlSIyMhKhYeF42kYdCgd9Hy2XSPv01DTo6WJSBMKMx7BSYonYTD7K8ipQkJ9PWzQFpqZmb3FU/wmBQDCS9jFEItFgAgYVqL2CRqNBcXExcnNzkZ4uQYY0E920WgPKmothGWSCn/KODT4ryqiRcWMYE8kQ8SgAvfkkf3oKhMnJND8PK1d+YsfRvRo8Hi9RpVKBlIBcLkdpaSmqqqpQVlY2qIJKqUR2jhy9vX2DhNqnGnwQYgEWzOCqOoOemn4cirMDC2PYkvAp5PS/+ZlKJAuTkCvPwx+3bXfiqF4NZ2fnceHh4REG6ZVEVlNTg7q6OlRUVAyqoWtspL+1ePbs+WACGAD6WwawSfgJWDTDmfSDQA3wvegvYHcYVoVbQ5mlwiPJI6gK1dhmu+P1CfwLfn5+SykBFypDKqlRQ6tvJU+0xsTwy2Nj45887+wiFcje1Be+aAR2y9aDRTDsjN6AlvI29GgHcCTBHt/d/wbF+aXIpLKVV2jw8fIVeziK4SMrK2sEGXFiYGDgBMOztfWi+fX11CC+AFQVBVgisgS7z/Dt/a/QpG6Dtrwa1ZpqdFR3oqmqhcpYBk2lBmJJBsaOHTt9MOh/i1OnzkYZKpCqFsIkhuHPgs/QlN+Ox2W1UJeoUamtREN9I3QNOjIxNY+E/fsd/LnpbwRGD4WpTYNJZAiRk5GLUmU5dUXywR1jMK5Op0MjecaA2Ni4Zpoz8eXUNwQ62ebEJyRqDUasLNVCkiGGLFsGZUHBoGkNB5MBKaKUx+PHj3+Xm/bGMebo0ePXUtMkrQaT1dTW0sqbBlefJ89rd3R0NDQob3blr4KRkZHx0qXLNm7atPnYps1bjtvY2Gyl12Yvv/4SjP0TsI+pb3wmQO0AAAAASUVORK5CYII=";
            IconConnectionString = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAimSURBVFhHzVYJUFRXFn2OijsIKpZLGMwkGolBU44Yo05SbjHGJY7o1MRSISoTo46jxuCW6ERQoiIIoo6sArI10LSytw10QwsNNA3dNHvTICDQgCwKNJtnbuOfqtSMEarGmppT9ar5//Puue/c89677P8OHq7X30sQBNumJgd9mJYUOiNbmjiK+/S/QfwD3mX/u4k4fy4Ufv5JfTH8xCfpaYk5xarUqPra9JvVGukppUL6VXmR7A9VmoL3tJoiE27qm0GNRuxr8+ltMPYPMOMHGDlbhOmLpLBeq8Sa7eW46FWB5LQKxCRXQJBcBKFY2ZGRVVCeK1eml5aoI+trNNfadNoj/T1ttl3dPcsLCwtHc6GHh5JCcarVkluUQDDYlHiwqZlgpkVgE+vo3XP86QTQ1A6kFwCJsgHEPupCXFYnjWeIk+ogym6AKLcJ6soONDXUgc/nu3Ohhwdpuqhi0kxPIgsEm/4AbJYY7Ld5YG9XUBKNWO/Qi75+oL0TaOkAmp8BOvqtbwOqmoHyeiBNDTxU6PFYWwwPD49ALvTQ6NG3Tk5KEPZOm3MTzCgAzDwGbGYKmEUO2JwSsGl1mLaiC7lFQK0OKK4CSh7TqCXiJ4C2AWhoBYroWV01gBJ1Hm7cuHGMCz809N1NH966zcPqjaEwtgwDmxQBNkMI9lYWmGUh2OwqUqIV/nzgMZHlFAO5JYC8jEYFkF8JlBL5QyqPTN0JRXaqQYHtXPihMdDf9OUJR1/Y7oyBzSo+lcGbK0MalUFBSWjIEzoccxlAsRZIyQFS5SS5AhATqUQFZJcD4RlAprIZj8Rx8PT0/JgLPzT69HVHHb65g3WbebDdRcSjyQumZEbzaFLiISkhA5ugxOd7dRDn9CNGNAC+6AVi0gCBBHhAxEnZgE8sKaOqR+pDgaEEFlz4odHUUOSxZdt1LFkZhF37YmFiQQqMoTGVymHwg6EcE8SYv7oIQfwOBEQ9h09EF3wje+Af3YeAmBcIJvJrAS8gV1QhIY7X7OXlNfxtWKTKEixbeQXWNnexeQcP7yzyBxtFKpjdIwPyqBxxYCYpmGIth5OnDu7+rbh8pw1XfJ7D1a8LbgF6Igcu3OyEPE+N6KiQQi708JApTcu1XuyKuQt9YLPCF/MX+2KEMSVg7EMqhFISUVSSOIy2TIfDSS3OuTXiexcdTl5uwamrrTjr1kH+6MHfPVugUuYiNCRIyIUeGkD3yJjoB82z3rkMi7m3YDnPk349MHaaO9h4gxdIDUMpzCIpkXis36nEgdNa2B2vgv2JWux1fAKH043YdrAVP12vQ2lRJgIC/D248EMD6LBwdqa9z87BaPI1TDK/gvFmlzDK+BIdQNdIBTobTOm7GSkxmYeFqyTY+rUSG3YV4os9JdhkX44v91diyZYanHMtR6k6FUKh8DAXfmgM9D9ddea0K962vITJ5lcpkQs0fqRxlsZ5sN9cIQN6kAeoHMZBmL0wFss2SPD7dVIs+VyGpRvlWL61AFMXqXDuihJPqjNx4cKFFVz4ocGP4tsF3L6JeGEQ/O/6wsX5Hv52NBifbbiN9xd5YNI0N9qWXpSM4Z6gy2pcIKbMjcKMBQJYLk7A7z4SYfSsZFIoAbd80ukYzsHFSy6iA98e8rO33/eDldWC158H111dzf568PDaXbt3OF50cuQJeN5lakUCygpTIctIQoB3MM7/4AP7r32wdn0ArD8Kw4x5Aoyafp92CpmT3cWCZVGIT85HR3s92tvpgvgFGnXNcL/ulWJuPv0DjvL1MDU1HfG+lZXVujVrttlu3exw5KDd1Ts3zhdGhVwFP8wdEcHeuBckgLevBPsOxMLhcAra2+gmGgJSWTbenTtvDUczPMycOdNy7969p+hUU7i7u+PM6dP48exJeLr9BF6IO/JzYyh090sGDgMDA9D36KHv6gG6gLsltyCoCgF66fhOy4CJyeTZXPhfx+rVq8c5OTlFZWZmQiaTIS4uDhEREfD28YHLzz/j0OHDsNuzG0olXQL/Br1ej+5OSor4w1T+mJDE8IV0IepK69DV0YPvTjgmcDS/jpycnKS8vDyIxWJIJBJkZ2dDoVDA8E4qlSIyMhKhYeF42kYdCgd9Hy2XSPv01DTo6WJSBMKMx7BSYonYTD7K8ipQkJ9PWzQFpqZmb3FU/wmBQDCS9jFEItFgAgYVqL2CRqNBcXExcnNzkZ4uQYY0E920WgPKmothGWSCn/KODT4ryqiRcWMYE8kQ8SgAvfkkf3oKhMnJND8PK1d+YsfRvRo8Hi9RpVKBlIBcLkdpaSmqqqpQVlY2qIJKqUR2jhy9vX2DhNqnGnwQYgEWzOCqOoOemn4cirMDC2PYkvAp5PS/+ZlKJAuTkCvPwx+3bXfiqF4NZ2fnceHh4REG6ZVEVlNTg7q6OlRUVAyqoWtspL+1ePbs+WACGAD6WwawSfgJWDTDmfSDQA3wvegvYHcYVoVbQ5mlwiPJI6gK1dhmu+P1CfwLfn5+SykBFypDKqlRQ6tvJU+0xsTwy2Nj45887+wiFcje1Be+aAR2y9aDRTDsjN6AlvI29GgHcCTBHt/d/wbF+aXIpLKVV2jw8fIVeziK4SMrK2sEGXFiYGDgBMOztfWi+fX11CC+AFQVBVgisgS7z/Dt/a/QpG6Dtrwa1ZpqdFR3oqmqhcpYBk2lBmJJBsaOHTt9MOh/i1OnzkYZKpCqFsIkhuHPgs/QlN+Ox2W1UJeoUamtREN9I3QNOjIxNY+E/fsd/LnpbwRGD4WpTYNJZAiRk5GLUmU5dUXywR1jMK5Op0MjecaA2Ni4Zpoz8eXUNwQ62ebEJyRqDUasLNVCkiGGLFsGZUHBoGkNB5MBKaKUx+PHj3+Xm/bGMebo0ePXUtMkrQaT1dTW0sqbBlefJ89rd3R0NDQob3blr4KRkZHx0qXLNm7atPnYps1bjtvY2Gyl12Yvv/4SjP0TsI+pb3wmQO0AAAAASUVORK5CYII=";
            Enabled = true;
        }
        public override string ToString()
        {
            return "Name:" + this.Name + System.Environment.NewLine +
                   "Adress:" + this.Adress + System.Environment.NewLine +
                   "User:" + this.User + System.Environment.NewLine +
                   "Domain:" + this.Domain + System.Environment.NewLine +
                   "Description:" + this.Description + System.Environment.NewLine +
                   "Protocol:" + this.Protocol + System.Environment.NewLine;
        }
    }
}
