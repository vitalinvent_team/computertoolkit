/****************************************************************************************************************
(C) Copyright 2007 Zuoliu Ding.  All Rights Reserved.
SysMonForm.cs:		class SystemMonitor
Created by:			Zuoliu Ding, 05/20/2006
Note:				Main SystemMonitor Form
****************************************************************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using ComputerToolkit.Properties;

namespace ComputerToolkit
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormSysMon : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label labelCpu;
        private System.Windows.Forms.Timer timer;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label labelMemV;
		private System.Windows.Forms.Label labelDiskR;
		private System.Windows.Forms.Label labelDiskW;
		private System.Windows.Forms.Label labelNetO;
        private System.Windows.Forms.Label labelNetI;

        private DataBar dataBarCPU;
		private DataBar dataBarMemV;
		private DataChart dataChartDiskR;
		private DataChart dataChartDiskW;
		private DataChart dataChartNetI;
		private DataChart dataChartNetO;
		private DataChart dataChartCPU;
		private DataChart dataChartMem;

		private ArrayList  _listDiskR = new ArrayList();
		private ArrayList  _listDiskW = new ArrayList();
		private ArrayList  _listNetI = new ArrayList();
		private ArrayList  _listNetO = new ArrayList();

		private ArrayList  _listCPU = new ArrayList();
		private ArrayList  _listMem = new ArrayList();

		private SystemData sd = new SystemData();
		private Size _sizeOrg;
		private Size _size;

        private Point firstPoint;
        private CheckBox checkBoxPin;
        private Button button1;
        private bool mouseIsDown;
        public FormSysMon()
		{
			InitializeComponent();

			_size = Size;
			_sizeOrg = Size;

			//labelModel.Text = sd.QueryComputerSystem("manufacturer") +", " + sd.QueryComputerSystem("model");
			//textBoxProcessor.Text = sd.QueryEnvironment("%PROCESSOR_IDENTIFIER%");
			//labelNames.Text = "User: " +sd.QueryComputerSystem("username");  //sd.LogicalDisk();

			//UpdateData();
			timer.Interval = 1000;
			timer.Start();
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSysMon));
            this.labelNetI = new System.Windows.Forms.Label();
            this.labelNetO = new System.Windows.Forms.Label();
            this.labelDiskW = new System.Windows.Forms.Label();
            this.labelDiskR = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelMemV = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelCpu = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.checkBoxPin = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataChartMem = new ComputerToolkit.DataChart();
            this.dataChartCPU = new ComputerToolkit.DataChart();
            this.dataChartNetI = new ComputerToolkit.DataChart();
            this.dataChartNetO = new ComputerToolkit.DataChart();
            this.dataChartDiskW = new ComputerToolkit.DataChart();
            this.dataChartDiskR = new ComputerToolkit.DataChart();
            this.dataBarMemV = new ComputerToolkit.DataBar();
            this.dataBarCPU = new ComputerToolkit.DataBar();
            this.SuspendLayout();
            // 
            // labelNetI
            // 
            this.labelNetI.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNetI.Location = new System.Drawing.Point(0, 111);
            this.labelNetI.Name = "labelNetI";
            this.labelNetI.Size = new System.Drawing.Size(160, 16);
            this.labelNetI.TabIndex = 18;
            this.labelNetI.Text = "Net I";
            this.labelNetI.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelNetI_MouseDown);
            this.labelNetI.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelNetI_MouseMove);
            this.labelNetI.MouseUp += new System.Windows.Forms.MouseEventHandler(this.labelNetI_MouseUp);
            // 
            // labelNetO
            // 
            this.labelNetO.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNetO.Location = new System.Drawing.Point(180, 111);
            this.labelNetO.Name = "labelNetO";
            this.labelNetO.Size = new System.Drawing.Size(160, 16);
            this.labelNetO.TabIndex = 17;
            this.labelNetO.Text = "Net O";
            this.labelNetO.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelNetO_MouseDown);
            this.labelNetO.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelNetO_MouseMove);
            this.labelNetO.MouseUp += new System.Windows.Forms.MouseEventHandler(this.labelNetO_MouseUp);
            // 
            // labelDiskW
            // 
            this.labelDiskW.BackColor = System.Drawing.Color.Transparent;
            this.labelDiskW.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiskW.Location = new System.Drawing.Point(179, 76);
            this.labelDiskW.Name = "labelDiskW";
            this.labelDiskW.Size = new System.Drawing.Size(176, 16);
            this.labelDiskW.TabIndex = 16;
            this.labelDiskW.Text = "Disk W";
            this.labelDiskW.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelDiskW_MouseDown);
            this.labelDiskW.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelDiskW_MouseMove);
            this.labelDiskW.MouseUp += new System.Windows.Forms.MouseEventHandler(this.labelDiskW_MouseUp);
            // 
            // labelDiskR
            // 
            this.labelDiskR.BackColor = System.Drawing.Color.Transparent;
            this.labelDiskR.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDiskR.Location = new System.Drawing.Point(0, 76);
            this.labelDiskR.Name = "labelDiskR";
            this.labelDiskR.Size = new System.Drawing.Size(176, 16);
            this.labelDiskR.TabIndex = 15;
            this.labelDiskR.Text = "Disk R";
            this.labelDiskR.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelDiskR_MouseDown);
            this.labelDiskR.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelDiskR_MouseMove);
            this.labelDiskR.MouseUp += new System.Windows.Forms.MouseEventHandler(this.labelDiskR_MouseUp);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(0, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 14);
            this.label4.TabIndex = 14;
            this.label4.Text = "Mem:";
            this.label4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label4_MouseDown);
            this.label4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label4_MouseMove);
            this.label4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label4_MouseUp);
            // 
            // labelMemV
            // 
            this.labelMemV.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMemV.Location = new System.Drawing.Point(0, 14);
            this.labelMemV.Name = "labelMemV";
            this.labelMemV.Size = new System.Drawing.Size(76, 10);
            this.labelMemV.TabIndex = 13;
            this.labelMemV.Text = "Mem:";
            this.labelMemV.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelMemV_MouseDown);
            this.labelMemV.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelMemV_MouseMove);
            this.labelMemV.MouseUp += new System.Windows.Forms.MouseEventHandler(this.labelMemV_MouseUp);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(0, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Cpu:";
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            this.label1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label1_MouseMove);
            this.label1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label1_MouseUp);
            // 
            // labelCpu
            // 
            this.labelCpu.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCpu.Location = new System.Drawing.Point(0, 40);
            this.labelCpu.Name = "labelCpu";
            this.labelCpu.Size = new System.Drawing.Size(33, 10);
            this.labelCpu.TabIndex = 10;
            this.labelCpu.Text = "CPU";
            this.labelCpu.Click += new System.EventHandler(this.labelCpu_Click);
            this.labelCpu.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelCpu_MouseDown);
            this.labelCpu.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelCpu_MouseMove);
            this.labelCpu.MouseUp += new System.Windows.Forms.MouseEventHandler(this.labelCpu_MouseUp);
            // 
            // timer
            // 
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // checkBoxPin
            // 
            this.checkBoxPin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxPin.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxPin.AutoSize = true;
            this.checkBoxPin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBoxPin.Image = global::ComputerToolkit.Properties.Resources.pin_small;
            this.checkBoxPin.Location = new System.Drawing.Point(286, 1);
            this.checkBoxPin.Name = "checkBoxPin";
            this.checkBoxPin.Size = new System.Drawing.Size(23, 23);
            this.checkBoxPin.TabIndex = 31;
            this.checkBoxPin.UseVisualStyleBackColor = true;
            this.checkBoxPin.CheckedChanged += new System.EventHandler(this.checkBoxPin_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Image = global::ComputerToolkit.Properties.Resources.cancel;
            this.button1.Location = new System.Drawing.Point(310, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 23);
            this.button1.TabIndex = 30;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataChartMem
            // 
            this.dataChartMem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataChartMem.BackColor = System.Drawing.Color.Silver;
            this.dataChartMem.ChartType = ComputerToolkit.ChartType.Line;
            this.dataChartMem.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataChartMem.GridColor = System.Drawing.Color.DarkGray;
            this.dataChartMem.GridPixels = 12;
            this.dataChartMem.InitialHeight = 100;
            this.dataChartMem.LineColor = System.Drawing.Color.RoyalBlue;
            this.dataChartMem.Location = new System.Drawing.Point(135, 1);
            this.dataChartMem.Name = "dataChartMem";
            this.dataChartMem.Size = new System.Drawing.Size(149, 24);
            this.dataChartMem.TabIndex = 29;
            this.dataChartMem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataChartMem_MouseDown);
            this.dataChartMem.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataChartMem_MouseMove);
            this.dataChartMem.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataChartMem_MouseUp);
            // 
            // dataChartCPU
            // 
            this.dataChartCPU.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataChartCPU.BackColor = System.Drawing.Color.Silver;
            this.dataChartCPU.ChartType = ComputerToolkit.ChartType.Line;
            this.dataChartCPU.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataChartCPU.GridColor = System.Drawing.Color.DarkGray;
            this.dataChartCPU.GridPixels = 8;
            this.dataChartCPU.InitialHeight = 100;
            this.dataChartCPU.LineColor = System.Drawing.Color.Green;
            this.dataChartCPU.Location = new System.Drawing.Point(136, 27);
            this.dataChartCPU.Name = "dataChartCPU";
            this.dataChartCPU.Size = new System.Drawing.Size(199, 24);
            this.dataChartCPU.TabIndex = 27;
            this.dataChartCPU.Load += new System.EventHandler(this.dataChartCPU_Load);
            this.dataChartCPU.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataChartCPU_MouseDown);
            this.dataChartCPU.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataChartCPU_MouseMove);
            this.dataChartCPU.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataChartCPU_MouseUp);
            // 
            // dataChartNetI
            // 
            this.dataChartNetI.BackColor = System.Drawing.Color.Silver;
            this.dataChartNetI.ChartType = ComputerToolkit.ChartType.Stick;
            this.dataChartNetI.GridColor = System.Drawing.Color.DarkGray;
            this.dataChartNetI.GridPixels = 0;
            this.dataChartNetI.InitialHeight = 1000;
            this.dataChartNetI.LineColor = System.Drawing.Color.Olive;
            this.dataChartNetI.Location = new System.Drawing.Point(2, 88);
            this.dataChartNetI.Name = "dataChartNetI";
            this.dataChartNetI.Size = new System.Drawing.Size(178, 24);
            this.dataChartNetI.TabIndex = 25;
            this.dataChartNetI.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataChartNetI_MouseDown);
            this.dataChartNetI.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataChartNetI_MouseMove);
            this.dataChartNetI.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataChartNetI_MouseUp);
            // 
            // dataChartNetO
            // 
            this.dataChartNetO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataChartNetO.BackColor = System.Drawing.Color.Silver;
            this.dataChartNetO.ChartType = ComputerToolkit.ChartType.Stick;
            this.dataChartNetO.GridColor = System.Drawing.Color.DarkGray;
            this.dataChartNetO.GridPixels = 0;
            this.dataChartNetO.InitialHeight = 1000;
            this.dataChartNetO.LineColor = System.Drawing.Color.Olive;
            this.dataChartNetO.Location = new System.Drawing.Point(182, 88);
            this.dataChartNetO.Name = "dataChartNetO";
            this.dataChartNetO.Size = new System.Drawing.Size(153, 24);
            this.dataChartNetO.TabIndex = 24;
            this.dataChartNetO.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataChartNetO_MouseDown);
            this.dataChartNetO.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataChartNetO_MouseMove);
            this.dataChartNetO.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataChartNetO_MouseUp);
            // 
            // dataChartDiskW
            // 
            this.dataChartDiskW.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataChartDiskW.BackColor = System.Drawing.Color.Silver;
            this.dataChartDiskW.ChartType = ComputerToolkit.ChartType.Stick;
            this.dataChartDiskW.GridColor = System.Drawing.Color.DarkGray;
            this.dataChartDiskW.GridPixels = 0;
            this.dataChartDiskW.InitialHeight = 100000;
            this.dataChartDiskW.LineColor = System.Drawing.Color.RoyalBlue;
            this.dataChartDiskW.Location = new System.Drawing.Point(182, 53);
            this.dataChartDiskW.Name = "dataChartDiskW";
            this.dataChartDiskW.Size = new System.Drawing.Size(153, 24);
            this.dataChartDiskW.TabIndex = 23;
            this.dataChartDiskW.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataChartDiskW_MouseDown);
            this.dataChartDiskW.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataChartDiskW_MouseMove);
            this.dataChartDiskW.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataChartDiskW_MouseUp);
            // 
            // dataChartDiskR
            // 
            this.dataChartDiskR.BackColor = System.Drawing.Color.Silver;
            this.dataChartDiskR.ChartType = ComputerToolkit.ChartType.Stick;
            this.dataChartDiskR.GridColor = System.Drawing.Color.DarkGray;
            this.dataChartDiskR.GridPixels = 0;
            this.dataChartDiskR.InitialHeight = 100000;
            this.dataChartDiskR.LineColor = System.Drawing.Color.RoyalBlue;
            this.dataChartDiskR.Location = new System.Drawing.Point(2, 53);
            this.dataChartDiskR.Name = "dataChartDiskR";
            this.dataChartDiskR.Size = new System.Drawing.Size(178, 24);
            this.dataChartDiskR.TabIndex = 22;
            this.dataChartDiskR.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataChartDiskR_MouseDown);
            this.dataChartDiskR.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataChartDiskR_MouseMove);
            this.dataChartDiskR.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataChartDiskR_MouseUp);
            // 
            // dataBarMemV
            // 
            this.dataBarMemV.BackColor = System.Drawing.Color.Silver;
            this.dataBarMemV.BarColor = System.Drawing.Color.RoyalBlue;
            this.dataBarMemV.Location = new System.Drawing.Point(29, 1);
            this.dataBarMemV.Name = "dataBarMemV";
            this.dataBarMemV.Size = new System.Drawing.Size(104, 24);
            this.dataBarMemV.TabIndex = 21;
            this.dataBarMemV.Value = 0;
            this.dataBarMemV.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataBarMemV_MouseDown);
            this.dataBarMemV.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataBarMemV_MouseMove);
            this.dataBarMemV.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataBarMemV_MouseUp);
            // 
            // dataBarCPU
            // 
            this.dataBarCPU.BackColor = System.Drawing.Color.Silver;
            this.dataBarCPU.BarColor = System.Drawing.Color.Green;
            this.dataBarCPU.Location = new System.Drawing.Point(29, 27);
            this.dataBarCPU.Name = "dataBarCPU";
            this.dataBarCPU.Size = new System.Drawing.Size(104, 24);
            this.dataBarCPU.TabIndex = 19;
            this.dataBarCPU.Value = 0;
            this.dataBarCPU.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataBarCPU_MouseDown);
            this.dataBarCPU.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataBarCPU_MouseMove);
            this.dataBarCPU.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataBarCPU_MouseUp);
            // 
            // FormSysMon
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(336, 122);
            this.ControlBox = false;
            this.Controls.Add(this.checkBoxPin);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataChartMem);
            this.Controls.Add(this.dataChartCPU);
            this.Controls.Add(this.dataChartNetI);
            this.Controls.Add(this.dataChartNetO);
            this.Controls.Add(this.dataChartDiskW);
            this.Controls.Add(this.dataChartDiskR);
            this.Controls.Add(this.dataBarMemV);
            this.Controls.Add(this.dataBarCPU);
            this.Controls.Add(this.labelNetI);
            this.Controls.Add(this.labelNetO);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelMemV);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelCpu);
            this.Controls.Add(this.labelDiskR);
            this.Controls.Add(this.labelDiskW);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSysMon";
            this.ShowInTaskbar = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSysMon_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormSysMon_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FormSysMon_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FormSysMon_MouseUp);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.EnableVisualStyles();
            Application.Run(new FormSysMon());
		}

		private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			timer.Stop();
		}

		private void timer_Tick(object sender, System.EventArgs e)
		{
            Thread threadProc = new Thread(UpdateData);
            threadProc.Start();
		}
        

	    private void UpdateData()
		{

			string s = sd.GetProcessorData();
            labelCpu.Invoke(new SetDelegatelabelCpu(UpdatelabelCpu), s);
            double d = double.Parse(s.Substring(0, s.IndexOf("%")));
            dataBarCPU.Invoke(new SetDelegatedataBarCPU(UpdatedataBarCPU), d);
            dataChartCPU.Invoke(new SetDelegatedataBarMemV(UpdatedataChartCPU), d);


	        s = sd.GetMemoryVData();
            labelMemV.Invoke(new SetDelegatelabelMemV(UpdatelabelMemV), s);
	        d = double.Parse(s.Substring(0, s.IndexOf("%")));
            dataBarMemV.Invoke(new SetDelegatedataBarMemV(UpdatedataBarMemV), d);
            dataChartMem.Invoke(new SetDelegatedataChartMem(UpdatedataChartMem), d);

	        ////s = sd.GetMemoryPData();
	        ////labelMemP.Text = s;
	        ////dataBarMemP.Value = (int)double.Parse(s.Substring(0, s.IndexOf("%")));

	        d = sd.GetDiskData(SystemData.DiskData.Read);
            labelDiskR.Invoke(new SetDelegatelabelDiskR(UpdatelabelDiskR), d);
            dataChartDiskR.Invoke(new SetDelegatedataChartDiskR(UpdatedataChartDiskR), d);

	        d = sd.GetDiskData(SystemData.DiskData.Write);
            labelDiskW.Invoke(new SetDelegatelabelDiskW(UpdatelabelDiskW), d);
            dataChartDiskW.Invoke(new SetDelegatedataChartDiskW(UpdatedataChartDiskW), d);

	        d = sd.GetNetData(SystemData.NetData.Received);
            labelNetI.Invoke(new SetDelegatelabelNetI(UpdatelabelNetI), d);
            dataChartNetI.Invoke(new SetDelegatedataChartNetI(UpdatedataChartNetI), d);

	        d = sd.GetNetData(SystemData.NetData.Sent);
            labelNetO.Invoke(new SetDelegatelabelNetO(UpdatelabelNetO), d);
            dataChartNetO.Invoke(new SetDelegatedataChartNetO(UpdatedataChartNetO), d);
		}
        private delegate void SetDelegatelabelCpu(string s);
        private void UpdatelabelCpu(string s)
        {
            labelCpu.Text = s;
        }
        private delegate void SetDelegatedataBarCPU(double d);
        private void UpdatedataBarCPU(double d)
        {
            dataBarCPU.Value = (int)d;
        }
        private delegate void SetDelegatedataChartCPU(double d);
        private void UpdatedataChartCPU(double d)
        {
            dataChartCPU.UpdateChart(d);
        }
        private delegate void SetDelegatelabelMemV(string s);
        private void UpdatelabelMemV(string s)
        {
            labelMemV.Text = s;
        }
        private delegate void SetDelegatedataChartMem(double d);
        private void UpdatedataChartMem(double d)
        {
            dataChartMem.UpdateChart(d);
        }
        private delegate void SetDelegatedataBarMemV(double d);
        private void UpdatedataBarMemV(double d)
        {
            dataBarMemV.Value = (int)d;
        }

        private delegate void SetDelegatelabelDiskR(double d);
        private void UpdatelabelDiskR(double d)
        {
            labelDiskR.Text = "Disk R (" + sd.FormatBytes(d) + "/s)";
        }
        private delegate void SetDelegatedataChartDiskR(double d);
        private void UpdatedataChartDiskR(double d)
        {
            dataChartDiskR.UpdateChart(d);
        }
        private delegate void SetDelegatelabelDiskW(double d);
        private void UpdatelabelDiskW(double d)
        {
            labelDiskW.Text = "Disk W (" + sd.FormatBytes(d) + "/s)";
        }
        private delegate void SetDelegatedataChartDiskW(double d);
        private void UpdatedataChartDiskW(double d)
        {
            dataChartDiskW.UpdateChart(d);
        }
        private delegate void SetDelegatelabelNetI(double d);
        private void UpdatelabelNetI(double d)
        {
            labelNetI.Text = "Net I (" + sd.FormatBytes(d) + "/s)";
        }
        private delegate void SetDelegatedataChartNetI(double d);
        private void UpdatedataChartNetI(double d)
        {
            dataChartNetI.UpdateChart(d);
        }
        private delegate void SetDelegatelabelNetO(double d);
        private void UpdatelabelNetO(double d)
        {
            labelNetO.Text = "Net O (" + sd.FormatBytes(d) + "/s)";
        }
        private delegate void SetDelegatedataChartNetO(double d);
        private void UpdatedataChartNetO(double d)
        {
            dataChartNetO.UpdateChart(d);
        }
		protected override void OnResize(EventArgs e)
		{
			if (0==_sizeOrg.Width || 0== _sizeOrg.Height) return;

			if (Size.Width != _size.Width && Size.Width > _sizeOrg.Width)		
			{
				int xChange = Size.Width - _size.Width;	  // Client window

				// Adjust three bars
				int newWidth = dataBarCPU.Size.Width +xChange;
				int labelStart = labelCpu.Location.X + xChange;

				dataBarCPU.Size = new Size(newWidth, dataBarCPU.Size.Height);
				labelCpu.Location = new Point(labelStart, labelCpu.Location.Y);

				dataBarMemV.Size = new Size(newWidth, dataBarCPU.Size.Height);
				labelMemV.Location = new Point(labelStart, labelMemV.Location.Y);

				//dataBarMemP.Size = new Size(newWidth, dataBarCPU.Size.Height);
				//labelMemP.Location = new Point(labelStart, labelMemP.Location.Y);
					
				// Resize four charts
				int margin = 8;
				Rectangle rt = this.ClientRectangle;

				newWidth = (rt.Width - 3*margin)/2;
				labelStart = newWidth +2*margin;

				dataChartDiskR.Size = new Size(newWidth, dataChartDiskR.Size.Height);
				labelDiskW.Location = new Point(labelStart, labelDiskW.Location.Y);
				dataChartDiskW.Location = new Point(labelStart, dataChartDiskW.Location.Y);
				dataChartDiskW.Size = new Size(newWidth, dataChartDiskW.Size.Height);

				dataChartNetI.Size = new Size(newWidth, dataChartNetI.Size.Height);
				labelNetO.Location = new Point(labelStart, labelNetO.Location.Y);
				dataChartNetO.Location = new Point(labelStart, dataChartNetO.Location.Y);
				dataChartNetO.Size = new Size(newWidth, dataChartNetO.Size.Height);

				// I use Anchor so not to Resize two usage charts
//				dataChartCPU.Size = new Size(rt.Width - 2*margin, dataChartCPU.Size.Height);
//				dataChartMem.Size = new Size(rt.Width - 2*margin, dataChartMem.Size.Height);

				_size = Size;
			}

			Size = new Size(Size.Width, _sizeOrg.Height);
		}

        private void Form1_Load(object sender, EventArgs e)
        {
            checkBoxPin.Checked = true;
            GetSettings();
        }

        private void FormSysMon_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        }
        private void GetSettings()
        {
            try
            {

                this.Left = Settings.Get("FormInfoFormLeft", 0);
                this.Top = Settings.Get("FormInfoFormTop", 0);
            }
            catch
            {
            }
        }
        private void SaveSettings()
        {
            try
            {
                Settings.Set("FormInfoFormLeft", this.Left);
                Settings.Set("FormInfoFormTop", this.Top);
            }
            catch { }
        }

        private void dataBarCPU_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void dataChartCPU_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void dataBarMemV_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void dataChartMem_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void dataChartDiskR_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void dataChartDiskW_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void dataChartNetI_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void dataChartNetO_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void dataBarCPU_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void dataChartCPU_Load(object sender, EventArgs e)
        {

        }

        private void dataChartCPU_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void dataBarMemV_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void dataChartMem_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void dataChartDiskR_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void dataChartDiskW_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void dataChartNetI_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void dataChartNetO_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void dataBarCPU_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }
        private void SetMove(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                firstPoint = e.Location;
                mouseIsDown = true;
            } else
            {
                 if (this.FormBorderStyle == FormBorderStyle.FixedToolWindow)
                {
                    this.FormBorderStyle = FormBorderStyle.SizableToolWindow;
                } else
                {
                    this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
                }
            }

        }
        private void dataChartCPU_MouseDown(object sender, MouseEventArgs e)
        {
            SetMove(e);
        }

        private void dataBarMemV_MouseDown(object sender, MouseEventArgs e)
        {
            SetMove(e);
        }

        private void dataChartMem_MouseDown(object sender, MouseEventArgs e)
        {
            SetMove(e);
        }

        private void dataChartDiskR_MouseDown(object sender, MouseEventArgs e)
        {
            SetMove(e);
        }

        private void dataChartDiskW_MouseDown(object sender, MouseEventArgs e)
        {
            SetMove(e);
        }

        private void dataChartNetI_MouseDown(object sender, MouseEventArgs e)
        {
            SetMove(e);
        }

        private void dataChartNetO_MouseDown(object sender, MouseEventArgs e)
        {
            SetMove(e);
        }

        private void checkBoxPin_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPin.Checked)
            {
                checkBoxPin.Image = ComputerToolkit.Properties.Resources.pin_small_pressed;
                this.TopMost = true;
            }
            else
            {
                checkBoxPin.Image = ComputerToolkit.Properties.Resources.pin_small;
                this.TopMost = false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();

        }

        private void FormSysMon_MouseDown(object sender, MouseEventArgs e)
        {
            SetMove(e);
        }

        private void FormSysMon_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void FormSysMon_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void label1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void labelCpu_Click(object sender, EventArgs e)
        {

        }

        private void labelCpu_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void label4_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void labelMemV_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void labelDiskR_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void labelDiskW_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void labelNetI_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void labelNetO_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void labelCpu_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void label4_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void labelMemV_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void labelDiskR_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void labelDiskW_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void labelNetI_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void labelNetO_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            mouseIsDown = true;
        }

        private void label1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void labelCpu_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void label4_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void labelMemV_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void labelDiskR_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void labelDiskW_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void labelNetI_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void labelNetO_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

	}
}
