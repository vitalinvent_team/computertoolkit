using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using ComputerToolkit;
using System.CodeDom.Compiler;
using System.Net;
using System.Web;
using System.Threading;
using System.Globalization;

public class FormNotepad : Form
{
    private NoteType noteType = NoteType.Note;
    private ContextMenuStrip contextMenuStrip1;
    private IContainer components;
    private ToolStripMenuItem File1;
    private ToolStripMenuItem Edit;
    private ToolStripMenuItem Format;
    private ToolStripMenuItem New;
    private ToolStripMenuItem Open;
    private ToolStripMenuItem Save;
    private ToolStripMenuItem saveAsToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripMenuItem pageSetupToolStripMenuItem;
    private ToolStripMenuItem printToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator2;
    private ToolStripMenuItem exitToolStripMenuItem;
    private ToolStripMenuItem undoToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator3;
    private ToolStripMenuItem cutToolStripMenuItem;
    private ToolStripMenuItem copyToolStripMenuItem;
    private ToolStripMenuItem pasteToolStripMenuItem;
    private ToolStripMenuItem deleteToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator4;
    private ToolStripSeparator toolStripSeparator5;
    private ToolStripMenuItem selectAllToolStripMenuItem;
    private ToolStripMenuItem timeDateToolStripMenuItem;
    private ToolStripMenuItem wordWrapToolStripMenuItem;
    private ToolStripMenuItem fontToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator6;
    private ToolStripMenuItem undoToolStripMenuItem1;
    private ToolStripSeparator toolStripSeparator7;
    private ToolStripMenuItem cutToolStripMenuItem1;
    private ToolStripMenuItem copyToolStripMenuItem1;
    private ToolStripMenuItem pasteToolStripMenuItem1;
    private ToolStripMenuItem deleteToolStripMenuItem1;
    private ToolStripSeparator toolStripSeparator8;
    private ToolStripMenuItem selectAllToolStripMenuItem1;
    private FontDialog fontDialog1;
    private PageSetupDialog pageSetupDialog1;
    private SaveFileDialog saveFileDialog1;
    private OpenFileDialog openFileDialog1;
    private ListBox listBox1;
    private MenuStrip menuStrip1;
    private PrintDialog printDialog1;
    private System.Drawing.Printing.PrintDocument docToPrint =
   new System.Drawing.Printing.PrintDocument();
    private TextBox textBoxNameNote;
    private Label label1;
    private Button button1;
    private CheckBox checkBoxPin;
    private TextBox textBoxNoteContent;
    private MenuStrip menuStrip2;
    private ToolTip toolTip1;
    private int pos = 0;
    private TextBox textBoxGuid;
    //private Notes notes = new Notes();
    //private Scripts scripts = new Scripts();
    //private Alsing.SourceCode.SyntaxDocument syntaxDocument1;
    private Button buttonSendViaJsonToSite;
    private Label label2;
    private TextBox textBoxTegs;
    private EngineScript scriptEngine;
    public void FindNext(string str)
    {
        try
        {
            int nStart = textBoxNoteContent.Text.IndexOf(str, pos);
            int nLength = str.Length;

            textBoxNoteContent.Select(nStart, nLength);
            textBoxNoteContent.Focus();
            pos = nStart + nLength;
        }
        catch (Exception e)
        {
            e.ToString();
            MessageBox.Show("Reached End of Document.");
            pos = 0;
        }
    }
    public void Goto(int line)
    {
        pos = 0;
        for (int i = 1; i <= line; i++)
            FindNext("\n");
    }

    public FormNotepad()
    {
        InitializeComponent();
    }

    [STAThread]
    public static void Main(string[] args)
    {
        Application.Run(new FormNotepad());
    }

    public void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNotepad));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.File1 = new System.Windows.Forms.ToolStripMenuItem();
            this.New = new System.Windows.Forms.ToolStripMenuItem();
            this.Open = new System.Windows.Forms.ToolStripMenuItem();
            this.Save = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.pageSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Edit = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timeDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Format = new System.Windows.Forms.ToolStripMenuItem();
            this.wordWrapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.undoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.textBoxNameNote = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBoxPin = new System.Windows.Forms.CheckBox();
            this.textBoxNoteContent = new System.Windows.Forms.TextBox();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.buttonSendViaJsonToSite = new System.Windows.Forms.Button();
            this.textBoxGuid = new System.Windows.Forms.TextBox();
            //this.syntaxDocument1 = new Alsing.SourceCode.SyntaxDocument(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTegs = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.File1,
            this.Edit,
            this.Format});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(558, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            this.menuStrip1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseDown);
            this.menuStrip1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseMove);
            this.menuStrip1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseUp);
            // 
            // File1
            // 
            this.File1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.New,
            this.Open,
            this.Save,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.pageSetupToolStripMenuItem,
            this.printToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.File1.Name = "File1";
            this.File1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
            this.File1.Size = new System.Drawing.Size(37, 20);
            this.File1.Text = "File";
            // 
            // New
            // 
            this.New.Name = "New";
            this.New.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.New.Size = new System.Drawing.Size(155, 22);
            this.New.Text = "New";
            this.New.Click += new System.EventHandler(this.New_Click);
            // 
            // Open
            // 
            this.Open.Name = "Open";
            this.Open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.Open.Size = new System.Drawing.Size(155, 22);
            this.Open.Text = "Open...";
            this.Open.Click += new System.EventHandler(this.Open_Click);
            // 
            // Save
            // 
            this.Save.Name = "Save";
            this.Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.Save.Size = new System.Drawing.Size(155, 22);
            this.Save.Text = "Save";
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(152, 6);
            // 
            // pageSetupToolStripMenuItem
            // 
            this.pageSetupToolStripMenuItem.Name = "pageSetupToolStripMenuItem";
            this.pageSetupToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.pageSetupToolStripMenuItem.Text = "Page Setup...";
            this.pageSetupToolStripMenuItem.Click += new System.EventHandler(this.pageSetupToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.printToolStripMenuItem.Text = "Print...";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(152, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // Edit
            // 
            this.Edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripSeparator4,
            this.toolStripSeparator5,
            this.selectAllToolStripMenuItem,
            this.timeDateToolStripMenuItem});
            this.Edit.Name = "Edit";
            this.Edit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.E)));
            this.Edit.Size = new System.Drawing.Size(39, 20);
            this.Edit.Text = "Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Enabled = false;
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(161, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Enabled = false;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cutToolStripMenuItem.Text = "Cut ";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Enabled = false;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Enabled = false;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(161, 6);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(161, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.selectAllToolStripMenuItem.Text = "Select All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // timeDateToolStripMenuItem
            // 
            this.timeDateToolStripMenuItem.Name = "timeDateToolStripMenuItem";
            this.timeDateToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.timeDateToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.timeDateToolStripMenuItem.Text = "Time/Date";
            this.timeDateToolStripMenuItem.Click += new System.EventHandler(this.timeDateToolStripMenuItem_Click);
            // 
            // Format
            // 
            this.Format.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wordWrapToolStripMenuItem,
            this.fontToolStripMenuItem});
            this.Format.Name = "Format";
            this.Format.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.O)));
            this.Format.Size = new System.Drawing.Size(57, 20);
            this.Format.Text = "Format";
            // 
            // wordWrapToolStripMenuItem
            // 
            this.wordWrapToolStripMenuItem.Checked = true;
            this.wordWrapToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.wordWrapToolStripMenuItem.Name = "wordWrapToolStripMenuItem";
            this.wordWrapToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.wordWrapToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.wordWrapToolStripMenuItem.Text = "Word Wrap";
            this.wordWrapToolStripMenuItem.Click += new System.EventHandler(this.wordWrapToolStripMenuItem_Click);
            // 
            // fontToolStripMenuItem
            // 
            this.fontToolStripMenuItem.Name = "fontToolStripMenuItem";
            this.fontToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F4)));
            this.fontToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.fontToolStripMenuItem.Text = "Font...";
            this.fontToolStripMenuItem.Click += new System.EventHandler(this.fontToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(155, 6);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.contextMenuStrip1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem1,
            this.toolStripSeparator7,
            this.cutToolStripMenuItem1,
            this.copyToolStripMenuItem1,
            this.pasteToolStripMenuItem1,
            this.deleteToolStripMenuItem1,
            this.toolStripSeparator8,
            this.selectAllToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(118, 148);
            // 
            // undoToolStripMenuItem1
            // 
            this.undoToolStripMenuItem1.Enabled = false;
            this.undoToolStripMenuItem1.Name = "undoToolStripMenuItem1";
            this.undoToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.undoToolStripMenuItem1.Text = "Undo";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(114, 6);
            // 
            // cutToolStripMenuItem1
            // 
            this.cutToolStripMenuItem1.Enabled = false;
            this.cutToolStripMenuItem1.Name = "cutToolStripMenuItem1";
            this.cutToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.cutToolStripMenuItem1.Text = "Cut";
            // 
            // copyToolStripMenuItem1
            // 
            this.copyToolStripMenuItem1.Enabled = false;
            this.copyToolStripMenuItem1.Name = "copyToolStripMenuItem1";
            this.copyToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.copyToolStripMenuItem1.Text = "Copy";
            // 
            // pasteToolStripMenuItem1
            // 
            this.pasteToolStripMenuItem1.Name = "pasteToolStripMenuItem1";
            this.pasteToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.pasteToolStripMenuItem1.Text = "Paste";
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Enabled = false;
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.deleteToolStripMenuItem1.Text = "Delete";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(114, 6);
            // 
            // selectAllToolStripMenuItem1
            // 
            this.selectAllToolStripMenuItem1.Enabled = false;
            this.selectAllToolStripMenuItem1.Name = "selectAllToolStripMenuItem1";
            this.selectAllToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.selectAllToolStripMenuItem1.Text = "Select All";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(250, 130);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 95);
            this.listBox1.TabIndex = 4;
            this.listBox1.Visible = false;
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // textBoxNameNote
            // 
            this.textBoxNameNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxNameNote.Location = new System.Drawing.Point(66, 1);
            this.textBoxNameNote.Name = "textBoxNameNote";
            this.textBoxNameNote.Size = new System.Drawing.Size(235, 20);
            this.textBoxNameNote.TabIndex = 6;
            this.textBoxNameNote.TextChanged += new System.EventHandler(this.textBoxNameNote_TextChanged);
            this.textBoxNameNote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNameNote_KeyPress);
            this.textBoxNameNote.MouseDown += new System.Windows.Forms.MouseEventHandler(this.textBoxNameNote_MouseDown);
            this.textBoxNameNote.MouseMove += new System.Windows.Forms.MouseEventHandler(this.textBoxNameNote_MouseMove);
            this.textBoxNameNote.MouseUp += new System.Windows.Forms.MouseEventHandler(this.textBoxNameNote_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "��������:";
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            this.label1.MouseLeave += new System.EventHandler(this.label1_MouseLeave);
            this.label1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label1_MouseMove);
            this.label1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label1_MouseUp);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Image = global::ComputerToolkit.Properties.Resources.cancel;
            this.button1.Location = new System.Drawing.Point(599, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 23);
            this.button1.TabIndex = 12;
            this.toolTip1.SetToolTip(this.button1, "�������");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBoxPin
            // 
            this.checkBoxPin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxPin.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxPin.AutoSize = true;
            this.checkBoxPin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBoxPin.Image = global::ComputerToolkit.Properties.Resources.pin_small;
            this.checkBoxPin.Location = new System.Drawing.Point(575, 0);
            this.checkBoxPin.Name = "checkBoxPin";
            this.checkBoxPin.Size = new System.Drawing.Size(23, 23);
            this.checkBoxPin.TabIndex = 16;
            this.toolTip1.SetToolTip(this.checkBoxPin, "����������");
            this.checkBoxPin.UseVisualStyleBackColor = true;
            this.checkBoxPin.CheckedChanged += new System.EventHandler(this.checkBoxPin_CheckedChanged);
            // 
            // textBoxNoteContent
            // 
            this.textBoxNoteContent.AllowDrop = true;
            this.textBoxNoteContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNoteContent.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNoteContent.Location = new System.Drawing.Point(0, 24);
            this.textBoxNoteContent.Multiline = true;
            this.textBoxNoteContent.Name = "textBoxNoteContent";
            this.textBoxNoteContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxNoteContent.Size = new System.Drawing.Size(625, 270);
            this.textBoxNoteContent.TabIndex = 2;
            this.textBoxNoteContent.DragDrop += new System.Windows.Forms.DragEventHandler(this.textBoxNoteContent_DragDrop);
            this.textBoxNoteContent.DragEnter += new System.Windows.Forms.DragEventHandler(this.textBoxNoteContent_DragEnter);
            this.textBoxNoteContent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNoteContent_KeyPress);
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(625, 24);
            this.menuStrip2.TabIndex = 29;
            this.menuStrip2.Text = "menuStrip2";
            this.menuStrip2.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip2_ItemClicked);
            this.menuStrip2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseDown);
            this.menuStrip2.MouseLeave += new System.EventHandler(this.menuStrip2_MouseLeave);
            this.menuStrip2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseMove);
            this.menuStrip2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseUp);
            // 
            // buttonSendViaJsonToSite
            // 
            this.buttonSendViaJsonToSite.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSendViaJsonToSite.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonSendViaJsonToSite.Location = new System.Drawing.Point(549, 0);
            this.buttonSendViaJsonToSite.Name = "buttonSendViaJsonToSite";
            this.buttonSendViaJsonToSite.Size = new System.Drawing.Size(25, 23);
            this.buttonSendViaJsonToSite.TabIndex = 35;
            this.toolTip1.SetToolTip(this.buttonSendViaJsonToSite, "��������� �� ����");
            this.buttonSendViaJsonToSite.UseVisualStyleBackColor = false;
            this.buttonSendViaJsonToSite.Visible = false;
            this.buttonSendViaJsonToSite.Click += new System.EventHandler(this.buttonSendViaJsonToSite_Click);
            // 
            // textBoxGuid
            // 
            this.textBoxGuid.Location = new System.Drawing.Point(461, 72);
            this.textBoxGuid.Name = "textBoxGuid";
            this.textBoxGuid.Size = new System.Drawing.Size(100, 20);
            this.textBoxGuid.TabIndex = 36;
            this.textBoxGuid.Visible = false;
            // 
            // syntaxDocument1
            // 
            //this.syntaxDocument1.Lines = new string[] {""};
            //this.syntaxDocument1.MaxUndoBufferSize = 1000;
            //this.syntaxDocument1.Modified = false;
            //this.syntaxDocument1.UndoStep = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(307, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "����:";
            this.label2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label2_MouseDown);
            this.label2.MouseLeave += new System.EventHandler(this.label2_MouseLeave);
            this.label2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label2_MouseMove);
            this.label2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label2_MouseUp);
            // 
            // textBoxTegs
            // 
            this.textBoxTegs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxTegs.Location = new System.Drawing.Point(347, 2);
            this.textBoxTegs.Name = "textBoxTegs";
            this.textBoxTegs.Size = new System.Drawing.Size(140, 20);
            this.textBoxTegs.TabIndex = 45;
            this.textBoxTegs.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxTegs_KeyPress);
            // 
            // FormNotepad
            // 
            this.ClientSize = new System.Drawing.Size(625, 294);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxTegs);
            this.Controls.Add(this.buttonSendViaJsonToSite);
            this.Controls.Add(this.checkBoxPin);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxNameNote);
            this.Controls.Add(this.textBoxNoteContent);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.menuStrip2);
            this.Controls.Add(this.textBoxGuid);
            this.Controls.Add(this.menuStrip1);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormNotepad";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormNotepad_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    string fname = "";
    private void Form1_Load(Object sender, EventArgs e)
    {
        GetSettings();
        if (Top < 0) Top = 0;
        if (Left < 0) Left = 0;
        if ((Left + Width) > Screen.PrimaryScreen.WorkingArea.Width) Left = 0;
        if ((Top + Height) > Screen.PrimaryScreen.WorkingArea.Height) Top = 0;

    }

    private void groupBoxButtons_MouseDown(object sender, MouseEventArgs e)
    {
        firstPoint = e.Location;
        mouseIsDown = true;
    }

    private void groupBoxButtons_MouseUp(object sender, MouseEventArgs e)
    {
        mouseIsDown = false;
    }

    private void groupBoxButtons_MouseMove(object sender, MouseEventArgs e)
    {
        if (mouseIsDown)
        {
            // Get the difference between the two points
            int xDiff = firstPoint.X - e.Location.X;
            int yDiff = firstPoint.Y - e.Location.Y;

            // Set the new point
            int x = this.Location.X - xDiff;
            int y = this.Location.Y - yDiff;
            this.Location = new Point(x, y);
        }
    }

    private void GetSettings()
    {
        //FontConverter fC = new FontConverter();
        //Font font = (Font)fC.ConvertFromString(Settings.Get("FormNotepadFont", ""));
        //textBoxNoteContent.Font = font;
        //System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml(Settings.Get("FormNotepadForeColor", ""));
        //textBoxNoteContent.ForeColor = col;
        this.Left = Settings.Get("FormNotepadFormLeft", 0);
        this.Top = Settings.Get("FormNotepadFormTop", 0);
    }
    private static String HexConverter(System.Drawing.Color c)
    {
        return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
    }
    private void SaveSettings()
    {
        //Settings.Set("FormNotepadForeColor", HexConverter(textBoxNoteContent.ForeColor));
        //var cvt = new FontConverter();
        //Settings.Set("FormNotepadFont", cvt.ConvertToString(textBoxNoteContent.Font));
        Settings.Set("FormNotepadFormLeft", this.Left);
        Settings.Set("FormNotepadFormTop", this.Top);
    }

    private void New_Click(object sender, EventArgs e)
    {

        //if (textBox1.Text != "")
        //{
        //    DialogResult click = MessageBox.Show("The text in the Untitled has changed.\n\n Do you want to save the changes?", " My Notepad", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        //    if (click == DialogResult.Yes)
        //    {
        //        if (fname == "")
        //        {
        //            saveFileDialog1.Filter = "Text Files|*.textBox1";
        //            DialogResult result = saveFileDialog1.ShowDialog();
        //            if (result == DialogResult.Cancel)
        //            {
        //                return;
        //            }
        //            fname = saveFileDialog1.FileName;
        //        }
        //        StreamWriter write = new StreamWriter(fname);
        //        write.WriteLine(textBox1.Text);
        //        write.Flush();
        //        write.Close();

        //        textBox1.Text = "";
        //        fname = "";
        //    }
        //    if (click == DialogResult.No)
        //    {
        //        textBox1.Text = "";
        //        fname = "";
        //    }
        //}
        //else
        //{
        //    textBox1.Text = "";
        //    fname = "";
        //}
        textBoxNoteContent.Text = "";
        fname = "";
    }

    private void Open_Click(object sender, EventArgs e)
    {
        openFileDialog1.Multiselect = false;
        openFileDialog1.Filter = "Text Files|*.txt|All files (*.*)|*.*";
        openFileDialog1.ShowDialog();
        if (File.Exists(openFileDialog1.FileName))
        {
            fname = openFileDialog1.FileName;
            StreamReader sr = new StreamReader(fname);
            textBoxNoteContent.Text = sr.ReadToEnd();
            sr.Close();
        }
    }

    private void Save_Click(object sender, EventArgs e)
    {
        if (fname == "")
        {
            saveFileDialog1.Filter = "Text Files|*.txt";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                return;
            }
            fname = saveFileDialog1.FileName;
            StreamWriter s = new StreamWriter(fname);
            s.WriteLine(textBoxNoteContent.Text);
            s.Flush();
            s.Close();
        }
    }

    private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
    {
        saveFileDialog1.Filter = "Text Files|*.txt";
        saveFileDialog1.ShowDialog();
        fname = saveFileDialog1.FileName;
        if (fname == "")
        {
            saveFileDialog1.Filter = "Text Files|*.txt";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                return;
            }
            fname = saveFileDialog1.FileName;
        }
        StreamWriter s = new StreamWriter(fname);
        s.WriteLine(textBoxNoteContent.Text);
        s.Flush();
        s.Close();
    }

    private void pageSetupToolStripMenuItem_Click(object sender, EventArgs e)
    {
        pageSetupDialog1.PageSettings = new System.Drawing.Printing.PageSettings();
        pageSetupDialog1.PrinterSettings = new System.Drawing.Printing.PrinterSettings();
        pageSetupDialog1.ShowNetwork = false;
        DialogResult result = pageSetupDialog1.ShowDialog();
        if (result == DialogResult.OK)
        {
            object[] results = new object[]{
                    pageSetupDialog1.PageSettings.Margins,
                    pageSetupDialog1.PageSettings.PaperSize,
                    pageSetupDialog1.PageSettings.Landscape,
                    pageSetupDialog1.PrinterSettings.PrinterName,
                    pageSetupDialog1.PrinterSettings.PrintRange};
            listBox1.Items.AddRange(results);
        }
    }

    private void printToolStripMenuItem_Click(object sender, EventArgs e)
    {
        // Allow the user to choose the page range he or she would
        // like to print.
        printDialog1.AllowSomePages = true;

        // Show the help button.
        printDialog1.ShowHelp = true;

        // Set the Document property to the PrintDocument for 
        // which the PrintPage Event has been handled. To display the
        // dialog, either this property or the PrinterSettings property 
        // must be set 
        printDialog1.Document = docToPrint;

        DialogResult result = printDialog1.ShowDialog();

        // If the result is OK then print the document.
        if (result == DialogResult.OK)
        {
            docToPrint.Print();
        }

    }

    // The PrintDialog will print the document
    // by handling the document's PrintPage event.
    private void document_PrintPage(object sender,
      System.Drawing.Printing.PrintPageEventArgs e)
    {

        // Insert code to render the page here.
        // This code will be called when the control is drawn.

        // The following code will render a simple
        // message on the printed document.
        string text = "In document_PrintPage method.";
        System.Drawing.Font printFont = new System.Drawing.Font
            ("Arial", 35, System.Drawing.FontStyle.Regular);

        // Draw the content.
        e.Graphics.DrawString(text, printFont,
            System.Drawing.Brushes.Black, 10, 10);
    }

    private void exitToolStripMenuItem_Click(object sender, EventArgs e)
    {
        //if (textBox1.Text != "")
        //{
        //    DialogResult click = MessageBox.Show("The text in the Untitled has changed.\n\n Do you want to save the changes?", " My Notepad", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        //    if (click == DialogResult.Yes)
        //    {
        //        if (fname == "")
        //        {
        //            saveFileDialog1.Filter = "Text Files|*.txt";
        //            DialogResult result = saveFileDialog1.ShowDialog();
        //            if (result == DialogResult.Cancel)
        //            {
        //                return;
        //            }
        //            fname = saveFileDialog1.FileName;
        //            // MessageBox.Show(fname);
        //        }
        //        StreamWriter write = new StreamWriter(fname);
        //        write.WriteLine(textBox1.Text);
        //        write.Flush();
        //        write.Close();
        //        this.Hide();
        //        //Application.Exit();
        //    }
        //    if (click == DialogResult.No)
        //    {
        //        this.Hide();
        //        //Application.Exit();
        //    }
        //}
        //else
        //{

        //    Application.AllowQuit = false;
        //}
        this.Hide();
    }

    private void undoToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (textBoxNoteContent.CanUndo)
            textBoxNoteContent.Undo();
    }


    private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (textBoxNoteContent.Text != "")
        {
            textBoxNoteContent.SelectAll();
        }
        else
        {
            MessageBox.Show("No Text is there");
        }
    }

    private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (textBoxNoteContent.Text != "")
        {
            // deleteToolStripMenuItem.Enabled = true;
            textBoxNoteContent.SelectedText = "";
        }
    }
    private void timeDateToolStripMenuItem_Click(object sender, EventArgs e)
    {
        string timeDate;
        timeDate = DateTime.Now.ToShortTimeString() + " " +
        DateTime.Now.ToShortDateString();
        int newSelectionStart = textBoxNoteContent.SelectionStart + timeDate.Length;
        textBoxNoteContent.Text = textBoxNoteContent.Text.Insert(textBoxNoteContent.SelectionStart, timeDate);
        textBoxNoteContent.SelectionStart = newSelectionStart;
    }

    private void fontToolStripMenuItem_Click(object sender, EventArgs e)
    {
        fontDialog1.Font = textBoxNoteContent.Font;
        fontDialog1.Color = textBoxNoteContent.ForeColor;
        fontDialog1.ShowColor = true;
        fontDialog1.ShowDialog();
        textBoxNoteContent.Font = fontDialog1.Font;
        textBoxNoteContent.ForeColor = fontDialog1.Color;
        //if (fontDialog1.ShowDialog() == DialogResult.OK)
        //{
        //    textBox1.Font = fontDialog1.Font;
        //}

        //fontDialog1.ShowColor = true;
        //fontDialog1.ShowDialog();
        //textBox1.Font = fontDialog1.Font;
        //textBox1.ForeColor = fontDialog1.Color;
    }

    private void cutToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (textBoxNoteContent.SelectedText != "")
        {
            cutToolStripMenuItem.Enabled = true;
            textBoxNoteContent.Cut();
        }
    }

    private void copyToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (textBoxNoteContent.SelectedText != "")
        {
            // copyToolStripMenuItem.Enabled = true;
            textBoxNoteContent.Copy();
            //Clipboard.SetDataObject(textBox1.SelectedText, true);
        }
    }

    private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
    {
        textBoxNoteContent.Paste();
    }

    private void wordWrapToolStripMenuItem_Click(object sender, EventArgs e)
    {
        // ww.Checked = !(ww.Checked);
        wordWrapToolStripMenuItem.Checked = !(wordWrapToolStripMenuItem.Checked);
        textBoxNoteContent.WordWrap = wordWrapToolStripMenuItem.Checked;
        // this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;

    }

    private void gototoolStripMenuItem_Click(object sender, EventArgs e)
    {
        //go g1 = new go();
        //g1.Show();
        //g1.myparentform = this;
    }

    private void menuStrip1_MouseDown(object sender, MouseEventArgs e)
    {
        firstPoint = e.Location;
        mouseIsDown = true;
    }

    private void menuStrip1_MouseUp(object sender, MouseEventArgs e)
    {
        mouseIsDown = false;
    }

    private void menuStrip1_MouseMove(object sender, MouseEventArgs e)
    {
        if (mouseIsDown)
        {
            // Get the difference between the two points
            int xDiff = firstPoint.X - e.Location.X;
            int yDiff = firstPoint.Y - e.Location.Y;

            // Set the new point
            int x = this.Location.X - xDiff;
            int y = this.Location.Y - yDiff;
            this.Location = new Point(x, y);
        }
    }

    private Point firstPoint;

    private bool mouseIsDown;

    private void button1_Click(object sender, EventArgs e)
    {
        SaveFinded();
        SaveSettings();
        this.Hide();
    }

    private void checkBoxPin_CheckedChanged(object sender, EventArgs e)
    {
        if (checkBoxPin.Checked)
        {
            checkBoxPin.Image = ComputerToolkit.Properties.Resources.pin_small_pressed;
            this.TopMost = true;
        }
        else
        {
            checkBoxPin.Image = ComputerToolkit.Properties.Resources.pin_small;
            this.TopMost = false;
        }
    }

    private void textBoxNameNote_MouseDown(object sender, MouseEventArgs e)
    {
        //firstPoint = e.Location;
        //mouseIsDown = true;
    }

    private void textBoxNameNote_MouseUp(object sender, MouseEventArgs e)
    {
        //mouseIsDown = false;
    }

    private void textBoxNameNote_MouseMove(object sender, MouseEventArgs e)
    {
        //if (mouseIsDown)
        //{
        //    // Get the difference between the two points
        //    int xDiff = firstPoint.X - e.Location.X;
        //    int yDiff = firstPoint.Y - e.Location.Y;

        //    // Set the new point
        //    int x = this.Location.X - xDiff;
        //    int y = this.Location.Y - yDiff;
        //    this.Location = new Point(x, y);
        //}
    }

    private void label1_MouseDown(object sender, MouseEventArgs e)
    {
        firstPoint = e.Location;
        mouseIsDown = true;
    }

    private void label1_MouseUp(object sender, MouseEventArgs e)
    {
        mouseIsDown = false;
    }

    private void label1_MouseMove(object sender, MouseEventArgs e)
    {
        if (mouseIsDown)
        {
            // Get the difference between the two points
            int xDiff = firstPoint.X - e.Location.X;
            int yDiff = firstPoint.Y - e.Location.Y;

            // Set the new point
            int x = this.Location.X - xDiff;
            int y = this.Location.Y - yDiff;
            this.Location = new Point(x, y);
        }
    }

    private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {

    }

    private void button2_Click(object sender, EventArgs e)
    {
        textBoxNoteContent.Text = "";
        fname = "";
    }

    private void toolStripMenuItem2_Click(object sender, EventArgs e)
    {

    }

    private void checkBoxWordWrap_CheckedChanged_1(object sender, EventArgs e)
    {
        
    }

    private void button3_MouseDown(object sender, MouseEventArgs e)
    {
        firstPoint = e.Location;
        mouseIsDown = true;
    }

    private void button3_MouseUp(object sender, MouseEventArgs e)
    {
        mouseIsDown = false;
    }

    private void button3_MouseMove(object sender, MouseEventArgs e)
    {
        if (mouseIsDown)
        {
            // Get the difference between the two points
            int xDiff = firstPoint.X - e.Location.X;
            int yDiff = firstPoint.Y - e.Location.Y;

            // Set the new point
            int x = this.Location.X - xDiff;
            int y = this.Location.Y - yDiff;
            this.Location = new Point(x, y);
        }
    }

    private void textBox1_MouseDown(object sender, MouseEventArgs e)
    {
        //firstPoint = e.Location;
        //mouseIsDown = true;
    }

    private void textBox1_MouseUp(object sender, MouseEventArgs e)
    {
        //mouseIsDown = false;
    }

    private void textBox1_MouseMove(object sender, MouseEventArgs e)
    {
        //if (mouseIsDown)
        //{
        //    // Get the difference between the two points
        //    int xDiff = firstPoint.X - e.Location.X;
        //    int yDiff = firstPoint.Y - e.Location.Y;

        //    // Set the new point
        //    int x = this.Location.X - xDiff;
        //    int y = this.Location.Y - yDiff;
        //    this.Location = new Point(x, y);
        //}
    }

    private void FormNotepad_FormClosing(object sender, FormClosingEventArgs e)
    {
        SaveSettings();
        SaveFinded();
        //this.Hide();
        //e.Cancel = true;
    }

    private void menuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {

    }
    private void saveAsBase_Click(object sender, EventArgs e)
    {
        SaveFinded();
    }
    private void SaveFinded()
    {
        if (textBoxNoteContent.Text.Length > 0)
        {
            NoteItem findedNote = ProcessIcon.lists.notes.Find(x => x.Guid == textBoxGuid.Text);
            if (findedNote != null)
            {
                findedNote.Name = textBoxNameNote.Text;
                findedNote.Content = textBoxNoteContent.Text;
                findedNote.Date = DateTime.Now.ToString();
                findedNote.Teg = textBoxTegs.Text;
                findedNote.User = Settings.GetString("UserGuid");
                ProcessIcon.lists.notes.SaveBase();
            }
            else
            {
                ProcessIcon.lists.notes.Add(new NoteItem(textBoxGuid.Text, textBoxNameNote.Text, DateTime.Now.ToString(), textBoxNoteContent.Text));
                ProcessIcon.lists.notes.SaveBase();
            }
        }
    }
    public void OpenNote(String guid,Form frm)
    {
            noteType = NoteType.Note;
            ProcessIcon.lists.notes.ReadBase();
            NoteItem note = ProcessIcon.lists.notes.Find(x => x.Guid == guid);
            textBoxGuid.Text = note.Guid;
            textBoxNoteContent.Text = note.Content;
            textBoxNameNote.Text = note.Name;
            this.Text = note.Name;
            this.Show(frm);
            this.Activate();
    }
    public void OpenNote(NoteItem noteToOpen)
    {
        noteType = NoteType.Note;
        ProcessIcon.lists.notes.ReadBase();
        NoteItem note = ProcessIcon.lists.notes.Find(x => x.Guid == noteToOpen.Guid);
        textBoxGuid.Text = note.Guid;
        textBoxNoteContent.Text = note.Content;
        textBoxNameNote.Text = note.Name;
        this.Text = note.Name;
        this.Activate();
    }
    public void NewNote()
    {
        checkBoxPin.Checked = true;
        //this.ShowInTaskbar = true;
        noteType = NoteType.Note;
        textBoxGuid.Text = Guid.NewGuid().ToString();
        textBoxNameNote.Text = "������ " + DateTime.Now.ToString();
        this.Text = "������ " + DateTime.Now.ToString();
        textBoxNoteContent.Text = "";
        //this.Name = textBoxNameNote.Text;
        //this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        this.Show();
        this.Activate();
    }


    private void FillParameters()
    {
            //scripts.ReadBase();
            //BindingSource bs = new BindingSource();
            //bs.DataSource = variablesClass.listParameterApplication;
            //bs.ResetBindings(false);
        
    }
    private void button10_Click(object sender, EventArgs e)
    {
        textBoxNoteContent.Text = "";
        textBoxNameNote.Text = "";
    }

    private void textBoxNameNote_TextChanged(object sender, EventArgs e)
    {
        this.Text = textBoxNameNote.Text;
    }

    private void checkBoxButtons_CheckedChanged(object sender, EventArgs e)
    {
    }

    private void buttonAddParameter_Click(object sender, EventArgs e)
    {
    }


    private void groupBoxButtons_Enter(object sender, EventArgs e)
    {

    }


    private void textBoxNoteContent_DragDrop(object sender, DragEventArgs e)
    {
        if (e.Data.GetDataPresent(typeof(System.String)))
        {
            textBoxNoteContent.Text += (System.String)e.Data.GetData(typeof(System.String));
        }
    }

    private void textBoxNoteContent_DragEnter(object sender, DragEventArgs e)
    {
        e.Effect = DragDropEffects.Copy;
    }




    private void textBoxNoteContent_KeyPress(object sender, KeyPressEventArgs e)
    {
        if (e.KeyChar == (char)Keys.Escape)
        {
            this.Hide();
        }
    }

    private void textBoxNameNote_KeyPress(object sender, KeyPressEventArgs e)
    {
        if (e.KeyChar == (char)Keys.Escape)
        {
            this.Hide();
        }

    }

    private void buttonSendViaJsonToSite_Click(object sender, EventArgs e)
    {
        //ExchangeEngine exchangeEngine = new ExchangeEngine();
        //exchangeEngine.UploadNote(notes.Find(x => x.Guid == textBoxGuid.Text));
    }
    private void wc_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
    {
        int i = 1;
        // e.Result will contain the page's output
    }

    private void menuStrip2_MouseLeave(object sender, EventArgs e)
    {
        mouseIsDown = false;
    }

    private void label2_MouseDown(object sender, MouseEventArgs e)
    {
        firstPoint = e.Location;
        mouseIsDown = true;

    }

    private void label2_MouseLeave(object sender, EventArgs e)
    {
        mouseIsDown = false;

    }

    private void label2_MouseMove(object sender, MouseEventArgs e)
    {
        if (mouseIsDown)
        {
            // Get the difference between the two points
            int xDiff = firstPoint.X - e.Location.X;
            int yDiff = firstPoint.Y - e.Location.Y;

            // Set the new point
            int x = this.Location.X - xDiff;
            int y = this.Location.Y - yDiff;
            this.Location = new Point(x, y);
        }

    }

    private void label2_MouseUp(object sender, MouseEventArgs e)
    {
        mouseIsDown = false;

    }

    private void label1_MouseLeave(object sender, EventArgs e)
    {
        mouseIsDown = false;

    }

    private void textBoxTegs_KeyPress(object sender, KeyPressEventArgs e)
    {
        if (e.KeyChar == (char)Keys.Escape)
        {
            this.Hide();
        }

    }
}
