﻿namespace ComputerToolkit
{
    partial class FormSensMon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSensMon));
            this.dataGridViewSensorsValues = new System.Windows.Forms.DataGridView();
            this.NameSensor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValueSensor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkBoxPin = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonTimer = new System.Windows.Forms.Button();
            this.buttonLogs = new System.Windows.Forms.Button();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.buttonClearLogs = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSensorsValues)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewSensorsValues
            // 
            this.dataGridViewSensorsValues.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSensorsValues.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewSensorsValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSensorsValues.ColumnHeadersVisible = false;
            this.dataGridViewSensorsValues.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameSensor,
            this.ValueSensor});
            this.dataGridViewSensorsValues.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataGridViewSensorsValues.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewSensorsValues.Location = new System.Drawing.Point(0, 24);
            this.dataGridViewSensorsValues.Name = "dataGridViewSensorsValues";
            this.dataGridViewSensorsValues.RowHeadersVisible = false;
            this.dataGridViewSensorsValues.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewSensorsValues.Size = new System.Drawing.Size(307, 155);
            this.dataGridViewSensorsValues.TabIndex = 0;
            this.dataGridViewSensorsValues.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewSensorsValues_MouseDown);
            this.dataGridViewSensorsValues.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridViewSensorsValues_MouseMove);
            this.dataGridViewSensorsValues.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridViewSensorsValues_MouseUp);
            // 
            // NameSensor
            // 
            this.NameSensor.HeaderText = "Name";
            this.NameSensor.Name = "NameSensor";
            this.NameSensor.Width = 200;
            // 
            // ValueSensor
            // 
            this.ValueSensor.HeaderText = "Value";
            this.ValueSensor.Name = "ValueSensor";
            // 
            // checkBoxPin
            // 
            this.checkBoxPin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxPin.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxPin.AutoSize = true;
            this.checkBoxPin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.checkBoxPin.Image = global::ComputerToolkit.Properties.Resources.pin_small;
            this.checkBoxPin.Location = new System.Drawing.Point(258, 0);
            this.checkBoxPin.Name = "checkBoxPin";
            this.checkBoxPin.Size = new System.Drawing.Size(23, 23);
            this.checkBoxPin.TabIndex = 18;
            this.checkBoxPin.UseVisualStyleBackColor = true;
            this.checkBoxPin.CheckedChanged += new System.EventHandler(this.checkBoxPin_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Image = global::ComputerToolkit.Properties.Resources.cancel;
            this.button1.Location = new System.Drawing.Point(282, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 23);
            this.button1.TabIndex = 17;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonTimer
            // 
            this.buttonTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTimer.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonTimer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonTimer.Image = global::ComputerToolkit.Properties.Resources.clock1;
            this.buttonTimer.Location = new System.Drawing.Point(232, 0);
            this.buttonTimer.Name = "buttonTimer";
            this.buttonTimer.Size = new System.Drawing.Size(25, 23);
            this.buttonTimer.TabIndex = 19;
            this.buttonTimer.UseVisualStyleBackColor = true;
            this.buttonTimer.Click += new System.EventHandler(this.buttonTimer_Click_1);
            // 
            // buttonLogs
            // 
            this.buttonLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLogs.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonLogs.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLogs.Image = global::ComputerToolkit.Properties.Resources.edit_padding_top;
            this.buttonLogs.Location = new System.Drawing.Point(206, 0);
            this.buttonLogs.Name = "buttonLogs";
            this.buttonLogs.Size = new System.Drawing.Size(25, 23);
            this.buttonLogs.TabIndex = 20;
            this.toolTip1.SetToolTip(this.buttonLogs, "Логи");
            this.buttonLogs.UseVisualStyleBackColor = true;
            this.buttonLogs.Click += new System.EventHandler(this.buttonLogs_Click);
            // 
            // listBoxLog
            // 
            this.listBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(0, 26);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(307, 160);
            this.listBoxLog.TabIndex = 21;
            this.listBoxLog.Visible = false;
            this.listBoxLog.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBoxLog_MouseDown);
            this.listBoxLog.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listBoxLog_MouseMove);
            this.listBoxLog.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listBoxLog_MouseUp);
            // 
            // buttonClearLogs
            // 
            this.buttonClearLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClearLogs.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClearLogs.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonClearLogs.Image = global::ComputerToolkit.Properties.Resources.rew;
            this.buttonClearLogs.Location = new System.Drawing.Point(166, 0);
            this.buttonClearLogs.Name = "buttonClearLogs";
            this.buttonClearLogs.Size = new System.Drawing.Size(38, 23);
            this.buttonClearLogs.TabIndex = 22;
            this.toolTip1.SetToolTip(this.buttonClearLogs, "Очистить логи");
            this.buttonClearLogs.UseVisualStyleBackColor = true;
            this.buttonClearLogs.Click += new System.EventHandler(this.buttonClearLogs_Click);
            // 
            // FormSensMon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 180);
            this.ControlBox = false;
            this.Controls.Add(this.buttonClearLogs);
            this.Controls.Add(this.listBoxLog);
            this.Controls.Add(this.buttonLogs);
            this.Controls.Add(this.buttonTimer);
            this.Controls.Add(this.checkBoxPin);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridViewSensorsValues);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSensMon";
            this.ShowInTaskbar = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormInfo_FormClosing);
            this.Load += new System.EventHandler(this.FormInfo_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormSensMon_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FormSensMon_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FormSensMon_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSensorsValues)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewSensorsValues;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameSensor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValueSensor;
        private System.Windows.Forms.CheckBox checkBoxPin;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonTimer;
        private System.Windows.Forms.Button buttonLogs;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonClearLogs;

    }
}