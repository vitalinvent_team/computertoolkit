﻿//namespace ComputerToolkit
//{
//    partial class LoginYandexDisk
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.components = new System.ComponentModel.Container();
//            this.browser = new System.Windows.Forms.WebBrowser();
//            this.timerCheckToken = new System.Windows.Forms.Timer(this.components);
//            this.SuspendLayout();
//            // 
//            // browser
//            // 
//            this.browser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
//            | System.Windows.Forms.AnchorStyles.Left) 
//            | System.Windows.Forms.AnchorStyles.Right)));
//            this.browser.Location = new System.Drawing.Point(0, -3);
//            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
//            this.browser.Name = "browser";
//            this.browser.Size = new System.Drawing.Size(662, 429);
//            this.browser.TabIndex = 0;
//            // 
//            // timerCheckToken
//            // 
//            this.timerCheckToken.Enabled = true;
//            this.timerCheckToken.Interval = 1000;
//            this.timerCheckToken.Tick += new System.EventHandler(this.timerCheckToken_Tick);
//            // 
//            // LoginYandexDisk
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.ClientSize = new System.Drawing.Size(661, 426);
//            this.Controls.Add(this.browser);
//            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
//            this.Margin = new System.Windows.Forms.Padding(6);
//            this.MaximizeBox = false;
//            this.MinimizeBox = false;
//            this.Name = "LoginYandexDisk";
//            this.Text = "Login Yandex Disk";
//            this.Load += new System.EventHandler(this.LoginYandexDisk_Load);
//            this.ResumeLayout(false);

//        }

//        #endregion

//        private System.Windows.Forms.WebBrowser browserOnForm;
//        private System.Windows.Forms.WebBrowser browser;
//        private System.Windows.Forms.Timer timerCheckToken;
//    }
//}