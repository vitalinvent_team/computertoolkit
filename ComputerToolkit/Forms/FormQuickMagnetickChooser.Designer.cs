﻿namespace ComputerToolkit
{
    partial class FormQuickMagnetickChooser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormQuickMagnetickChooser));
            this.treeViewQuckChooser = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // treeViewQuckChooser
            // 
            this.treeViewQuckChooser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeViewQuckChooser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeViewQuckChooser.HotTracking = true;
            this.treeViewQuckChooser.ImageIndex = 5;
            this.treeViewQuckChooser.ImageList = this.imageList1;
            this.treeViewQuckChooser.Location = new System.Drawing.Point(0, 0);
            this.treeViewQuckChooser.Name = "treeViewQuckChooser";
            this.treeViewQuckChooser.SelectedImageIndex = 5;
            this.treeViewQuckChooser.ShowNodeToolTips = true;
            this.treeViewQuckChooser.Size = new System.Drawing.Size(300, 522);
            this.treeViewQuckChooser.TabIndex = 0;
            this.treeViewQuckChooser.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewQuckChooser_AfterSelect);
            this.treeViewQuckChooser.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewQuckChooser_NodeMouseDoubleClick);
            this.treeViewQuckChooser.DoubleClick += new System.EventHandler(this.treeViewQuckChooser_DoubleClick);
            this.treeViewQuckChooser.Layout += new System.Windows.Forms.LayoutEventHandler(this.treeViewQuckChooser_Layout);
            this.treeViewQuckChooser.Leave += new System.EventHandler(this.treeViewQuckChooser_Leave);
            this.treeViewQuckChooser.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeViewQuckChooser_MouseDown);
            this.treeViewQuckChooser.MouseLeave += new System.EventHandler(this.treeViewQuckChooser_MouseLeave);
            this.treeViewQuckChooser.MouseHover += new System.EventHandler(this.treeViewQuckChooser_MouseHover);
            this.treeViewQuckChooser.MouseMove += new System.Windows.Forms.MouseEventHandler(this.treeViewQuckChooser_MouseMove);
            this.treeViewQuckChooser.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeViewQuckChooser_MouseUp);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "rdp.PNG");
            this.imageList1.Images.SetKeyName(1, "Window.png");
            this.imageList1.Images.SetKeyName(2, "group.ICO");
            this.imageList1.Images.SetKeyName(3, "Windows.png");
            this.imageList1.Images.SetKeyName(4, "bloknot.ico");
            this.imageList1.Images.SetKeyName(5, "play.png");
            this.imageList1.Images.SetKeyName(6, "Start.ico");
            // 
            // FormQuickMagnetickChooser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 496);
            this.ControlBox = false;
            this.Controls.Add(this.treeViewQuckChooser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormQuickMagnetickChooser";
            this.ShowInTaskbar = false;
            this.Text = "Quick panel";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormQuickMagnetickChooser_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormQuickMagnetickChooser_FormClosing);
            this.Load += new System.EventHandler(this.FormQuickMagnetickChooser_Load);
            this.Leave += new System.EventHandler(this.FormQuickMagnetickChooser_Leave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FormQuickMagnetickChooser_MouseMove);
            this.Move += new System.EventHandler(this.FormQuickMagnetickChooser_Move);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeViewQuckChooser;
        private System.Windows.Forms.ImageList imageList1;
    }
}