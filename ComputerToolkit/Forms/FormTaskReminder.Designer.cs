﻿namespace ComputerToolkit
{
    partial class FormTaskReminder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageMain = new System.Windows.Forms.TabPage();
            this.richTextBoxHotstring = new System.Windows.Forms.RichTextBox();
            this.textBoxDescription = new System.Windows.Forms.RichTextBox();
            this.buttonEndTask = new System.Windows.Forms.Button();
            this.buttonMoveLater = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.textBoxaskReminderLaterTime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPageMain.SuspendLayout();
            this.tabPageSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageMain);
            this.tabControl1.Controls.Add(this.tabPageSettings);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(307, 268);
            this.tabControl1.TabIndex = 4;
            this.tabControl1.TabStop = false;
            // 
            // tabPageMain
            // 
            this.tabPageMain.Controls.Add(this.richTextBoxHotstring);
            this.tabPageMain.Controls.Add(this.textBoxDescription);
            this.tabPageMain.Controls.Add(this.buttonEndTask);
            this.tabPageMain.Controls.Add(this.buttonMoveLater);
            this.tabPageMain.Controls.Add(this.dateTimePicker1);
            this.tabPageMain.Location = new System.Drawing.Point(4, 18);
            this.tabPageMain.Name = "tabPageMain";
            this.tabPageMain.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMain.Size = new System.Drawing.Size(299, 246);
            this.tabPageMain.TabIndex = 0;
            this.tabPageMain.Text = "Напоминание";
            this.tabPageMain.UseVisualStyleBackColor = true;
            // 
            // richTextBoxHotstring
            // 
            this.richTextBoxHotstring.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxHotstring.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBoxHotstring.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBoxHotstring.Location = new System.Drawing.Point(0, 132);
            this.richTextBoxHotstring.Name = "richTextBoxHotstring";
            this.richTextBoxHotstring.Size = new System.Drawing.Size(296, 69);
            this.richTextBoxHotstring.TabIndex = 5;
            this.richTextBoxHotstring.Text = "";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxDescription.Location = new System.Drawing.Point(0, 35);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(296, 96);
            this.textBoxDescription.TabIndex = 4;
            this.textBoxDescription.Text = "";
            // 
            // buttonEndTask
            // 
            this.buttonEndTask.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEndTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonEndTask.Location = new System.Drawing.Point(153, 207);
            this.buttonEndTask.Name = "buttonEndTask";
            this.buttonEndTask.Size = new System.Drawing.Size(138, 31);
            this.buttonEndTask.TabIndex = 1;
            this.buttonEndTask.Text = "Завершено";
            this.buttonEndTask.UseVisualStyleBackColor = true;
            this.buttonEndTask.Click += new System.EventHandler(this.buttonEndTask_Click);
            // 
            // buttonMoveLater
            // 
            this.buttonMoveLater.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMoveLater.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonMoveLater.Location = new System.Drawing.Point(5, 207);
            this.buttonMoveLater.Name = "buttonMoveLater";
            this.buttonMoveLater.Size = new System.Drawing.Size(140, 31);
            this.buttonMoveLater.TabIndex = 0;
            this.buttonMoveLater.Text = "Ок";
            this.buttonMoveLater.UseVisualStyleBackColor = true;
            this.buttonMoveLater.Click += new System.EventHandler(this.buttonMoveLater_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd.mm.yyyy HH:mm:ss";
            this.dateTimePicker1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(3, 3);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(7);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(293, 22);
            this.dateTimePicker1.TabIndex = 2;
            this.dateTimePicker1.Value = new System.DateTime(2016, 11, 10, 12, 11, 2, 0);
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.Controls.Add(this.textBoxaskReminderLaterTime);
            this.tabPageSettings.Controls.Add(this.label1);
            this.tabPageSettings.Location = new System.Drawing.Point(4, 18);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSettings.Size = new System.Drawing.Size(299, 246);
            this.tabPageSettings.TabIndex = 1;
            this.tabPageSettings.Text = "Настройки";
            this.tabPageSettings.UseVisualStyleBackColor = true;
            // 
            // textBoxaskReminderLaterTime
            // 
            this.textBoxaskReminderLaterTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxaskReminderLaterTime.Location = new System.Drawing.Point(125, 10);
            this.textBoxaskReminderLaterTime.Name = "textBoxaskReminderLaterTime";
            this.textBoxaskReminderLaterTime.Size = new System.Drawing.Size(100, 20);
            this.textBoxaskReminderLaterTime.TabIndex = 1;
            this.textBoxaskReminderLaterTime.TextChanged += new System.EventHandler(this.textBoxaskReminderLaterTime_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(11, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Отложить на минут:";
            // 
            // FormTaskReminder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 268);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormTaskReminder";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormTaskReminder_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTaskReminder_FormClosing);
            this.Load += new System.EventHandler(this.FormTaskReminder_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPageMain.ResumeLayout(false);
            this.tabPageSettings.ResumeLayout(false);
            this.tabPageSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageSettings;
        private System.Windows.Forms.TextBox textBoxaskReminderLaterTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPageMain;
        private System.Windows.Forms.Button buttonEndTask;
        private System.Windows.Forms.Button buttonMoveLater;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.RichTextBox textBoxDescription;
        private System.Windows.Forms.RichTextBox richTextBoxHotstring;
    }
}