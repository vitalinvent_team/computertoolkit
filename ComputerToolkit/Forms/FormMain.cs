﻿#region using
using System.Collections;
using ComputerToolkit.Items;
using ComputerToolkit.MainClasses;
using ComputerToolkit.Properties;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
#endregion using

namespace ComputerToolkit
{
    public partial class FormMain : Form
    {
        #region VARIABLES
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        //Sensors sensors = new Sensors();
        public static String CurrentKey;
        public string StatusInfo { get { return StatusLabel.Text; } set { StatusLabel.Text = value; } }
        private List<FileItem> listFindedFiles = new List<FileItem>();
        private String universalItemListCurrentGuid;
        private String universalItemListCurrentGuidLeave;
        private String SortHeaderName;
        private String SortHeaderDirection;
        private int IndexSelectedColumnHeader;
        private bool InvertSelection = false;
        private string appPath;
        public Thread FindThread;
        private String GridHeaderswithdataGridFindedFiles = "";
        private String GridHeaderswithDataGridMainFiles = "";
        private String GridHeaderswithDataGridConnections = "";
        private String GridHeaderswithDataGridNotes = "";
        private String GridHeaderswithDataGridScripts = "";
        private String GridHeaderswithDataGridUniversals = "";
        private String GridHeaderswithDataGridUniversalsAtom = "";
        private String GridHeaderswithdataGridViewHotkeys = "";
        private String GridHeaderswithdataGridViewTasks = "";
        private String GridHeaderswithDataGridMainFilesQ = "";
        private String GridHeaderswithdataGridViewSensors = "";
        private FormNotepad formNotepad = new FormNotepad();
        private FormScriptEditor formScriptEditor = new FormScriptEditor();
        User user = new User();
        
        #endregion VARIABLES
        #region PathFunctions
        private String GetGroupFile(String s)
        {
            string searchStr = s.Substring(TextBoxDirSearch.Text.Length + 1, s.Length - TextBoxDirSearch.Text.Length - 1);
            int lastSlash = 0;
            for (int i = 0; i < searchStr.Length - 1; i = i + 2)
            {
                if (searchStr.Substring(i, 2).IndexOf("\\") > -1)
                {
                    lastSlash = i;
                    break;
                }
            }
            if (lastSlash != 0)
            {
                return searchStr.Substring(0, lastSlash + 1);
            }
            return "Root";
        }
        private string GetFileNameInPath(String s)
        {
            String pz = GetDirInPath(s);
            return s.Substring(pz.Length + 1, (s.Length - (pz.Length + 1)));
        }
        private string GetDirInPath(String s)
        {
            int lastSlash = 0;
            for (int i = s.Length - 2; i > 0; i--)
            {
                if (s.Substring(i, 2).IndexOf("\\") > -1)
                {
                    lastSlash = i;
                    break;
                }
            }
            if (lastSlash != 0)
            {
                return s.Substring(0, lastSlash);
            }
            return s;
        }
        private String GetRootPath(String s)
        {
            return s.Substring(TextBoxDirSearch.Text.Length + 1, s.Length - TextBoxDirSearch.Text.Length - 1);
        }
        private string GetStringAndTest(object o)
        {
            if (o != null)
            {
                return o.ToString();
            }
            else
            {
                return "";
            }
        }
        private bool GetBoolAndTest(object o)
        {
            try
            {
                if (o != null)
                {
                    return Boolean.Parse(o.ToString());
                }
                else
                {
                    return false;
                }
            }
            catch { return false; }
        }
        private Icon GetStringAndTestIcon(object o)
        {
            if (o != null)
            {
                return (Icon)o;
            }
            else
            {
                return null;
            }
        }
        //****************************
        #endregion
        #region ButtonEventsSelectDelInGrid
        private void Form1_Load(object sender, EventArgs e)
        {
            tabControlMain.TabPages.Remove(tabPageGoogle);
            tabControlMain.TabPages.Remove(tabPageMailRu);
            tabControlMain.TabPages.Remove(tabPageUniversalList);
            tabControlMain.TabPages.Remove(tabPageYandexDisk);
            try
            {
                if (Settings.Get("debug", false))
                {
                    buttonBackupExe.Visible = true;
                }
                if (Settings.Get("debug", false))
                {
                    buttonDownloadExe.Visible = true;
                }
                timerProgressBar.Enabled = true;
                appPath = Application.StartupPath.ToString();
                try
                {
                    string subPath = Application.StartupPath.ToString() + "\\" + VariablesClass.TEMP_FOLDER; // your code goes here
                    bool isExists = System.IO.Directory.Exists(subPath);
                    if (!isExists)
                        System.IO.Directory.CreateDirectory(subPath);
                }
                catch (Exception ex)
                { Functions.Message(ex); }
                comboBoxFormatImages.Items.Add(".bmp");
                comboBoxFormatImages.Items.Add(".gif");
                comboBoxFormatImages.Items.Add(".ico");
                comboBoxFormatImages.Items.Add(".jpeg");
                comboBoxFormatImages.Items.Add(".png");
                comboBoxFormatImages.Items.Add(".tiff");
                List<string> listFiles = Localization.GetLocalizations();
                foreach (string files in listFiles)
                {
                    comboBoxLocalization.Items.Add(files);
                }
                comboBoxUnloadList.Items.Add("Записи блокнота");
                comboBoxUnloadList.Items.Add("Скрипты");
                comboBoxUnloadList.Items.Add("Файлы (пкм)");
                comboBoxUnloadList.Items.Add("Файлы (лкм)");
                comboBoxUnloadList.Items.Add("Соединения");
                comboBoxUnloadList.Items.Add("Универсальный список");
                File.Delete(appPath + "\\" + Settings.Get("FILE_PROCESSES", "ComputerToolkitProcesses.bin"));
                Functions.DeleteFiles(appPath + "\\" + VariablesClass.TEMP_FOLDER);
                //this.TopMost = true;
                this.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + " " + Assembly.GetEntryAssembly().GetName().Version;
                FillGroups();
                FillGridFromFiles();
                FillGridFromListHotkeys();
                FillGridFromList();
                FillGridFromListConnections();
                FillGridFromListNotes();
                FillGridFromListFilesQuick();
                FillGridFromListScripts();
                FillGridFromListUniversals();
                FillGridFromListTasks();
                FillGridFromListSensors();
                GetMainSettings();
                GetDropDataFormats();
                GetWithGridHeaders();
                if (Top < -20) Top = 0;
                if (Left < -20) Left = 0;
                if ((Left + Width) > Screen.PrimaryScreen.WorkingArea.Width+20) Left = 0;
                if ((Top + Height) > Screen.PrimaryScreen.WorkingArea.Height+20) Top = 0;
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
            }
        }

        private void DelListItemConnections(string s)
        {
            try
            {
                var ff = ProcessIcon.lists.connections.Find(x => x.Guid == s);
                ProcessIcon.lists.connections.Remove(ff);
            }
            catch { }

        }
        private void DelListItemFilesQuick(string s)
        {
            var ff = ProcessIcon.lists.filesQuick.Find(x => x.Guid == s);
            ProcessIcon.lists.filesQuick.Remove(ff);
        }
        public void AddFileItemQuickToList(string s)
        {
            try
            {
                ProcessIcon.lists.filesQuick.Add(new FileItemQuick(Guid.NewGuid().ToString(), false, GetFileNameInPath(s), FileVersionInfo.GetVersionInfo(s).FileName));
                ProcessIcon.lists.filesQuick.SaveBase();
                FillGridFromListFilesQuick();
            }
            catch { }
        }
        private void TabPagesDrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            //Change appearance of tabcontrol
            Brush backBrush;
            Brush foreBrush;

            backBrush = new SolidBrush(Color.Red);
            foreBrush = new SolidBrush(Color.Blue);

            e.Graphics.FillRectangle(backBrush, e.Bounds);

            //You may need to write the label here also?
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;

            Rectangle r = e.Bounds;
            r = new Rectangle(r.X, r.Y + 3, r.Width, r.Height - 3);
            e.Graphics.DrawString("my label", e.Font, foreBrush, r, sf);
        }
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerProgressBar.Enabled = false;
            SaveSettings();
            SetHeaderthWidth();

            ProcessIcon.cmtmnu = new ContextMenus().CreateMainMenu();
        }
        private void buttonSelAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dg in dataGridFindedFiles.Rows)
            {
                dg.Cells["Checked"].Value = true;
            }
        }

        private void buttonDeselAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dg in dataGridFindedFiles.Rows)
            {
                dg.Cells["Checked"].Value = false;
            }
        }

        private void buttonInvertAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dg in dataGridFindedFiles.Rows)
            {
                if (dg.Cells["Checked"].Value != null)
                {
                    if (dg.Cells["Checked"].Value.Equals(false))
                    {
                        dg.Cells["Checked"].Value = true;
                    }
                    else
                    {
                        dg.Cells["Checked"].Value = false;
                    }
                }
            }
        }

        private void buttonSelAllInList_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dg in DataGridMainFiles.Rows)
            {
                dg.Cells["Checked"].Value = true;
            }
        }

        private void buttonDeselAllList_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dg in DataGridMainFiles.Rows)
            {
                dg.Cells["Checked"].Value = false;
            }
        }

        private void buttonInvertAllList_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dg in DataGridMainFiles.Rows)
            {
                try
                {
                    if (dg.Cells["Checked"].Value.Equals(false))
                    {
                        dg.Cells["Checked"].Value = true;
                    }
                    else
                    {
                        dg.Cells["Checked"].Value = false;
                    }
                }
                catch { }
            }

        }
        private void buttonDelSelected_Click_1(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
            if (res == DialogResult.OK)
            {
                for (int i = 1; i < 10; i++)
                    foreach (DataGridViewRow dg in DataGridMainFiles.Rows)
                    {
                        if (dg.Cells["Checked"].Value != null)
                        {
                            if ((bool)dg.Cells["Checked"].Value)
                            {
                                DelListItem(dg.Cells["Guid"].Value.ToString());
                                //DataGridMainFiles.Rows.Remove(dg);
                            }
                        }
                    }
                FillGridFromList();
                //ProcessIcon.lists.files.SaveBase();
                InvertSelection = false;
                if (SortHeaderDirection == "Desc")
                {
                    SortHeaderDirection = "Asc";
                }
                else
                {
                    SortHeaderDirection = "Desc";
                }
                SortGrid(IndexSelectedColumnHeader);
            }
        }
        private void DelListItem(string guid)
        {
            var ff = ProcessIcon.lists.files.Find(x => x.Guid == guid);
            ProcessIcon.lists.files.Remove(ff);
        }
        private void buttonCheckSelected_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridFindedFiles.SelectedRows)
            {
                if (row.Cells["Checked"].Value.Equals(false))
                {
                    row.Cells["Checked"].Value = true;
                }
                else
                {
                    row.Cells["Checked"].Value = false;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in DataGridMainFiles.SelectedRows)
            {
                if (row.Cells["Check"].Value.Equals(false))
                {
                    row.Cells["Check"].Value = true;
                }
                else
                {
                    row.Cells["Check"].Value = false;
                }
            }

        }
        //************** Region SELINGRID
        #endregion
        #region SavesBasesAndSettingsSerialize
        private void GetMainSettings()
        {
            comboBoxScriptsGropu.Text = Settings.Get("comboBoxScriptsGropu", "Невыбрано");
            comboBoxCurrentGroup.Text = Settings.Get("comboBoxCurrentGroup","Невыбрано");
            comboBoxTaskGroup.Text=Settings.Get("comboBoxTaskGroup", "");
            textBoxPassword.Text=Settings.Get("onlinePassword", "");
            checkBoxYandexDrive.Checked = Settings.Get("YandexDisk", false);
            checkBoxCheckUpdate.Checked = Settings.Get("checkBoxCheckUpdate", false);
            checkBoxCopyMessageToclipboard.Checked = Settings.Get("checkBoxCopyMessageToclipboard", false);            
            checkBoxGoogleEnable.Checked = Settings.Get("checkBoxGoogleEnable", false);
            textBoxTimeToRefreshUpdateGoogle.Text = Settings.Get("textBoxTimeToRefreshUpdateGoogle", "15");
            textBoxTimeToUpdateGoogle.Text = Settings.Get("textBoxTimeToUpdateGoogle", "09:30");
            textBoxGoogleUser.Text = Settings.Get("textBoxGoogleUser", "user@gmail.com");
            textBoxGooglePassword.Text = Settings.Get("textBoxGooglePassword", "*****");
            comboBoxLocalization.Text = Settings.Get("comboBoxLocalization", "");
            Localize();
            tabControlMain.SelectedIndex = Settings.Get("tabControlMain.SelectedIndex", 0);
            checkBoxRunWithWindows.Checked = Settings.Get("checkBoxRunWithWindows", false);
            textBoxUserEmail.Text = Settings.Get("textBoxUserEmail", "user@mail.com");
            textBoxUser.Text = Settings.Get("textBoxUser", "user");
            textBoxPassword.Text = Settings.Get("textBoxPassword", "");
            if (textBoxPassword.Text.Length > 0)
            {
                textBoxUserPasswordToEnter.Text = "passwords is set";
            }
            if (textBoxGooglePassword.Text.Length > 0)
            {
                textBoxGooglePasswordToEnter.Text = "passwords is set";
            }
            textBoxCsvDelimiter.Text = Settings.Get("CsvDelimiter", ";");
            textBoxFileNameUniversal.Text = Settings.Get("FILE_UNIVERSALS", "ComputerToolkitUniversals.bin");
            textBoxScriptsFileName.Text = Settings.Get("FILE_SCRIPTS", "ComputerToolkitScripts.bin");
            textBoxNotesFileName.Text = Settings.Get("FILE_NOTES", "ComputerToolkitNotes.bin");
            textBoxFilesFileName.Text = Settings.Get("FILE_FILES", "ComputerToolkitFiles.bin");
            textBoxFilesQuickFileName.Text = Settings.Get("FILE_FILES_QUICK", "ComputerToolkitFilesQiuck.bin");
            textBoxConnectionsFileName.Text = Settings.Get("FILE_CONNECTIONS", "ComputerToolkitConnections.bin");
            textBoxTasks.Text = Settings.Get("FILE_TASKS", "ComputerToolkitTasks.bin");
            textBoxHotkeys.Text = Settings.Get("FILE_HOTKEYS", "ComputerToolkitHotkeys.bin");
            checkBoxEnabledTasks.Checked = Settings.Get("TasksEnabled", false);


            TextBoxDirSearch.Text = Settings.Get("TextBoxDirSearch", "");
            textBoxRootDir.Text = Settings.Get("textBoxRootDir", "");
            IndexSelectedColumnHeader = Settings.Get("IndexSelectedColumnHeader", 0);

            comboBoxDropDataFormats.Text = Settings.Get("comboBoxDropDataFormats", "");

            this.Left = Settings.Get("FormMainLeft", 0);
            this.Top = Settings.Get("FormMainTop", 0);
            this.Width= Settings.Get("FormMainWidth", 1232);
            this.Height = Settings.Get("FormMainHeight", 598);
            this.TopMost = Settings.Get("checkBoxMainFormTopmost", false);

            this.checkBoxMainFormTopmost.Checked = Settings.Get("checkBoxMainFormTopmost", false);
            this.checkBoxConnectionsFormTopmost.Checked = Settings.Get("checkBoxConnectionsFormTopmost", false);

            this.checkBoxShowGroupsInConnections.Checked = Settings.Get("checkBoxShowGroupsInConnections", false);
            this.checkBoxMakeSubmenuInConnections.Checked = Settings.Get("checkBoxMakeSubmenuInConnections", false);
            textBoxProxy.Text = Settings.Get("textBoxProxy", "10.4.197.19:8080");

            textBoxTeamViewerGUIDToOpenConnection.Text = Settings.Get("textBoxTeamViewerGUIDToOpenConnection", "");

            textBoxFileExtensionsToAdd.Text = Settings.Get("textBoxFileExtensionsToAdd", "*.exe,*.jar,*.cmd,*.bat,*.com,*.htm,*.pdf");

        }
        void GetWithGridHeaders()
        {
            GridHeaderswithdataGridFindedFiles = Settings.Get("GridHeaderswithdataGridFindedFiles", "");
            GridHeaderswithDataGridMainFiles = Settings.Get("GridHeaderswithDataGridMainFiles", "");
            GridHeaderswithDataGridConnections = Settings.Get("GridHeaderswithDataGridConnections", "");
            GridHeaderswithDataGridNotes = Settings.Get("GridHeaderswithDataGridNotes", "");
            GridHeaderswithDataGridScripts = Settings.Get("GridHeaderswithDataGridScripts", "");
            GridHeaderswithDataGridUniversals = Settings.Get("GridHeaderswithDataGridUniversals", "");
            GridHeaderswithDataGridUniversalsAtom = Settings.Get("GridHeaderswithDataGridUniversalsAtom", "");
            GridHeaderswithdataGridViewHotkeys = Settings.Get("GridHeaderswithdataGridViewHotkeys", "");
            GridHeaderswithdataGridViewHotkeys = Settings.Get("GridHeaderswithdataGridViewHotkeys", "");
            GridHeaderswithDataGridUniversals = Settings.Get("GridHeaderswithDataGridUniversals", "");
            GridHeaderswithDataGridUniversalsAtom = Settings.Get("GridHeaderswithDataGridUniversalsAtom", "");
            GridHeaderswithDataGridMainFilesQ = Settings.Get("GridHeaderswithDataGridMainFilesQ", "");
            GridHeaderswithdataGridViewTasks = Settings.Get("GridHeaderswithdataGridViewTasks", "");
            GridHeaderswithdataGridViewSensors = Settings.Get("GridHeaderswithdataGridViewSensors", "");
            SetGridHeadersWidth(GridHeaderswithDataGridUniversals, dataGridViewUniversal);
            SetGridHeadersWidth(GridHeaderswithDataGridUniversalsAtom, dataGridViewUniversalItem);
            SetGridHeadersWidth(GridHeaderswithDataGridMainFilesQ, DataGridMainFilesQuick);
            SetGridHeadersWidth(GridHeaderswithdataGridViewTasks, dataGridViewTasks);
            SetGridHeadersWidth(GridHeaderswithdataGridViewSensors, dataGridViewSensors);
            SetGridHeadersWidth(GridHeaderswithdataGridFindedFiles, dataGridFindedFiles);
            SetGridHeadersWidth(GridHeaderswithDataGridMainFiles, DataGridMainFiles);
            SetGridHeadersWidth(GridHeaderswithDataGridConnections, dataGridConnections);
            SetGridHeadersWidth(GridHeaderswithDataGridNotes, dataGridNotes);
            SetGridHeadersWidth(GridHeaderswithDataGridScripts, dataGridScripts);
            SetGridHeadersWidth(GridHeaderswithDataGridUniversals, dataGridViewUniversal);
            SetGridHeadersWidth(GridHeaderswithDataGridUniversalsAtom, dataGridViewUniversalItem);
            SetGridHeadersWidth(GridHeaderswithdataGridViewHotkeys, dataGridViewHotkeys);

        }
        private void SaveSettings()
        {
            Settings.Set("comboBoxScriptsGropu", comboBoxScriptsGropu.Text);
            Settings.Set("comboBoxCurrentGroup", comboBoxCurrentGroup.Text);
            Settings.Set("onlinePassword", textBoxPassword.Text);
            Settings.Set("YandexDisk", checkBoxYandexDrive.Checked);
            //Settings.Set("YandexToken", VariablesClass.YandexToken);
            //Settings.Set("YandexToken", VariablesClass.YandexToken);
            Settings.Set("checkBoxCheckUpdate", checkBoxCheckUpdate.Checked);
            Settings.Set("checkBoxCopyMessageToclipboard", checkBoxCopyMessageToclipboard.Checked);
            Settings.Set("checkBoxGoogleEnable", checkBoxGoogleEnable.Checked);
            Settings.Set("textBoxGoogleUser", textBoxGoogleUser.Text);
            Settings.Set("textBoxGooglePassword", textBoxGooglePassword.Text);
            Settings.Set("textBoxTimeToRefreshUpdateGoogle", textBoxTimeToRefreshUpdateGoogle.Text);
            Settings.Set("textBoxTimeToUpdateGoogle", textBoxTimeToUpdateGoogle.Text);
            Settings.Set("comboBoxLocalization", comboBoxLocalization.Text);
            Settings.Set("checkBoxRunWithWindows", checkBoxRunWithWindows.Checked);
            Settings.Set("textBoxUserEmail", textBoxUserEmail.Text);
            Settings.Set("textBoxPassword", textBoxPassword.Text);
            Settings.Set("textBoxUser", textBoxUser.Text);
            Settings.Set("textBoxProxy", textBoxProxy.Text);
            Settings.Set("CsvDelimiter", textBoxCsvDelimiter.Text);
            Settings.Set("TasksEnabled", checkBoxEnabledTasks.Checked);
            Settings.Set("FILE_UNIVERSALS", textBoxFileNameUniversal.Text);
            Settings.Set("FILE_SCRIPTS", textBoxScriptsFileName.Text);
            Settings.Set("FILE_NOTES", textBoxNotesFileName.Text);
            Settings.Set("FILE_FILES", textBoxFilesFileName.Text);
            Settings.Set("FILE_FILES_QUICK", textBoxFilesQuickFileName.Text);
            Settings.Set("FILE_CONNECTIONS", textBoxConnectionsFileName.Text);
            Settings.Set("FILE_TASKS", textBoxTasks.Text);
            Settings.Set("FILE_HOTKEYS", textBoxHotkeys.Text);


            Settings.Set("SortHeaderDirection", SortHeaderDirection);
            Settings.Set("IndexSelectedColumnHeader", IndexSelectedColumnHeader);
            Settings.Set("TextBoxDirSearch", TextBoxDirSearch.Text);
            Settings.Set("textBoxRootDir", textBoxRootDir.Text);
            Settings.Set("SortHeaderName", SortHeaderName);

            Settings.Set("comboBoxDropDataFormats", comboBoxDropDataFormats.Text);


            Settings.Set("FormMainLeft", this.Left);
            Settings.Set("FormMainTop", this.Top);

            Settings.Set("FormMainWidth", this.Width);
            Settings.Set("FormMainHeight", this.Height);

            Settings.Set("checkBoxShowGroupsInConnections", this.checkBoxShowGroupsInConnections.Checked);
            Settings.Set("checkBoxMakeSubmenuInConnections", this.checkBoxMakeSubmenuInConnections.Checked);

            Settings.Set("textBoxTeamViewerGUIDToOpenConnection", textBoxTeamViewerGUIDToOpenConnection.Text);

            Settings.Set("textBoxFileExtensionsToAdd", textBoxFileExtensionsToAdd.Text);
        }
        void SetHeaderthWidth()
        {
            GridHeaderswithDataGridNotes = GetGridHeadersWidth(dataGridNotes);
            GridHeaderswithdataGridFindedFiles = GetGridHeadersWidth(dataGridFindedFiles);
            GridHeaderswithDataGridMainFiles = GetGridHeadersWidth(DataGridMainFiles);
            GridHeaderswithDataGridConnections = GetGridHeadersWidth(dataGridConnections);
            GridHeaderswithDataGridScripts = GetGridHeadersWidth(dataGridScripts);
            GridHeaderswithdataGridViewHotkeys = GetGridHeadersWidth(dataGridViewHotkeys);
            GridHeaderswithDataGridUniversals = GetGridHeadersWidth(dataGridViewUniversal);
            GridHeaderswithDataGridUniversalsAtom = GetGridHeadersWidth(dataGridViewUniversalItem);
            GridHeaderswithDataGridMainFilesQ = GetGridHeadersWidth(DataGridMainFilesQuick);
            GridHeaderswithdataGridViewTasks = GetGridHeadersWidth(dataGridViewTasks);
            GridHeaderswithdataGridViewSensors = GetGridHeadersWidth(dataGridViewSensors);
            Settings.Set("GridHeaderswithdataGridViewSensors", GridHeaderswithdataGridViewSensors);
            Settings.Set("GridHeaderswithdataGridViewHotkeys", GridHeaderswithdataGridViewHotkeys);
            Settings.Set("GridHeaderswithDataGridUniversals", GridHeaderswithDataGridUniversals);
            Settings.Set("GridHeaderswithDataGridUniversalsAtom", GridHeaderswithDataGridUniversalsAtom);
            Settings.Set("GridHeaderswithDataGridMainFilesQ", GridHeaderswithDataGridMainFilesQ);
            Settings.Set("GridHeaderswithdataGridViewTasks", GridHeaderswithdataGridViewTasks);
            Settings.Set("GridHeaderswithDataGridNotes", GridHeaderswithDataGridNotes);
            Settings.Set("GridHeaderswithdataGridFindedFiles", GridHeaderswithdataGridFindedFiles);
            Settings.Set("GridHeaderswithDataGridMainFiles", GridHeaderswithDataGridMainFiles);
            Settings.Set("GridHeaderswithDataGridConnections", GridHeaderswithDataGridConnections);
            Settings.Set("GridHeaderswithDataGridScripts", GridHeaderswithDataGridScripts);
            Settings.Set("GridHeaderswithDataGridUniversals", GridHeaderswithDataGridUniversals);
            Settings.Set("GridHeaderswithDataGridUniversalsAtom", GridHeaderswithDataGridUniversalsAtom);

        }
        private String GetGridHeadersWidth(DataGridView DGW)
        {
            string retVal = "";
            foreach (DataGridViewColumn GH in DGW.Columns)
            {
                retVal += GH.Width.ToString() + ";";
            }
            return retVal;
        }
        private void SetGridHeadersWidth(string param, DataGridView DGW)
        {
            try
            {
                string[] width = param.Split(';');
                int indx = 0;
                foreach (DataGridViewColumn GH in DGW.Columns)
                {
                    GH.Width = int.Parse(width[indx]);
                    indx++;
                }
            }
            catch { }
        }


        private void FillGroups()
        {
            foreach (String group in ProcessIcon.lists.files.listGroups)
            {
                if (comboBoxCurrentGroup.ValueMember.Contains(group) == false)
                {
                    comboBoxCurrentGroup.Items.Add(group);
                }
            }
            foreach (String group in ProcessIcon.lists.tasks.listGroups)
            {
                if (comboBoxTaskGroup.ValueMember.Contains(group) == false)
                {
                    comboBoxTaskGroup.Items.Add(group);
                }
            }
            foreach (String group in ProcessIcon.lists.scripts.listGroups)
            {
                if (comboBoxScriptsGropu.ValueMember.Contains(group) == false)
                {
                    comboBoxScriptsGropu.Items.Add(group);
                }
            }
            
        }
        #endregion
        #region ButtonsEventsChoses
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://console.developers.google.com");
        }
        private void textBoxUserPasswordToEnter_TextChanged(object sender, EventArgs e)
        {
            if (textBoxUserPasswordToEnter.Text != "passwords is set")
            {
                textBoxPassword.Text = Functions.CalculateMD5Hash(textBoxUserPasswordToEnter.Text);
                Settings.Set("textBoxPassword", textBoxPassword.Text);
            }

        }
        private void buttonRefreshConnections_Click(object sender, EventArgs e)
        {
            Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.connections));
        }
        private void buttonUpdateUniversalList_Click(object sender, EventArgs e)
        {

        }
        private void buttonRefreshUniversalList_Click(object sender, EventArgs e)
        {
            FillGridFromListUniversals();
        }
        private void buttonUpdateTasks_Click(object sender, EventArgs e)
        {
            Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.tasks));
        }
        private void buttonRefreshTasks_Click(object sender, EventArgs e)
        {
            FillGridFromListTasks();
        }
        private void buttonupdateHotkeys_Click(object sender, EventArgs e)
        {
            Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.hotkeys));
        }
        private void buttonRefreshHotkeys_Click(object sender, EventArgs e)
        {
            FillGridFromListHotkeys();
        }
        private void buttonUpdateSensors_Click(object sender, EventArgs e)
        {
            Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.sensors));
        }
        private void buttonRefreshSensors_Click(object sender, EventArgs e)
        {
            FillGridFromListSensors();
        }
        private void buttonSynchronizeAll_Click(object sender, EventArgs e)
        {
            try
            {
                //if (exchangeEngine == null) Functions.Message("Enable yandex and restart programm");
                Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.connections));
                Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.tasks));
                Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.notes));
                Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.scripts));
                Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.tasks));
                Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.hotkeys));
                Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.sensors));
                Functions.Message("All synchronized.");
            }
            catch (Exception ex)
            { Functions.Message(ex); }
        }
        private void buttonDropBoxConnect_Click(object sender, EventArgs e)
        {
            //if (yandexEngine != null)
            //{
            //    yandexEngine.Refresh("/bin/bases/");
            //    timerYandexDisk.Enabled = true;
            //}
            //else
            //{
            //    ProcessIcon.notifyIcon.BalloonTipText = "Yandex disabled.";
            //    ProcessIcon.notifyIcon.ShowBalloonTip(3);
            //}
        }
        private void buttonQporgAdd_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.filesQuick.Add(new FileItemQuick(Guid.NewGuid().ToString(), false, "", ""));
            //ProcessIcon.lists.filesQuick.SaveBase();
            FillGridFromListFilesQuick();
        }
        private void buttonInputFile_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            FolderBrowserDialog fb = new FolderBrowserDialog();
            try { fb.SelectedPath = GetDirInPath(TextBoxDirSearch.Text); }
            catch { }
            if (fb.ShowDialog() == DialogResult.OK)
            {
                TextBoxDirSearch.Text = fb.SelectedPath;
            }
        }
        private void buttonLoadExternalSettings_Click(object sender, EventArgs e)
        {
            ConnectionItem findedItem = ProcessIcon.lists.connections.Find(x => x.Guid == dataGridConnections.Rows[dataGridConnections.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
            try
            {
                Stream myStream = null;
                System.Windows.Forms.OpenFileDialog fi = new System.Windows.Forms.OpenFileDialog();
                fi.InitialDirectory = "c:\\";
                fi.Filter = "txt files (*.*)|*.*|All files (*.*)|*.*";
                fi.FilterIndex = 2;
                fi.RestoreDirectory = true;
                String externRdp;
                if (fi.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        if ((myStream = fi.OpenFile()) != null)
                        {
                            using (StreamReader sr = new StreamReader(fi.FileName))
                            {
                                findedItem.ExternalSettings = sr.ReadToEnd();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            catch (Exception)
            {
            }
            //ProcessIcon.lists.connections.SaveBase();
        }
        private void checkBoxCheckUpdate_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Set("checkBoxCheckUpdate", checkBoxCheckUpdate.Checked);
        }

        private void dataGridViewSensors_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonCheckGoogleEvents_Click(object sender, EventArgs e)
        {
            //Thread googleThread = new Thread(GoogleEngine.CheckGoogleEvents);
            //googleThread.Start();
        }
        private void textBoxGooglePasswordToEnter_TextChanged(object sender, EventArgs e)
        {
            if (textBoxGooglePasswordToEnter.Text != "passwords is set")
            {
                textBoxGooglePassword.Text = Crypting.Encrypt(textBoxGooglePasswordToEnter.Text, true);
                Settings.Set("textBoxGooglePassword", textBoxGooglePassword.Text);
            }

        }

        private void buttonGoogleSAve_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }

        private void buttonRefreshGoogleCalendar_Click(object sender, EventArgs e)
        {
            //SyncService service = new SyncService(textBoxGoogleUser.Text, textBoxGooglePassword.Text);
            //IList list = service.SynchronizeEvents();

            //dataGridViewGoogle.DataSource = list;
        }

        private void checkBoxGoogleEnable_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Set("checkBoxGoogleEnable", checkBoxGoogleEnable.Checked);
        }
        private void buttonDelSensor_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
                if (res == DialogResult.OK)
                {
                    for (int i = 1; i < 10; i++)
                        foreach (DataGridViewRow dg in dataGridViewSensors.Rows)
                        {
                            if (dg.Cells["Checked"].Value != null)
                            {
                                if ((bool)dg.Cells["Checked"].Value)
                                {
                                    ProcessIcon.lists.sensors.Remove(ProcessIcon.lists.sensors.Find(x => x.Guid == dg.Cells["Guid"].Value.ToString()));
                                }
                            }
                        }
                    ProcessIcon.lists.sensors.SaveBase();
                    FillGridFromListSensors();

                }
            }
            catch { }
        }

        private void dataGridViewUniversalItem_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextMenuStrip menu = CreateMenuUniversalsAtom();
                menu.Show(Cursor.Position);
            }
        }

        private void DataGridMainFilesQuick_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            GridHeaderswithDataGridMainFilesQ = GetGridHeadersWidth(DataGridMainFilesQuick);
        }

        private void dataGridViewTasks_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            GridHeaderswithdataGridViewTasks = GetGridHeadersWidth(dataGridViewTasks);
        }

        private void dataGridViewHotkeys_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            GridHeaderswithdataGridViewHotkeys = GetGridHeadersWidth(dataGridViewHotkeys);
        }

        private void buttonAddHotkey_Click_1(object sender, EventArgs e)
        {
            ProcessIcon.lists.hotkeys.Add(new HotkeyItem("Hotkey " + DateTime.Now.ToString(), "", DateTime.Now.ToString()));
            ProcessIcon.lists.hotkeys.SaveBase();
            FillGridFromListHotkeys();

        }

        private void buttonAddSensor_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.sensors.Add(new SensorItem("Sensor " + DateTime.Now.ToString(), "", DateTime.Now.ToString()));
            ProcessIcon.lists.sensors.SaveBase();
            FillGridFromListSensors();

        }

        private void buttonDelHotkey_Click_1(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
                if (res == DialogResult.OK)
                {
                    for (int i = 1; i < 10; i++)
                        foreach (DataGridViewRow dg in dataGridViewHotkeys.Rows)
                        {
                            if (dg.Cells["Checked"].Value != null)
                            {
                                if ((bool)dg.Cells["Checked"].Value)
                                {
                                    ProcessIcon.lists.hotkeys.Remove(ProcessIcon.lists.hotkeys.Find(x => x.Guid == dg.Cells["Guid"].Value.ToString()));
                                }
                            }
                        }
                    ProcessIcon.lists.hotkeys.SaveBase();
                    FillGridFromListHotkeys();
                }
            }
            catch { }


        }

        private void buttonOpenConn_Click(object sender, EventArgs e)
        {
            ConnectionItem findedItem = ProcessIcon.lists.connections.Find(x => x.Guid == dataGridConnections.Rows[dataGridConnections.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
            ProcessIcon.formConnection.Show();
            //ProcessIcon.formConnection.AddTab(findedItem.Adress, findedItem.Domain, findedItem.User, "", findedItem.Protocol, findedItem.Name, findedItem.Port);
            ProcessIcon.formConnection.AddTab(findedItem);
        }

        private void DataGridMainFiles_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
                Clipboard.SetText(DataGridMainFiles.Rows[DataGridMainFiles.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());

        }

        private void DataGridMainFilesQuick_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
                Clipboard.SetText(DataGridMainFilesQuick.Rows[DataGridMainFilesQuick.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());

        }

        private void dataGridConnections_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
                Clipboard.SetText(dataGridConnections.Rows[dataGridConnections.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());

        }

        private void dataGridViewUniversal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
                Clipboard.SetText(dataGridViewUniversal.Rows[dataGridViewUniversal.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
        }

        private void dataGridViewTasks_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
                Clipboard.SetText(dataGridViewTasks.Rows[dataGridViewTasks.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
        }

        private void tabControlMain_Click(object sender, EventArgs e)
        {

        }

        private void dataGridViewTasks_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextMenuStrip menu = CreateMenuTasks();
                menu.Show(Cursor.Position);
            }
        }

        private void dataGridScripts_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextMenuStrip menu = CreateMenuScripts();
                menu.Show(Cursor.Position);
            }

        }

        private void DataGridMainFiles_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextMenuStrip menu = CreateMenuFiles();
                menu.Show(Cursor.Position);
            }
        }

        private void DataGridMainFilesQuick_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextMenuStrip menu = CreateMenuFilesQ();
                menu.Show(Cursor.Position);
            }
        }

        private void dataGridConnections_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextMenuStrip menu = CreateMenuConnections();
                menu.Show(Cursor.Position);
            }
        }

        private void dataGridViewUniversal_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextMenuStrip menu = CreateMenuUniversals();
                menu.Show(Cursor.Position);
            }
        }

        private void dataGridNotes_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextMenuStrip menu = CreateMenuNotes();
                menu.Show(Cursor.Position);
            }

        }

        private void buttonUnloadUnuversal_Click(object sender, EventArgs e)
        {
            UnloadUniversal();
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            //dataGridViewHotkeys.CurrentCell.Value = Program.CurrentKey;
            dataGridViewHotkeys.CurrentCell.Value = CurrentKey;
        }

        private void dataGridViewHotkeys_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            int columnIndex = dataGridViewHotkeys.CurrentCell.ColumnIndex;
            string columnName = dataGridViewHotkeys.Columns[columnIndex].Name;
            if (columnName == "Keystring")
            {
                timer.Enabled = true;
            }
        }

        private void dataGridViewHotkeys_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.hotkeys);
            timer.Enabled = false;
        }

        private void button12_Click_1(object sender, EventArgs e)
        {
        }
        private void buttonDelHotkey_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
                if (res == DialogResult.OK)
                {
                    for (int i = 1; i < 10; i++)
                        foreach (DataGridViewRow dg in dataGridViewHotkeys.Rows)
                        {
                            if (dg.Cells["Checked"].Value != null)
                            {
                                if ((bool)dg.Cells["Checked"].Value)
                                {
                                    var ff = ProcessIcon.lists.hotkeys.Find(x => x.Guid == dg.Cells["Guid"].Value.ToString());
                                    if (ff != null) ProcessIcon.lists.hotkeys.Remove(ff);
                                }
                            }
                        }
                    FillGridFromListHotkeys();
                    //ProcessIcon.lists.hotkeys.SaveBase();
                }
            }
            catch { }
        }
        private void buttonAddHotkey_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.hotkeys.Add(new HotkeyItem("Hotkey " + DateTime.Now.ToString(), "", DateTime.Now.ToString()));
            //ProcessIcon.lists.hotkeys.SaveBase();
            FillGridFromListHotkeys();
        }
        private void dataGridViewTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            int columnIndex = dataGridViewHotkeys.CurrentCell.ColumnIndex;
            string columnName = dataGridViewHotkeys.Columns[columnIndex].Name;
        }
        private void dataGridViewHotkeys_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;
            //tb.KeyPress += new KeyPressEventHandler(dataGridViewTextBox_KeyPress);
            //e.Control.KeyPress += new KeyPressEventHandler(dataGridViewTextBox_KeyPress);
            //dataGridViewHotkeys.CurrentCell.Value = CurrentKey;
        }
        private void buttonLoadFilesFromXml_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = Functions.OpenFileDialog();
                string s = "";
                if (fileName.Length != 0)
                {
                    using (StreamReader sw = new StreamReader(fileName))
                    {
                        s = sw.ReadToEnd();
                    }
                    switch (comboBoxUnloadList.Text)
                    {
                        case "Записи блокнота":
                            List<NoteItem> notesSerialized = Functions.FromXML<List<NoteItem>>(s);
                            foreach (NoteItem fileSerialized in notesSerialized)
                            {
                                ProcessIcon.lists.notes.Add(fileSerialized);
                            }
                            ProcessIcon.lists.notes.SaveBase();
                            break;
                        case "Скрипты":
                            List<ScriptItem> ScriptsSerialized = Functions.FromXML<List<ScriptItem>>(s);
                            foreach (ScriptItem fileSerialized in ScriptsSerialized)
                            {
                                ProcessIcon.lists.scripts.Add(fileSerialized);
                            }
                            ProcessIcon.lists.scripts.SaveBase();
                            break;
                        case "Файлы (пкм)":
                            List<FileItemQuick> filesQSerialized = Functions.FromXML<List<FileItemQuick>>(s);
                            foreach (FileItemQuick fileSerialized in filesQSerialized)
                            {
                                ProcessIcon.lists.filesQuick.Add(fileSerialized);
                            }
                            foreach (FileItemQuick file in ProcessIcon.lists.filesQuick)
                            {
                                try
                                {
                                    file.IconFile = System.Drawing.Icon.ExtractAssociatedIcon(file.PathFile);
                                }
                                catch { file.IconFile = Resources.file_icon16; }
                            }
                            ProcessIcon.lists.filesQuick.SaveBase();
                            break;
                        case "Файлы (лкм)":
                            List<FileItem> filesSerialized = Functions.FromXML<List<FileItem>>(s);
                            foreach (FileItem fileSerialized in filesSerialized)
                            {
                                ProcessIcon.lists.files.Add(fileSerialized);
                            }
                            foreach (FileItem file in ProcessIcon.lists.files)
                            {
                                try
                                {
                                    file.IconFile = System.Drawing.Icon.ExtractAssociatedIcon(file.PathFileFull);
                                }
                                catch { file.IconFile = Resources.file_icon16; }
                            }
                            ProcessIcon.lists.files.SaveBase();
                            break;
                        case "Соединения":
                            List<ConnectionItem> connectsSerialized = Functions.FromXML<List<ConnectionItem>>(s);
                            foreach (ConnectionItem fileSerialized in connectsSerialized)
                            {
                                ProcessIcon.lists.connections.Add(fileSerialized);
                            }
                            ProcessIcon.lists.connections.SaveBase();
                            break;
                        case "Универсальный список":
                            List<UniversalItem> universalsSerialized = Functions.FromXML<List<UniversalItem>>(s);
                            foreach (UniversalItem fileSerialized in universalsSerialized)
                            {
                                ProcessIcon.lists.universals.Add(fileSerialized);
                            }
                            ProcessIcon.lists.universals.SaveBase();
                            break;
                    }
                }
            }
            catch { }
        }
        private void buttonSaveFilesToXml_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName;
                string str;
                switch (comboBoxUnloadList.Text)
                {
                    case "Записи блокнота":
                        fileName = Functions.SaveFileDialog();
                        if (fileName.Length == 0) break;
                        str = Functions.ToXML(ProcessIcon.lists.notes);

                        using (StreamWriter sw = new StreamWriter(fileName))
                        {
                            sw.Write(str);
                        }
                        break;
                    case "Скрипты":
                        fileName = Functions.SaveFileDialog();
                        if (fileName.Length == 0) break;
                        str = Functions.ToXML(ProcessIcon.lists.scripts);
                        using (StreamWriter sw = new StreamWriter(fileName))
                        {
                            sw.Write(str);
                        }
                        break;
                    case "Файлы (пкм)":
                        fileName = Functions.SaveFileDialog();
                        if (fileName.Length == 0) break;
                        str = Functions.ToXML(ProcessIcon.lists.filesQuick);
                        using (StreamWriter sw = new StreamWriter(fileName))
                        {
                            sw.Write(str);
                        }
                        break;
                    case "Файлы (лкм)":
                        fileName = Functions.SaveFileDialog();
                        if (fileName.Length == 0) break;
                        str = Functions.ToXML(ProcessIcon.lists.files);
                        using (StreamWriter sw = new StreamWriter(fileName))
                        {
                            sw.Write(str);
                        }
                        break;
                    case "Соединения":
                        fileName = Functions.SaveFileDialog();
                        if (fileName.Length == 0) break;
                        str = Functions.ToXML(ProcessIcon.lists.connections);
                        using (StreamWriter sw = new StreamWriter(fileName))
                        {
                            sw.Write(str);
                        }
                        break;
                    case "Универсальный список":
                        fileName = Functions.SaveFileDialog();
                        if (fileName.Length == 0) break;
                        str = Functions.ToXML(ProcessIcon.lists.universals);
                        using (StreamWriter sw = new StreamWriter(fileName))
                        {
                            sw.Write(str);
                        }
                        break;
                }

            }
            catch { }


        }

        private void comboBoxLocalization_SelectedIndexChanged(object sender, EventArgs e)
        {
            Localize();
        }

        private void checkBoxEnabledTasks_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Set("TasksEnabled", checkBoxEnabledTasks.Checked);
            ProcessIcon.timerTasks.Enabled = checkBoxEnabledTasks.Checked;
        }

        private void comboBoxDropDataFormats_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridViewUniversal_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //ProcessIcon.lists.universals.SaveBase();
        }

        private void dataGridViewUniversal_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridViewUniversal.CurrentCell != null)
                {
                    if (dataGridViewUniversal.CurrentCell.RowIndex > -1)
                    {
                        string guidtosearsh = dataGridViewUniversal.Rows[dataGridViewUniversal.CurrentCell.RowIndex].Cells["guid"].Value.ToString();
                        universalItemListCurrentGuid = ProcessIcon.lists.universals.Find(x => x.Guid == guidtosearsh).Guid;
                        FillGridFromListUniversalsList();
                    }
                }
            }
            catch { }

        }

        private void dataGridViewUniversal_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void buttonCheckUserConnect_Click(object sender, EventArgs e)
        {
            SaveSettings();


        }
        private void buttonAddUser_Click(object sender, EventArgs e)
        {

        }




        private void buttonSynchronizeNotes_Click(object sender, EventArgs e)
        {
            Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.notes));
            FillGridFromListConnections();
        }


        private void button13_Click(object sender, EventArgs e)
        {

        }

        private void checkBoxRunWithWindows_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Settings.Set("checkBoxRunWithWindows", checkBoxRunWithWindows.Checked);
                switch (checkBoxRunWithWindows.Checked)
                {
                    case false:
                        RegistryKey del = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                        del.DeleteValue(Assembly.GetEntryAssembly().GetName().Name);
                        break;
                    case true:
                        RegistryKey add = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                        add.SetValue(Assembly.GetEntryAssembly().GetName().Name, "\"" + Application.ExecutablePath.ToString().Replace("/",@"\").ToString() + "\"");
                        break;
                }
            }
            catch (Exception ex) { Functions.ShowBalloonToolTip(ex.Message); }
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptItem findedScript = ProcessIcon.lists.scripts.Find(x => x.Guid == dataGridScripts.Rows[dataGridScripts.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
                Functions.ExecuteScript(findedScript);

            }
            catch (Exception ex) {
                Functions.Message(ex);

            }
        }

        private void buttonChoseRootDir_Click_1(object sender, EventArgs e)
        {
            Stream myStream = null;
            FolderBrowserDialog fb = new FolderBrowserDialog();
            try { fb.SelectedPath = GetDirInPath(textBoxRootDir.Text); }
            catch { }
            if (fb.ShowDialog() == DialogResult.OK)
            {
                textBoxRootDir.Text = fb.SelectedPath;
            }

        }

        private void buttonFixPathFiles_Click(object sender, EventArgs e)
        {
            try
            {
                int col = 0;
                foreach (FileItem file in ProcessIcon.lists.files)
                {
                    file.PathFile = file.PathFile.Replace(textBoxRootDir.Text, "");
                    col++;
                }
                MessageBox.Show("Processed " + col + " files.");
                ProcessIcon.lists.files.SaveBase();
            }
            catch { }
        }

        private void tabPageSettings_Click(object sender, EventArgs e)
        {

        }

        private void buttonSaveToJson_Click(object sender, EventArgs e)
        {

        }


        private void dataGridViewUniversalItem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridViewUniversalItem_RowEnter(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridViewUniversalItem_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            SaveAtomsEnter();
        }



        private void buttonAtomCheckAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dg in dataGridViewUniversalItem.Rows)
            {
                dg.Cells["Checked"].Value = true;
            }
            SaveAtoms();
        }

        private void buttonAtomUnCheckAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dg in dataGridViewUniversalItem.Rows)
            {
                dg.Cells["Checked"].Value = false;
            }
            SaveAtoms();
        }

        private void buttonAtomInvertCheckAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dg in dataGridViewUniversalItem.Rows)
            {
                try
                {
                    if (dg.Cells["Checked"].Value.Equals(false))
                    {
                        dg.Cells["Checked"].Value = true;
                    }
                    else
                    {
                        dg.Cells["Checked"].Value = false;
                    }
                }
                catch { }
            }
            SaveAtoms();
        }

        private void buttonAtomCDeleteChecked_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
                if (res == DialogResult.OK)
                {
                    UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                    //UniversalItem universalItemTemp;
                    //universalItem.CopyTo(universalItemTemp,1);
                    foreach (DataGridViewRow dg in dataGridViewUniversalItem.Rows)
                    {
                        if (dg.Cells["Checked"].Value != null)
                        {
                            if ((bool)dg.Cells["Checked"].Value)
                            {
                                UniversalAtom ff = universalItem.Find(x => x.Guid == dg.Cells["Guid"].Value.ToString());
                                //universalItemTemp.Add(ff);
                                universalItem.Remove(ff);
                            }
                        }
                    }
                    //ProcessIcon.lists.universals.SaveBase();
                    FillGridFromListUniversalsList();
                }
            }
            catch { }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
                if (res == DialogResult.OK)
                {

                    UniversalItem ui = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                    ProcessIcon.lists.universals.Remove(ui);
                    //ProcessIcon.lists.universals.SaveBase();
                    FillGridFromListUniversals();
                }
            }
            catch { }
        }

        private void buttonSaveSettings_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }
        private void FillGridFromListTasks()
        {
            ProcessIcon.lists.tasks.ReadBase();
            BindingSource bs = new BindingSource();
            bs.DataSource = ProcessIcon.lists.tasks;
            dataGridViewTasks.DataSource = bs;
            bs.ResetBindings(false);
            //SetGridHeadersWidth(GridHeaderswithDataGridNotes, dataGridNotes);
            //GetWithGridHeaders();
        }

        private void buttonAddTasks_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.tasks.Add(new TaskItem("Задача " + DateTime.Now.ToString(), DateTime.Now.TimeOfDay.ToString(), "", DateTime.Now.ToString()));
            //ProcessIcon.lists.tasks.SaveBase();
            FillGridFromListTasks();
        }

        private void buttonDeleteTasks_Click(object sender, EventArgs e)
        {
            //try
            //{
            DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
            if (res == DialogResult.OK)
            {
                for (int i = 1; i < 10; i++)
                    foreach (DataGridViewRow dg in dataGridViewTasks.Rows)
                    {
                        if (dg.Cells["Checked"].Value != null)
                        {
                            if ((bool)dg.Cells["Checked"].Value)
                            {
                                var ff = ProcessIcon.lists.tasks.Find(x => x.Guid == dg.Cells["Guid"].Value.ToString());
                                ProcessIcon.lists.tasks.Remove(ff);
                            }
                        }
                    }
                ProcessIcon.lists.tasks.SaveBase();
            }
            FillGridFromListTasks();
            //}
            //catch { }
        }

        private void dataGridViewTasks_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //DataGridView cell = (DataGridView)sender;
            //dateTimePicker1.Format = DateTimePickerFormat.Time;
            //dateTimePicker1.ShowUpDown = true;
            //dateTimePicker1.Show();
        }

        private void dataGridViewTasks_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //ProcessIcon.lists.tasks.SaveBase();
        }

        private void dataGridViewTasks_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            StatusLabel.Text = e.Exception.Message;
        }

        private void StatusLabel_Click(object sender, EventArgs e)
        {
            StatusLabel.Text = "";
        }

        private void dataGridViewUniversalItem_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            StatusLabel.Text = e.Exception.Message;
        }

        private void dataGridViewUniversal_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            StatusLabel.Text = e.Exception.Message;
        }

        private void dataGridScripts_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            StatusLabel.Text = e.Exception.Message;
        }

        private void dataGridConnections_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            StatusLabel.Text = e.Exception.Message;
        }

        private void dataGridNotes_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            StatusLabel.Text = e.Exception.Message;
        }

        private void DataGridMainFilesQuick_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            StatusLabel.Text = e.Exception.Message;
        }

        private void DataGridMainFiles_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            StatusLabel.Text = e.Exception.Message;
        }

        private void dataGridFindedFiles_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            StatusLabel.Text = e.Exception.Message;
        }
        private void dataGridViewUniversalItem_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            GridHeaderswithDataGridUniversalsAtom = GetGridHeadersWidth(dataGridViewUniversalItem);
        }

        private void dataGridViewUniversal_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            GridHeaderswithDataGridUniversals = GetGridHeadersWidth(dataGridViewUniversal);
        }

        private void dataGridViewUniversalItem_DragEnter(object sender, DragEventArgs e)
        {
            try
            {
                switch (comboBoxDropDataFormats.Text)
                {
                    case "Текст":
                        GetObjectsText(e);
                        break;
                    case "Файл содержимое":
                        FilesToBytesArrays((string[])(e.Data.GetData(DataFormats.FileDrop)));
                        break;
                    case "Файл путь":
                        GetObjectsFilesPath(e);
                        break;
                    case "Файл имя файла":
                        GetObjectsFilesNamesPath(e);
                        break;
                    case "Картинка":
                        GetObjectsImages(e);
                        break;
                    case "CSV текст с разделителями":
                        CsvToList((string[])(e.Data.GetData(DataFormats.FileDrop)), e);
                        break;
                }
            }
            catch { }
        }

        private void textBoxScriptFindContent_TextChanged(object sender, EventArgs e)
        {
            SetHeaderthWidth();

            FillGridFromListScripts();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                String guid = Guid.NewGuid().ToString();
                string s = "";
                switch (comboBoxDropDataFormats.Text)
                {
                    case "Текст":
                        ProcessIcon.lists.universals.Add(new UniversalItem(s, "Universal " + DateTime.Now.ToString(), "", "", DateTime.Now.ToString()));
                        break;
                    case "Файл содержимое":
                        Byte[] ba = { 0, 0 };
                        ProcessIcon.lists.universals.Add(new UniversalItem(ba, "Universal " + DateTime.Now.ToString(), "", "", DateTime.Now.ToString()));
                        break;
                    case "Файл путь":
                        ProcessIcon.lists.universals.Add(new UniversalItem(s, "Universal " + DateTime.Now.ToString(), "", "", DateTime.Now.ToString()));
                        break;
                    case "Файл имя файла":
                        ProcessIcon.lists.universals.Add(new UniversalItem(s, "Universal " + DateTime.Now.ToString(), "", "", DateTime.Now.ToString()));
                        break;
                    case "Картинка":
                        ProcessIcon.lists.universals.Add(new UniversalItem(Properties.Resources.zero, "Universal image" + DateTime.Now.ToString(), "", "", DateTime.Now.ToString()));
                        break;
                    case "CSV текст с разделителями":
                        string[] str = { "", "" };
                        ProcessIcon.lists.universals.Add(new UniversalItem(str, "Universal CSV" + DateTime.Now.ToString(), "", "", DateTime.Now.ToString()));
                        break;
                }
                UniversalItem si = ProcessIcon.lists.universals.Find(x => x.Guid == guid);
                ProcessIcon.lists.universals.SaveBase();
                FillGridFromListUniversals();
            }
            catch { }
        }
        private void textBoxNotesFindContent_TextChanged(object sender, EventArgs e)
        {
            FillGridFromListNotes();
        }

        private void textBoxScriptFindName_TextChanged(object sender, EventArgs e)
        {
            SetHeaderthWidth();

            FillGridFromListScripts();
        }
        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
                if (res == DialogResult.OK)
                {

                    var ff = ProcessIcon.lists.scripts.Find(
            x => x.Guid == dataGridScripts.Rows[dataGridScripts.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
                    ProcessIcon.lists.scripts.Remove(ff);
                    ProcessIcon.lists.scripts.SaveBase();
                    FillGridFromListScripts();
                }
            }
            catch { }

        }

        private void DataGridMainFilesQuick_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridNotes_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //ProcessIcon.lists.notes.SaveBase();
        }

        private void dataGridScripts_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //scripts.SaveBase();
        }

        private void dataGridScripts_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            //SetGridHeadersWidth(GridHeaderswithDataGridScripts, dataGridScripts);
        }

        private void dataGridScripts_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void button9_Click(object sender, EventArgs e)
        {
            FillGridFromListScripts();
        }

        private void buttonReloadClasses_Click(object sender, EventArgs e)
        {
            Functions.Message(ProcessIcon.exchangeEngine.SynchronizeList(ProcessIcon.lists.scripts));
        }

        private void dataGridScripts_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1)
                {
                    FormScriptEditor formScriptEditor = new FormScriptEditor();
                    formScriptEditor.OpenScript(dataGridScripts.Rows[e.RowIndex].Cells["Guid"].Value.ToString(), this);
                }
            }
            catch { }

        }

        private void dataGridNotes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1)
                {
                    formNotepad.OpenNote(dataGridNotes.Rows[e.RowIndex].Cells["Guid"].Value.ToString(), this);
                }
            }
            catch { }


        }

        private void dataGridScripts_CellEnter(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridScripts_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Enter)
                {
                    if (dataGridScripts.CurrentCell.RowIndex > -1)
                    {
                        formScriptEditor.OpenScript(dataGridScripts.Rows[dataGridScripts.CurrentCell.RowIndex].Cells["Guid"].Value.ToString(), this);
                    }

                }
            }
            catch { }

        }

        private void dataGridScripts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
            }
            if (e.Control && e.KeyCode == Keys.C)
                Clipboard.SetText(dataGridScripts.Rows[dataGridScripts.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());

        }

        private void dataGridNotes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
            }
            if (e.Control && e.KeyCode == Keys.C)
                Clipboard.SetText(dataGridNotes.Rows[dataGridNotes.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());

        }

        private void dataGridNotes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (dataGridScripts.CurrentCell.RowIndex > -1)
                {
                    formNotepad.OpenNote(dataGridNotes.Rows[dataGridNotes.CurrentCell.RowIndex].Cells["Guid"].Value.ToString(), this);
                }

            }
        }

        private void tabControlMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Set("tabControlMain.SelectedIndex", tabControlMain.SelectedIndex);
            if (tabControlMain.SelectedIndex == 2)
            {
                dataGridNotes.Focus();
            }
            if (tabControlMain.SelectedIndex == 4)
            {
                dataGridScripts.Focus();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            FillGridFromListNotes();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AddFileItemQuickToList(TextBoxFileNameQuick.Text);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                Stream myStream = null;
                System.Windows.Forms.OpenFileDialog fi = new System.Windows.Forms.OpenFileDialog();
                fi.InitialDirectory = "c:\\";
                fi.Filter = "txt files (*.*)|*.*|All files (*.*)|*.*";
                fi.FilterIndex = 2;
                fi.RestoreDirectory = true;
                if (fi.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        if ((myStream = fi.OpenFile()) != null)
                        {
                            TextBoxFileNameQuick.Text = fi.FileName;
                            using (myStream)
                            {

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            catch { }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                String guid = Guid.NewGuid().ToString();
                string curUser = Settings.GetString("textBoxClientId") + ";" + Settings.GetString("textBoxUserEmail");
                string content="";
                content += "using System;"+System.Environment.NewLine;
                content += "using System.Windows.Forms;"+System.Environment.NewLine;
                content += "namespace ComputerToolkit"+System.Environment.NewLine;
                content += "{"+System.Environment.NewLine;
                content += "    public class MainClass : ComputerToolkit.Script"+System.Environment.NewLine;
                content += "    {"+System.Environment.NewLine;
                content += "        public MainClass(){}"+System.Environment.NewLine;
                content += "        public object Main()"+System.Environment.NewLine;
                content += "        {"+System.Environment.NewLine;
                content += "            MessageBox.Show(\"Helo world!\");"+System.Environment.NewLine;
                content += "            return \"\";"+System.Environment.NewLine;
                content += "        }"+System.Environment.NewLine;
                content += "    }"+System.Environment.NewLine;
                content += "};"+System.Environment.NewLine;
                ProcessIcon.lists.scripts.Add(new ScriptItem(guid, "Скрипт " + DateTime.Now.ToString(), DateTime.Now.ToString(), content, "", ScriptLanguage.CSharp, curUser));
                ScriptItem si = ProcessIcon.lists.scripts.Find(x => x.Guid == guid);
                //si.AddParameter("baseConnectionList", baseConnectionList.GetType());
                ProcessIcon.lists.scripts.SaveBase();
                FillGridFromListScripts();
            }
            catch { }


        }
        private void buttonQprogDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
                if (res == DialogResult.OK)
                {

                    for (int i = 1; i < 10; i++)
                        foreach (DataGridViewRow dg in DataGridMainFilesQuick.Rows)
                        {
                            if (dg.Cells["Checked"].Value != null)
                            {
                                if ((bool)dg.Cells["Checked"].Value)
                                {
                                    DelListItemFilesQuick(dg.Cells["Guid"].Value.ToString());
                                }
                            }
                        }
                    ProcessIcon.lists.filesQuick.SaveBase();
                    FillGridFromListFilesQuick();
                }
            }
            catch { }

        }

        private void dataGridNotes_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            GridHeaderswithDataGridNotes = GetGridHeadersWidth(dataGridNotes);
        }

        private void buttonDelSelNote_Click(object sender, EventArgs e)
        {

            DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
            if (res == DialogResult.OK)
            {
                for (int i = 1; i < 10; i++)
                    foreach (DataGridViewRow dg in dataGridNotes.Rows)
                    {
                        if (dg.Cells["Checked"].Value != null)
                        {
                            if ((bool)dg.Cells["Checked"].Value)
                            {
                                try
                                {
                                    var ff = ProcessIcon.lists.notes.Find(x => x.Guid == dg.Cells["Guid"].Value.ToString());
                                    ProcessIcon.lists.notes.Remove(ff);
                                }
                                catch { }
                            }
                        }
                    }
                ProcessIcon.lists.notes.SaveBase();
            }
            FillGridFromListNotes();

        }

        private void buttonReloadList_Click(object sender, EventArgs e)
        {
            FillGridFromListNotes();
        }

        private void buttonReloadConnections_Click(object sender, EventArgs e)
        {
            FillGridFromListConnections();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (FileItem fi in ProcessIcon.lists.files)
            {
                fi.Guid = Guid.NewGuid().ToString();
            }
            ProcessIcon.lists.files.SaveBase();
        }

        private void checkBoxShowGroupsInConnections_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Set("checkBoxShowGroupsInConnections", this.checkBoxShowGroupsInConnections.Checked);
        }

        private void checkBoxMakeSubmenuInConnections_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Set("checkBoxMakeSubmenuInConnections", this.checkBoxMakeSubmenuInConnections.Checked);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            foreach (ConnectionItem fi in ProcessIcon.lists.connections)
            {
                fi.Guid = Guid.NewGuid().ToString();
            }
            ProcessIcon.lists.connections.SaveBase();
        }
        private void checkBoxMainFormTopmost_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Set("checkBoxMainFormTopmost", this.checkBoxMainFormTopmost.Checked);
        }

        private void checkBoxConnectionsFormTopmost_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Set("checkBoxConnectionsFormTopmost", this.checkBoxConnectionsFormTopmost.Checked);
        }

        private void buttonAddNote_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.notes.Add(new NoteItem(Guid.NewGuid().ToString(), "Запись " + DateTime.Now.ToString(), DateTime.Now.ToString(), ""));
            ProcessIcon.lists.notes.SaveBase();
            FillGridFromListNotes();

        }
        private void comboBoxCurrentGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillGridFromFiles();
        }
        void FillGridFromFiles()
        {
            //SetHeaderthWidth();
            try
            {
                InvertSelection = false;
                DataGridMainFiles.DataSource = null;
                DataGridMainFiles.DataSource = GetSelectedGroup();
            }
            catch { }
            //GetWithGridHeaders();

        }
        private void DataGridMainFiles_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewColumn column in DataGridMainFiles.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.Programmatic;
            }

        }

        private void TextBoxDirSearch_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridFindedFiles_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            GridHeaderswithdataGridFindedFiles = GetGridHeadersWidth(dataGridFindedFiles);
        }

        private void DataGridMainFiles_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            GridHeaderswithDataGridMainFiles = GetGridHeadersWidth(DataGridMainFiles);
        }

        private void dataGridConnections_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            UpdateListFromGridCurrentRowConnection();
        }

        private void buttonAddConnection_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.connections.Add(new ConnectionItem("connection" + ProcessIcon.lists.connections.Count.ToString(), ""
                , "address", "user", "password", ConnectionProtocol.RDP, "3389", DateTime.Now.ToString()));
            ProcessIcon.lists.connections.SaveBase();
            FillGridFromListConnections();
        }

        private void dataGridConnections_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridConnections_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            GridHeaderswithDataGridConnections = GetGridHeadersWidth(dataGridConnections);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Удалить?", "Вопрос", MessageBoxButtons.OKCancel);
                if (res == DialogResult.OK)
                {
                    for (int i = 1; i < 10; i++)
                        foreach (DataGridViewRow dg in dataGridConnections.Rows)
                        {
                            if (dg.Cells["Checked"].Value != null)
                            {
                                if ((bool)dg.Cells["Checked"].Value)
                                {
                                    DelListItemConnections(dg.Cells["Guid"].Value.ToString());
                                }
                            }
                        }
                    ProcessIcon.lists.connections.SaveBase();
                    FillGridFromListConnections();

                }
            }
            catch { }

        }
        private void buttonDelSelected_Click(object sender, EventArgs e)
        {

        }

        private void buttonChoseRootDir_Click(object sender, EventArgs e)
        {
            try
            {
                Stream myStream = null;
                FolderBrowserDialog fb = new FolderBrowserDialog();
                try { fb.SelectedPath = GetDirInPath(textBoxRootDir.Text); }
                catch { }
                if (fb.ShowDialog() == DialogResult.OK)
                {
                    textBoxRootDir.Text = fb.SelectedPath;
                }
            }
            catch { }

        }


        private void DataGridMainFiles_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            UpdateListFromGridCurrentRow();
        }
        private void buttonMoveCheckedToBase_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Перенести?", "Вопрос", MessageBoxButtons.OKCancel);
                if (res == DialogResult.OK)
                {
                    foreach (DataGridViewRow dg in dataGridFindedFiles.Rows)
                    {
                        bool Checked;
                        Icon FileIcon;
                        String GuidString;
                        String PathFull;
                        String GroupFile;
                        String Comments;
                        String CompanyName;
                        String FileDescription;
                        String FileName;
                        String FileVersion;
                        String OriginalFilename;
                        String ProductName;
                        if (dg.Cells["Checked"].Value != null)
                        {
                            Checked = Boolean.Parse(GetStringAndTest(dg.Cells["Checked"].Value));
                        }
                        else
                        {
                            Checked = false;
                        }
                        if (Checked)
                        {
                            if (dg.Cells["Path"].Value != null)
                            {
                                PathFull = GetStringAndTest(dg.Cells["Path"].Value);
                                GroupFile = GetGroupFile(PathFull);
                                //Guid = GetStringAndTest(dg.Cells["Guid"].Value);
                                Name = GetStringAndTest(dg.Cells["FileDescription"].Value);
                                FileIcon = GetStringAndTestIcon(System.Drawing.Icon.ExtractAssociatedIcon(PathFull));
                                FileVersion = GetStringAndTest(dg.Cells["FileVersion"].Value);
                                Comments = GetStringAndTest(dg.Cells["Comments"].Value);
                                CompanyName = GetStringAndTest(dg.Cells["CompanyName"].Value);
                                FileDescription = GetStringAndTest(dg.Cells["FileDescription"].Value);
                                FileName = GetStringAndTest(dg.Cells["FileName"].Value);
                                OriginalFilename = GetStringAndTest(dg.Cells["OriginalFilename"].Value);
                                ProductName = GetStringAndTest(dg.Cells["ProductName"].Value);
                                if (Name.Length == 0)
                                {
                                    Name = GetFileNameInPath(PathFull); ;
                                }
                                if (FileIcon != null)
                                {
                                    string rootdir = Settings.GetString("textBoxRootDir");
                                    string Path="";
                                    if (rootdir.Length > 0)
                                    {
                                        Path = PathFull.Replace(rootdir, "");
                                    }
                                    else
                                    {
                                        Path = PathFull.Replace(Application.StartupPath.ToString() + "\\bin\\", "\\");
                                    }
                                    ProcessIcon.lists.files.Add(new FileItem(Guid.NewGuid().ToString(), Checked, Name.ToString(), GroupFile, FileIcon, Path, PathFull, Comments, CompanyName, FileDescription, FileName, FileVersion, OriginalFilename, ProductName));
                                }
                            }
                        }
                    }
                    FillBaseList();
                    ProcessIcon.lists.files.SaveBase();
                }
            }
            catch { }
        }
        private void dataGridViewHotkeys_KeyPress(object sender, KeyPressEventArgs e)
        {
            //CurrentKey = Control.ModifierKeys
        }

        private void ButtonFindFiles_Click(object sender, EventArgs e)
        {
            if (FindThread != null)
            {
                if (FindThread.ThreadState == System.Threading.ThreadState.Stopped)
                {
                    dataGridFindedFiles.Rows.Clear();
                    FindThread = new Thread(search_files);
                    FindThread.Start();
                }
                else
                {
                    FindThread.Abort();
                }
            }
            else
            {
                FindThread = new Thread(search_files);
                FindThread.Start();
            }
        }


        private void gHook_KeyDown(object sender, KeyEventArgs e)
        {
            //throw new NotImplementedException();
        }
        #endregion
        #region FindFiles
        private void search_files()
        {
            string[] Extensions = Settings.Get("textBoxFileExtensionsToAdd", "").Split(',');
            foreach(string extension in Extensions)
            {
                FindFiles(TextBoxDirSearch.Text, extension);
            }

        }
        private delegate void SetDGVValueDelegate(string s);
        public void FindInDir(DirectoryInfo dir, string pattern, bool recursive)
        {
            foreach (FileInfo file in dir.GetFiles(pattern))
            {
                AddItemToList(file.FullName);
            }
            if (recursive)
            {
                foreach (DirectoryInfo subdir in dir.GetDirectories())
                {
                    this.FindInDir(subdir, pattern, recursive);
                }
            }
        }
        public void AddItemToList(string s)
        {
            try
            {
                if (dataGridFindedFiles.InvokeRequired)
                {
                    dataGridFindedFiles.Invoke(new SetDGVValueDelegate(AddItemToList), s);
                }
                else
                {
                    int idx = dataGridFindedFiles.Rows.Add();
                    DataGridViewRow row = dataGridFindedFiles.Rows[idx];
                    row.Cells["Checked"].Value = false;
                    row.Cells["FileIcon"].Value = System.Drawing.Icon.ExtractAssociatedIcon(s);
                    row.Cells["Path"].Value = FileVersionInfo.GetVersionInfo(s).FileName;
                    row.Cells["FileName"].Value = GetFileNameInPath(s);//GetRootPath(s);
                    row.Cells["FileSize"].Value = (int)(new FileInfo(s).Length / 1024);
                    row.Cells["FileDate"].Value = new FileInfo(s).CreationTime.ToString();
                    row.Cells["Comments"].Value = FileVersionInfo.GetVersionInfo(s).Comments;
                    row.Cells["CompanyName"].Value = FileVersionInfo.GetVersionInfo(s).CompanyName;
                    row.Cells["FileDescription"].Value = FileVersionInfo.GetVersionInfo(s).FileDescription;
                    //row.Cells["FileName"].Value = FileVersionInfo.GetVersionInfo(s).FileName;
                    row.Cells["FileVersion"].Value = FileVersionInfo.GetVersionInfo(s).FileVersion;
                    row.Cells["OriginalFilename"].Value = FileVersionInfo.GetVersionInfo(s).OriginalFilename;
                    row.Cells["ProductName"].Value = FileVersionInfo.GetVersionInfo(s).ProductName;
                }
            }
            catch { }
        }
        public void FindFiles(string dir, string pattern)
        {
            this.FindInDir(new DirectoryInfo(dir), pattern, true);
        }
        //**************************************************************
        #endregion
        #region Sotring
        private List<FileItem> GetSelectedGroup()
        {
            if (comboBoxCurrentGroup.Text == "Невыбрано")
            {
                return ProcessIcon.lists.files;
            }
            else
            {
                return ProcessIcon.lists.files.FindAll(x => x.Group == comboBoxCurrentGroup.Text);
            }
        }
        private SortOrder GetOrderByHeaderGrid(String nameHeader)
        {
            return DataGridMainFiles.Columns[nameHeader].HeaderCell.SortGlyphDirection;
        }
        private void DataGridMainFiles_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            InvertSelection = true;
            SortGrid(e.ColumnIndex);
            IndexSelectedColumnHeader = e.ColumnIndex;
        }
        private void SortGrid(int selectedColumnIndex)
        {
            try //fuck it hard code
            {
                List<FileItem> ListToSort = GetSelectedGroup();
                string strColumnName;
                if (DataGridMainFiles.Columns[selectedColumnIndex].Name.Length > 0)
                {
                    strColumnName = DataGridMainFiles.Columns[selectedColumnIndex].Name;
                }
                else
                {
                    strColumnName = SortHeaderName;
                }
                switch (strColumnName)
                {
                    case "Checked":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.Checked.CompareTo(b.Checked));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.Checked.CompareTo(a.Checked));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "FileGroup":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.Group.CompareTo(b.Group));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.Group.CompareTo(a.Group));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "NameFile":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.Name.CompareTo(b.Name));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.Name.CompareTo(a.Name));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "PathFile":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.PathFile.CompareTo(b.PathFile));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.PathFile.CompareTo(a.PathFile));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "PathFileFull":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.PathFileFull.CompareTo(b.PathFileFull));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.PathFileFull.CompareTo(a.PathFileFull));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "Comments":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.Comments.CompareTo(b.Comments));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.Comments.CompareTo(a.Comments));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "CompanyName":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.CompanyName.CompareTo(b.CompanyName));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.CompanyName.CompareTo(a.CompanyName));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "FileDescription":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.FileDescription.CompareTo(b.FileDescription));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.FileDescription.CompareTo(a.FileDescription));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "FileName":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.FileName.CompareTo(b.FileName));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.FileName.CompareTo(a.FileName));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "FileVersion":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.FileVersion.CompareTo(b.FileVersion));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.FileVersion.CompareTo(a.FileVersion));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "OriginalFilename":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.OriginalFilename.CompareTo(b.OriginalFilename));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.OriginalFilename.CompareTo(a.OriginalFilename));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "ProductName":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.ProductName.CompareTo(b.ProductName));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.ProductName.CompareTo(a.ProductName));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    case "UrlProgrammToUpdate":
                        if (SortHeaderDirection == "Desc")
                        {
                            ListToSort.Sort((a, b) => a.UrlProgrammToUpdate.CompareTo(b.UrlProgrammToUpdate));
                            SortHeaderDirection = "Asc";
                        }
                        else
                        {
                            ListToSort.Sort((a, b) => b.UrlProgrammToUpdate.CompareTo(a.UrlProgrammToUpdate));
                            SortHeaderDirection = "Desc";
                        }
                        break;

                        break;
                    default:
                        break;
                }
                DataGridMainFiles.DataSource = null;
                DataGridMainFiles.DataSource = ListToSort;
                SetGridHeadersWidth(GridHeaderswithDataGridMainFiles, DataGridMainFiles);
            }
            catch { }

        }
        #endregion
        #region MenusLists
        private ContextMenuStrip CreateMenuNotes()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem itemQuickNote = new ToolStripMenuItem();
            itemQuickNote.Text = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Name = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Tag = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Image = Resources.copy.ToBitmap();
            itemQuickNote.Click += new System.EventHandler(MenuQuickNote_CopyGuidNotes);
            menu.Items.Add(itemQuickNote);
            return menu;
        }
        private void MenuQuickNote_CopyGuidNotes(object sender, EventArgs e)
        {
            Clipboard.SetText(dataGridNotes.Rows[dataGridNotes.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
        }
        private ContextMenuStrip CreateMenuScripts()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem itemQuickNote = new ToolStripMenuItem();
            itemQuickNote.Text = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Name = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Tag = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Image = Resources.copy.ToBitmap();
            itemQuickNote.Click += new System.EventHandler(MenuQuickNote_CopyGuidScripts);
            menu.Items.Add(itemQuickNote);

            ToolStripMenuItem itemScript = new ToolStripMenuItem();
            itemScript.Text = "Использовать в меню";
            itemScript.Name = "Использовать в меню";
            itemScript.Tag = "Использовать в меню";
            itemScript.Image = Resources.copy.ToBitmap();
            itemScript.Click += new System.EventHandler(MenuScriptUseInMenu_CopyGuidScripts);
            menu.Items.Add(itemScript);

            ToolStripMenuItem itemScript2 = new ToolStripMenuItem();
            itemScript2.Text = "Глобальный скрипт";
            itemScript2.Name = "Глобальный скрипт";
            itemScript2.Tag = "Глобальный скрипт";
            itemScript2.Image = Resources.copy.ToBitmap();
            itemScript2.Click += new System.EventHandler(MenuScriptGlobal_CopyGuidScripts);
            menu.Items.Add(itemScript2);

            return menu;
        }

        private void MenuScriptGlobal_CopyGuidScripts(object sender, EventArgs e)
        {
            ScriptItem findedScript = ProcessIcon.lists.scripts.Find(x => x.Guid == dataGridScripts.Rows[dataGridScripts.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
            findedScript.Global = !findedScript.Global;
            ProcessIcon.lists.scripts.SaveBase();
        }

        private void MenuScriptUseInMenu_CopyGuidScripts(object sender, EventArgs e)
        {
            ScriptItem findedScript = ProcessIcon.lists.scripts.Find(x => x.Guid == dataGridScripts.Rows[dataGridScripts.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
            findedScript.UseInMenu = !findedScript.UseInMenu;
            ProcessIcon.lists.scripts.SaveBase();
            //FillGridFromListScripts();
        }
        private void MenuQuickNote_CopyGuidScripts(object sender, EventArgs e)
        {
            Clipboard.SetText(dataGridScripts.Rows[dataGridScripts.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
        }
        private ContextMenuStrip CreateMenuFiles()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem itemQuickNote = new ToolStripMenuItem();
            itemQuickNote.Text = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Name = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Tag = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Image = Resources.copy.ToBitmap();
            itemQuickNote.Click += new System.EventHandler(MenuQuickNote_CopyGuidFiles);
            menu.Items.Add(itemQuickNote);
            return menu;
        }
        private void MenuQuickNote_CopyGuidFiles(object sender, EventArgs e)
        {
            Clipboard.SetText(DataGridMainFiles.Rows[DataGridMainFiles.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
        }
        private ContextMenuStrip CreateMenuFilesQ()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem itemQuickNote = new ToolStripMenuItem();
            itemQuickNote.Text = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Name = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Tag = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Image = Resources.copy.ToBitmap();
            itemQuickNote.Click += new System.EventHandler(MenuQuickNote_CopyGuidFilesQ);
            menu.Items.Add(itemQuickNote);
            return menu;
        }
        private void MenuQuickNote_CopyGuidFilesQ(object sender, EventArgs e)
        {
            Clipboard.SetText(DataGridMainFilesQuick.Rows[DataGridMainFilesQuick.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
        }
        private ContextMenuStrip CreateMenuConnections()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem itemQuickNote = new ToolStripMenuItem();
            itemQuickNote.Text = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Name = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Tag = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Image = Resources.copy.ToBitmap();
            itemQuickNote.Click += new System.EventHandler(MenuQuickNote_CopyGuidConnections);
            menu.Items.Add(itemQuickNote);
            return menu;
        }
        private void MenuQuickNote_CopyGuidConnections(object sender, EventArgs e)
        {
            Clipboard.SetText(dataGridConnections.Rows[dataGridConnections.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
        }
        private ContextMenuStrip CreateMenuTasks()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem itemQuickNote = new ToolStripMenuItem();
            itemQuickNote.Text = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Name = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Tag = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Image = Resources.copy.ToBitmap();
            itemQuickNote.Click += new System.EventHandler(MenuQuickNote_CopyGuidTasks);
            menu.Items.Add(itemQuickNote);
            return menu;
        }
        private void MenuQuickNote_CopyGuidTasks(object sender, EventArgs e)
        {
            Clipboard.SetText(dataGridViewTasks.Rows[dataGridViewTasks.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
        }

        private ContextMenuStrip CreateMenuUniversals()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem itemQuickNote = new ToolStripMenuItem();
            itemQuickNote.Text = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Name = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Tag = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Image = Resources.copy.ToBitmap();
            itemQuickNote.Click += new System.EventHandler(MenuQuickNote_CopyGuidUniversals);
            menu.Items.Add(itemQuickNote);
            return menu;
        }
        private void MenuQuickNote_CopyGuidUniversals(object sender, EventArgs e)
        {
            Clipboard.SetText(dataGridViewUniversal.Rows[dataGridViewUniversal.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
        }
        private ContextMenuStrip CreateMenuUniversalsAtom()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem itemQuickNote = new ToolStripMenuItem();
            itemQuickNote.Text = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Name = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Tag = "Копировать Guid (Ctrl+C)";
            itemQuickNote.Image = Resources.copy.ToBitmap();
            itemQuickNote.Click += new System.EventHandler(MenuQuickNote_CopyGuidUniversalsAtom);
            menu.Items.Add(itemQuickNote);
            ToolStripMenuItem itemUnlToFileUniversal = new ToolStripMenuItem();
            itemUnlToFileUniversal.Text = "Выгрузить в файл";
            itemUnlToFileUniversal.Name = "Выгрузить в файл";
            itemUnlToFileUniversal.Tag = "Выгрузить в файл";
            itemUnlToFileUniversal.Image = Resources.copy.ToBitmap();
            itemUnlToFileUniversal.Click += new System.EventHandler(MenuQuickNote_UnlToFileUniversal);
            menu.Items.Add(itemUnlToFileUniversal);
            return menu;
        }

        private void MenuQuickNote_UnlToFileUniversal(object sender, EventArgs e)
        {
            UnloadUniversal();
        }
        private void MenuQuickNote_CopyGuidUniversalsAtom(object sender, EventArgs e)
        {
            Clipboard.SetText(dataGridViewUniversalItem.Rows[dataGridViewUniversalItem.CurrentCell.RowIndex].Cells["Guid"].Value.ToString());
        }

        #endregion MenusLists
        #region FILLS_LISTT_ETC
        private void FillGridFromListFilesQuick()
        {
            //SetHeaderthWidth();
            ProcessIcon.lists.filesQuick.ReadBase();
            BindingSource bs = new BindingSource();
            bs.DataSource = ProcessIcon.lists.filesQuick;
            DataGridMainFilesQuick.DataSource = bs;
            bs.ResetBindings(false);
            //SetGridHeadersWidth(GridHeaderswithDataGridNotes, dataGridNotes);
            //GetWithGridHeaders();
        }
        private void FillGridFromListNotes()
        {
            ProcessIcon.lists.notes.ReadBase();
            BindingSource bs = new BindingSource();
            bs.DataSource = GetSelectedNotes();
            dataGridNotes.DataSource = bs;
            bs.ResetBindings(false);
            //SetGridHeadersWidth(GridHeaderswithDataGridNotes, dataGridNotes);
            //GetWithGridHeaders();
        }
        private void FillGridFromListScripts()
        {
            ProcessIcon.lists.scripts.ReadBase();
            BindingSource bs = new BindingSource();
            bs.DataSource = GetSelectedScripts();
            dataGridScripts.DataSource = bs;
            bs.ResetBindings(false);
            //GetWithGridHeaders();
        }
        private void FillGridFromListUniversals()
        {
            try
            {
                ProcessIcon.lists.universals.ReadBase();
                BindingSource bs = new BindingSource();
                bs.DataSource = GetSelectedUniversal();
                dataGridViewUniversal.DataSource = bs;
                bs.ResetBindings(false);
                foreach (DataGridViewColumn col in dataGridViewUniversal.Columns)
                {
                    if (col.Name.ToString() == "Name")
                    {
                        col.ReadOnly = false;
                    }
                    else
                    {
                        col.ReadOnly = true;
                    }
                }
                SetGridHeadersWidth(GridHeaderswithDataGridUniversals, dataGridViewUniversal);
            }
            catch { }
        }
        private void FillGridFromListUniversalsList()
        {
            try
            {
                ProcessIcon.lists.universals.ReadBase();
                dataGridViewUniversalItem.Rows.Clear();
                int countStringItems = 0;//получим макс количество столбцов с csv
                UniversalItem ui = GetSelectedUniversalItem();
                if (ui != null)
                {
                    dataGridViewUniversalItem.Columns.Clear();
                    foreach (UniversalAtom atom in ui)
                    {
                        if (atom.Type == AtomType.Boolean)
                        {
                            DataGridViewCheckBoxColumn columnCheck = new DataGridViewCheckBoxColumn();
                            columnCheck.Name = "Checked";
                            dataGridViewUniversalItem.Columns.Add(columnCheck);
                            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                            column.ReadOnly = false;
                            column.Name = "Name";
                            dataGridViewUniversalItem.Columns.Add(column);
                            DataGridViewTextBoxColumn columnName = new DataGridViewTextBoxColumn();
                            columnName.ReadOnly = false;
                            columnName.Name = "Value";
                            dataGridViewUniversalItem.Columns.Add(columnName);
                            DataGridViewTextBoxColumn column2 = new DataGridViewTextBoxColumn();
                            column2.ReadOnly = true;
                            column2.Name = "Guid";
                            dataGridViewUniversalItem.Columns.Add(column2);
                            break;
                        }
                        if (atom.Type == AtomType.Byte)
                        {
                            DataGridViewCheckBoxColumn columnCheck = new DataGridViewCheckBoxColumn();
                            columnCheck.Name = "Checked";
                            dataGridViewUniversalItem.Columns.Add(columnCheck);
                            DataGridViewTextBoxColumn columnName = new DataGridViewTextBoxColumn();
                            columnName.ReadOnly = false;
                            columnName.Name = "Name";
                            dataGridViewUniversalItem.Columns.Add(columnName);
                            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                            column.ReadOnly = false;
                            column.Name = "Value";
                            dataGridViewUniversalItem.Columns.Add(column);
                            DataGridViewTextBoxColumn column2 = new DataGridViewTextBoxColumn();
                            column2.ReadOnly = true;
                            column2.Name = "Guid";
                            dataGridViewUniversalItem.Columns.Add(column2);
                            break;
                        }
                        if (atom.Type == AtomType.ByteArray)
                        {
                            DataGridViewCheckBoxColumn columnCheck = new DataGridViewCheckBoxColumn();
                            columnCheck.Name = "Checked";
                            dataGridViewUniversalItem.Columns.Add(columnCheck);
                            DataGridViewTextBoxColumn columnName = new DataGridViewTextBoxColumn();
                            columnName.ReadOnly = false;
                            columnName.Name = "Name";
                            dataGridViewUniversalItem.Columns.Add(columnName);
                            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                            column.ReadOnly = false;
                            column.Name = "Value";
                            dataGridViewUniversalItem.Columns.Add(column);
                            DataGridViewTextBoxColumn column2 = new DataGridViewTextBoxColumn();
                            column2.ReadOnly = true;
                            column2.Name = "Guid";
                            dataGridViewUniversalItem.Columns.Add(column2);
                            break;
                        }
                        if (atom.Type == AtomType.Icon)
                        {
                            DataGridViewCheckBoxColumn columnCheck = new DataGridViewCheckBoxColumn();
                            columnCheck.Name = "Checked";
                            dataGridViewUniversalItem.Columns.Add(columnCheck);
                            DataGridViewTextBoxColumn columnName = new DataGridViewTextBoxColumn();
                            columnName.ReadOnly = false;
                            columnName.Name = "Name";
                            dataGridViewUniversalItem.Columns.Add(columnName);
                            DataGridViewImageColumn column = new DataGridViewImageColumn();
                            column.ReadOnly = false;
                            column.Name = "Value";
                            dataGridViewUniversalItem.Columns.Add(column);
                            DataGridViewTextBoxColumn column2 = new DataGridViewTextBoxColumn();
                            column2.ReadOnly = true;
                            column2.Name = "Guid";
                            dataGridViewUniversalItem.Columns.Add(column2);
                            break;
                        }
                        if (atom.Type == AtomType.Image)
                        {
                            DataGridViewCheckBoxColumn columnCheck = new DataGridViewCheckBoxColumn();
                            columnCheck.Name = "Checked";
                            dataGridViewUniversalItem.Columns.Add(columnCheck);
                            DataGridViewTextBoxColumn columnName = new DataGridViewTextBoxColumn();
                            columnName.ReadOnly = false;
                            columnName.Name = "Name";
                            dataGridViewUniversalItem.Columns.Add(columnName);
                            DataGridViewImageColumn column = new DataGridViewImageColumn();
                            column.Name = "Value";
                            column.ImageLayout = DataGridViewImageCellLayout.Zoom;
                            dataGridViewUniversalItem.Columns.Add(column);
                            DataGridViewTextBoxColumn column2 = new DataGridViewTextBoxColumn();
                            column2.Name = "Guid";
                            dataGridViewUniversalItem.Columns.Add(column2);
                            break;
                        }
                        if (atom.Type == AtomType.Integer)
                        {
                            DataGridViewCheckBoxColumn columnCheck = new DataGridViewCheckBoxColumn();
                            columnCheck.Name = "Checked";
                            dataGridViewUniversalItem.Columns.Add(columnCheck);
                            DataGridViewTextBoxColumn columnName = new DataGridViewTextBoxColumn();
                            columnName.ReadOnly = false;
                            columnName.Name = "Name";
                            dataGridViewUniversalItem.Columns.Add(columnName);
                            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                            column.ReadOnly = false;
                            column.Name = "Value";
                            dataGridViewUniversalItem.Columns.Add(column);
                            DataGridViewTextBoxColumn column2 = new DataGridViewTextBoxColumn();
                            column2.ReadOnly = true;
                            column2.Name = "Guid";
                            dataGridViewUniversalItem.Columns.Add(column2);
                            break;
                        }
                        if (atom.Type == AtomType.String)
                        {
                            DataGridViewCheckBoxColumn columnCheck = new DataGridViewCheckBoxColumn();
                            columnCheck.Name = "Checked";
                            dataGridViewUniversalItem.Columns.Add(columnCheck);
                            DataGridViewTextBoxColumn columnName = new DataGridViewTextBoxColumn();
                            columnName.ReadOnly = false;
                            columnName.Name = "Name";
                            dataGridViewUniversalItem.Columns.Add(columnName);
                            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                            column.ReadOnly = false;
                            column.Name = "Value";
                            dataGridViewUniversalItem.Columns.Add(column);
                            DataGridViewTextBoxColumn column2 = new DataGridViewTextBoxColumn();
                            column2.ReadOnly = true;
                            column2.Name = "Guid";
                            dataGridViewUniversalItem.Columns.Add(column2);
                            break;
                        }
                        if (atom.Type == AtomType.CSV)
                        {
                            DataGridViewCheckBoxColumn columnCheck = new DataGridViewCheckBoxColumn();
                            columnCheck.Name = "Checked";
                            dataGridViewUniversalItem.Columns.Add(columnCheck);
                            foreach (UniversalAtom atomMax in ui)
                            {
                                if (countStringItems < atomMax.CSV.Length) countStringItems = atomMax.CSV.Length;
                            }
                            for (int i = 0; i <= countStringItems; i++)
                            {
                                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                                column.ReadOnly = false;
                                column.Name = "column" + i.ToString();
                                dataGridViewUniversalItem.Columns.Add(column);
                            }
                            DataGridViewTextBoxColumn column2 = new DataGridViewTextBoxColumn();
                            column2.ReadOnly = true;
                            column2.Name = "Guid";
                            dataGridViewUniversalItem.Columns.Add(column2);
                            break;
                        }
                    }
                    foreach (UniversalAtom atom in ui)
                    {
                        if (atom.Type == AtomType.Boolean)
                        {
                        }
                        switch (atom.Type)
                        {
                            case AtomType.Boolean:
                                dataGridViewUniversalItem.Rows.Add(atom.Checked, atom.Name, atom.Boolean, atom.Guid);
                                break;
                            case AtomType.Byte:
                                dataGridViewUniversalItem.Rows.Add(atom.Checked, atom.Name, atom.Byte, atom.Guid);
                                break;
                            case AtomType.ByteArray:
                                dataGridViewUniversalItem.Rows.Add(atom.Checked, atom.Name, "byte_array: " + atom.ByteArray.Length.ToString() + " bytes", atom.Guid);
                                break;
                            case AtomType.Icon:
                                dataGridViewUniversalItem.Rows.Add(atom.Checked, atom.Name, atom.Icon, atom.Guid);
                                break;
                            case AtomType.Image:
                                dataGridViewUniversalItem.Rows.Add(atom.Checked, atom.Name, atom.Image, atom.Guid);
                                break;
                            case AtomType.Integer:
                                dataGridViewUniversalItem.Rows.Add(atom.Checked, atom.Name, atom.Integer, atom.Guid);
                                break;
                            case AtomType.String:
                                dataGridViewUniversalItem.Rows.Add(atom.Checked, atom.Name, atom.String, atom.Guid);
                                break;
                            case AtomType.CSV:
                                string[] rowStr = new string[countStringItems + 3];
                                rowStr[0] = atom.Checked.ToString();
                                rowStr[countStringItems + 2] = atom.Guid;
                                int idx = 1;
                                for (int i = 0; i < countStringItems; i++)
                                {
                                    try
                                    {
                                        rowStr[idx] = atom.CSV[idx - 1];
                                        idx++;
                                    }
                                    catch { }
                                }
                                dataGridViewUniversalItem.Rows.Add(rowStr);
                                break;
                        }
                    }
                    SetGridHeadersWidth(GridHeaderswithDataGridUniversalsAtom, dataGridViewUniversalItem);
                }
            }
            catch { }
        }
        private void FillBaseList()
        {
            try
            {
                DataGridMainFiles.DataSource = null;
                DataGridMainFiles.DataSource = GetSelectedGroup();
                SetGridHeadersWidth(GridHeaderswithDataGridMainFiles, DataGridMainFiles);
            }
            catch { }
        }

        private void FillGridFromList()
        {
            try
            {
                DataGridMainFiles.DataSource = null;
                DataGridMainFiles.DataSource = GetSelectedGroup();
                SetGridHeadersWidth(GridHeaderswithDataGridMainFiles, DataGridMainFiles);
            }
            catch { }

        }
        private void FillGridFromListHotkeys()
        {
            try
            {
                ProcessIcon.lists.hotkeys.ReadBase();
                BindingSource bs = new BindingSource();
                bs.DataSource = ProcessIcon.lists.hotkeys;
                dataGridViewHotkeys.DataSource = bs;
                bs.ResetBindings(false);
            }
            catch { }

        }
        private void FillGridFromListSensors()
        {
            try
            {
                ProcessIcon.lists.sensors.ReadBase();
                BindingSource bs = new BindingSource();
                bs.DataSource = ProcessIcon.lists.sensors;
                dataGridViewSensors.DataSource = bs;
                bs.ResetBindings(false);
            }
            catch { }

        }
        private void FillGridFromListConnections()
        {
            try
            {
                ProcessIcon.lists.connections.ReadBase();
                BindingSource bs = new BindingSource();
                bs.DataSource = ProcessIcon.lists.connections;
                //dataGridConnections.DataSource = null;
                dataGridConnections.DataSource = bs;
                bs.ResetBindings(false);
                SetGridHeadersWidth(GridHeaderswithDataGridConnections, dataGridConnections);
            }
            catch { }

        }

        #endregion FILLS_LISTT_ETC
        #region GETS
        private void GetObjectsText(DragEventArgs e)
        {
            try
            {
                string text = ((string)(e.Data.GetData(DataFormats.Text)));
                UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                universalItem.Add(new UniversalAtom(text));
                ProcessIcon.lists.universals.SaveBase();
                FillGridFromListUniversalsList();
            }
            catch { }
        }
        private AtomType GetTypeDragObject(DragEventArgs e)
        {
            try
            {
                var FileDrop = e.Data.GetData(DataFormats.FileDrop);
                var Bitmap = e.Data.GetData(DataFormats.Bitmap);
                var CommaSeparatedValue = e.Data.GetData(DataFormats.CommaSeparatedValue);
                var Dib = e.Data.GetData(DataFormats.Dib);
                var OemText = e.Data.GetData(DataFormats.OemText);
                var StringFormat = e.Data.GetData(DataFormats.StringFormat);
                var SymbolicLink = e.Data.GetData(DataFormats.SymbolicLink);
                var Text = e.Data.GetData(DataFormats.Text);
                var Tiff = e.Data.GetData(DataFormats.Tiff);
                var UnicodeText = e.Data.GetData(DataFormats.UnicodeText);
                if (FileDrop != null) return AtomType.ByteArray;
                if (Text != null) return AtomType.String;
                if (Bitmap != null) return AtomType.Image;
                if (CommaSeparatedValue != null) return AtomType.CSV;
                if (Dib != null) return AtomType.Image;
                if (OemText != null) return AtomType.String;
                if (StringFormat != null) return AtomType.String;
                if (SymbolicLink != null) return AtomType.String;
                if (Tiff != null) return AtomType.Image;
                if (UnicodeText != null) return AtomType.String;
            }
            catch { }
            return AtomType.Null;
        }
        private void GetDropDataFormats()
        {
            comboBoxDropDataFormats.Items.Add("Текст");
            comboBoxDropDataFormats.Items.Add("Файл содержимое");
            comboBoxDropDataFormats.Items.Add("Файл путь");
            comboBoxDropDataFormats.Items.Add("Файл имя файла");
            comboBoxDropDataFormats.Items.Add("Картинка");
            comboBoxDropDataFormats.Items.Add("CSV текст с разделителями");
        }
        Control GetControlByName(string Name)
        {
            foreach (Control c in this.Controls)
                if (c.Name == Name)
                    return c;

            return null;
        }
        private List<NoteItem> GetSelectedNotes()
        {
            try
            {
                List<NoteItem> tempList;
                tempList = ProcessIcon.lists.notes;
                if (textBoxNotesFindName.Text.Length > 0)
                {
                    tempList = ProcessIcon.lists.notes.FindAll(x => x.Name.IndexOf(textBoxNotesFindName.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (textBoxNotesFindContent.Text.Length > 0)
                {
                    tempList = tempList.FindAll(x => x.Content.IndexOf(textBoxNotesFindContent.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                return tempList;
            }
            catch { }
            return ProcessIcon.lists.notes;
        }


        private List<ScriptItem> GetSelectedScripts()
        {
            try
            {
                List<ScriptItem> tempList;
                tempList = ProcessIcon.lists.scripts;
                if (textBoxScriptFindName.Text.Length > 0)
                {
                    tempList = ProcessIcon.lists.scripts.FindAll(x => x.Name.IndexOf(textBoxScriptFindName.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (textBoxScriptFindContent.Text.Length > 0)
                {
                    tempList = tempList.FindAll(x => x.Content.IndexOf(textBoxScriptFindContent.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                return tempList;
            }
            catch { }
            return ProcessIcon.lists.scripts;
        }
        private List<UniversalItem> GetSelectedUniversal()
        {
            try
            {
                List<UniversalItem> tempList;
                tempList = ProcessIcon.lists.universals;
                if (textBoxUniversalFindName.Text.Length > 0)
                {
                    tempList = ProcessIcon.lists.universals.FindAll(x => x.Name.IndexOf(textBoxUniversalFindName.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                return tempList;
            }
            catch { }
            return ProcessIcon.lists.universals;
        }



        private UniversalItem GetSelectedUniversalItem()
        {
            return ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
        }

        private void GetObjectsFilesNamesPath(DragEventArgs e)
        {
            try
            {
                string[] files = ((string[])(e.Data.GetData(DataFormats.FileDrop)));
                UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                foreach (string file in files)
                {
                    universalItem.Add(new UniversalAtom(GetFileNameInPath(file)));
                }
                ProcessIcon.lists.universals.SaveBase();
                FillGridFromListUniversalsList();
            }
            catch { }
        }
        private void GetObjectsImages(DragEventArgs e)
        {
            string[] files = ((string[])(e.Data.GetData(DataFormats.FileDrop)));
            UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
            foreach (string file in files)
            {
                universalItem.Add(new UniversalAtom(Image.FromFile(file)));
            }
            ProcessIcon.lists.universals.SaveBase();
            FillGridFromListUniversalsList();
        }
        private void GetObjectsFilesPath(DragEventArgs e)
        {
            try
            {
                string[] files = ((string[])(e.Data.GetData(DataFormats.FileDrop)));
                UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                foreach (string file in files)
                {
                    universalItem.Add(new UniversalAtom(file));
                }
                ProcessIcon.lists.universals.SaveBase();
                FillGridFromListUniversalsList();
            }
            catch { }
        }
        #endregion GETS
        #region UPDATES
        private void UpdateListFromGridCurrentRow()
        {
            try
            {
                string Guid = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["Guid"].Value);
                bool Checked = GetBoolAndTest(DataGridMainFiles.CurrentRow.Cells["Checked"].Value);
                string FileGroup = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["Group"].Value.ToString());
                string NameFile = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["Name"].Value);
                //Icon IconFile  =(Icon) DataGridMainFiles.CurrentRow.Cells["IconFile"].Value;
                string PathFile = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["PathFile"].Value);
                //string PathFileFull  = DataGridMainFiles.CurrentRow.Cells["PathFileFull"].Value.ToString();
                string Comments = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["Comments"].Value);
                string CompanyName = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["CompanyName"].Value);
                string FileDescription = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["FileDescription"].Value);
                string FileName = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["FileName"].Value);
                string FileVersion = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["FileVersion"].Value);
                string OriginalFilename = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["OriginalFilename"].Value);
                string ProductName = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["ProductName"].Value);
                string UrlProgrammToUpdate = GetStringAndTest(DataGridMainFiles.CurrentRow.Cells["UrlProgrammToUpdate"].Value);
                FileItem listItem = ProcessIcon.lists.files.Find(x => x.Guid == Guid);
                listItem.Checked = Checked;
                listItem.Group = FileGroup;
                listItem.Name = NameFile;
                //listItem.IconFile = IconFile;
                //listItem.PathFile = PathFile;
                //listItem.PathFileFull = PathFileFull;
                listItem.Comments = Comments;
                listItem.CompanyName = CompanyName;
                listItem.FileDescription = FileDescription;
                listItem.FileName = FileName;
                listItem.FileVersion = FileVersion;
                listItem.OriginalFilename = OriginalFilename;
                listItem.ProductName = ProductName;
                listItem.UrlProgrammToUpdate = UrlProgrammToUpdate;
            }
            catch { }

        }
        private void UpdateListFromGridCurrentRowConnection()
        {
            try
            {
                string Description = GetStringAndTest(dataGridConnections.CurrentRow.Cells["Description"].Value);
                string NameConnection = GetStringAndTest(dataGridConnections.CurrentRow.Cells["Name"].Value);
                string GroupConnection = GetStringAndTest(dataGridConnections.CurrentRow.Cells["Group"].Value);
                string Adress = GetStringAndTest(dataGridConnections.CurrentRow.Cells["Adress"].Value);
                string User = GetStringAndTest(dataGridConnections.CurrentRow.Cells["User"].Value);
                string Password = GetStringAndTest(dataGridConnections.CurrentRow.Cells["Password"].Value);
                string Protocol = GetStringAndTest(dataGridConnections.CurrentRow.Cells["Protocol"].Value);
                ConnectionItem listItem = ProcessIcon.lists.connections.Find(x => x.Name == NameConnection);
                listItem.Name = NameConnection;
                listItem.Group = GroupConnection;
                listItem.Adress = Adress;
                listItem.User = User;
                listItem.Password = Password;
                listItem.Description = Description;
                switch (Protocol)
                {
                    case "RDP":
                        listItem.Protocol = ConnectionProtocol.RDP;
                        break;
                    case "VNC":
                        listItem.Protocol = ConnectionProtocol.VNC;
                        break;
                }
            }
            catch { }

        }

        #endregion UPDATES
        #region CONVERSIONS
        public void CsvToList(String[] file, DragEventArgs e)
        {
            try
            {
                if (universalItemListCurrentGuid != null)
                {
                    string[] readText = File.ReadAllLines(file[0]);
                    string[] files = ((string[])(e.Data.GetData(DataFormats.FileDrop)));
                    UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                    foreach (string sLine in readText)
                    {
                        char[] delimiters = new char[] { textBoxCsvDelimiter.Text.ToCharArray()[0] };
                        string[] splStr = sLine.Split(delimiters);
                        universalItem.Add(new UniversalAtom(splStr));
                    }
                    ProcessIcon.lists.universals.SaveBase();
                    FillGridFromListUniversalsList();
                }
            }
            catch { }
        }
        private void FilesToBytesArrays(string[] artArr)
        {
            try
            {
                if (universalItemListCurrentGuid != null)
                {
                    UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                    foreach (string str in artArr)
                    {
                        universalItem.Add(new UniversalAtom(FileToByteArray(str)));

                    }
                    ProcessIcon.lists.universals.SaveBase();
                    FillGridFromListUniversalsList();
                }
            }
            catch { }
        }
        private byte[] FileToByteArray(string files)
        {
            try
            {
                byte[] buff = null;
                FileStream fs = new FileStream(files,
                                               FileMode.Open,
                                               FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                long numBytes = new FileInfo(files).Length;
                buff = br.ReadBytes((int)numBytes);
                return buff;
            }
            catch { }
            return null;
        }
        #endregion CONVERSIONS
        #region Write_Unload_Save
        private void SaveAtoms()
        {
            try
            {
                UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                foreach (DataGridViewRow row in dataGridViewUniversalItem.Rows)
                {
                    if (row.Cells["Guid"].Value != null)
                    {
                        UniversalAtom atom = universalItem.Find(x => x.Guid == row.Cells["Guid"].Value);
                        if (row.Cells["Checked"].Value == null)
                        {
                            atom.Checked = false;
                        }
                        else
                        {
                            atom.Checked = Boolean.Parse(row.Cells["Checked"].Value.ToString());
                        }
                    }
                }
                ProcessIcon.lists.universals.SaveBase();
            }
            catch { }
        }
        private void SaveAtomsEnter()
        {
            try
            {
                if (dataGridViewUniversalItem.CurrentCell != null)
                {
                    string guidtosearsh = dataGridViewUniversalItem.Rows[dataGridViewUniversalItem.CurrentCell.RowIndex].Cells["Guid"].Value.ToString();
                    var Checked = dataGridViewUniversalItem.Rows[dataGridViewUniversalItem.CurrentCell.RowIndex].Cells["Checked"].Value;
                    string Name = dataGridViewUniversalItem.Rows[dataGridViewUniversalItem.CurrentCell.RowIndex].Cells["Name"].Value.ToString();
                    UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                    UniversalAtom atom = universalItem.Find(x => x.Guid == guidtosearsh);
                    atom.Name = Name;
                    if (Checked == null)
                    {
                        atom.Checked = false;
                    }
                    if (Checked.ToString().Contains("True"))
                    {
                        atom.Checked = true;
                    }
                    if (Checked.ToString().Contains("False"))
                    {
                        atom.Checked = false;
                    }
                    ProcessIcon.lists.universals.SaveBase();
                }
            }
            catch { }
        }
        private void WriteCSVToFile(string fileName)
        {
            try
            {
                UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    string lineStr;
                    foreach (UniversalAtom atom in universalItem)
                    {
                        string csvline = "";
                        foreach (string str in atom.CSV)
                        {
                            csvline += atom.CSV + ";";
                        }
                        sw.WriteLine(csvline.Substring(0, csvline.Length - 1));
                    }
                }
            }
            catch { }
        }
        private void WriteTextToFile(string fileName)
        {
            try
            {
                UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    foreach (UniversalAtom atom in universalItem)
                    {
                        sw.Write(atom.String + textBoxCsvDelimiter.Text);
                    }
                }
            }
            catch { }
        }
        private void WriteBytesArrayToFile(string fileName)
        {
            try
            {
                UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                BinaryWriter Writer = null;
                using (Writer = new BinaryWriter(File.OpenWrite(fileName)))
                {
                    foreach (UniversalAtom atom in universalItem)
                    {
                        if (atom.ByteArray.Length > 0)
                        {
                            Writer.Write(atom.ByteArray);
                            Writer.Flush();
                            Writer.Close();
                        }
                    }
                }
            }
            catch { }
        }
        private void WriteFilesPathToFile(string fileName)
        {
            try
            {
                UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    foreach (UniversalAtom atom in universalItem)
                    {
                        sw.WriteLine(atom.String);
                    }
                }
            }
            catch { }
        }
        private void WriteImagesPathToFile(string fileName)
        {
            try
            {
                int idx = 0;
                UniversalItem universalItem = ProcessIcon.lists.universals.Find(x => x.Guid == universalItemListCurrentGuid);
                foreach (UniversalAtom atom in universalItem)
                {
                    switch (comboBoxFormatImages.SelectedText)
                    {
                        case ".bmp":
                            atom.Image.Save(fileName + idx.ToString() + ".bmp", ImageFormat.Bmp);
                            idx++;
                            break;
                        case ".gif":
                            atom.Image.Save(fileName + idx.ToString() + ".gif", ImageFormat.Gif);
                            idx++;
                            break;
                        case ".ico":
                            atom.Image.Save(fileName + idx.ToString() + ".ico", ImageFormat.Icon);
                            idx++;
                            break;
                        case ".jpg":
                            atom.Image.Save(fileName + idx.ToString() + ".jpg", ImageFormat.Jpeg);
                            idx++;
                            break;
                        case ".png":
                            atom.Image.Save(fileName + idx.ToString() + ".png", ImageFormat.Png);
                            idx++;
                            break;
                        case ".tiff":
                            atom.Image.Save(fileName + idx.ToString() + ".tiff", ImageFormat.Tiff);
                            idx++;
                            break;
                    }
                }
            }
            catch { }
        }

        private void UnloadUniversal()
        {
            string fileName;
            string str;
            fileName = Functions.SaveFileDialog();
            if (fileName.Length == 0)
            {
                return;
            }
            try
            {
                switch (comboBoxDropDataFormats.Text)
                {
                    case "Текст":
                        WriteTextToFile(fileName);
                        break;
                    case "Файл содержимое":
                        WriteBytesArrayToFile(fileName);
                        break;
                    case "Файл путь":
                        WriteFilesPathToFile(fileName);
                        break;
                    case "Файл имя файла":
                        WriteFilesPathToFile(fileName);
                        break;
                    case "Картинка":
                        WriteImagesPathToFile(fileName);
                        break;
                    case "CSV текст с разделителями":
                        WriteCSVToFile(fileName);
                        break;
                }
            }
            catch { }

        }

        #endregion WriteUnload
        public FormMain()
        {
            //appPath = Application.StartupPath.ToString();
            //dataGridFindedFiles.DataSource = listFindedFiles; уутютю
            InitializeComponent();
            timer.Interval = 100;
            timer.Tick += timer_Tick;
            foreach (Control X in this.Controls)
            {
                //X.Text=Resources.butt
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            //CurrentKey = Control.ModifierKeys+" + "+keyData.ToString();
            CurrentKey = keyData.ToString();
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void Localize()
        {
            Dictionary<string, string> locDict = Localization.GetLocalization(comboBoxLocalization.Text);
            foreach (string control in locDict.Keys)
            {
                string[] controlLocal = control.Split('.');
                try
                {
                    Control[] controlFinded = this.Controls.Find(controlLocal[0], true);
                    controlFinded[0].GetType().GetProperty(controlLocal[1]).SetValue(controlFinded[0], locDict[control], null);
                }
                catch { }
            }
        }
        private void timerYandexDisk_Tick(object sender, EventArgs e)
        {
            //if (yandexEngine != null)
            //{
            //    if (yandexEngine.FolderItems != null)
            //    {
            //        if (yandexEngine.FolderItems.Count > 0)
            //        {
            //            filesQuick.ReadBase();
            //            BindingSource bs = new BindingSource();
            //            bs.DataSource = yandexEngine.filesList;
            //            dataGridViewYandexDisk.DataSource = bs;
            //            bs.ResetBindings(false);
            //            timerYandexDisk.Enabled = false;
            //        }
            //    }
            //}
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Set("YandexDrive", checkBoxYandexDrive.Checked);
        }

        private void buttonBackupExe_Click(object sender, EventArgs e)
        {
            try
            {
                timerProgressBar.Enabled = true;
                Updates updates = new Updates();
                updates.xDoDevelopUpload();
            }
            catch (Exception ex)
            {
                timerProgressBar.Enabled = false;
                Functions.Message(ex);
            }
        }

        private void buttonCheckUpdates_Click(object sender, EventArgs e)
        {
            Updates updates = new Updates();
            updates.VerifyUpdate();
        }

        private void timerProgressBar_Tick(object sender, EventArgs e)
        {
            //progressBarMain.Maximum =(int) VariablesClass.progressBarTotal;
            //progressBarMain.Value =(int) VariablesClass.progressBarCurrent;
        }

        private void buttonMailRuConnect_Click(object sender, EventArgs e)
        {
            //MailRuWrapper.ApiFormat = MailRuWrapper.ApiFormat.Xml;
            //MailRuWrapper.;
        }

        private void dataGridScripts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonSaveAllLists_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.notes.SaveBase();
            ProcessIcon.lists.scripts.SaveBase();
            ProcessIcon.lists.files.SaveBase();
            ProcessIcon.lists.filesQuick.SaveBase();
            ProcessIcon.lists.connections.SaveBase();
            ProcessIcon.lists.universals.SaveBase();
            ProcessIcon.lists.tasks.SaveBase();
            ProcessIcon.lists.hotkeys.SaveBase();
            ProcessIcon.lists.sensors.SaveBase();
        }

        private void buttonLoadAll_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.notes.ReadBase();
            ProcessIcon.lists.scripts.ReadBase();
            ProcessIcon.lists.files.ReadBase();
            ProcessIcon.lists.filesQuick.ReadBase();
            ProcessIcon.lists.connections.ReadBase();
            ProcessIcon.lists.universals.ReadBase();
            ProcessIcon.lists.tasks.ReadBase();
            ProcessIcon.lists.hotkeys.ReadBase();
            ProcessIcon.lists.sensors.ReadBase();
        }

        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            Settings.Set("checkBoxCopyMessageToclipboard", checkBoxCopyMessageToclipboard.Checked);
        }

        private void dataGridViewTasks_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridViewTasks_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1)
                {
                    if (((Boolean)dataGridScripts.Rows[e.RowIndex].Cells["isReminder"].Value) == true)
                    {
                        FormTaskReminder formTaskReminder = new FormTaskReminder(dataGridScripts.Rows[e.RowIndex].Cells["Guid"].Value.ToString());
                        formTaskReminder.Show();
                    }
                }
            }
            catch { }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //SetHeaderthWidth();
            SelectTasks();
        }
        void SelectTasks()
        {
            try
            {
                dataGridViewTasks.DataSource = null;
                dataGridViewTasks.DataSource = GetSelectedGroupTasks();
            }
            catch { }
            //GetWithGridHeaders();
            GetWithGridHeaders();
        }
        private List<TaskItem> GetSelectedGroupTasks()
        {
            List<TaskItem> tasks = new List<TaskItem>();
            foreach (TaskItem task in ProcessIcon.lists.tasks)
            {
            if (comboBoxTaskGroup.Text == "Невыбрано")
                {
                        tasks.Add(task);

                }
                else
                {
                    if (task.Group == comboBoxTaskGroup.Text)
                    {
                            tasks.Add(task);

                    }
                }
                if (textBoxTaskSearchText.Text.Length>0)
                    if (task.ToString().Contains(textBoxTaskSearchText.Text))
                        tasks.Add(task);
            }
            return tasks;
        }

        private void comboBoxTaskGroup_TextChanged(object sender, EventArgs e)
        {
            Settings.Set("comboBoxTaskGroup", comboBoxTaskGroup.Text);
        }

        private void checkBoxEnabled_CheckedChanged(object sender, EventArgs e)
        {
            SelectTasks();

        }

        private void checkBoxReminder_CheckedChanged(object sender, EventArgs e)
        {
            SelectTasks();

        }

        private void checkBox1_CheckedChanged_2(object sender, EventArgs e)
        {
            SelectTasks();

        }

        private void dataGridConnections_Leave(object sender, EventArgs e)
        {

        }
        private void dataGridScripts_Leave(object sender, EventArgs e)
        {

        }
        private void dataGridViewTasks_Leave(object sender, EventArgs e)
        {

        }
        private void dataGridViewSensors_Leave(object sender, EventArgs e)
        {

        }
        private void dataGridViewHotkeys_Leave(object sender, EventArgs e)
        {

        }
        private void tabControlMain_Leave(object sender, EventArgs e)
        {

        }
        private void DataGridMainFilesQuick_Leave(object sender, EventArgs e)
        {

        }
        private void dataGridNotes_Leave(object sender, EventArgs e)
        {

        }
        private void dataGridConnections_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.connections);
        }

        private void dataGridScripts_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.scripts);

        }

        private void dataGridViewTasks_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.tasks);

        }

        private void dataGridViewSensors_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.sensors);

        }

        private void DataGridMainFiles_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.files);

        }

        private void DataGridMainFilesQuick_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.filesQuick);

        }

        private void dataGridNotes_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.notes);

        }

        private void buttonSaveConnections_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.connections);
        }

        private void buttonSaveScripts_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.scripts);
        }

        private void buttonSaveTasks_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.tasks);
        }

        private void buttonSaveSensors_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.sensors);
        }

        private void buttonSaveHotkeys_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.hotkeys);
        }

        private void buttonSaveFiles_Click(object sender, EventArgs e)
        {
            ProcessIcon.lists.SaveBase(ProcessIcon.lists.files);
        }

        private void textBoxTaskSearchText_TextChanged(object sender, EventArgs e)
        {
            //SetHeaderthWidth();
            SelectTasks();
        }

        private void comboBoxScriptsGropu_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectScript();
        }
        void SelectScript()
        {
                try
                {
                dataGridScripts.DataSource = null;
                dataGridScripts.DataSource = GetSelectedGroupScripts();
                }
                catch { }
            GetWithGridHeaders();
        }

        private List<ScriptItem> GetSelectedGroupScripts()
        {
            List<ScriptItem> scripts = new List<ScriptItem>();
            foreach (ScriptItem script in ProcessIcon.lists.scripts)
            {
                if (comboBoxScriptsGropu.Text == "Невыбрано")
                {
                    scripts.Add(script);

                }
                else
                {
                    if (script.Group.Trim() == comboBoxScriptsGropu.Text.Trim())
                    {
                        scripts.Add(script);

                    }
                }
                if (textBoxScriptFindContent.Text.Length > 0)
                    if (script.ToString().Contains(textBoxScriptFindContent.Text))
                        scripts.Add(script);
            }
            return scripts;
        }

        private void buttonDownloadSettings_Click(object sender, EventArgs e)
        {
            EngineExchange engineExchange = new EngineExchange();
            engineExchange.DownloadSettings();
        }

        private void buttonUploadsettings_Click(object sender, EventArgs e)
        {
            EngineExchange engineExchange = new EngineExchange();
            engineExchange.UpdateSettings();
        }
    }
}
