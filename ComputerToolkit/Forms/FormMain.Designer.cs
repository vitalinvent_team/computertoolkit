﻿namespace ComputerToolkit
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageConnections = new System.Windows.Forms.TabPage();
            this.buttonSaveConnections = new System.Windows.Forms.Button();
            this.buttonRefreshConnections = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.buttonLoadExternalSettings = new System.Windows.Forms.Button();
            this.buttonOpenConn = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.buttonReloadConnections = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonAddConnection = new System.Windows.Forms.Button();
            this.dataGridConnections = new System.Windows.Forms.DataGridView();
            this.tabPageScripts = new System.Windows.Forms.TabPage();
            this.label43 = new System.Windows.Forms.Label();
            this.comboBoxScriptsGropu = new System.Windows.Forms.ComboBox();
            this.buttonSaveScripts = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxScriptFindContent = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxScriptFindName = new System.Windows.Forms.TextBox();
            this.buttonReloadClasses = new System.Windows.Forms.Button();
            this.buttonRun = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.dataGridScripts = new System.Windows.Forms.DataGridView();
            this.tabPageTasks = new System.Windows.Forms.TabPage();
            this.textBoxTaskSearchText = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.buttonSaveTasks = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.comboBoxTaskGroup = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.buttonUpdateTasks = new System.Windows.Forms.Button();
            this.buttonRefreshTasks = new System.Windows.Forms.Button();
            this.checkBoxEnabledTasks = new System.Windows.Forms.CheckBox();
            this.buttonDeleteTasks = new System.Windows.Forms.Button();
            this.buttonAddTasks = new System.Windows.Forms.Button();
            this.dataGridViewTasks = new System.Windows.Forms.DataGridView();
            this.tabPageSensors = new System.Windows.Forms.TabPage();
            this.buttonSaveSensors = new System.Windows.Forms.Button();
            this.buttonUpdateSensors = new System.Windows.Forms.Button();
            this.buttonRefreshSensors = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.buttonDelSensor = new System.Windows.Forms.Button();
            this.buttonAddSensor = new System.Windows.Forms.Button();
            this.dataGridViewSensors = new System.Windows.Forms.DataGridView();
            this.tabPageHotkeys = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxHotNames = new System.Windows.Forms.TextBox();
            this.groupBoxHotkeys = new System.Windows.Forms.GroupBox();
            this.buttonSaveHotkeys = new System.Windows.Forms.Button();
            this.buttonupdateHotkeys = new System.Windows.Forms.Button();
            this.buttonRefreshHotkeys = new System.Windows.Forms.Button();
            this.buttonDelHotkey = new System.Windows.Forms.Button();
            this.buttonAddHotkey = new System.Windows.Forms.Button();
            this.dataGridViewHotkeys = new System.Windows.Forms.DataGridView();
            this.tabPageFiles = new System.Windows.Forms.TabPage();
            this.tabControlFiles = new System.Windows.Forms.TabControl();
            this.tabPageListFiles = new System.Windows.Forms.TabPage();
            this.buttonSaveFiles = new System.Windows.Forms.Button();
            this.buttonSaveToJson = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxCurrentGroup = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonDelSelected = new System.Windows.Forms.Button();
            this.buttonInvertAllList = new System.Windows.Forms.Button();
            this.buttonDeselAllList = new System.Windows.Forms.Button();
            this.buttonSelAllInList = new System.Windows.Forms.Button();
            this.DataGridMainFiles = new System.Windows.Forms.DataGridView();
            this.tabPageFindFiles = new System.Windows.Forms.TabPage();
            this.buttonCheckSelected = new System.Windows.Forms.Button();
            this.buttonMoveCheckedToBase = new System.Windows.Forms.Button();
            this.buttonInvertAll = new System.Windows.Forms.Button();
            this.buttonDeselAll = new System.Windows.Forms.Button();
            this.buttonSelAll = new System.Windows.Forms.Button();
            this.buttonInputFile = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxDirSearch = new System.Windows.Forms.TextBox();
            this.ButtonFindFiles = new System.Windows.Forms.Button();
            this.dataGridFindedFiles = new System.Windows.Forms.DataGridView();
            this.Checked = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FileIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileVersion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OriginalFilename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Path = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tabPageUniversalList = new System.Windows.Forms.TabPage();
            this.buttonUpdateUniversalList = new System.Windows.Forms.Button();
            this.buttonRefreshUniversalList = new System.Windows.Forms.Button();
            this.textBoxUniversalListFindName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxFormatImages = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.buttonUnloadUnuversal = new System.Windows.Forms.Button();
            this.buttonAtomInvertCheckAll = new System.Windows.Forms.Button();
            this.buttonAtomUnCheckAll = new System.Windows.Forms.Button();
            this.buttonAtomCheckAll = new System.Windows.Forms.Button();
            this.buttonAtomCDeleteChecked = new System.Windows.Forms.Button();
            this.comboBoxDropDataFormats = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dataGridViewUniversalItem = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxUniversalFindName = new System.Windows.Forms.TextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.dataGridViewUniversal = new System.Windows.Forms.DataGridView();
            this.tabPageFilesQuick = new System.Windows.Forms.TabPage();
            this.checkBoxPin = new System.Windows.Forms.CheckBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.TextBoxFileNameQuick = new System.Windows.Forms.TextBox();
            this.buttonQprogDelete = new System.Windows.Forms.Button();
            this.buttonQporgAdd = new System.Windows.Forms.Button();
            this.DataGridMainFilesQuick = new System.Windows.Forms.DataGridView();
            this.tabPageNotes = new System.Windows.Forms.TabPage();
            this.buttonSynchronizeNotes = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxNotesFindContent = new System.Windows.Forms.TextBox();
            this.buttonReloadList = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNotesFindName = new System.Windows.Forms.TextBox();
            this.buttonDelSelNote = new System.Windows.Forms.Button();
            this.buttonAddNote = new System.Windows.Forms.Button();
            this.dataGridNotes = new System.Windows.Forms.DataGridView();
            this.tabPageGoogle = new System.Windows.Forms.TabPage();
            this.buttonCheckGoogleEvents = new System.Windows.Forms.Button();
            this.checkBoxGoogleEnable = new System.Windows.Forms.CheckBox();
            this.buttonGoogleSAve = new System.Windows.Forms.Button();
            this.buttonRefreshGoogleCalendar = new System.Windows.Forms.Button();
            this.dataGridViewGoogle = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxTimeToRefreshUpdateGoogle = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBoxGooglePasswordToEnter = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxGoogleUser = new System.Windows.Forms.TextBox();
            this.textBoxTimeToUpdateGoogle = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBoxGooglePassword = new System.Windows.Forms.TextBox();
            this.tabPageYandexDisk = new System.Windows.Forms.TabPage();
            this.buttonYandexDiskConnect = new System.Windows.Forms.Button();
            this.buttonYandexRefresh = new System.Windows.Forms.Button();
            this.dataGridViewYandexDisk = new System.Windows.Forms.DataGridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxDropBoxPasswordToEnter = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.textBoxDropBoxUser = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBoxDropBoxPassword = new System.Windows.Forms.TextBox();
            this.tabPageMailRu = new System.Windows.Forms.TabPage();
            this.buttonMailRuConnect = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.dataGridViewMailRu = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.buttonDownloadSettings = new System.Windows.Forms.Button();
            this.buttonUploadsettings = new System.Windows.Forms.Button();
            this.buttonDownloadExe = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.textBoxFileExtensionsToAdd = new System.Windows.Forms.TextBox();
            this.checkBoxCopyMessageToclipboard = new System.Windows.Forms.CheckBox();
            this.buttonLoadAll = new System.Windows.Forms.Button();
            this.buttonSaveAllLists = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.textBoxTeamViewerGUIDToOpenConnection = new System.Windows.Forms.TextBox();
            this.buttonBackupExe = new System.Windows.Forms.Button();
            this.buttonSynchronizeAll = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxHotkeys = new System.Windows.Forms.TextBox();
            this.checkBoxCheckUpdate = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.comboBoxLocalization = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxTasks = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBoxUnloadList = new System.Windows.Forms.ComboBox();
            this.buttonSaveFilesToXml = new System.Windows.Forms.Button();
            this.buttonLoadFilesFromXml = new System.Windows.Forms.Button();
            this.buttonFixPathFiles = new System.Windows.Forms.Button();
            this.checkBoxRunWithWindows = new System.Windows.Forms.CheckBox();
            this.buttonCheckUpdates = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonAddUser = new System.Windows.Forms.Button();
            this.checkBoxYandexDrive = new System.Windows.Forms.CheckBox();
            this.textBoxUserPasswordToEnter = new System.Windows.Forms.TextBox();
            this.textBoxUser = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxUserEmail = new System.Windows.Forms.TextBox();
            this.buttonCheckUserConnect = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxProxy = new System.Windows.Forms.TextBox();
            this.textBoxCsvDelimiter = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.buttonSaveSettings = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxFilesQuickFileName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxScriptsFileName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxConnectionsFileName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxNotesFileName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxFilesFileName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxFileNameUniversal = new System.Windows.Forms.TextBox();
            this.checkBoxMakeSubmenuInConnections = new System.Windows.Forms.CheckBox();
            this.checkBoxShowGroupsInConnections = new System.Windows.Forms.CheckBox();
            this.buttonChoseRootDir = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxRootDir = new System.Windows.Forms.TextBox();
            this.checkBoxConnectionsFormTopmost = new System.Windows.Forms.CheckBox();
            this.checkBoxMainFormTopmost = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.timerYandexDisk = new System.Windows.Forms.Timer(this.components);
            this.progressBarMain = new System.Windows.Forms.ProgressBar();
            this.timerProgressBar = new System.Windows.Forms.Timer(this.components);
            this.tabControlMain.SuspendLayout();
            this.tabPageConnections.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridConnections)).BeginInit();
            this.tabPageScripts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridScripts)).BeginInit();
            this.tabPageTasks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTasks)).BeginInit();
            this.tabPageSensors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSensors)).BeginInit();
            this.tabPageHotkeys.SuspendLayout();
            this.groupBoxHotkeys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHotkeys)).BeginInit();
            this.tabPageFiles.SuspendLayout();
            this.tabControlFiles.SuspendLayout();
            this.tabPageListFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridMainFiles)).BeginInit();
            this.tabPageFindFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFindedFiles)).BeginInit();
            this.tabPageUniversalList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUniversalItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUniversal)).BeginInit();
            this.tabPageFilesQuick.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridMainFilesQuick)).BeginInit();
            this.tabPageNotes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridNotes)).BeginInit();
            this.tabPageGoogle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGoogle)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.tabPageYandexDisk.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewYandexDisk)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.tabPageMailRu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMailRu)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.tabPageSettings.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlMain
            // 
            resources.ApplyResources(this.tabControlMain, "tabControlMain");
            this.tabControlMain.Controls.Add(this.tabPageConnections);
            this.tabControlMain.Controls.Add(this.tabPageScripts);
            this.tabControlMain.Controls.Add(this.tabPageTasks);
            this.tabControlMain.Controls.Add(this.tabPageSensors);
            this.tabControlMain.Controls.Add(this.tabPageHotkeys);
            this.tabControlMain.Controls.Add(this.tabPageFiles);
            this.tabControlMain.Controls.Add(this.tabPageUniversalList);
            this.tabControlMain.Controls.Add(this.tabPageFilesQuick);
            this.tabControlMain.Controls.Add(this.tabPageNotes);
            this.tabControlMain.Controls.Add(this.tabPageGoogle);
            this.tabControlMain.Controls.Add(this.tabPageYandexDisk);
            this.tabControlMain.Controls.Add(this.tabPageMailRu);
            this.tabControlMain.Controls.Add(this.tabPageSettings);
            this.tabControlMain.ImageList = this.imageList1;
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.TabPagesDrawItem);
            this.tabControlMain.SelectedIndexChanged += new System.EventHandler(this.tabControlMain_SelectedIndexChanged);
            this.tabControlMain.Click += new System.EventHandler(this.tabControlMain_Click);
            this.tabControlMain.Leave += new System.EventHandler(this.tabControlMain_Leave);
            // 
            // tabPageConnections
            // 
            this.tabPageConnections.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(206)))), ((int)(((byte)(217)))));
            this.tabPageConnections.Controls.Add(this.buttonSaveConnections);
            this.tabPageConnections.Controls.Add(this.buttonRefreshConnections);
            this.tabPageConnections.Controls.Add(this.label32);
            this.tabPageConnections.Controls.Add(this.buttonLoadExternalSettings);
            this.tabPageConnections.Controls.Add(this.buttonOpenConn);
            this.tabPageConnections.Controls.Add(this.button4);
            this.tabPageConnections.Controls.Add(this.buttonReloadConnections);
            this.tabPageConnections.Controls.Add(this.button1);
            this.tabPageConnections.Controls.Add(this.buttonAddConnection);
            this.tabPageConnections.Controls.Add(this.dataGridConnections);
            resources.ApplyResources(this.tabPageConnections, "tabPageConnections");
            this.tabPageConnections.Name = "tabPageConnections";
            // 
            // buttonSaveConnections
            // 
            resources.ApplyResources(this.buttonSaveConnections, "buttonSaveConnections");
            this.buttonSaveConnections.Image = global::ComputerToolkit.Properties.Resources.save;
            this.buttonSaveConnections.Name = "buttonSaveConnections";
            this.toolTip1.SetToolTip(this.buttonSaveConnections, resources.GetString("buttonSaveConnections.ToolTip"));
            this.buttonSaveConnections.UseVisualStyleBackColor = true;
            this.buttonSaveConnections.Click += new System.EventHandler(this.buttonSaveConnections_Click);
            // 
            // buttonRefreshConnections
            // 
            resources.ApplyResources(this.buttonRefreshConnections, "buttonRefreshConnections");
            this.buttonRefreshConnections.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonRefreshConnections.Name = "buttonRefreshConnections";
            this.toolTip1.SetToolTip(this.buttonRefreshConnections, resources.GetString("buttonRefreshConnections.ToolTip"));
            this.buttonRefreshConnections.UseVisualStyleBackColor = true;
            this.buttonRefreshConnections.Click += new System.EventHandler(this.buttonRefreshConnections_Click);
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.Name = "label32";
            // 
            // buttonLoadExternalSettings
            // 
            resources.ApplyResources(this.buttonLoadExternalSettings, "buttonLoadExternalSettings");
            this.buttonLoadExternalSettings.Name = "buttonLoadExternalSettings";
            this.buttonLoadExternalSettings.UseVisualStyleBackColor = true;
            this.buttonLoadExternalSettings.Click += new System.EventHandler(this.buttonLoadExternalSettings_Click);
            // 
            // buttonOpenConn
            // 
            resources.ApplyResources(this.buttonOpenConn, "buttonOpenConn");
            this.buttonOpenConn.Image = global::ComputerToolkit.Properties.Resources.Display;
            this.buttonOpenConn.Name = "buttonOpenConn";
            this.buttonOpenConn.UseVisualStyleBackColor = true;
            this.buttonOpenConn.Click += new System.EventHandler(this.buttonOpenConn_Click);
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // buttonReloadConnections
            // 
            resources.ApplyResources(this.buttonReloadConnections, "buttonReloadConnections");
            this.buttonReloadConnections.Image = global::ComputerToolkit.Properties.Resources.reload;
            this.buttonReloadConnections.Name = "buttonReloadConnections";
            this.toolTip1.SetToolTip(this.buttonReloadConnections, resources.GetString("buttonReloadConnections.ToolTip"));
            this.buttonReloadConnections.UseVisualStyleBackColor = true;
            this.buttonReloadConnections.Click += new System.EventHandler(this.buttonReloadConnections_Click);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttonAddConnection
            // 
            resources.ApplyResources(this.buttonAddConnection, "buttonAddConnection");
            this.buttonAddConnection.Name = "buttonAddConnection";
            this.buttonAddConnection.UseVisualStyleBackColor = true;
            this.buttonAddConnection.Click += new System.EventHandler(this.buttonAddConnection_Click);
            // 
            // dataGridConnections
            // 
            resources.ApplyResources(this.dataGridConnections, "dataGridConnections");
            this.dataGridConnections.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridConnections.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridConnections.Name = "dataGridConnections";
            this.dataGridConnections.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridConnections_CellContentClick);
            this.dataGridConnections.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridConnections_CellEndEdit);
            this.dataGridConnections.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridNotes_CellValueChanged);
            this.dataGridConnections.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridConnections_ColumnWidthChanged);
            this.dataGridConnections.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridConnections_DataError);
            this.dataGridConnections.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridConnections_KeyDown);
            this.dataGridConnections.Leave += new System.EventHandler(this.dataGridConnections_Leave);
            this.dataGridConnections.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridConnections_MouseClick);
            // 
            // tabPageScripts
            // 
            this.tabPageScripts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPageScripts.Controls.Add(this.label43);
            this.tabPageScripts.Controls.Add(this.comboBoxScriptsGropu);
            this.tabPageScripts.Controls.Add(this.buttonSaveScripts);
            this.tabPageScripts.Controls.Add(this.label5);
            this.tabPageScripts.Controls.Add(this.textBoxScriptFindContent);
            this.tabPageScripts.Controls.Add(this.label8);
            this.tabPageScripts.Controls.Add(this.textBoxScriptFindName);
            this.tabPageScripts.Controls.Add(this.buttonReloadClasses);
            this.tabPageScripts.Controls.Add(this.buttonRun);
            this.tabPageScripts.Controls.Add(this.button9);
            this.tabPageScripts.Controls.Add(this.button7);
            this.tabPageScripts.Controls.Add(this.button8);
            this.tabPageScripts.Controls.Add(this.dataGridScripts);
            resources.ApplyResources(this.tabPageScripts, "tabPageScripts");
            this.tabPageScripts.Name = "tabPageScripts";
            // 
            // label43
            // 
            resources.ApplyResources(this.label43, "label43");
            this.label43.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label43.Name = "label43";
            // 
            // comboBoxScriptsGropu
            // 
            resources.ApplyResources(this.comboBoxScriptsGropu, "comboBoxScriptsGropu");
            this.comboBoxScriptsGropu.FormattingEnabled = true;
            this.comboBoxScriptsGropu.Items.AddRange(new object[] {
            resources.GetString("comboBoxScriptsGropu.Items")});
            this.comboBoxScriptsGropu.Name = "comboBoxScriptsGropu";
            this.comboBoxScriptsGropu.SelectedIndexChanged += new System.EventHandler(this.comboBoxScriptsGropu_SelectedIndexChanged);
            // 
            // buttonSaveScripts
            // 
            resources.ApplyResources(this.buttonSaveScripts, "buttonSaveScripts");
            this.buttonSaveScripts.Image = global::ComputerToolkit.Properties.Resources.save;
            this.buttonSaveScripts.Name = "buttonSaveScripts";
            this.toolTip1.SetToolTip(this.buttonSaveScripts, resources.GetString("buttonSaveScripts.ToolTip"));
            this.buttonSaveScripts.UseVisualStyleBackColor = true;
            this.buttonSaveScripts.Click += new System.EventHandler(this.buttonSaveScripts_Click);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // textBoxScriptFindContent
            // 
            resources.ApplyResources(this.textBoxScriptFindContent, "textBoxScriptFindContent");
            this.textBoxScriptFindContent.Name = "textBoxScriptFindContent";
            this.textBoxScriptFindContent.TextChanged += new System.EventHandler(this.textBoxScriptFindContent_TextChanged);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // textBoxScriptFindName
            // 
            resources.ApplyResources(this.textBoxScriptFindName, "textBoxScriptFindName");
            this.textBoxScriptFindName.Name = "textBoxScriptFindName";
            this.textBoxScriptFindName.TextChanged += new System.EventHandler(this.textBoxScriptFindName_TextChanged);
            // 
            // buttonReloadClasses
            // 
            resources.ApplyResources(this.buttonReloadClasses, "buttonReloadClasses");
            this.buttonReloadClasses.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonReloadClasses.Name = "buttonReloadClasses";
            this.toolTip1.SetToolTip(this.buttonReloadClasses, resources.GetString("buttonReloadClasses.ToolTip"));
            this.buttonReloadClasses.UseVisualStyleBackColor = true;
            this.buttonReloadClasses.Click += new System.EventHandler(this.buttonReloadClasses_Click);
            // 
            // buttonRun
            // 
            resources.ApplyResources(this.buttonRun, "buttonRun");
            this.buttonRun.Image = global::ComputerToolkit.Properties.Resources.play;
            this.buttonRun.Name = "buttonRun";
            this.toolTip1.SetToolTip(this.buttonRun, resources.GetString("buttonRun.ToolTip"));
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
            // 
            // button9
            // 
            resources.ApplyResources(this.button9, "button9");
            this.button9.Image = global::ComputerToolkit.Properties.Resources.reload;
            this.button9.Name = "button9";
            this.toolTip1.SetToolTip(this.button9, resources.GetString("button9.ToolTip"));
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button7
            // 
            resources.ApplyResources(this.button7, "button7");
            this.button7.Name = "button7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            resources.ApplyResources(this.button8, "button8");
            this.button8.Name = "button8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // dataGridScripts
            // 
            resources.ApplyResources(this.dataGridScripts, "dataGridScripts");
            this.dataGridScripts.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridScripts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridScripts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridScripts.Name = "dataGridScripts";
            this.dataGridScripts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridScripts.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridScripts_CellContentClick);
            this.dataGridScripts.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridScripts_CellDoubleClick);
            this.dataGridScripts.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridScripts_CellEndEdit);
            this.dataGridScripts.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridScripts_CellValueChanged);
            this.dataGridScripts.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridScripts_ColumnWidthChanged);
            this.dataGridScripts.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridScripts_DataError);
            this.dataGridScripts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridScripts_KeyDown);
            this.dataGridScripts.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridScripts_KeyPress);
            this.dataGridScripts.Leave += new System.EventHandler(this.dataGridScripts_Leave);
            this.dataGridScripts.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridScripts_MouseClick);
            // 
            // tabPageTasks
            // 
            this.tabPageTasks.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(214)))), ((int)(((byte)(200)))));
            this.tabPageTasks.Controls.Add(this.textBoxTaskSearchText);
            this.tabPageTasks.Controls.Add(this.label42);
            this.tabPageTasks.Controls.Add(this.buttonSaveTasks);
            this.tabPageTasks.Controls.Add(this.label41);
            this.tabPageTasks.Controls.Add(this.comboBoxTaskGroup);
            this.tabPageTasks.Controls.Add(this.label40);
            this.tabPageTasks.Controls.Add(this.buttonUpdateTasks);
            this.tabPageTasks.Controls.Add(this.buttonRefreshTasks);
            this.tabPageTasks.Controls.Add(this.checkBoxEnabledTasks);
            this.tabPageTasks.Controls.Add(this.buttonDeleteTasks);
            this.tabPageTasks.Controls.Add(this.buttonAddTasks);
            this.tabPageTasks.Controls.Add(this.dataGridViewTasks);
            resources.ApplyResources(this.tabPageTasks, "tabPageTasks");
            this.tabPageTasks.Name = "tabPageTasks";
            // 
            // textBoxTaskSearchText
            // 
            this.textBoxTaskSearchText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxTaskSearchText, "textBoxTaskSearchText");
            this.textBoxTaskSearchText.Name = "textBoxTaskSearchText";
            this.textBoxTaskSearchText.TextChanged += new System.EventHandler(this.textBoxTaskSearchText_TextChanged);
            // 
            // label42
            // 
            resources.ApplyResources(this.label42, "label42");
            this.label42.Name = "label42";
            // 
            // buttonSaveTasks
            // 
            resources.ApplyResources(this.buttonSaveTasks, "buttonSaveTasks");
            this.buttonSaveTasks.Image = global::ComputerToolkit.Properties.Resources.save;
            this.buttonSaveTasks.Name = "buttonSaveTasks";
            this.toolTip1.SetToolTip(this.buttonSaveTasks, resources.GetString("buttonSaveTasks.ToolTip"));
            this.buttonSaveTasks.UseVisualStyleBackColor = true;
            this.buttonSaveTasks.Click += new System.EventHandler(this.buttonSaveTasks_Click);
            // 
            // label41
            // 
            resources.ApplyResources(this.label41, "label41");
            this.label41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label41.Name = "label41";
            // 
            // comboBoxTaskGroup
            // 
            resources.ApplyResources(this.comboBoxTaskGroup, "comboBoxTaskGroup");
            this.comboBoxTaskGroup.FormattingEnabled = true;
            this.comboBoxTaskGroup.Items.AddRange(new object[] {
            resources.GetString("comboBoxTaskGroup.Items")});
            this.comboBoxTaskGroup.Name = "comboBoxTaskGroup";
            this.comboBoxTaskGroup.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBoxTaskGroup.TextChanged += new System.EventHandler(this.comboBoxTaskGroup_TextChanged);
            // 
            // label40
            // 
            resources.ApplyResources(this.label40, "label40");
            this.label40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(214)))), ((int)(((byte)(200)))));
            this.label40.Name = "label40";
            // 
            // buttonUpdateTasks
            // 
            resources.ApplyResources(this.buttonUpdateTasks, "buttonUpdateTasks");
            this.buttonUpdateTasks.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonUpdateTasks.Name = "buttonUpdateTasks";
            this.toolTip1.SetToolTip(this.buttonUpdateTasks, resources.GetString("buttonUpdateTasks.ToolTip"));
            this.buttonUpdateTasks.UseVisualStyleBackColor = true;
            this.buttonUpdateTasks.Click += new System.EventHandler(this.buttonUpdateTasks_Click);
            // 
            // buttonRefreshTasks
            // 
            resources.ApplyResources(this.buttonRefreshTasks, "buttonRefreshTasks");
            this.buttonRefreshTasks.Image = global::ComputerToolkit.Properties.Resources.reload;
            this.buttonRefreshTasks.Name = "buttonRefreshTasks";
            this.toolTip1.SetToolTip(this.buttonRefreshTasks, resources.GetString("buttonRefreshTasks.ToolTip"));
            this.buttonRefreshTasks.UseVisualStyleBackColor = true;
            this.buttonRefreshTasks.Click += new System.EventHandler(this.buttonRefreshTasks_Click);
            // 
            // checkBoxEnabledTasks
            // 
            resources.ApplyResources(this.checkBoxEnabledTasks, "checkBoxEnabledTasks");
            this.checkBoxEnabledTasks.Name = "checkBoxEnabledTasks";
            this.checkBoxEnabledTasks.UseVisualStyleBackColor = true;
            this.checkBoxEnabledTasks.CheckedChanged += new System.EventHandler(this.checkBoxEnabledTasks_CheckedChanged);
            // 
            // buttonDeleteTasks
            // 
            resources.ApplyResources(this.buttonDeleteTasks, "buttonDeleteTasks");
            this.buttonDeleteTasks.Name = "buttonDeleteTasks";
            this.buttonDeleteTasks.UseVisualStyleBackColor = true;
            this.buttonDeleteTasks.Click += new System.EventHandler(this.buttonDeleteTasks_Click);
            // 
            // buttonAddTasks
            // 
            resources.ApplyResources(this.buttonAddTasks, "buttonAddTasks");
            this.buttonAddTasks.Name = "buttonAddTasks";
            this.buttonAddTasks.UseVisualStyleBackColor = true;
            this.buttonAddTasks.Click += new System.EventHandler(this.buttonAddTasks_Click);
            // 
            // dataGridViewTasks
            // 
            resources.ApplyResources(this.dataGridViewTasks, "dataGridViewTasks");
            this.dataGridViewTasks.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewTasks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTasks.Name = "dataGridViewTasks";
            this.dataGridViewTasks.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridViewTasks_CellBeginEdit);
            this.dataGridViewTasks.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTasks_CellContentClick);
            this.dataGridViewTasks.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTasks_CellDoubleClick);
            this.dataGridViewTasks.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTasks_CellEndEdit);
            this.dataGridViewTasks.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTasks_CellValueChanged);
            this.dataGridViewTasks.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridViewTasks_ColumnWidthChanged);
            this.dataGridViewTasks.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewTasks_DataError);
            this.dataGridViewTasks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridViewTasks_KeyDown);
            this.dataGridViewTasks.Leave += new System.EventHandler(this.dataGridViewTasks_Leave);
            this.dataGridViewTasks.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridViewTasks_MouseClick);
            // 
            // tabPageSensors
            // 
            this.tabPageSensors.Controls.Add(this.buttonSaveSensors);
            this.tabPageSensors.Controls.Add(this.buttonUpdateSensors);
            this.tabPageSensors.Controls.Add(this.buttonRefreshSensors);
            this.tabPageSensors.Controls.Add(this.label31);
            this.tabPageSensors.Controls.Add(this.textBox3);
            this.tabPageSensors.Controls.Add(this.buttonDelSensor);
            this.tabPageSensors.Controls.Add(this.buttonAddSensor);
            this.tabPageSensors.Controls.Add(this.dataGridViewSensors);
            resources.ApplyResources(this.tabPageSensors, "tabPageSensors");
            this.tabPageSensors.Name = "tabPageSensors";
            this.tabPageSensors.UseVisualStyleBackColor = true;
            // 
            // buttonSaveSensors
            // 
            resources.ApplyResources(this.buttonSaveSensors, "buttonSaveSensors");
            this.buttonSaveSensors.Image = global::ComputerToolkit.Properties.Resources.save;
            this.buttonSaveSensors.Name = "buttonSaveSensors";
            this.toolTip1.SetToolTip(this.buttonSaveSensors, resources.GetString("buttonSaveSensors.ToolTip"));
            this.buttonSaveSensors.UseVisualStyleBackColor = true;
            this.buttonSaveSensors.Click += new System.EventHandler(this.buttonSaveSensors_Click);
            // 
            // buttonUpdateSensors
            // 
            resources.ApplyResources(this.buttonUpdateSensors, "buttonUpdateSensors");
            this.buttonUpdateSensors.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonUpdateSensors.Name = "buttonUpdateSensors";
            this.toolTip1.SetToolTip(this.buttonUpdateSensors, resources.GetString("buttonUpdateSensors.ToolTip"));
            this.buttonUpdateSensors.UseVisualStyleBackColor = true;
            this.buttonUpdateSensors.Click += new System.EventHandler(this.buttonUpdateSensors_Click);
            // 
            // buttonRefreshSensors
            // 
            resources.ApplyResources(this.buttonRefreshSensors, "buttonRefreshSensors");
            this.buttonRefreshSensors.Image = global::ComputerToolkit.Properties.Resources.reload;
            this.buttonRefreshSensors.Name = "buttonRefreshSensors";
            this.toolTip1.SetToolTip(this.buttonRefreshSensors, resources.GetString("buttonRefreshSensors.ToolTip"));
            this.buttonRefreshSensors.UseVisualStyleBackColor = true;
            this.buttonRefreshSensors.Click += new System.EventHandler(this.buttonRefreshSensors_Click);
            // 
            // label31
            // 
            resources.ApplyResources(this.label31, "label31");
            this.label31.Name = "label31";
            // 
            // textBox3
            // 
            resources.ApplyResources(this.textBox3, "textBox3");
            this.textBox3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.ForeColor = System.Drawing.Color.Black;
            this.textBox3.Name = "textBox3";
            // 
            // buttonDelSensor
            // 
            resources.ApplyResources(this.buttonDelSensor, "buttonDelSensor");
            this.buttonDelSensor.Name = "buttonDelSensor";
            this.buttonDelSensor.UseVisualStyleBackColor = true;
            this.buttonDelSensor.Click += new System.EventHandler(this.buttonDelSensor_Click);
            // 
            // buttonAddSensor
            // 
            resources.ApplyResources(this.buttonAddSensor, "buttonAddSensor");
            this.buttonAddSensor.Name = "buttonAddSensor";
            this.buttonAddSensor.UseVisualStyleBackColor = true;
            this.buttonAddSensor.Click += new System.EventHandler(this.buttonAddSensor_Click);
            // 
            // dataGridViewSensors
            // 
            resources.ApplyResources(this.dataGridViewSensors, "dataGridViewSensors");
            this.dataGridViewSensors.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewSensors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSensors.Name = "dataGridViewSensors";
            this.dataGridViewSensors.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewSensors_CellContentDoubleClick);
            this.dataGridViewSensors.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewSensors_CellEndEdit);
            this.dataGridViewSensors.Leave += new System.EventHandler(this.dataGridViewSensors_Leave);
            // 
            // tabPageHotkeys
            // 
            this.tabPageHotkeys.Controls.Add(this.label26);
            this.tabPageHotkeys.Controls.Add(this.textBoxHotNames);
            this.tabPageHotkeys.Controls.Add(this.groupBoxHotkeys);
            resources.ApplyResources(this.tabPageHotkeys, "tabPageHotkeys");
            this.tabPageHotkeys.Name = "tabPageHotkeys";
            this.tabPageHotkeys.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            resources.ApplyResources(this.label26, "label26");
            this.label26.Name = "label26";
            // 
            // textBoxHotNames
            // 
            resources.ApplyResources(this.textBoxHotNames, "textBoxHotNames");
            this.textBoxHotNames.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBoxHotNames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxHotNames.ForeColor = System.Drawing.Color.Black;
            this.textBoxHotNames.Name = "textBoxHotNames";
            // 
            // groupBoxHotkeys
            // 
            resources.ApplyResources(this.groupBoxHotkeys, "groupBoxHotkeys");
            this.groupBoxHotkeys.Controls.Add(this.buttonSaveHotkeys);
            this.groupBoxHotkeys.Controls.Add(this.buttonupdateHotkeys);
            this.groupBoxHotkeys.Controls.Add(this.buttonRefreshHotkeys);
            this.groupBoxHotkeys.Controls.Add(this.buttonDelHotkey);
            this.groupBoxHotkeys.Controls.Add(this.buttonAddHotkey);
            this.groupBoxHotkeys.Controls.Add(this.dataGridViewHotkeys);
            this.groupBoxHotkeys.Name = "groupBoxHotkeys";
            this.groupBoxHotkeys.TabStop = false;
            // 
            // buttonSaveHotkeys
            // 
            resources.ApplyResources(this.buttonSaveHotkeys, "buttonSaveHotkeys");
            this.buttonSaveHotkeys.Image = global::ComputerToolkit.Properties.Resources.save;
            this.buttonSaveHotkeys.Name = "buttonSaveHotkeys";
            this.toolTip1.SetToolTip(this.buttonSaveHotkeys, resources.GetString("buttonSaveHotkeys.ToolTip"));
            this.buttonSaveHotkeys.UseVisualStyleBackColor = true;
            this.buttonSaveHotkeys.Click += new System.EventHandler(this.buttonSaveHotkeys_Click);
            // 
            // buttonupdateHotkeys
            // 
            resources.ApplyResources(this.buttonupdateHotkeys, "buttonupdateHotkeys");
            this.buttonupdateHotkeys.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonupdateHotkeys.Name = "buttonupdateHotkeys";
            this.toolTip1.SetToolTip(this.buttonupdateHotkeys, resources.GetString("buttonupdateHotkeys.ToolTip"));
            this.buttonupdateHotkeys.UseVisualStyleBackColor = true;
            this.buttonupdateHotkeys.Click += new System.EventHandler(this.buttonupdateHotkeys_Click);
            // 
            // buttonRefreshHotkeys
            // 
            resources.ApplyResources(this.buttonRefreshHotkeys, "buttonRefreshHotkeys");
            this.buttonRefreshHotkeys.Image = global::ComputerToolkit.Properties.Resources.reload;
            this.buttonRefreshHotkeys.Name = "buttonRefreshHotkeys";
            this.toolTip1.SetToolTip(this.buttonRefreshHotkeys, resources.GetString("buttonRefreshHotkeys.ToolTip"));
            this.buttonRefreshHotkeys.UseVisualStyleBackColor = true;
            this.buttonRefreshHotkeys.Click += new System.EventHandler(this.buttonRefreshHotkeys_Click);
            // 
            // buttonDelHotkey
            // 
            resources.ApplyResources(this.buttonDelHotkey, "buttonDelHotkey");
            this.buttonDelHotkey.Name = "buttonDelHotkey";
            this.buttonDelHotkey.UseVisualStyleBackColor = true;
            this.buttonDelHotkey.Click += new System.EventHandler(this.buttonDelHotkey_Click_1);
            // 
            // buttonAddHotkey
            // 
            resources.ApplyResources(this.buttonAddHotkey, "buttonAddHotkey");
            this.buttonAddHotkey.Name = "buttonAddHotkey";
            this.buttonAddHotkey.UseVisualStyleBackColor = true;
            this.buttonAddHotkey.Click += new System.EventHandler(this.buttonAddHotkey_Click_1);
            // 
            // dataGridViewHotkeys
            // 
            resources.ApplyResources(this.dataGridViewHotkeys, "dataGridViewHotkeys");
            this.dataGridViewHotkeys.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewHotkeys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHotkeys.Name = "dataGridViewHotkeys";
            this.dataGridViewHotkeys.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridViewHotkeys_CellBeginEdit);
            this.dataGridViewHotkeys.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewHotkeys_CellEndEdit);
            this.dataGridViewHotkeys.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridViewHotkeys_ColumnWidthChanged);
            this.dataGridViewHotkeys.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridViewHotkeys_EditingControlShowing);
            this.dataGridViewHotkeys.Leave += new System.EventHandler(this.dataGridViewHotkeys_Leave);
            // 
            // tabPageFiles
            // 
            this.tabPageFiles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tabPageFiles.Controls.Add(this.tabControlFiles);
            resources.ApplyResources(this.tabPageFiles, "tabPageFiles");
            this.tabPageFiles.Name = "tabPageFiles";
            // 
            // tabControlFiles
            // 
            resources.ApplyResources(this.tabControlFiles, "tabControlFiles");
            this.tabControlFiles.Controls.Add(this.tabPageListFiles);
            this.tabControlFiles.Controls.Add(this.tabPageFindFiles);
            this.tabControlFiles.ImageList = this.imageList1;
            this.tabControlFiles.Name = "tabControlFiles";
            this.tabControlFiles.SelectedIndex = 0;
            // 
            // tabPageListFiles
            // 
            this.tabPageListFiles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tabPageListFiles.Controls.Add(this.buttonSaveFiles);
            this.tabPageListFiles.Controls.Add(this.buttonSaveToJson);
            this.tabPageListFiles.Controls.Add(this.button3);
            this.tabPageListFiles.Controls.Add(this.label4);
            this.tabPageListFiles.Controls.Add(this.comboBoxCurrentGroup);
            this.tabPageListFiles.Controls.Add(this.button2);
            this.tabPageListFiles.Controls.Add(this.buttonDelSelected);
            this.tabPageListFiles.Controls.Add(this.buttonInvertAllList);
            this.tabPageListFiles.Controls.Add(this.buttonDeselAllList);
            this.tabPageListFiles.Controls.Add(this.buttonSelAllInList);
            this.tabPageListFiles.Controls.Add(this.DataGridMainFiles);
            resources.ApplyResources(this.tabPageListFiles, "tabPageListFiles");
            this.tabPageListFiles.Name = "tabPageListFiles";
            // 
            // buttonSaveFiles
            // 
            resources.ApplyResources(this.buttonSaveFiles, "buttonSaveFiles");
            this.buttonSaveFiles.Image = global::ComputerToolkit.Properties.Resources.save;
            this.buttonSaveFiles.Name = "buttonSaveFiles";
            this.toolTip1.SetToolTip(this.buttonSaveFiles, resources.GetString("buttonSaveFiles.ToolTip"));
            this.buttonSaveFiles.UseVisualStyleBackColor = true;
            this.buttonSaveFiles.Click += new System.EventHandler(this.buttonSaveFiles_Click);
            // 
            // buttonSaveToJson
            // 
            resources.ApplyResources(this.buttonSaveToJson, "buttonSaveToJson");
            this.buttonSaveToJson.Name = "buttonSaveToJson";
            this.buttonSaveToJson.UseVisualStyleBackColor = true;
            this.buttonSaveToJson.Click += new System.EventHandler(this.buttonSaveToJson_Click);
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Name = "label4";
            // 
            // comboBoxCurrentGroup
            // 
            resources.ApplyResources(this.comboBoxCurrentGroup, "comboBoxCurrentGroup");
            this.comboBoxCurrentGroup.FormattingEnabled = true;
            this.comboBoxCurrentGroup.Items.AddRange(new object[] {
            resources.GetString("comboBoxCurrentGroup.Items")});
            this.comboBoxCurrentGroup.Name = "comboBoxCurrentGroup";
            this.comboBoxCurrentGroup.SelectedIndexChanged += new System.EventHandler(this.comboBoxCurrentGroup_SelectedIndexChanged);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonDelSelected
            // 
            resources.ApplyResources(this.buttonDelSelected, "buttonDelSelected");
            this.buttonDelSelected.Name = "buttonDelSelected";
            this.buttonDelSelected.UseVisualStyleBackColor = true;
            this.buttonDelSelected.Click += new System.EventHandler(this.buttonDelSelected_Click_1);
            // 
            // buttonInvertAllList
            // 
            resources.ApplyResources(this.buttonInvertAllList, "buttonInvertAllList");
            this.buttonInvertAllList.Name = "buttonInvertAllList";
            this.buttonInvertAllList.UseVisualStyleBackColor = true;
            this.buttonInvertAllList.Click += new System.EventHandler(this.buttonInvertAllList_Click);
            // 
            // buttonDeselAllList
            // 
            resources.ApplyResources(this.buttonDeselAllList, "buttonDeselAllList");
            this.buttonDeselAllList.Name = "buttonDeselAllList";
            this.buttonDeselAllList.UseVisualStyleBackColor = true;
            this.buttonDeselAllList.Click += new System.EventHandler(this.buttonDeselAllList_Click);
            // 
            // buttonSelAllInList
            // 
            resources.ApplyResources(this.buttonSelAllInList, "buttonSelAllInList");
            this.buttonSelAllInList.Name = "buttonSelAllInList";
            this.buttonSelAllInList.UseVisualStyleBackColor = true;
            this.buttonSelAllInList.Click += new System.EventHandler(this.buttonSelAllInList_Click);
            // 
            // DataGridMainFiles
            // 
            resources.ApplyResources(this.DataGridMainFiles, "DataGridMainFiles");
            this.DataGridMainFiles.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.DataGridMainFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridMainFiles.Name = "DataGridMainFiles";
            this.DataGridMainFiles.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridMainFiles_CellEndEdit);
            this.DataGridMainFiles.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridMainFiles_CellValueChanged);
            this.DataGridMainFiles.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridMainFiles_ColumnHeaderMouseClick);
            this.DataGridMainFiles.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.DataGridMainFiles_ColumnWidthChanged);
            this.DataGridMainFiles.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.DataGridMainFiles_DataBindingComplete);
            this.DataGridMainFiles.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridMainFiles_DataError);
            this.DataGridMainFiles.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridMainFiles_KeyDown);
            this.DataGridMainFiles.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridMainFiles_MouseClick);
            // 
            // tabPageFindFiles
            // 
            this.tabPageFindFiles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tabPageFindFiles.Controls.Add(this.buttonCheckSelected);
            this.tabPageFindFiles.Controls.Add(this.buttonMoveCheckedToBase);
            this.tabPageFindFiles.Controls.Add(this.buttonInvertAll);
            this.tabPageFindFiles.Controls.Add(this.buttonDeselAll);
            this.tabPageFindFiles.Controls.Add(this.buttonSelAll);
            this.tabPageFindFiles.Controls.Add(this.buttonInputFile);
            this.tabPageFindFiles.Controls.Add(this.label2);
            this.tabPageFindFiles.Controls.Add(this.TextBoxDirSearch);
            this.tabPageFindFiles.Controls.Add(this.ButtonFindFiles);
            this.tabPageFindFiles.Controls.Add(this.dataGridFindedFiles);
            resources.ApplyResources(this.tabPageFindFiles, "tabPageFindFiles");
            this.tabPageFindFiles.Name = "tabPageFindFiles";
            // 
            // buttonCheckSelected
            // 
            resources.ApplyResources(this.buttonCheckSelected, "buttonCheckSelected");
            this.buttonCheckSelected.Name = "buttonCheckSelected";
            this.buttonCheckSelected.UseVisualStyleBackColor = true;
            this.buttonCheckSelected.Click += new System.EventHandler(this.buttonCheckSelected_Click);
            // 
            // buttonMoveCheckedToBase
            // 
            resources.ApplyResources(this.buttonMoveCheckedToBase, "buttonMoveCheckedToBase");
            this.buttonMoveCheckedToBase.Name = "buttonMoveCheckedToBase";
            this.buttonMoveCheckedToBase.UseVisualStyleBackColor = true;
            this.buttonMoveCheckedToBase.Click += new System.EventHandler(this.buttonMoveCheckedToBase_Click);
            // 
            // buttonInvertAll
            // 
            resources.ApplyResources(this.buttonInvertAll, "buttonInvertAll");
            this.buttonInvertAll.Name = "buttonInvertAll";
            this.buttonInvertAll.UseVisualStyleBackColor = true;
            this.buttonInvertAll.Click += new System.EventHandler(this.buttonInvertAll_Click);
            // 
            // buttonDeselAll
            // 
            resources.ApplyResources(this.buttonDeselAll, "buttonDeselAll");
            this.buttonDeselAll.Name = "buttonDeselAll";
            this.buttonDeselAll.UseVisualStyleBackColor = true;
            this.buttonDeselAll.Click += new System.EventHandler(this.buttonDeselAll_Click);
            // 
            // buttonSelAll
            // 
            resources.ApplyResources(this.buttonSelAll, "buttonSelAll");
            this.buttonSelAll.Name = "buttonSelAll";
            this.buttonSelAll.UseVisualStyleBackColor = true;
            this.buttonSelAll.Click += new System.EventHandler(this.buttonSelAll_Click);
            // 
            // buttonInputFile
            // 
            resources.ApplyResources(this.buttonInputFile, "buttonInputFile");
            this.buttonInputFile.Name = "buttonInputFile";
            this.buttonInputFile.UseVisualStyleBackColor = true;
            this.buttonInputFile.Click += new System.EventHandler(this.buttonInputFile_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // TextBoxDirSearch
            // 
            this.TextBoxDirSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.TextBoxDirSearch, "TextBoxDirSearch");
            this.TextBoxDirSearch.Name = "TextBoxDirSearch";
            this.TextBoxDirSearch.TextChanged += new System.EventHandler(this.TextBoxDirSearch_TextChanged);
            // 
            // ButtonFindFiles
            // 
            resources.ApplyResources(this.ButtonFindFiles, "ButtonFindFiles");
            this.ButtonFindFiles.Name = "ButtonFindFiles";
            this.ButtonFindFiles.UseVisualStyleBackColor = true;
            this.ButtonFindFiles.Click += new System.EventHandler(this.ButtonFindFiles_Click);
            // 
            // dataGridFindedFiles
            // 
            resources.ApplyResources(this.dataGridFindedFiles, "dataGridFindedFiles");
            this.dataGridFindedFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridFindedFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Checked,
            this.FileIcon,
            this.FileName,
            this.FileSize,
            this.FileDate,
            this.Comments,
            this.CompanyName,
            this.FileDescription,
            this.FileVersion,
            this.OriginalFilename,
            this.ProductName,
            this.Path});
            this.dataGridFindedFiles.Name = "dataGridFindedFiles";
            this.dataGridFindedFiles.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridFindedFiles_ColumnWidthChanged);
            this.dataGridFindedFiles.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridFindedFiles_DataError);
            // 
            // Checked
            // 
            this.Checked.Frozen = true;
            resources.ApplyResources(this.Checked, "Checked");
            this.Checked.Name = "Checked";
            // 
            // FileIcon
            // 
            this.FileIcon.Frozen = true;
            resources.ApplyResources(this.FileIcon, "FileIcon");
            this.FileIcon.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.FileIcon.Name = "FileIcon";
            // 
            // FileName
            // 
            this.FileName.Frozen = true;
            resources.ApplyResources(this.FileName, "FileName");
            this.FileName.Name = "FileName";
            // 
            // FileSize
            // 
            this.FileSize.Frozen = true;
            resources.ApplyResources(this.FileSize, "FileSize");
            this.FileSize.Name = "FileSize";
            // 
            // FileDate
            // 
            this.FileDate.Frozen = true;
            resources.ApplyResources(this.FileDate, "FileDate");
            this.FileDate.Name = "FileDate";
            // 
            // Comments
            // 
            this.Comments.Frozen = true;
            resources.ApplyResources(this.Comments, "Comments");
            this.Comments.Name = "Comments";
            // 
            // CompanyName
            // 
            this.CompanyName.Frozen = true;
            resources.ApplyResources(this.CompanyName, "CompanyName");
            this.CompanyName.Name = "CompanyName";
            // 
            // FileDescription
            // 
            this.FileDescription.Frozen = true;
            resources.ApplyResources(this.FileDescription, "FileDescription");
            this.FileDescription.Name = "FileDescription";
            // 
            // FileVersion
            // 
            this.FileVersion.Frozen = true;
            resources.ApplyResources(this.FileVersion, "FileVersion");
            this.FileVersion.Name = "FileVersion";
            // 
            // OriginalFilename
            // 
            this.OriginalFilename.Frozen = true;
            resources.ApplyResources(this.OriginalFilename, "OriginalFilename");
            this.OriginalFilename.Name = "OriginalFilename";
            // 
            // ProductName
            // 
            this.ProductName.Frozen = true;
            resources.ApplyResources(this.ProductName, "ProductName");
            this.ProductName.Name = "ProductName";
            // 
            // Path
            // 
            this.Path.Frozen = true;
            resources.ApplyResources(this.Path, "Path");
            this.Path.Name = "Path";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "about.ico");
            this.imageList1.Images.SetKeyName(1, "bloknot.ico");
            this.imageList1.Images.SetKeyName(2, "close-icon16.png");
            this.imageList1.Images.SetKeyName(3, "close-icon24.png");
            this.imageList1.Images.SetKeyName(4, "comp.ICO");
            this.imageList1.Images.SetKeyName(5, "delete.png");
            this.imageList1.Images.SetKeyName(6, "dellgroup.ICO");
            this.imageList1.Images.SetKeyName(7, "edit.png");
            this.imageList1.Images.SetKeyName(8, "favs.addto.png");
            this.imageList1.Images.SetKeyName(9, "favs.png");
            this.imageList1.Images.SetKeyName(10, "file48.png");
            this.imageList1.Images.SetKeyName(11, "fromdisk.ico");
            this.imageList1.Images.SetKeyName(12, "i.png");
            this.imageList1.Images.SetKeyName(13, "icontray.ico");
            this.imageList1.Images.SetKeyName(14, "next.png");
            this.imageList1.Images.SetKeyName(15, "pause.png");
            this.imageList1.Images.SetKeyName(16, "PC0111.ICO");
            this.imageList1.Images.SetKeyName(17, "pin_small.png");
            this.imageList1.Images.SetKeyName(18, "pin_small_pressed.png");
            this.imageList1.Images.SetKeyName(19, "ref0.ico");
            this.imageList1.Images.SetKeyName(20, "ref1.ico");
            this.imageList1.Images.SetKeyName(21, "ref30.ico");
            this.imageList1.Images.SetKeyName(22, "ref60.ico");
            this.imageList1.Images.SetKeyName(23, "refresh.png");
            this.imageList1.Images.SetKeyName(24, "reload.png");
            this.imageList1.Images.SetKeyName(25, "settings17.png");
            this.imageList1.Images.SetKeyName(26, "settings48.png");
            this.imageList1.Images.SetKeyName(27, "user.ico");
            this.imageList1.Images.SetKeyName(28, "users.ico");
            this.imageList1.Images.SetKeyName(29, "group.ICO");
            this.imageList1.Images.SetKeyName(30, "search.png");
            this.imageList1.Images.SetKeyName(31, "script.png");
            // 
            // tabPageUniversalList
            // 
            this.tabPageUniversalList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tabPageUniversalList.Controls.Add(this.buttonUpdateUniversalList);
            this.tabPageUniversalList.Controls.Add(this.buttonRefreshUniversalList);
            this.tabPageUniversalList.Controls.Add(this.textBoxUniversalListFindName);
            this.tabPageUniversalList.Controls.Add(this.label10);
            this.tabPageUniversalList.Controls.Add(this.comboBoxFormatImages);
            this.tabPageUniversalList.Controls.Add(this.label25);
            this.tabPageUniversalList.Controls.Add(this.buttonUnloadUnuversal);
            this.tabPageUniversalList.Controls.Add(this.buttonAtomInvertCheckAll);
            this.tabPageUniversalList.Controls.Add(this.buttonAtomUnCheckAll);
            this.tabPageUniversalList.Controls.Add(this.buttonAtomCheckAll);
            this.tabPageUniversalList.Controls.Add(this.buttonAtomCDeleteChecked);
            this.tabPageUniversalList.Controls.Add(this.comboBoxDropDataFormats);
            this.tabPageUniversalList.Controls.Add(this.label11);
            this.tabPageUniversalList.Controls.Add(this.dataGridViewUniversalItem);
            this.tabPageUniversalList.Controls.Add(this.label9);
            this.tabPageUniversalList.Controls.Add(this.textBoxUniversalFindName);
            this.tabPageUniversalList.Controls.Add(this.button10);
            this.tabPageUniversalList.Controls.Add(this.button11);
            this.tabPageUniversalList.Controls.Add(this.dataGridViewUniversal);
            resources.ApplyResources(this.tabPageUniversalList, "tabPageUniversalList");
            this.tabPageUniversalList.Name = "tabPageUniversalList";
            // 
            // buttonUpdateUniversalList
            // 
            resources.ApplyResources(this.buttonUpdateUniversalList, "buttonUpdateUniversalList");
            this.buttonUpdateUniversalList.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonUpdateUniversalList.Name = "buttonUpdateUniversalList";
            this.toolTip1.SetToolTip(this.buttonUpdateUniversalList, resources.GetString("buttonUpdateUniversalList.ToolTip"));
            this.buttonUpdateUniversalList.UseVisualStyleBackColor = true;
            this.buttonUpdateUniversalList.Click += new System.EventHandler(this.buttonUpdateUniversalList_Click);
            // 
            // buttonRefreshUniversalList
            // 
            resources.ApplyResources(this.buttonRefreshUniversalList, "buttonRefreshUniversalList");
            this.buttonRefreshUniversalList.Image = global::ComputerToolkit.Properties.Resources.reload;
            this.buttonRefreshUniversalList.Name = "buttonRefreshUniversalList";
            this.toolTip1.SetToolTip(this.buttonRefreshUniversalList, resources.GetString("buttonRefreshUniversalList.ToolTip"));
            this.buttonRefreshUniversalList.UseVisualStyleBackColor = true;
            this.buttonRefreshUniversalList.Click += new System.EventHandler(this.buttonRefreshUniversalList_Click);
            // 
            // textBoxUniversalListFindName
            // 
            this.textBoxUniversalListFindName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxUniversalListFindName, "textBoxUniversalListFindName");
            this.textBoxUniversalListFindName.Name = "textBoxUniversalListFindName";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // comboBoxFormatImages
            // 
            resources.ApplyResources(this.comboBoxFormatImages, "comboBoxFormatImages");
            this.comboBoxFormatImages.FormattingEnabled = true;
            this.comboBoxFormatImages.Name = "comboBoxFormatImages";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // buttonUnloadUnuversal
            // 
            resources.ApplyResources(this.buttonUnloadUnuversal, "buttonUnloadUnuversal");
            this.buttonUnloadUnuversal.Image = global::ComputerToolkit.Properties.Resources.save_as;
            this.buttonUnloadUnuversal.Name = "buttonUnloadUnuversal";
            this.buttonUnloadUnuversal.UseVisualStyleBackColor = true;
            this.buttonUnloadUnuversal.Click += new System.EventHandler(this.buttonUnloadUnuversal_Click);
            // 
            // buttonAtomInvertCheckAll
            // 
            resources.ApplyResources(this.buttonAtomInvertCheckAll, "buttonAtomInvertCheckAll");
            this.buttonAtomInvertCheckAll.Name = "buttonAtomInvertCheckAll";
            this.buttonAtomInvertCheckAll.UseVisualStyleBackColor = true;
            this.buttonAtomInvertCheckAll.Click += new System.EventHandler(this.buttonAtomInvertCheckAll_Click);
            // 
            // buttonAtomUnCheckAll
            // 
            resources.ApplyResources(this.buttonAtomUnCheckAll, "buttonAtomUnCheckAll");
            this.buttonAtomUnCheckAll.Name = "buttonAtomUnCheckAll";
            this.buttonAtomUnCheckAll.UseVisualStyleBackColor = true;
            this.buttonAtomUnCheckAll.Click += new System.EventHandler(this.buttonAtomUnCheckAll_Click);
            // 
            // buttonAtomCheckAll
            // 
            resources.ApplyResources(this.buttonAtomCheckAll, "buttonAtomCheckAll");
            this.buttonAtomCheckAll.Name = "buttonAtomCheckAll";
            this.buttonAtomCheckAll.UseVisualStyleBackColor = true;
            this.buttonAtomCheckAll.Click += new System.EventHandler(this.buttonAtomCheckAll_Click);
            // 
            // buttonAtomCDeleteChecked
            // 
            resources.ApplyResources(this.buttonAtomCDeleteChecked, "buttonAtomCDeleteChecked");
            this.buttonAtomCDeleteChecked.Name = "buttonAtomCDeleteChecked";
            this.buttonAtomCDeleteChecked.UseVisualStyleBackColor = true;
            this.buttonAtomCDeleteChecked.Click += new System.EventHandler(this.buttonAtomCDeleteChecked_Click);
            // 
            // comboBoxDropDataFormats
            // 
            resources.ApplyResources(this.comboBoxDropDataFormats, "comboBoxDropDataFormats");
            this.comboBoxDropDataFormats.FormattingEnabled = true;
            this.comboBoxDropDataFormats.Name = "comboBoxDropDataFormats";
            this.comboBoxDropDataFormats.SelectedIndexChanged += new System.EventHandler(this.comboBoxDropDataFormats_SelectedIndexChanged);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            this.toolTip1.SetToolTip(this.label11, resources.GetString("label11.ToolTip"));
            // 
            // dataGridViewUniversalItem
            // 
            this.dataGridViewUniversalItem.AllowDrop = true;
            resources.ApplyResources(this.dataGridViewUniversalItem, "dataGridViewUniversalItem");
            this.dataGridViewUniversalItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUniversalItem.Name = "dataGridViewUniversalItem";
            this.dataGridViewUniversalItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewUniversalItem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUniversalItem_CellContentClick);
            this.dataGridViewUniversalItem.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUniversalItem_CellValueChanged);
            this.dataGridViewUniversalItem.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridViewUniversalItem_ColumnWidthChanged);
            this.dataGridViewUniversalItem.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewUniversalItem_DataError);
            this.dataGridViewUniversalItem.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUniversalItem_RowEnter);
            this.dataGridViewUniversalItem.DragEnter += new System.Windows.Forms.DragEventHandler(this.dataGridViewUniversalItem_DragEnter);
            this.dataGridViewUniversalItem.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridViewUniversalItem_MouseClick);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // textBoxUniversalFindName
            // 
            this.textBoxUniversalFindName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxUniversalFindName, "textBoxUniversalFindName");
            this.textBoxUniversalFindName.Name = "textBoxUniversalFindName";
            // 
            // button10
            // 
            resources.ApplyResources(this.button10, "button10");
            this.button10.Name = "button10";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            resources.ApplyResources(this.button11, "button11");
            this.button11.Name = "button11";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // dataGridViewUniversal
            // 
            resources.ApplyResources(this.dataGridViewUniversal, "dataGridViewUniversal");
            this.dataGridViewUniversal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUniversal.Name = "dataGridViewUniversal";
            this.dataGridViewUniversal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewUniversal.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUniversal_CellClick);
            this.dataGridViewUniversal.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUniversal_CellDoubleClick);
            this.dataGridViewUniversal.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUniversal_CellValueChanged);
            this.dataGridViewUniversal.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridViewUniversal_ColumnWidthChanged);
            this.dataGridViewUniversal.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewUniversal_DataError);
            this.dataGridViewUniversal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridViewUniversal_KeyDown);
            this.dataGridViewUniversal.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridViewUniversal_MouseClick);
            // 
            // tabPageFilesQuick
            // 
            this.tabPageFilesQuick.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tabPageFilesQuick.Controls.Add(this.checkBoxPin);
            this.tabPageFilesQuick.Controls.Add(this.button5);
            this.tabPageFilesQuick.Controls.Add(this.button6);
            this.tabPageFilesQuick.Controls.Add(this.label7);
            this.tabPageFilesQuick.Controls.Add(this.TextBoxFileNameQuick);
            this.tabPageFilesQuick.Controls.Add(this.buttonQprogDelete);
            this.tabPageFilesQuick.Controls.Add(this.buttonQporgAdd);
            this.tabPageFilesQuick.Controls.Add(this.DataGridMainFilesQuick);
            resources.ApplyResources(this.tabPageFilesQuick, "tabPageFilesQuick");
            this.tabPageFilesQuick.Name = "tabPageFilesQuick";
            // 
            // checkBoxPin
            // 
            resources.ApplyResources(this.checkBoxPin, "checkBoxPin");
            this.checkBoxPin.Name = "checkBoxPin";
            this.toolTip1.SetToolTip(this.checkBoxPin, resources.GetString("checkBoxPin.ToolTip"));
            this.checkBoxPin.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            resources.ApplyResources(this.button5, "button5");
            this.button5.Name = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            resources.ApplyResources(this.button6, "button6");
            this.button6.Name = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // TextBoxFileNameQuick
            // 
            this.TextBoxFileNameQuick.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.TextBoxFileNameQuick, "TextBoxFileNameQuick");
            this.TextBoxFileNameQuick.Name = "TextBoxFileNameQuick";
            // 
            // buttonQprogDelete
            // 
            resources.ApplyResources(this.buttonQprogDelete, "buttonQprogDelete");
            this.buttonQprogDelete.Name = "buttonQprogDelete";
            this.buttonQprogDelete.UseVisualStyleBackColor = true;
            this.buttonQprogDelete.Click += new System.EventHandler(this.buttonQprogDelete_Click);
            // 
            // buttonQporgAdd
            // 
            resources.ApplyResources(this.buttonQporgAdd, "buttonQporgAdd");
            this.buttonQporgAdd.Name = "buttonQporgAdd";
            this.buttonQporgAdd.UseVisualStyleBackColor = true;
            this.buttonQporgAdd.Click += new System.EventHandler(this.buttonQporgAdd_Click);
            // 
            // DataGridMainFilesQuick
            // 
            resources.ApplyResources(this.DataGridMainFilesQuick, "DataGridMainFilesQuick");
            this.DataGridMainFilesQuick.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridMainFilesQuick.Name = "DataGridMainFilesQuick";
            this.DataGridMainFilesQuick.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridMainFilesQuick_CellContentClick);
            this.DataGridMainFilesQuick.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridMainFilesQuick_CellEndEdit);
            this.DataGridMainFilesQuick.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.DataGridMainFilesQuick_ColumnWidthChanged);
            this.DataGridMainFilesQuick.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridMainFilesQuick_DataError);
            this.DataGridMainFilesQuick.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridMainFilesQuick_KeyDown);
            this.DataGridMainFilesQuick.Leave += new System.EventHandler(this.DataGridMainFilesQuick_Leave);
            this.DataGridMainFilesQuick.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridMainFilesQuick_MouseClick);
            // 
            // tabPageNotes
            // 
            this.tabPageNotes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tabPageNotes.Controls.Add(this.buttonSynchronizeNotes);
            this.tabPageNotes.Controls.Add(this.label6);
            this.tabPageNotes.Controls.Add(this.textBoxNotesFindContent);
            this.tabPageNotes.Controls.Add(this.buttonReloadList);
            this.tabPageNotes.Controls.Add(this.label3);
            this.tabPageNotes.Controls.Add(this.textBoxNotesFindName);
            this.tabPageNotes.Controls.Add(this.buttonDelSelNote);
            this.tabPageNotes.Controls.Add(this.buttonAddNote);
            this.tabPageNotes.Controls.Add(this.dataGridNotes);
            this.tabPageNotes.ForeColor = System.Drawing.SystemColors.ControlText;
            resources.ApplyResources(this.tabPageNotes, "tabPageNotes");
            this.tabPageNotes.Name = "tabPageNotes";
            // 
            // buttonSynchronizeNotes
            // 
            resources.ApplyResources(this.buttonSynchronizeNotes, "buttonSynchronizeNotes");
            this.buttonSynchronizeNotes.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonSynchronizeNotes.Name = "buttonSynchronizeNotes";
            this.toolTip1.SetToolTip(this.buttonSynchronizeNotes, resources.GetString("buttonSynchronizeNotes.ToolTip"));
            this.buttonSynchronizeNotes.UseVisualStyleBackColor = true;
            this.buttonSynchronizeNotes.Click += new System.EventHandler(this.buttonSynchronizeNotes_Click);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // textBoxNotesFindContent
            // 
            this.textBoxNotesFindContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxNotesFindContent, "textBoxNotesFindContent");
            this.textBoxNotesFindContent.Name = "textBoxNotesFindContent";
            this.textBoxNotesFindContent.TextChanged += new System.EventHandler(this.textBoxNotesFindContent_TextChanged);
            // 
            // buttonReloadList
            // 
            resources.ApplyResources(this.buttonReloadList, "buttonReloadList");
            this.buttonReloadList.Image = global::ComputerToolkit.Properties.Resources.reload;
            this.buttonReloadList.Name = "buttonReloadList";
            this.toolTip1.SetToolTip(this.buttonReloadList, resources.GetString("buttonReloadList.ToolTip"));
            this.buttonReloadList.UseVisualStyleBackColor = true;
            this.buttonReloadList.Click += new System.EventHandler(this.buttonReloadList_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // textBoxNotesFindName
            // 
            this.textBoxNotesFindName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxNotesFindName, "textBoxNotesFindName");
            this.textBoxNotesFindName.Name = "textBoxNotesFindName";
            this.textBoxNotesFindName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // buttonDelSelNote
            // 
            resources.ApplyResources(this.buttonDelSelNote, "buttonDelSelNote");
            this.buttonDelSelNote.Name = "buttonDelSelNote";
            this.buttonDelSelNote.UseVisualStyleBackColor = true;
            this.buttonDelSelNote.Click += new System.EventHandler(this.buttonDelSelNote_Click);
            // 
            // buttonAddNote
            // 
            resources.ApplyResources(this.buttonAddNote, "buttonAddNote");
            this.buttonAddNote.Name = "buttonAddNote";
            this.buttonAddNote.UseVisualStyleBackColor = true;
            this.buttonAddNote.Click += new System.EventHandler(this.buttonAddNote_Click);
            // 
            // dataGridNotes
            // 
            resources.ApplyResources(this.dataGridNotes, "dataGridNotes");
            this.dataGridNotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridNotes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dataGridNotes.Name = "dataGridNotes";
            this.dataGridNotes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridNotes.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridNotes_CellDoubleClick);
            this.dataGridNotes.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridNotes_CellEndEdit);
            this.dataGridNotes.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridNotes_CellValueChanged);
            this.dataGridNotes.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridNotes_ColumnWidthChanged);
            this.dataGridNotes.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridNotes_DataError);
            this.dataGridNotes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridNotes_KeyDown);
            this.dataGridNotes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridNotes_KeyPress);
            this.dataGridNotes.Leave += new System.EventHandler(this.dataGridNotes_Leave);
            this.dataGridNotes.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridNotes_MouseClick);
            // 
            // tabPageGoogle
            // 
            this.tabPageGoogle.Controls.Add(this.buttonCheckGoogleEvents);
            this.tabPageGoogle.Controls.Add(this.checkBoxGoogleEnable);
            this.tabPageGoogle.Controls.Add(this.buttonGoogleSAve);
            this.tabPageGoogle.Controls.Add(this.buttonRefreshGoogleCalendar);
            this.tabPageGoogle.Controls.Add(this.dataGridViewGoogle);
            this.tabPageGoogle.Controls.Add(this.groupBox3);
            resources.ApplyResources(this.tabPageGoogle, "tabPageGoogle");
            this.tabPageGoogle.Name = "tabPageGoogle";
            this.tabPageGoogle.UseVisualStyleBackColor = true;
            // 
            // buttonCheckGoogleEvents
            // 
            resources.ApplyResources(this.buttonCheckGoogleEvents, "buttonCheckGoogleEvents");
            this.buttonCheckGoogleEvents.Name = "buttonCheckGoogleEvents";
            this.buttonCheckGoogleEvents.UseVisualStyleBackColor = true;
            this.buttonCheckGoogleEvents.Click += new System.EventHandler(this.buttonCheckGoogleEvents_Click);
            // 
            // checkBoxGoogleEnable
            // 
            resources.ApplyResources(this.checkBoxGoogleEnable, "checkBoxGoogleEnable");
            this.checkBoxGoogleEnable.Name = "checkBoxGoogleEnable";
            this.checkBoxGoogleEnable.UseVisualStyleBackColor = true;
            this.checkBoxGoogleEnable.CheckedChanged += new System.EventHandler(this.checkBoxGoogleEnable_CheckedChanged);
            // 
            // buttonGoogleSAve
            // 
            resources.ApplyResources(this.buttonGoogleSAve, "buttonGoogleSAve");
            this.buttonGoogleSAve.Image = global::ComputerToolkit.Properties.Resources.check1;
            this.buttonGoogleSAve.Name = "buttonGoogleSAve";
            this.buttonGoogleSAve.UseVisualStyleBackColor = true;
            this.buttonGoogleSAve.Click += new System.EventHandler(this.buttonGoogleSAve_Click);
            // 
            // buttonRefreshGoogleCalendar
            // 
            resources.ApplyResources(this.buttonRefreshGoogleCalendar, "buttonRefreshGoogleCalendar");
            this.buttonRefreshGoogleCalendar.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonRefreshGoogleCalendar.Name = "buttonRefreshGoogleCalendar";
            this.toolTip1.SetToolTip(this.buttonRefreshGoogleCalendar, resources.GetString("buttonRefreshGoogleCalendar.ToolTip"));
            this.buttonRefreshGoogleCalendar.UseVisualStyleBackColor = true;
            this.buttonRefreshGoogleCalendar.Click += new System.EventHandler(this.buttonRefreshGoogleCalendar_Click);
            // 
            // dataGridViewGoogle
            // 
            resources.ApplyResources(this.dataGridViewGoogle, "dataGridViewGoogle");
            this.dataGridViewGoogle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGoogle.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewGoogle.Name = "dataGridViewGoogle";
            this.dataGridViewGoogle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxTimeToRefreshUpdateGoogle);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.textBoxGooglePasswordToEnter);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.textBoxGoogleUser);
            this.groupBox3.Controls.Add(this.textBoxTimeToUpdateGoogle);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(this.textBoxGooglePassword);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // textBoxTimeToRefreshUpdateGoogle
            // 
            resources.ApplyResources(this.textBoxTimeToRefreshUpdateGoogle, "textBoxTimeToRefreshUpdateGoogle");
            this.textBoxTimeToRefreshUpdateGoogle.Name = "textBoxTimeToRefreshUpdateGoogle";
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.Name = "label30";
            // 
            // textBoxGooglePasswordToEnter
            // 
            this.textBoxGooglePasswordToEnter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxGooglePasswordToEnter, "textBoxGooglePasswordToEnter");
            this.textBoxGooglePasswordToEnter.Name = "textBoxGooglePasswordToEnter";
            this.textBoxGooglePasswordToEnter.TextChanged += new System.EventHandler(this.textBoxGooglePasswordToEnter_TextChanged);
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.Name = "label28";
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            // 
            // textBoxGoogleUser
            // 
            this.textBoxGoogleUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxGoogleUser, "textBoxGoogleUser");
            this.textBoxGoogleUser.Name = "textBoxGoogleUser";
            // 
            // textBoxTimeToUpdateGoogle
            // 
            resources.ApplyResources(this.textBoxTimeToUpdateGoogle, "textBoxTimeToUpdateGoogle");
            this.textBoxTimeToUpdateGoogle.Name = "textBoxTimeToUpdateGoogle";
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.Name = "label27";
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.textBox2, "textBox2");
            this.textBox2.Name = "textBox2";
            // 
            // textBoxGooglePassword
            // 
            this.textBoxGooglePassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxGooglePassword, "textBoxGooglePassword");
            this.textBoxGooglePassword.Name = "textBoxGooglePassword";
            // 
            // tabPageYandexDisk
            // 
            this.tabPageYandexDisk.Controls.Add(this.buttonYandexDiskConnect);
            this.tabPageYandexDisk.Controls.Add(this.buttonYandexRefresh);
            this.tabPageYandexDisk.Controls.Add(this.dataGridViewYandexDisk);
            this.tabPageYandexDisk.Controls.Add(this.groupBox4);
            resources.ApplyResources(this.tabPageYandexDisk, "tabPageYandexDisk");
            this.tabPageYandexDisk.Name = "tabPageYandexDisk";
            this.tabPageYandexDisk.UseVisualStyleBackColor = true;
            // 
            // buttonYandexDiskConnect
            // 
            resources.ApplyResources(this.buttonYandexDiskConnect, "buttonYandexDiskConnect");
            this.buttonYandexDiskConnect.Image = global::ComputerToolkit.Properties.Resources.check1;
            this.buttonYandexDiskConnect.Name = "buttonYandexDiskConnect";
            this.buttonYandexDiskConnect.UseVisualStyleBackColor = true;
            this.buttonYandexDiskConnect.Click += new System.EventHandler(this.buttonDropBoxConnect_Click);
            // 
            // buttonYandexRefresh
            // 
            resources.ApplyResources(this.buttonYandexRefresh, "buttonYandexRefresh");
            this.buttonYandexRefresh.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonYandexRefresh.Name = "buttonYandexRefresh";
            this.toolTip1.SetToolTip(this.buttonYandexRefresh, resources.GetString("buttonYandexRefresh.ToolTip"));
            this.buttonYandexRefresh.UseVisualStyleBackColor = true;
            // 
            // dataGridViewYandexDisk
            // 
            resources.ApplyResources(this.dataGridViewYandexDisk, "dataGridViewYandexDisk");
            this.dataGridViewYandexDisk.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewYandexDisk.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewYandexDisk.Name = "dataGridViewYandexDisk";
            this.dataGridViewYandexDisk.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxDropBoxPasswordToEnter);
            this.groupBox4.Controls.Add(this.label34);
            this.groupBox4.Controls.Add(this.label35);
            this.groupBox4.Controls.Add(this.textBoxDropBoxUser);
            this.groupBox4.Controls.Add(this.textBox8);
            this.groupBox4.Controls.Add(this.textBoxDropBoxPassword);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // textBoxDropBoxPasswordToEnter
            // 
            this.textBoxDropBoxPasswordToEnter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxDropBoxPasswordToEnter, "textBoxDropBoxPasswordToEnter");
            this.textBoxDropBoxPasswordToEnter.Name = "textBoxDropBoxPasswordToEnter";
            // 
            // label34
            // 
            resources.ApplyResources(this.label34, "label34");
            this.label34.Name = "label34";
            // 
            // label35
            // 
            resources.ApplyResources(this.label35, "label35");
            this.label35.Name = "label35";
            // 
            // textBoxDropBoxUser
            // 
            this.textBoxDropBoxUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxDropBoxUser, "textBoxDropBoxUser");
            this.textBoxDropBoxUser.Name = "textBoxDropBoxUser";
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.textBox8, "textBox8");
            this.textBox8.Name = "textBox8";
            // 
            // textBoxDropBoxPassword
            // 
            this.textBoxDropBoxPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxDropBoxPassword, "textBoxDropBoxPassword");
            this.textBoxDropBoxPassword.Name = "textBoxDropBoxPassword";
            // 
            // tabPageMailRu
            // 
            this.tabPageMailRu.Controls.Add(this.buttonMailRuConnect);
            this.tabPageMailRu.Controls.Add(this.button13);
            this.tabPageMailRu.Controls.Add(this.dataGridViewMailRu);
            this.tabPageMailRu.Controls.Add(this.groupBox5);
            resources.ApplyResources(this.tabPageMailRu, "tabPageMailRu");
            this.tabPageMailRu.Name = "tabPageMailRu";
            this.tabPageMailRu.UseVisualStyleBackColor = true;
            // 
            // buttonMailRuConnect
            // 
            resources.ApplyResources(this.buttonMailRuConnect, "buttonMailRuConnect");
            this.buttonMailRuConnect.Image = global::ComputerToolkit.Properties.Resources.check1;
            this.buttonMailRuConnect.Name = "buttonMailRuConnect";
            this.buttonMailRuConnect.UseVisualStyleBackColor = true;
            this.buttonMailRuConnect.Click += new System.EventHandler(this.buttonMailRuConnect_Click);
            // 
            // button13
            // 
            resources.ApplyResources(this.button13, "button13");
            this.button13.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.button13.Name = "button13";
            this.toolTip1.SetToolTip(this.button13, resources.GetString("button13.ToolTip"));
            this.button13.UseVisualStyleBackColor = true;
            // 
            // dataGridViewMailRu
            // 
            resources.ApplyResources(this.dataGridViewMailRu, "dataGridViewMailRu");
            this.dataGridViewMailRu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMailRu.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewMailRu.Name = "dataGridViewMailRu";
            this.dataGridViewMailRu.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Controls.Add(this.textBox4);
            this.groupBox5.Controls.Add(this.textBox5);
            this.groupBox5.Controls.Add(this.textBox6);
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            // 
            // label36
            // 
            resources.ApplyResources(this.label36, "label36");
            this.label36.Name = "label36";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.Name = "label37";
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBox4, "textBox4");
            this.textBox4.Name = "textBox4";
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.textBox5, "textBox5");
            this.textBox5.Name = "textBox5";
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBox6, "textBox6");
            this.textBox6.Name = "textBox6";
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.Controls.Add(this.buttonDownloadSettings);
            this.tabPageSettings.Controls.Add(this.buttonUploadsettings);
            this.tabPageSettings.Controls.Add(this.buttonDownloadExe);
            this.tabPageSettings.Controls.Add(this.label39);
            this.tabPageSettings.Controls.Add(this.textBoxFileExtensionsToAdd);
            this.tabPageSettings.Controls.Add(this.checkBoxCopyMessageToclipboard);
            this.tabPageSettings.Controls.Add(this.buttonLoadAll);
            this.tabPageSettings.Controls.Add(this.buttonSaveAllLists);
            this.tabPageSettings.Controls.Add(this.label38);
            this.tabPageSettings.Controls.Add(this.textBoxTeamViewerGUIDToOpenConnection);
            this.tabPageSettings.Controls.Add(this.buttonBackupExe);
            this.tabPageSettings.Controls.Add(this.buttonSynchronizeAll);
            this.tabPageSettings.Controls.Add(this.label33);
            this.tabPageSettings.Controls.Add(this.textBoxHotkeys);
            this.tabPageSettings.Controls.Add(this.checkBoxCheckUpdate);
            this.tabPageSettings.Controls.Add(this.label24);
            this.tabPageSettings.Controls.Add(this.linkLabel1);
            this.tabPageSettings.Controls.Add(this.comboBoxLocalization);
            this.tabPageSettings.Controls.Add(this.label23);
            this.tabPageSettings.Controls.Add(this.textBoxTasks);
            this.tabPageSettings.Controls.Add(this.groupBox2);
            this.tabPageSettings.Controls.Add(this.buttonFixPathFiles);
            this.tabPageSettings.Controls.Add(this.checkBoxRunWithWindows);
            this.tabPageSettings.Controls.Add(this.buttonCheckUpdates);
            this.tabPageSettings.Controls.Add(this.groupBox1);
            this.tabPageSettings.Controls.Add(this.label19);
            this.tabPageSettings.Controls.Add(this.textBoxProxy);
            this.tabPageSettings.Controls.Add(this.textBoxCsvDelimiter);
            this.tabPageSettings.Controls.Add(this.label18);
            this.tabPageSettings.Controls.Add(this.buttonSaveSettings);
            this.tabPageSettings.Controls.Add(this.label17);
            this.tabPageSettings.Controls.Add(this.textBoxFilesQuickFileName);
            this.tabPageSettings.Controls.Add(this.label16);
            this.tabPageSettings.Controls.Add(this.textBoxScriptsFileName);
            this.tabPageSettings.Controls.Add(this.label15);
            this.tabPageSettings.Controls.Add(this.textBoxConnectionsFileName);
            this.tabPageSettings.Controls.Add(this.label14);
            this.tabPageSettings.Controls.Add(this.textBoxNotesFileName);
            this.tabPageSettings.Controls.Add(this.label13);
            this.tabPageSettings.Controls.Add(this.textBoxFilesFileName);
            this.tabPageSettings.Controls.Add(this.label12);
            this.tabPageSettings.Controls.Add(this.textBoxFileNameUniversal);
            this.tabPageSettings.Controls.Add(this.checkBoxMakeSubmenuInConnections);
            this.tabPageSettings.Controls.Add(this.checkBoxShowGroupsInConnections);
            this.tabPageSettings.Controls.Add(this.buttonChoseRootDir);
            this.tabPageSettings.Controls.Add(this.label1);
            this.tabPageSettings.Controls.Add(this.textBoxRootDir);
            this.tabPageSettings.Controls.Add(this.checkBoxConnectionsFormTopmost);
            this.tabPageSettings.Controls.Add(this.checkBoxMainFormTopmost);
            resources.ApplyResources(this.tabPageSettings, "tabPageSettings");
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.UseVisualStyleBackColor = true;
            this.tabPageSettings.Click += new System.EventHandler(this.tabPageSettings_Click);
            // 
            // buttonDownloadSettings
            // 
            resources.ApplyResources(this.buttonDownloadSettings, "buttonDownloadSettings");
            this.buttonDownloadSettings.Image = global::ComputerToolkit.Properties.Resources.download;
            this.buttonDownloadSettings.Name = "buttonDownloadSettings";
            this.buttonDownloadSettings.UseVisualStyleBackColor = true;
            this.buttonDownloadSettings.Click += new System.EventHandler(this.buttonDownloadSettings_Click);
            // 
            // buttonUploadsettings
            // 
            resources.ApplyResources(this.buttonUploadsettings, "buttonUploadsettings");
            this.buttonUploadsettings.Image = global::ComputerToolkit.Properties.Resources.upload;
            this.buttonUploadsettings.Name = "buttonUploadsettings";
            this.buttonUploadsettings.UseVisualStyleBackColor = true;
            this.buttonUploadsettings.Click += new System.EventHandler(this.buttonUploadsettings_Click);
            // 
            // buttonDownloadExe
            // 
            resources.ApplyResources(this.buttonDownloadExe, "buttonDownloadExe");
            this.buttonDownloadExe.Image = global::ComputerToolkit.Properties.Resources.download;
            this.buttonDownloadExe.Name = "buttonDownloadExe";
            this.buttonDownloadExe.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            resources.ApplyResources(this.label39, "label39");
            this.label39.Name = "label39";
            // 
            // textBoxFileExtensionsToAdd
            // 
            this.textBoxFileExtensionsToAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxFileExtensionsToAdd, "textBoxFileExtensionsToAdd");
            this.textBoxFileExtensionsToAdd.Name = "textBoxFileExtensionsToAdd";
            // 
            // checkBoxCopyMessageToclipboard
            // 
            resources.ApplyResources(this.checkBoxCopyMessageToclipboard, "checkBoxCopyMessageToclipboard");
            this.checkBoxCopyMessageToclipboard.Name = "checkBoxCopyMessageToclipboard";
            this.checkBoxCopyMessageToclipboard.UseVisualStyleBackColor = true;
            this.checkBoxCopyMessageToclipboard.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // buttonLoadAll
            // 
            resources.ApplyResources(this.buttonLoadAll, "buttonLoadAll");
            this.buttonLoadAll.Image = global::ComputerToolkit.Properties.Resources.folder;
            this.buttonLoadAll.Name = "buttonLoadAll";
            this.buttonLoadAll.UseVisualStyleBackColor = true;
            this.buttonLoadAll.Click += new System.EventHandler(this.buttonLoadAll_Click);
            // 
            // buttonSaveAllLists
            // 
            resources.ApplyResources(this.buttonSaveAllLists, "buttonSaveAllLists");
            this.buttonSaveAllLists.Image = global::ComputerToolkit.Properties.Resources.save;
            this.buttonSaveAllLists.Name = "buttonSaveAllLists";
            this.buttonSaveAllLists.UseVisualStyleBackColor = true;
            this.buttonSaveAllLists.Click += new System.EventHandler(this.buttonSaveAllLists_Click);
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.Name = "label38";
            // 
            // textBoxTeamViewerGUIDToOpenConnection
            // 
            this.textBoxTeamViewerGUIDToOpenConnection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxTeamViewerGUIDToOpenConnection, "textBoxTeamViewerGUIDToOpenConnection");
            this.textBoxTeamViewerGUIDToOpenConnection.Name = "textBoxTeamViewerGUIDToOpenConnection";
            // 
            // buttonBackupExe
            // 
            resources.ApplyResources(this.buttonBackupExe, "buttonBackupExe");
            this.buttonBackupExe.Image = global::ComputerToolkit.Properties.Resources.upload;
            this.buttonBackupExe.Name = "buttonBackupExe";
            this.buttonBackupExe.UseVisualStyleBackColor = true;
            this.buttonBackupExe.Click += new System.EventHandler(this.buttonBackupExe_Click);
            // 
            // buttonSynchronizeAll
            // 
            resources.ApplyResources(this.buttonSynchronizeAll, "buttonSynchronizeAll");
            this.buttonSynchronizeAll.Image = global::ComputerToolkit.Properties.Resources._ref;
            this.buttonSynchronizeAll.Name = "buttonSynchronizeAll";
            this.buttonSynchronizeAll.UseVisualStyleBackColor = true;
            this.buttonSynchronizeAll.Click += new System.EventHandler(this.buttonSynchronizeAll_Click);
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.Name = "label33";
            // 
            // textBoxHotkeys
            // 
            this.textBoxHotkeys.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxHotkeys, "textBoxHotkeys");
            this.textBoxHotkeys.Name = "textBoxHotkeys";
            // 
            // checkBoxCheckUpdate
            // 
            resources.ApplyResources(this.checkBoxCheckUpdate, "checkBoxCheckUpdate");
            this.checkBoxCheckUpdate.Name = "checkBoxCheckUpdate";
            this.checkBoxCheckUpdate.UseVisualStyleBackColor = true;
            this.checkBoxCheckUpdate.CheckedChanged += new System.EventHandler(this.checkBoxCheckUpdate_CheckedChanged);
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // linkLabel1
            // 
            resources.ApplyResources(this.linkLabel1, "linkLabel1");
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.TabStop = true;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // comboBoxLocalization
            // 
            this.comboBoxLocalization.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxLocalization, "comboBoxLocalization");
            this.comboBoxLocalization.Name = "comboBoxLocalization";
            this.comboBoxLocalization.SelectedIndexChanged += new System.EventHandler(this.comboBoxLocalization_SelectedIndexChanged);
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // textBoxTasks
            // 
            this.textBoxTasks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxTasks, "textBoxTasks");
            this.textBoxTasks.Name = "textBoxTasks";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxUnloadList);
            this.groupBox2.Controls.Add(this.buttonSaveFilesToXml);
            this.groupBox2.Controls.Add(this.buttonLoadFilesFromXml);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // comboBoxUnloadList
            // 
            resources.ApplyResources(this.comboBoxUnloadList, "comboBoxUnloadList");
            this.comboBoxUnloadList.FormattingEnabled = true;
            this.comboBoxUnloadList.Name = "comboBoxUnloadList";
            // 
            // buttonSaveFilesToXml
            // 
            resources.ApplyResources(this.buttonSaveFilesToXml, "buttonSaveFilesToXml");
            this.buttonSaveFilesToXml.Name = "buttonSaveFilesToXml";
            this.buttonSaveFilesToXml.UseVisualStyleBackColor = true;
            this.buttonSaveFilesToXml.Click += new System.EventHandler(this.buttonSaveFilesToXml_Click);
            // 
            // buttonLoadFilesFromXml
            // 
            resources.ApplyResources(this.buttonLoadFilesFromXml, "buttonLoadFilesFromXml");
            this.buttonLoadFilesFromXml.Name = "buttonLoadFilesFromXml";
            this.buttonLoadFilesFromXml.UseVisualStyleBackColor = true;
            this.buttonLoadFilesFromXml.Click += new System.EventHandler(this.buttonLoadFilesFromXml_Click);
            // 
            // buttonFixPathFiles
            // 
            resources.ApplyResources(this.buttonFixPathFiles, "buttonFixPathFiles");
            this.buttonFixPathFiles.Name = "buttonFixPathFiles";
            this.toolTip1.SetToolTip(this.buttonFixPathFiles, resources.GetString("buttonFixPathFiles.ToolTip"));
            this.buttonFixPathFiles.UseVisualStyleBackColor = true;
            this.buttonFixPathFiles.Click += new System.EventHandler(this.buttonFixPathFiles_Click);
            // 
            // checkBoxRunWithWindows
            // 
            resources.ApplyResources(this.checkBoxRunWithWindows, "checkBoxRunWithWindows");
            this.checkBoxRunWithWindows.Name = "checkBoxRunWithWindows";
            this.checkBoxRunWithWindows.UseVisualStyleBackColor = true;
            this.checkBoxRunWithWindows.CheckedChanged += new System.EventHandler(this.checkBoxRunWithWindows_CheckedChanged);
            // 
            // buttonCheckUpdates
            // 
            resources.ApplyResources(this.buttonCheckUpdates, "buttonCheckUpdates");
            this.buttonCheckUpdates.Image = global::ComputerToolkit.Properties.Resources.reload;
            this.buttonCheckUpdates.Name = "buttonCheckUpdates";
            this.buttonCheckUpdates.UseVisualStyleBackColor = true;
            this.buttonCheckUpdates.Click += new System.EventHandler(this.buttonCheckUpdates_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonAddUser);
            this.groupBox1.Controls.Add(this.checkBoxYandexDrive);
            this.groupBox1.Controls.Add(this.textBoxUserPasswordToEnter);
            this.groupBox1.Controls.Add(this.textBoxUser);
            this.groupBox1.Controls.Add(this.textBoxPassword);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.textBoxUserEmail);
            this.groupBox1.Controls.Add(this.buttonCheckUserConnect);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // buttonAddUser
            // 
            resources.ApplyResources(this.buttonAddUser, "buttonAddUser");
            this.buttonAddUser.Image = global::ComputerToolkit.Properties.Resources.add1;
            this.buttonAddUser.Name = "buttonAddUser";
            this.buttonAddUser.UseVisualStyleBackColor = true;
            this.buttonAddUser.Click += new System.EventHandler(this.buttonAddUser_Click);
            // 
            // checkBoxYandexDrive
            // 
            resources.ApplyResources(this.checkBoxYandexDrive, "checkBoxYandexDrive");
            this.checkBoxYandexDrive.ForeColor = System.Drawing.Color.DarkRed;
            this.checkBoxYandexDrive.Name = "checkBoxYandexDrive";
            this.checkBoxYandexDrive.UseVisualStyleBackColor = true;
            // 
            // textBoxUserPasswordToEnter
            // 
            this.textBoxUserPasswordToEnter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxUserPasswordToEnter, "textBoxUserPasswordToEnter");
            this.textBoxUserPasswordToEnter.Name = "textBoxUserPasswordToEnter";
            this.textBoxUserPasswordToEnter.TextChanged += new System.EventHandler(this.textBoxUserPasswordToEnter_TextChanged);
            // 
            // textBoxUser
            // 
            this.textBoxUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxUser, "textBoxUser");
            this.textBoxUser.Name = "textBoxUser";
            // 
            // textBoxPassword
            // 
            resources.ApplyResources(this.textBoxPassword, "textBoxPassword");
            this.textBoxPassword.Name = "textBoxPassword";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // textBoxUserEmail
            // 
            this.textBoxUserEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxUserEmail, "textBoxUserEmail");
            this.textBoxUserEmail.Name = "textBoxUserEmail";
            // 
            // buttonCheckUserConnect
            // 
            resources.ApplyResources(this.buttonCheckUserConnect, "buttonCheckUserConnect");
            this.buttonCheckUserConnect.Image = global::ComputerToolkit.Properties.Resources.check1;
            this.buttonCheckUserConnect.Name = "buttonCheckUserConnect";
            this.buttonCheckUserConnect.UseVisualStyleBackColor = true;
            this.buttonCheckUserConnect.Click += new System.EventHandler(this.buttonCheckUserConnect_Click);
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // textBoxProxy
            // 
            this.textBoxProxy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxProxy, "textBoxProxy");
            this.textBoxProxy.Name = "textBoxProxy";
            // 
            // textBoxCsvDelimiter
            // 
            this.textBoxCsvDelimiter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxCsvDelimiter, "textBoxCsvDelimiter");
            this.textBoxCsvDelimiter.Name = "textBoxCsvDelimiter";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // buttonSaveSettings
            // 
            resources.ApplyResources(this.buttonSaveSettings, "buttonSaveSettings");
            this.buttonSaveSettings.Image = global::ComputerToolkit.Properties.Resources.check1;
            this.buttonSaveSettings.Name = "buttonSaveSettings";
            this.buttonSaveSettings.UseVisualStyleBackColor = true;
            this.buttonSaveSettings.Click += new System.EventHandler(this.buttonSaveSettings_Click);
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // textBoxFilesQuickFileName
            // 
            this.textBoxFilesQuickFileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxFilesQuickFileName, "textBoxFilesQuickFileName");
            this.textBoxFilesQuickFileName.Name = "textBoxFilesQuickFileName";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // textBoxScriptsFileName
            // 
            this.textBoxScriptsFileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxScriptsFileName, "textBoxScriptsFileName");
            this.textBoxScriptsFileName.Name = "textBoxScriptsFileName";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // textBoxConnectionsFileName
            // 
            this.textBoxConnectionsFileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxConnectionsFileName, "textBoxConnectionsFileName");
            this.textBoxConnectionsFileName.Name = "textBoxConnectionsFileName";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // textBoxNotesFileName
            // 
            this.textBoxNotesFileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxNotesFileName, "textBoxNotesFileName");
            this.textBoxNotesFileName.Name = "textBoxNotesFileName";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // textBoxFilesFileName
            // 
            this.textBoxFilesFileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxFilesFileName, "textBoxFilesFileName");
            this.textBoxFilesFileName.Name = "textBoxFilesFileName";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // textBoxFileNameUniversal
            // 
            this.textBoxFileNameUniversal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxFileNameUniversal, "textBoxFileNameUniversal");
            this.textBoxFileNameUniversal.Name = "textBoxFileNameUniversal";
            // 
            // checkBoxMakeSubmenuInConnections
            // 
            resources.ApplyResources(this.checkBoxMakeSubmenuInConnections, "checkBoxMakeSubmenuInConnections");
            this.checkBoxMakeSubmenuInConnections.Name = "checkBoxMakeSubmenuInConnections";
            this.checkBoxMakeSubmenuInConnections.UseVisualStyleBackColor = true;
            this.checkBoxMakeSubmenuInConnections.CheckedChanged += new System.EventHandler(this.checkBoxMakeSubmenuInConnections_CheckedChanged);
            // 
            // checkBoxShowGroupsInConnections
            // 
            resources.ApplyResources(this.checkBoxShowGroupsInConnections, "checkBoxShowGroupsInConnections");
            this.checkBoxShowGroupsInConnections.Name = "checkBoxShowGroupsInConnections";
            this.checkBoxShowGroupsInConnections.UseVisualStyleBackColor = true;
            this.checkBoxShowGroupsInConnections.CheckedChanged += new System.EventHandler(this.checkBoxShowGroupsInConnections_CheckedChanged);
            // 
            // buttonChoseRootDir
            // 
            resources.ApplyResources(this.buttonChoseRootDir, "buttonChoseRootDir");
            this.buttonChoseRootDir.Name = "buttonChoseRootDir";
            this.buttonChoseRootDir.UseVisualStyleBackColor = true;
            this.buttonChoseRootDir.Click += new System.EventHandler(this.buttonChoseRootDir_Click_1);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // textBoxRootDir
            // 
            this.textBoxRootDir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.textBoxRootDir, "textBoxRootDir");
            this.textBoxRootDir.Name = "textBoxRootDir";
            // 
            // checkBoxConnectionsFormTopmost
            // 
            resources.ApplyResources(this.checkBoxConnectionsFormTopmost, "checkBoxConnectionsFormTopmost");
            this.checkBoxConnectionsFormTopmost.Name = "checkBoxConnectionsFormTopmost";
            this.checkBoxConnectionsFormTopmost.UseVisualStyleBackColor = true;
            this.checkBoxConnectionsFormTopmost.CheckedChanged += new System.EventHandler(this.checkBoxConnectionsFormTopmost_CheckedChanged);
            // 
            // checkBoxMainFormTopmost
            // 
            resources.ApplyResources(this.checkBoxMainFormTopmost, "checkBoxMainFormTopmost");
            this.checkBoxMainFormTopmost.Name = "checkBoxMainFormTopmost";
            this.checkBoxMainFormTopmost.UseVisualStyleBackColor = true;
            this.checkBoxMainFormTopmost.CheckedChanged += new System.EventHandler(this.checkBoxMainFormTopmost_CheckedChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            resources.ApplyResources(this.StatusLabel, "StatusLabel");
            this.StatusLabel.Click += new System.EventHandler(this.StatusLabel_Click);
            // 
            // notifyIcon1
            // 
            resources.ApplyResources(this.notifyIcon1, "notifyIcon1");
            // 
            // timerYandexDisk
            // 
            this.timerYandexDisk.Interval = 500;
            this.timerYandexDisk.Tick += new System.EventHandler(this.timerYandexDisk_Tick);
            // 
            // progressBarMain
            // 
            resources.ApplyResources(this.progressBarMain, "progressBarMain");
            this.progressBarMain.Name = "progressBarMain";
            // 
            // timerProgressBar
            // 
            this.timerProgressBar.Interval = 1000;
            this.timerProgressBar.Tick += new System.EventHandler(this.timerProgressBar_Tick);
            // 
            // FormMain
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.progressBarMain);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControlMain);
            this.KeyPreview = true;
            this.Name = "FormMain";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControlMain.ResumeLayout(false);
            this.tabPageConnections.ResumeLayout(false);
            this.tabPageConnections.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridConnections)).EndInit();
            this.tabPageScripts.ResumeLayout(false);
            this.tabPageScripts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridScripts)).EndInit();
            this.tabPageTasks.ResumeLayout(false);
            this.tabPageTasks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTasks)).EndInit();
            this.tabPageSensors.ResumeLayout(false);
            this.tabPageSensors.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSensors)).EndInit();
            this.tabPageHotkeys.ResumeLayout(false);
            this.tabPageHotkeys.PerformLayout();
            this.groupBoxHotkeys.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHotkeys)).EndInit();
            this.tabPageFiles.ResumeLayout(false);
            this.tabControlFiles.ResumeLayout(false);
            this.tabPageListFiles.ResumeLayout(false);
            this.tabPageListFiles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridMainFiles)).EndInit();
            this.tabPageFindFiles.ResumeLayout(false);
            this.tabPageFindFiles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFindedFiles)).EndInit();
            this.tabPageUniversalList.ResumeLayout(false);
            this.tabPageUniversalList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUniversalItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUniversal)).EndInit();
            this.tabPageFilesQuick.ResumeLayout(false);
            this.tabPageFilesQuick.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridMainFilesQuick)).EndInit();
            this.tabPageNotes.ResumeLayout(false);
            this.tabPageNotes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridNotes)).EndInit();
            this.tabPageGoogle.ResumeLayout(false);
            this.tabPageGoogle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGoogle)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPageYandexDisk.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewYandexDisk)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPageMailRu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMailRu)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPageSettings.ResumeLayout(false);
            this.tabPageSettings.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageFiles;
        private System.Windows.Forms.TabControl tabControlFiles;
        private System.Windows.Forms.TabPage tabPageFindFiles;
        private System.Windows.Forms.Button buttonMoveCheckedToBase;
        private System.Windows.Forms.Button buttonInvertAll;
        private System.Windows.Forms.Button buttonDeselAll;
        private System.Windows.Forms.Button buttonSelAll;
        private System.Windows.Forms.Button buttonInputFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxDirSearch;
        private System.Windows.Forms.Button ButtonFindFiles;
        private System.Windows.Forms.DataGridView dataGridFindedFiles;
        internal System.Windows.Forms.TabPage tabPageListFiles;
        private System.Windows.Forms.Button buttonInvertAllList;
        private System.Windows.Forms.Button buttonDeselAllList;
        private System.Windows.Forms.Button buttonSelAllInList;
        private System.Windows.Forms.DataGridView DataGridMainFiles;
        private System.Windows.Forms.TabPage tabPageNotes;
        private System.Windows.Forms.TabPage tabPageConnections;
        private System.Windows.Forms.Button buttonDelSelected;
        private System.Windows.Forms.Button buttonCheckSelected;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Checked;
        private System.Windows.Forms.DataGridViewImageColumn FileIcon;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comments;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileVersion;
        private System.Windows.Forms.DataGridViewTextBoxColumn OriginalFilename;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Path;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxCurrentGroup;
        private System.Windows.Forms.DataGridView dataGridConnections;
        private System.Windows.Forms.Button buttonAddConnection;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPageSettings;
        private System.Windows.Forms.CheckBox checkBoxConnectionsFormTopmost;
        private System.Windows.Forms.CheckBox checkBoxMainFormTopmost;
        private System.Windows.Forms.Button buttonDelSelNote;
        private System.Windows.Forms.Button buttonAddNote;
        private System.Windows.Forms.DataGridView dataGridNotes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNotesFindName;
        private System.Windows.Forms.Button buttonReloadList;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonReloadConnections;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button buttonChoseRootDir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxRootDir;
        private System.Windows.Forms.CheckBox checkBoxMakeSubmenuInConnections;
        private System.Windows.Forms.CheckBox checkBoxShowGroupsInConnections;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabPage tabPageFilesQuick;
        private System.Windows.Forms.Button buttonQprogDelete;
        private System.Windows.Forms.Button buttonQporgAdd;
        private System.Windows.Forms.DataGridView DataGridMainFilesQuick;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TextBoxFileNameQuick;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabPage tabPageScripts;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.DataGridView dataGridScripts;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.Button buttonReloadClasses;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxNotesFindContent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxScriptFindContent;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxScriptFindName;
        private System.Windows.Forms.TabPage tabPageUniversalList;
        private System.Windows.Forms.TextBox textBoxUniversalListFindName;
        private System.Windows.Forms.DataGridView dataGridViewUniversalItem;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxUniversalFindName;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.DataGridView dataGridViewUniversal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxDropDataFormats;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonAtomCDeleteChecked;
        private System.Windows.Forms.Button buttonAtomInvertCheckAll;
        private System.Windows.Forms.Button buttonAtomUnCheckAll;
        private System.Windows.Forms.Button buttonAtomCheckAll;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxFileNameUniversal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxScriptsFileName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxConnectionsFileName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxNotesFileName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxFilesFileName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxFilesQuickFileName;
        private System.Windows.Forms.Button buttonSaveSettings;
        private System.Windows.Forms.TabPage tabPageTasks;
        private System.Windows.Forms.Button buttonDeleteTasks;
        private System.Windows.Forms.Button buttonAddTasks;
        private System.Windows.Forms.DataGridView dataGridViewTasks;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.CheckBox checkBoxEnabledTasks;
        private System.Windows.Forms.TextBox textBoxCsvDelimiter;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxProxy;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button buttonCheckUserConnect;
        private System.Windows.Forms.Button buttonSynchronizeNotes;
        private System.Windows.Forms.Button buttonCheckUpdates;
        public System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.CheckBox checkBoxRunWithWindows;
        private System.Windows.Forms.Button buttonFixPathFiles;
        private System.Windows.Forms.Button buttonSaveToJson;
        private System.Windows.Forms.Button buttonLoadFilesFromXml;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonSaveFilesToXml;
        private System.Windows.Forms.ComboBox comboBoxUnloadList;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxTasks;
        private System.Windows.Forms.ComboBox comboBoxLocalization;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox checkBoxPin;
        private System.Windows.Forms.Button buttonUnloadUnuversal;
        private System.Windows.Forms.ComboBox comboBoxFormatImages;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button buttonOpenConn;
        private System.Windows.Forms.TabPage tabPageHotkeys;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBoxHotNames;
        private System.Windows.Forms.GroupBox groupBoxHotkeys;
        private System.Windows.Forms.Button buttonDelHotkey;
        private System.Windows.Forms.Button buttonAddHotkey;
        private System.Windows.Forms.DataGridView dataGridViewHotkeys;
        private System.Windows.Forms.TabPage tabPageSensors;
        private System.Windows.Forms.Button buttonDelSensor;
        private System.Windows.Forms.Button buttonAddSensor;
        private System.Windows.Forms.DataGridView dataGridViewSensors;
        private System.Windows.Forms.TabPage tabPageGoogle;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxTimeToRefreshUpdateGoogle;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxGooglePasswordToEnter;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxGoogleUser;
        private System.Windows.Forms.TextBox textBoxTimeToUpdateGoogle;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBoxGooglePassword;
        private System.Windows.Forms.Button buttonRefreshGoogleCalendar;
        private System.Windows.Forms.DataGridView dataGridViewGoogle;
        private System.Windows.Forms.Button buttonGoogleSAve;
        private System.Windows.Forms.CheckBox checkBoxGoogleEnable;
        private System.Windows.Forms.CheckBox checkBoxCheckUpdate;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button buttonCheckGoogleEvents;
        private System.Windows.Forms.Button buttonLoadExternalSettings;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tabPageYandexDisk;
        private System.Windows.Forms.Button buttonYandexDiskConnect;
        private System.Windows.Forms.Button buttonYandexRefresh;
        private System.Windows.Forms.DataGridView dataGridViewYandexDisk;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxDropBoxPasswordToEnter;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBoxDropBoxUser;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBoxDropBoxPassword;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxUserEmail;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.TextBox textBoxUserPasswordToEnter;
        private System.Windows.Forms.TextBox textBoxUser;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Button buttonAddUser;
        private System.Windows.Forms.Button buttonRefreshConnections;
        private System.Windows.Forms.Button buttonUpdateUniversalList;
        private System.Windows.Forms.Button buttonRefreshUniversalList;
        private System.Windows.Forms.Button buttonUpdateTasks;
        private System.Windows.Forms.Button buttonRefreshTasks;
        private System.Windows.Forms.Button buttonupdateHotkeys;
        private System.Windows.Forms.Button buttonRefreshHotkeys;
        private System.Windows.Forms.Button buttonUpdateSensors;
        private System.Windows.Forms.Button buttonRefreshSensors;
        private System.Windows.Forms.Button buttonSynchronizeAll;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBoxHotkeys;
        private System.Windows.Forms.Timer timerYandexDisk;
        private System.Windows.Forms.CheckBox checkBoxYandexDrive;
        private System.Windows.Forms.Button buttonBackupExe;
        private System.Windows.Forms.ProgressBar progressBarMain;
        private System.Windows.Forms.Timer timerProgressBar;
        private System.Windows.Forms.TabPage tabPageMailRu;
        private System.Windows.Forms.Button buttonMailRuConnect;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.DataGridView dataGridViewMailRu;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBoxTeamViewerGUIDToOpenConnection;
        private System.Windows.Forms.Button buttonSaveAllLists;
        private System.Windows.Forms.Button buttonLoadAll;
        private System.Windows.Forms.CheckBox checkBoxCopyMessageToclipboard;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBoxFileExtensionsToAdd;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button buttonDownloadExe;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox comboBoxTaskGroup;
        private System.Windows.Forms.Button buttonSaveConnections;
        private System.Windows.Forms.Button buttonSaveScripts;
        private System.Windows.Forms.Button buttonSaveTasks;
        private System.Windows.Forms.Button buttonSaveSensors;
        private System.Windows.Forms.Button buttonSaveHotkeys;
        private System.Windows.Forms.Button buttonSaveFiles;
        private System.Windows.Forms.TextBox textBoxTaskSearchText;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox comboBoxScriptsGropu;
        private System.Windows.Forms.Button buttonDownloadSettings;
        private System.Windows.Forms.Button buttonUploadsettings;
    }
}

