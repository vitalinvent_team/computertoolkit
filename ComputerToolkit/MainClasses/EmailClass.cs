﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit
{
    class EmailClass
    {
        public EmailClass()
        {
        }
        public void SendErrorOverEmail(String err)
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.To.Add(Settings.Get("EMAIL","vitalinvent@gmail.com"));
            message.Subject = "at.error";
            message.From = new System.Net.Mail.MailAddress("at.error@xxx.com");
            message.Body = err;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("localhost");
            DialogResult res = MessageBox.Show("Отослать письмо с ошибкой автору? Ошибка:"+err,"Ошибка в программе",MessageBoxButtons.OKCancel);
            if (res == DialogResult.OK)
            smtp.Send(message);
        }
    }
}
