﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    public static class VariablesClass
    {
        //***************************************API VARIABLES****************************************
        public static string API_VAL_EXE_FILE = "COMPUTERTOOLKIT_EXE";
        public static string API_VAL_EXE_VER = "COMPUTERTOOLKIT_EXE_VERSION";
        public static string API_VAL_SETTINGS = "COMPUTERTOOLKIT_SETTINGS";
        public static string API_VAL_TABLE = "computertoolkit";
        public static string API_VAL_NAME = API_VAL_TABLE;
        public static string API_VAL_DATA_IN_POST = "DATA_IN_POST";
        //***************************************API VARIABLES****************************************
        public static string API_VARIABLE_NAME_REPLACE = "#VARIABLE_NAME#";
        public static string API_VARIABLE_VALUE_REPLACE = "#VARIABLE_VALUE#";
        public static string TEMP_FOLDER = "temp";
        public static string API_URL = "http://api.vitalinvent.com";
        public static string API_INSERTUPDATE = "/?api="+ API_VAL_NAME + "&table="+ API_VAL_TABLE + "&operation=base_insertupdate&columns_iu=name,value&values_iu=COMPUTERTOOLKIT_#TYPE_VARIABLE#,#VALUE#&columns_where=name&values_where=#COMPUTERTOOLKIT_CONNECTIONS#";
        public static string API_INSERTUPDATEFILE = "/?api=" + API_VAL_NAME + "&table=" + API_VAL_TABLE + "&operation=base_insertupdatefile&columns_iu=name,value&values_iu=#VARIABLE_NAME#,#VARIABLE_VALUE#&columns_where=name&values_where=#VARIABLE_NAME#";
        public static string API_SELECT = "/?api=" + API_VAL_NAME + "&table=" + API_VAL_TABLE + "&operation=base_select&columns_where=name&values_where=#COMPUTERTOOLKIT_CONNECTIONS#&format=text&columns_select=value";
        public static string API_VARIABLE_INSERTUPD = "/?api=" + API_VAL_NAME + "&table=" + API_VAL_TABLE + "&operation=base_insertupdate&columns_iu=name,value&values_iu=#VARIABLE_NAME#,#VARIABLE_VALUE#&columns_where=name&values_where=#VARIABLE_NAME#";
        public static string API_VARIABLE_SELECT = "/?api=" + API_VAL_NAME + "&table=" + API_VAL_TABLE + "&operation=base_select&columns_where=name&values_where="+ API_VARIABLE_NAME_REPLACE + "&format=text&columns_select=value";
        //public static string TEMP_FOLDER = System.IO.Path.GetTempPath();
        public static string CurrentKey;
        public static User user;
        //public static string ONLINE_FOLDER = "computertoolkit";
        //public static string MIME_GOOGLE_OCTETSTREAM = "application/octet-stream";
        //public static string GoogleID = "740272425732-u8auuf2rb1b46oir7ra358qr50s96sa4.apps.googleusercontent.com";
        //public static string GoogleSecret = "X-ZDsONF7CTaOeOC7XnumcAe";
        //public static string GoogleEmail = "computertoolkitapp@gmail.com";
        //public static string YandexToken = "";
        //public static ulong progressBarCurrent = 0;
        //public static ulong progressBarTotal = 100;
        private static string settingsPath = Application.StartupPath.ToString() + "\\settings.txt";
    }
    //public enum State
    //{
    //    inProgress,
    //    ready,
    //    error,
    //    unknown
    //}
    //public enum TypeMenu
    //{
    //    NOTE,
    //    SCRIPT,
    //    FILE,
    //    FILEQUICK,
    //    CONNECTION,
    //    UNIVERSAL,
    //    TASK
    //}
    public enum AtomType
    {
        Image,
        Icon,
        String,
        ByteArray,
        Byte,
        Integer,
        Boolean,
        CSV,
        Null
    }
    public enum ConnectionProtocol
    {
        RDP,
        VNC,
        TEAMVIEWER,
        AMMY
    }
    public enum ScriptLanguage
    {
        CSharp,
        VBasic
    }
    public enum NoteType
    {
        Note,
        Script
    }

}
