﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ComputerToolkit.MainClasses
{
    public static class EngineParse
    {
        public static List<string[]> ParseToList(string fileName, string separator = ";")
        {
            List<string[]> outList = new List<string[]>();
            using (StreamReader sr = new StreamReader(fileName))
            {
                string line = "";
                while (line != null)
                {
                    line = sr.ReadLine();
                    string[] arrLine = line.Split(new string[] { separator }, StringSplitOptions.None);
                    outList.Add(arrLine);
                }
                return outList;
            }
        }
    }
}
