﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit
{
    class EngineScript
    {
        private static string appPath = Application.StartupPath.ToString();
        Dictionary<string, string> providerOptions = new Dictionary<string, string>
        {
          {"CompilerVersion", "v4.0"}
        };

        public EngineScript(ScriptLanguage scriptLanguage)
        {
        }

        public void AddParameters()
        {

        }
        public object ExecuteCode(String script)
        {
            return ExecuteCode(new ScriptItem("", "", "", script, "", ScriptLanguage.CSharp, ""));
        }
        public object ExecuteCode(ScriptItem script)
        {
            object output = "";
            string folderToexecute = Application.StartupPath.ToString(); //+ "\\temp";
            string fileToExecute = folderToexecute + "\\"+Functions.GetRandomString(8)+".dll";
            //string fileToExecute = folderToexecute + "\\TestClass.dll";
            //Functions.DeleteFiles(folderToexecute, "*.dll");
            CSharpCodeProvider provider = new CSharpCodeProvider(new Dictionary<string, string>() { { "CompilerVersion", "v3.5" } });
            //CSharpCodeProvider provider = new CSharpCodeProvider();
            ICodeCompiler cc = provider.CreateCompiler();
            CompilerParameters compilerParams = new CompilerParameters();

            if (!Directory.Exists(folderToexecute))
            {
                Directory.CreateDirectory(folderToexecute);
            }
            compilerParams.OutputAssembly = fileToExecute;
            compilerParams.ReferencedAssemblies.Add("System.dll");
            compilerParams.ReferencedAssemblies.Add("System.Core.dll");
            compilerParams.ReferencedAssemblies.Add("System.Data.dll");
            compilerParams.ReferencedAssemblies.Add("System.Xml.dll");
            compilerParams.ReferencedAssemblies.Add("System.Xml.Linq.dll");
            compilerParams.ReferencedAssemblies.Add("System.Drawing.dll");
            //compilerParams.ReferencedAssemblies.Add("System.Net.dll");
            compilerParams.ReferencedAssemblies.Add("mscorlib.dll");
            compilerParams.ReferencedAssemblies.Add("System.Windows.Forms.dll");
            //compilerParams.ReferencedAssemblies.Add("PJLControls.dll");
            //compilerParams.ReferencedAssemblies.Add("characteristics.dll");            
            compilerParams.ReferencedAssemblies.Add(Application.StartupPath.ToString()+"\\ComputerToolkit.exe");
            compilerParams.WarningLevel = 3;
            compilerParams.CompilerOptions = "/target:library /optimize";
            compilerParams.GenerateExecutable = false;
            compilerParams.GenerateInMemory = true;
            System.CodeDom.Compiler.TempFileCollection tfc = new TempFileCollection(folderToexecute, false);
            CompilerResults cr = new CompilerResults(tfc);
            do { } while (cr == null);
            do { } while (tfc == null);
            cr = cc.CompileAssemblyFromSource(compilerParams, script.Content);
            string err = "";
            if (cr.Errors.Count > 0)
            {
                foreach (CompilerError ce in cr.Errors)
                {
                    err += (ce.ErrorNumber.ToString() + ": " + ce.ErrorText);
                }
                Functions.Message(err);
            }
            else
            {
                //System.Collections.Specialized.StringCollection sc = cr.Output;
                //foreach (string s in sc)
                //{
                //    output += s;
                //}
                //Functions.Message(output);
                //EXECUTE

                AppDomainSetup ads = new AppDomainSetup();
                ads.ShadowCopyFiles = "true";
                AppDomain.CurrentDomain.SetShadowCopyFiles();

                AppDomain newDomain = AppDomain.CreateDomain("newDomain");

                byte[] rawAssembly = loadFile(fileToExecute);
                Assembly assembly = newDomain.Load(rawAssembly, null);

                Script testClass = (Script)assembly.CreateInstance("ComputerToolkit.MainClass");
                output=testClass.Main();

                testClass = null;
                assembly = null;

                AppDomain.Unload(newDomain);
                newDomain = null;
                if (File.Exists(fileToExecute))
                    File.Delete(fileToExecute);
            }
            return output;
        }
        private byte[] loadFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open);
            byte[] buffer = new byte[(int)fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            fs.Close();
            fs = null;
            return buffer;
        }
 
    }
}
