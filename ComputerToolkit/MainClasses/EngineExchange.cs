﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Net;
using System.Collections.Specialized;

namespace ComputerToolkit
{
    public class EngineExchange 
    {
        private User user = new User();
        private string appPath;
        public EngineExchange() 
        {
            appPath = Application.StartupPath.ToString();
        }
        private string CopyFile(string srcPath, string destName)
        {
            try
            {
                string path = Functions.GetDirInPath(srcPath);
                string descPath = path + "\\" + destName;
                if (File.Exists(descPath))
                {
                    File.Delete(descPath);
                    File.Copy(srcPath, descPath);
                }
                else
                {
                    File.Copy(srcPath, descPath);
                }
                return descPath;
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
                return null;
            }
        }
        private void AddUser(User user)
        {

        }
        private void VerifyUser(string user, string password)
        {

        }
        private String UpdateString(String name, String value)
        {
            string response;
            string url = VariablesClass.API_URL + VariablesClass.API_VARIABLE_INSERTUPD.Replace(VariablesClass.API_VARIABLE_NAME_REPLACE, name).Replace(VariablesClass.API_VARIABLE_VALUE_REPLACE, VariablesClass.API_VAL_DATA_IN_POST);
            using (WebClient client = new WebClient())
            {
                response = client.UploadString(url, Functions.Base64ForUrlEncode(value));
            }
            return response;
        }
        private String DowloadString(String name)
        {
            string result = "";
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                result = wc.UploadString(VariablesClass.API_URL + VariablesClass.API_VARIABLE_SELECT.Replace(VariablesClass.API_VARIABLE_NAME_REPLACE, name), "");
            }
            return Functions.Base64ForUrlDecode(result);
        }
        private void CompareList(List<ConnectionItem> objItemsWhat, List<ConnectionItem> objItemsWhere)
        {
            try
            {
                foreach (ConnectionItem objWhat in objItemsWhat)
                {
                    if (objItemsWhere.Find(x => x.Guid == objWhat.Guid) == null)
                    {
                        objItemsWhere.Add(objWhat);
                    }
                    else
                    {
                        IItem iitem = objItemsWhere.Find(x => x.Guid == objWhat.Guid);
                        iitem = objWhat;
                    }
                }
            } catch (Exception ex)
            { Functions.Message(ex); }
        }
        public string[] SynchronizeList(Object list)
        {
            string[] result = new string[] { "", "" };
            if (list.GetType() == typeof(Connections))
            {
                String xmlDownloaded = DowloadString(list.GetType().ToString().Split('.')[1]);
                List<ConnectionItem> listDownConn = null;
                int upd = 0;
                if (xmlDownloaded.Length > 0)
                {
                    listDownConn = xmlDownloaded.DeserializeFromStringXml<List<ConnectionItem>>();

                    foreach (ConnectionItem conn in listDownConn)
                    {
                        try
                        {
                            ConnectionItem findedConn = ProcessIcon.lists.connections.Find(x => x.Guid == conn.Guid);
                            if (findedConn.Enabled)
                            {
                                if (findedConn == null)
                                {
                                    ProcessIcon.lists.connections.Add(conn);
                                    upd++;
                                }
                                else if (DateTime.Parse(findedConn.Date) < DateTime.Parse(conn.Date))
                                {
                                    ProcessIcon.lists.connections[ProcessIcon.lists.connections.FindIndex(x => x.Guid == conn.Guid)] = conn;
                                }
                            }
                        } catch (Exception ex)
                        { Functions.Message(ex); }
                    }
                }
                String xml = ((Connections)ProcessIcon.lists.connections).SerializeToStringXml();
                int countDnld = (listDownConn == null) ? 0 : listDownConn.Count;
                result = new string[] { "Connections", "all/dwld/dupd/upld-" + ((Connections)list).Count + "/" + countDnld + "/" + upd.ToString() + "/" + ((Connections)list).Count + Environment.NewLine + UpdateString(list.GetType().ToString().Split('.')[1], xml) };
                ProcessIcon.lists.connections.SaveBase();
            }

            if (list.GetType() == typeof(Tasks))
            {
                String xmlDownloaded = DowloadString(list.GetType().ToString().Split('.')[1]);
                List<TaskItem> listDown = null;
                int upd = 0;
                if (xmlDownloaded.Length > 0)
                {
                    listDown = xmlDownloaded.DeserializeFromStringXml<List<TaskItem>>();

                    foreach (TaskItem item in listDown)
                    {
                        try
                        {
                            if (item.Enabled)
                            {
                                TaskItem findedTask = ProcessIcon.lists.tasks.Find(x => x.Guid == item.Guid);
                                if (findedTask == null)
                                {
                                    ProcessIcon.lists.tasks.Add(item);
                                    upd++;
                                }
                                else if (DateTime.Parse(findedTask.Date) < DateTime.Parse(item.Date))
                                {
                                    ProcessIcon.lists.tasks[ProcessIcon.lists.tasks.FindIndex(x => x.Guid == item.Guid)] = item;
                                }
                            }
                        } catch (Exception ex)
                        { Functions.Message(ex); }


                    }
                }
                String xml = ((Tasks)ProcessIcon.lists.tasks).SerializeToStringXml();
                int countDnld = (listDown == null) ? 0 : listDown.Count;
                result = new string[] { "Tasks", "all/dwld/dupd/upld-" + ((Tasks)list).Count + "/" + countDnld + "/" + upd.ToString() + "/" + ((Tasks)list).Count + Environment.NewLine + UpdateString(list.GetType().ToString().Split('.')[1], xml) };
                ProcessIcon.lists.tasks.SaveBase();
            }
            if (list.GetType() == typeof(Scripts))
            {
                String xmlDownloaded = DowloadString(list.GetType().ToString().Split('.')[1]);
                List<ScriptItem> listDown = null;
                int upd = 0;
                if (xmlDownloaded.Length > 0)
                {
                    listDown = xmlDownloaded.DeserializeFromStringXml<List<ScriptItem>>();

                    foreach (ScriptItem item in listDown)
                    {
                        try
                        {
                            ScriptItem findedTask = ProcessIcon.lists.scripts.Find(x => x.Guid == item.Guid);
                            if (findedTask == null)
                            {
                                ProcessIcon.lists.scripts.Add(item);
                                upd++;
                            }
                            else if (DateTime.Parse(findedTask.Date) < DateTime.Parse(item.Date))
                            {
                                ProcessIcon.lists.scripts[ProcessIcon.lists.scripts.FindIndex(x => x.Guid == item.Guid)] = item;
                            }
                        }
                        catch (Exception ex)
                        { Functions.Message(ex); }

                    }
                }
                Scripts scriptsToUpdate = new Scripts();
                scriptsToUpdate.Clear();
                foreach (ScriptItem script in ProcessIcon.lists.scripts)
                {
                    if (script.Global == true)
                    {
                        scriptsToUpdate.Add(script);
                    }
                }
                String xml = ((Scripts)scriptsToUpdate).SerializeToStringXml();
                int countDnld = (listDown == null) ? 0 : listDown.Count;
                result = new string[] { "Scripts", "all/dwld/dupd/upld-" + ((Scripts)list).Count + "/" + countDnld + "/" + upd.ToString() + "/" + ((Scripts)scriptsToUpdate).Count + Environment.NewLine + UpdateString(list.GetType().ToString().Split('.')[1], xml) };
                ProcessIcon.lists.tasks.SaveBase();
            }
            return result;
        }
        public void UpdateSettings()
        {
            String settings = File.ReadAllText(Settings.settingsPath, Encoding.GetEncoding("windows-1251")).ToString();
            UpdateString(VariablesClass.API_VAL_SETTINGS.ToString(), settings);
            Functions.Message("Settings","Uploaded " + settings.Length + " bytes");
        }
        public void DownloadSettings()
        {
            String settings = DowloadString(VariablesClass.API_VAL_SETTINGS.ToString());
            Functions.Message("Settings", "Downloaded " + settings.Length + " bytes");
            File.WriteAllText(Settings.settingsPath, settings, Encoding.GetEncoding("windows-1251"));
        }

        private Object DeserializeObjFromFile(string fileName)
        {
            try
            {
                Object obj;
                using (Stream stream = File.Open(fileName, FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                    obj = (Object)bformatter.Deserialize(stream);
                }
                return obj;
            }
            catch (SerializationException e)
            {
                Functions.Message(e);
                return null;
            }

        }
        private string SerializeObjToFile(Object obj)
        {
            try
            {
                string fname = System.Guid.NewGuid().ToString() + ".bin";
                using (Stream stream = File.Open(appPath + "\\" + VariablesClass.TEMP_FOLDER + "\\" + fname, FileMode.Create))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    bformatter.Serialize(stream, obj);
                }

                return appPath + "\\" + VariablesClass.TEMP_FOLDER + "\\" + fname;
            }
            catch (SerializationException e)
            {
                Functions.Message(e);
                return null;
            }
        }
        private string StreamToString(MemoryStream stream)
        {
            try
            {
                stream.Position = 0;
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
                return null;
            }
        }
        private Stream StringToStream(string src)
        {
            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(src);
                return new MemoryStream(byteArray);
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
                return null;
            }

        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}


