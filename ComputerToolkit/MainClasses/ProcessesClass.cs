﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit
{
    class ProcessesClass
    {
        public List<ProcessItem> listProcesses = new List<ProcessItem>();
        private const int SW_RESTORE = 9;
        private int SW_SHOW = 5;
        private const int SW_MINIMIZE = 6;
        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr SetFocus(HandleRef hWnd);
        private string appPath;
        public ProcessesClass()
        {
            appPath = Application.StartupPath.ToString();
            OpenProcesses();
        }
        public void SaveProcesses()
        {
            using (Stream stream = File.Open(appPath + "\\ComputerToolkitProcesses.bin", FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                bformatter.Serialize(stream, listProcesses);
            }

        }
        private void OpenProcesses()
        {
            try
            {
                using (Stream stream = File.Open(appPath + "\\ComputerToolkitProcesses.bin", FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                    listProcesses = (List<ProcessItem>)bformatter.Deserialize(stream);
                }
            }
            catch { }

        }
        public void RefreshProcesses()
        {
            OpenProcesses();
            CheckAndDeleteNonExistedProcesses();
            foreach (ProcessItem proc in listProcesses)
            {
                proc.ScreenshootWindow = GetScreenShootWindow(proc.MainWindowHandle);
            }
        }
        private Image GetScreenShootWindow(IntPtr handle)
        {
            ScreenCapture sc = new ScreenCapture();
            return sc.CaptureWindow(handle);
        }
        private void CheckAndDeleteNonExistedProcesses()
        {
            List<ProcessItem> listNoneExistedProcesses = new List<ProcessItem>();
            foreach (ProcessItem proc in listProcesses)
            {
                try
                {
                    Process procFinded = Process.GetProcessById(proc.Id);
                }
                catch
                {
                    listNoneExistedProcesses.Add(proc);
                }
            }
            foreach (ProcessItem proc in listNoneExistedProcesses)
            {
                listProcesses.Remove(proc);
            }
        }
        public void ShowProcessByHandle(string Id)
        {
            try
            {
            Process proc = Process.GetProcessById(int.Parse(Id));
            ShowWindow(proc.MainWindowHandle, SW_MINIMIZE);
            ShowWindow(proc.MainWindowHandle, SW_RESTORE);
            //IntPtr hWnd = proc.Handle;
            //SetFocus(new HandleRef(null, hWnd));
            }
            catch { }
        }

    }
}
