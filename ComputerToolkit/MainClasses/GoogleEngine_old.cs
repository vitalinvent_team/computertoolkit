﻿//using Google.Apis.Auth.OAuth2;
//using Google.Apis.Drive.v2;
//using Google.Apis.Drive.v2.Data;
//using Google.Apis.Services;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Threading;
//using System.Windows.Forms;
//using Google.Apis.Authentication;
//using Google.Apis;
//using System.Net;
//using System.Text;


//namespace ComputerToolkit
//{
//    public class GoogleEngine
//    {
//        #region variables
//        static ClientSecrets clientSecrets = new ClientSecrets
//        {
//            ClientId = VariablesClass.GoogleID,
//            ClientSecret = VariablesClass.GoogleSecret,
//        };
//        public DriveService service = new DriveService(new BaseClientService.Initializer()
//        {
//            HttpClientInitializer = GoogleWebAuthorizationBroker.AuthorizeAsync(
//            clientSecrets, 
//            new[] { DriveService.Scope.Drive },
//            VariablesClass.GoogleEmail,
//            CancellationToken.None).Result,
//            ApplicationName = "ComputerToolkit",
//        });
//        public List<Google.Apis.Drive.v2.Data.File> listFiles;
//        #endregion variables

//        #region PUBLIC
//        public GoogleEngine()
//        {
//            listFiles = retrieveAllFiles();
//        }
//        public void Refresh()
//        {
//            listFiles = retrieveAllFiles();
//        }
//        public void UploadFile(string filePath)
//        {

//            Google.Apis.Drive.v2.Data.File body = new Google.Apis.Drive.v2.Data.File();
//            body.Title = Functions.GetFileNameInPath(filePath);
//            body.Description = Functions.GetFileNameInPath(filePath);
//            body.MimeType = VariablesClass.MIME_GOOGLE_OCTETSTREAM;

//            byte[] byteArray = System.IO.File.ReadAllBytes(filePath);
//            System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);

//            FilesResource.InsertMediaUpload request = service.Files.Insert(body, stream, VariablesClass.MIME_GOOGLE_OCTETSTREAM);
//            request.Upload();

//            Google.Apis.Drive.v2.Data.File file = request.ResponseBody;
//        }
//        public void UploadFileToFolder(string filePath, string fileTitle, string folderTitle)
//        {
//            Google.Apis.Drive.v2.Data.File body = new Google.Apis.Drive.v2.Data.File();
//            body.Title = fileTitle;
//            body.Description = fileTitle;
//            body.MimeType = VariablesClass.MIME_GOOGLE_OCTETSTREAM;

//            body.Parents = new List<ParentReference>() { new ParentReference() { Id = GetIdFileByTitle(folderTitle) } };
//            using (Stream stream = System.IO.File.Open(filePath,FileMode.Open))
//            {
//                FilesResource.InsertMediaUpload request = service.Files.Insert(body, stream, VariablesClass.MIME_GOOGLE_OCTETSTREAM);
//                request.Upload();
//                Google.Apis.Drive.v2.Data.File file = request.ResponseBody;
//            }
//        }
//        public void UploadFileToFolder(string filePath, string folderTitle)
//        {
//            Google.Apis.Drive.v2.Data.File body = new Google.Apis.Drive.v2.Data.File();
//            body.Title = Functions.GetFileNameInPath(filePath);
//            body.Description = Functions.GetFileNameInPath(filePath);
//            body.MimeType = VariablesClass.MIME_GOOGLE_OCTETSTREAM;
//            body.Parents = new List<ParentReference>() { new ParentReference() { Id = GetIdFileByTitle(folderTitle) } };
//            byte[] byteArray = System.IO.File.ReadAllBytes(filePath);
//            System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);

//            FilesResource.InsertMediaUpload request = service.Files.Insert(body, stream, VariablesClass.MIME_GOOGLE_OCTETSTREAM);
//            request.Upload();

//            Google.Apis.Drive.v2.Data.File file = request.ResponseBody;
//        }
//        public void UpdateFile(string filePath, string fileTitle)
//        {
//            try
//            {
//                string fileId = GetIdFileByTitle(fileTitle);
//                Google.Apis.Drive.v2.Data.File file = service.Files.Get(fileId).Execute();
//                file.Title = file.Title;
//                file.Description = file.Description;
//                file.MimeType = file.MimeType;
//                byte[] byteArray = System.IO.File.ReadAllBytes(filePath);
//                System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);
//                FilesResource.UpdateMediaUpload request = service.Files.Update(file, fileId, stream, VariablesClass.MIME_GOOGLE_OCTETSTREAM);
//                request.Upload();
//                Google.Apis.Drive.v2.Data.File updatedFile = request.ResponseBody;
//            }
//            catch (Exception e)
//            {
//                MessageBox.Show("An error occurred: " + e.Message);
//            }
//        }
//        public string DownloadFileToString(string fileTitle)
//        {
//            FilesResource.GetRequest getFile = service.Files.Get(GetIdFileByTitle(fileTitle));
//            var stream = service.HttpClient.GetStreamAsync(getFile.Execute().WebContentLink);
//            var result = stream.Result;
//            var memStream = new MemoryStream();
//            result.CopyTo(memStream);
//            string subPath =Application.StartupPath.ToString() +"\\"+ VariablesClass.TEMP_FOLDER; // your code goes here
//            string filePath = subPath + "\\" + fileTitle;
//            string retval  = StreamToString(memStream);
//            return retval;
//        }
//        public string DownloadFile(string fileTitle)
//        {
//            Refresh();
//            string ID = GetIdFileByTitle(fileTitle);
//            if (ID != null)
//            {
//                FilesResource.GetRequest getFile = service.Files.Get(ID);
//                var stream = service.HttpClient.GetStreamAsync(getFile.Execute().WebContentLink);
//                var result = stream.Result;
//                string subPath = Application.StartupPath.ToString() + "\\" + VariablesClass.TEMP_FOLDER; // your code goes here
//                string filePath = subPath + "\\" + fileTitle;
//                using (var fileStream = System.IO.File.Create(filePath))
//                {
//                    result.CopyTo(fileStream);
//                }
//                return filePath;
//            }
//            else
//            {
//                return null;
//            }
//        }
//        #endregion PUBLIC

//        #region PRIVATE
//        private Stream FileToStream(string filePath)
//        {
//            Stream stream =  System.IO.File.Open(filePath,FileMode.Open);
//            return stream;
//        }
//        private string StreamToString(MemoryStream stream)
//        {
//            stream.Position = 0;
//            using (StreamReader reader = new StreamReader(stream))
//            {
//                return reader.ReadToEnd();
//            }
//        }
//        private Stream StringToStream(string src)
//        {
//            byte[] byteArray = Encoding.UTF8.GetBytes(src);
//            return new MemoryStream(byteArray);
//        }
//        private byte[] GetBytes(string str)
//        {
//            byte[] bytes = new byte[str.Length * sizeof(char)];
//            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
//            return bytes;
//        }

//        private string GetString(byte[] bytes)
//        {
//            char[] chars = new char[bytes.Length / sizeof(char)];
//            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
//            return new string(chars);
//        }
//        private List<Google.Apis.Drive.v2.Data.File> retrieveAllFiles()
//        {
//            List<Google.Apis.Drive.v2.Data.File> result = new List<Google.Apis.Drive.v2.Data.File>();
//            FilesResource.ListRequest request = service.Files.List();

//            do
//            {
//                try
//                {
//                    FileList files = request.Execute();

//                    result.AddRange(files.Items);
//                    request.PageToken = files.NextPageToken;
//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine("An error occurred: " + e.Message);
//                    request.PageToken = null;
//                }
//            } while (!String.IsNullOrEmpty(request.PageToken));
//            return result;
//        }
//        private string GetIdFileByTitle(string title)
//        {
//            Refresh();
//            try
//            {
//                Google.Apis.Drive.v2.Data.File findedFile = listFiles.Find(x => x.Title == title);
//                if (findedFile != null)
//                {
//                    return findedFile.Id;
//                }
//                return null;
//            } catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message);
//                return null;
//            }
//        }
//        private string GetDownloadUrlFileByTitle(string title)
//        {
//            Refresh();
//            try
//            {
//                Google.Apis.Drive.v2.Data.File findedFile = listFiles.Find(x => x.Title == title);
//                if (findedFile != null)
//                {
//                    string DownloadUrl = listFiles.Find(x => x.Title == title).DownloadUrl;
//                    return DownloadUrl;
//                }
//                return null;
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message);
//                return null;
//            }
//        }        
//        #endregion PRIVATE

//    }
//}
