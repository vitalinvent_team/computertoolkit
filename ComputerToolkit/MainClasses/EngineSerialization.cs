﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    public static class EngineSerialization
    {
        public static XmlSerializer serializer = null;
        //public EngineSerialization()
        //{
        //}
        public static string SerializeObjectToString(object obj)
        {
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                serializer.Serialize(ms, obj);
                ms.Position = 0;
                xmlDoc.Load(ms);
                return xmlDoc.InnerXml;
            }
        }
        public static string SerializeToStringXml(this object objectInstance)
        {
            try
            {
                serializer = new XmlSerializer(objectInstance.GetType());
                var sb = new StringBuilder();

                using (TextWriter writer = new StringWriter(sb))
                {
                    serializer.Serialize(writer, objectInstance);
                }

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
                return null;
            }
        }
        public static string SerializeHeaderToStringXml(this object objectInstance)
        {
            try
            {
                serializer = new XmlSerializer(objectInstance.GetType());
                var sb = new StringBuilder();

                using (TextWriter writer = new StringWriter(sb))
                {
                    serializer.Serialize(writer, objectInstance);
                }
                sb = sb.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                sb = sb.Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
                return null;
            }
        }
        public static T DeserializeFromStringXml<T>(this string objectData)
        {
            try
            {
                return (T)DeserializeFromStringXml(objectData, typeof(T));
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
                return default(T);
            }
        }

        public static object DeserializeFromStringXml(this string objectData, Type type)
        {
            try
            {
                serializer = new XmlSerializer(type);
                object result;

                using (TextReader reader = new StringReader(objectData))
                {
                    result = serializer.Deserialize(reader);
                }

                return result;
            }
            catch (Exception ex) {
                Functions.Message(ex); return null; 
            }
        }
    }
}
