﻿using System.Net.NetworkInformation;
using ComputerToolkit.Items;
using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Threading;
using System.Linq;
using System.Windows.Media.Imaging;
using System.IO.Compression;
using System.Web;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace ComputerToolkit
{
    public static class Functions
    {
        [DllImport("shell32.dll", EntryPoint = "ShellExecute")]
        public static extern long ShellExecute(int hwnd, string cmd, string file, string param1, string param2, int swmode);
        private static NotifyIcon notifyIcon = new NotifyIcon();
        private static System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private static string appPath = Application.StartupPath.ToString();
        private static Random rand = new Random(DateTime.Now.Millisecond);
        public static int TimeStringToMinutes(string subjectString)
        {
            int res = 0;
            int.TryParse(Regex.Match(subjectString, @"\d+").Value,out res);
            if (res > 0)
            {
                string timeIndent = subjectString.Replace(res.ToString(), "");
                switch (timeIndent.Substring(0, 1).ToLower())
                {
                    case "d":
                        res = res * 24 * 60;
                        break;
                    case "h":
                        res = res * 60;
                        break;
                    default:
                        break;
                }
            }
            return res;
        }

        public static string BytesArrayToString(byte[] arrayB)
        {
            return System.Text.Encoding.Default.GetString(arrayB);
        }
        public static string CryptEncrypt(string clearText,string password)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                byte[] IV = new byte[15];
                rand.NextBytes(IV);
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(password, IV);
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(IV) + Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public static string CryptDecrypt(string cipherText,string password)
        {
            byte[] IV = Convert.FromBase64String(cipherText.Substring(0, 20));
            cipherText = cipherText.Substring(20).Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(password, IV);
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        public static String Base64FromFile (String path)
        {
            Byte[] bytes = File.ReadAllBytes(path);
            return Convert.ToBase64String(bytes);
        }
        public static void Base64ToFile(String base64,String path)
        {
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(path, bytes);
        }
        public static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }

        public static byte[] ZipString(string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);

            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    //msi.CopyTo(gs);
                    CopyTo(msi, gs);
                }

                return mso.ToArray();
            }
        }

        public static string UnZipToString(byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    //gs.CopyTo(mso);
                    CopyTo(gs, mso);
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }
        ///<summary>
        /// Base 64 Encoding with URL and Filename Safe Alphabet using UTF-8 character set.
        ///</summary>
        ///<param name="str">The origianl string</param>
        ///<returns>The Base64 encoded string</returns>
        public static string Base64ForUrlEncode(string str)
        {
            byte[] encbuff = Encoding.UTF8.GetBytes(str);
            return HttpServerUtility.UrlTokenEncode(encbuff);
        }
        ///<summary>
        /// Decode Base64 encoded string with URL and Filename Safe Alphabet using UTF-8.
        ///</summary>
        ///<param name="str">Base64 code</param>
        ///<returns>The decoded string.</returns>
        public static string Base64ForUrlDecode(string str)
        {
            byte[] decbuff = HttpServerUtility.UrlTokenDecode(str);
            return Encoding.UTF8.GetString(decbuff);
        }
        public static Color HexToColor(string hexString)
        {
            ColorConverter colorConverter = new ColorConverter();
            return (Color)colorConverter.ConvertFromString(hexString);
        }
        public static PJLControls.CustomColorPicker ColorPicker()
        {
            PJLControls.CustomColorPicker customColorPicker = new PJLControls.CustomColorPicker(); ;
            customColorPicker.Color = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(192)), ((System.Byte)(128)));
			customColorPicker.Location = new System.Drawing.Point(8, 8);
			customColorPicker.Name = "customColorPicker";
			customColorPicker.Size = new System.Drawing.Size(448, 280);
			customColorPicker.TabIndex = 19;
            return customColorPicker;

			//customColorPicker.ColorChanged += new PJLControls.ColorChangedEventHandler(customColorPicker_ColorChanged);
        }


        #region PrimaryFunctions

        public static void DeleteFiles(string path,string pattern)
        {
            foreach (string file in Directory.GetFiles(path, "*.zip").Where(item => item.EndsWith(pattern)))
            {
                File.Delete(file);
            }
        }
        public static string GetRandomString(int length)
        {
            var chars = "qwertyuioplkjhgfdsazxcvbnm";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
        public static string Ping(string address)
        {
            Ping p1 = new Ping();
            return p1.Send(address).RoundtripTime.ToString();
        }
        #endregion

        private static void NotifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            NotifyIcon notifyIcon = (NotifyIcon)sender;
            if (notifyIcon.Tag.GetType() == typeof(TaskItem))
            {
                Functions.OpenObject(notifyIcon.Tag);
                ProcessIcon.notifyIcon.Tag = "";
            }
        }

        private static void NotifyIcon_BalloonTipClosed(object sender, EventArgs e)
        {
            NotifyIcon notifyIcon = (NotifyIcon)sender;
            if (notifyIcon.Tag.GetType() == typeof(TaskItem))
            {
                DateTime.Parse(((TaskItem)notifyIcon.Tag).TimeToRun).AddMinutes(Settings.Get("textBoxaskReminderLaterTime_TextChanged", 60));
                ((TaskItem)notifyIcon.Tag).Date = DateTime.Now.ToString();
                ProcessIcon.lists.tasks.SaveBase();
                ProcessIcon.notifyIcon.Tag = "";
            }
        }

        private static void NotifyIconTaskReminder_Click(object sender, EventArgs e)
        {
            NotifyIcon notifyIcon = (NotifyIcon)sender;
            if (notifyIcon.Tag.GetType()==typeof(TaskItem))
            {
                Functions.OpenObject(notifyIcon.Tag);
                ProcessIcon.notifyIcon.Tag = "";
            }
        }
        public static void Message(String title,String message,int time=5)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + title+" "+ message);
            }
            catch { }

            try
            {
                if (ProcessIcon.notifyIcon != null)
                {
                    if (message.Length > 0)
                    {
                        ProcessIcon.notifyIcon.BalloonTipTitle = title;
                        ProcessIcon.notifyIcon.BalloonTipText = message;
                        ProcessIcon.notifyIcon.ShowBalloonTip(time);
                        //Logs logs = new Logs("messages_log");
                        //logs.Add(message);
                        if (Settings.Get("checkBoxCopyMessageToclipboard", false))
                        {
                            Clipboard.SetText(message);
                        }
                    }

                }
            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.Message);
            }
        }
        public static void Message(String message)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + " " +  message);
            }
            catch { }
            try
            {
                if (ProcessIcon.notifyIcon != null)
                {
                    if (message.Length > 0)
                    {
                        ProcessIcon.notifyIcon.BalloonTipTitle = "Information";
                        ProcessIcon.notifyIcon.BalloonTipText = message;
                        ProcessIcon.notifyIcon.ShowBalloonTip(5);
                        //Logs logs = new Logs("messages_log");
                        //logs.Add(message);
                        if (Settings.Get("checkBoxCopyMessageToclipboard", false))
                        {
                            Clipboard.SetText(message);
                        }
                    }

                }
            }
            catch (Exception ex2) { 
                MessageBox.Show(ex2.Message); 
            }
        }
        public static void Message(String[] message)
        {
            try
            {
                ProcessIcon.logs.Add("Functions.Message" + " " + message[0]+" "+ message[1]);
            }
            catch { }

            try
            {
                if (ProcessIcon.notifyIcon != null)
                {
                    if (message[1].Length > 0)
                    {
                        ProcessIcon.notifyIcon.BalloonTipTitle = message[0];
                        ProcessIcon.notifyIcon.BalloonTipText = message[1];
                        ProcessIcon.notifyIcon.ShowBalloonTip(5);
                        //Logs logs = new Logs("messages_log");
                        //logs.Add(message);
                        if (Settings.Get("checkBoxCopyMessageToclipboard", false))
                        {
                            Clipboard.SetText(message[0]+ " "+ message[1]);
                        }
                    }

                }
            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.Message);
            }
        }
        public static void Message(Object _object, int timeToshowSeconds = 60)
        {
            try
            {
                ProcessIcon.logs.Add(_object.ToString() + " " + ((TaskItem)_object).Description);
            }
            catch { }

            try
            {
                if (_object.GetType() == typeof(TaskItem))
                {
                    ProcessIcon.notifyIcon.BalloonTipTitle = "Напоминание";
                    ProcessIcon.notifyIcon.BalloonTipText = ((TaskItem)_object).Description;
                    ProcessIcon.notifyIcon.Click += NotifyIconTaskReminder_Click;
                    ProcessIcon.notifyIcon.BalloonTipClosed += NotifyIcon_BalloonTipClosed;
                    ProcessIcon.notifyIcon.BalloonTipClicked += NotifyIcon_BalloonTipClicked;
                    ProcessIcon.notifyIcon.Tag = ((TaskItem)_object);
                    ProcessIcon.notifyIcon.ShowBalloonTip(timeToshowSeconds);

                }
            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.Message);
            }
        }
        public static void Message(Exception ex)
        {
            try
            {
                ProcessIcon.logs.Add(ex.Message + " " + ex.StackTrace);
            }
            catch { }
            try
            {
                if (ProcessIcon.notifyIcon != null)
                {
                    if (ex != null)
                    {
                        ProcessIcon.notifyIcon.BalloonTipTitle = "Error";
                        ProcessIcon.notifyIcon.BalloonTipText = ex.Message + " " + ex.StackTrace;
                        ProcessIcon.notifyIcon.ShowBalloonTip(5);
                        //Logs logs = new Logs("messages_log");
                        //logs.Add(ex.Message + " " + ex.StackTrace);
                        if (Settings.Get("checkBoxCopyMessageToclipboard", false))
                        {
                            Clipboard.SetText(ex.Message + " " + ex.StackTrace + " " + ex.InnerException);
                        }
                    }
                }
            }
            catch (Exception ex2) { 
                MessageBox.Show(ex2.Message); 
            }
        }
        public static void Message(SerializationException ex)
        {
            try
            {
                ProcessIcon.logs.Add(ex.Message + " " + ex.StackTrace);
            }
            catch { }
            try
            {
                if (ProcessIcon.notifyIcon != null)
                {
                    if (ex != null)
                    {
                        ProcessIcon.notifyIcon.BalloonTipTitle = "Error ";
                        ProcessIcon.notifyIcon.BalloonTipText = ex.Message + " " + ex.StackTrace;
                        ProcessIcon.notifyIcon.ShowBalloonTip(5);
                        //Logs logs = new Logs("messages_log");
                        //logs.Add(ex.Message + " " + ex.StackTrace);
                        if (Settings.Get("checkBoxCopyMessageToclipboard", false))
                        {
                            Clipboard.SetText(ex.Message + " " + ex.StackTrace + " " + ex.InnerException);
                        }
                    }
                }
            }
            catch (Exception ex2) { 
                MessageBox.Show(ex2.Message); 
            }
        }
        
        public static void DeleteFiles(string path)
        {
            try
            {
                System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(path);

                foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                {
                    file.Delete();
                }
            }
            catch (Exception ex)
            {
                Message(ex);
            }
        }
        public static List<Object> FindObjectByGuid(string str)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name+""+str);
            }
            catch { }
            List<Object> listObjects = new List<object>();
            Object findedItem;
            //Notes notes = new Notes();
            //Scripts scripts = new Scripts();
            //Files files = new Files();
            //FilesQuick filesQuick = new FilesQuick();
            //Universals universals = new Universals();
            //Connections connections = new Connections();
            //*********** notes name
            try
            {
                List<NoteItem> findedItems1 = ProcessIcon.lists.notes.FindAll(x => x.Name.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (NoteItem note in findedItems1)
                {
                    listObjects.Add(note);
                }
            }
            catch { }
            //*********** notes content
            try
            {
                List<NoteItem> findedItems2 = ProcessIcon.lists.notes.FindAll(x => x.Content.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (NoteItem note in findedItems2)
                {
                    listObjects.Add(note);
                }
            }
            catch { }
            //***********
            //findedItem = scripts.Find(x => x.Name.Contains(str));
            //if (findedItem != null) listObjects.Add(findedItem);
            try
            {
                List<ScriptItem> findedItems3 = ProcessIcon.lists.scripts.FindAll(x => x.Name.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (ScriptItem scr in findedItems3)
                {
                    listObjects.Add(scr);
                }
            }
            catch { }
            //***********
            //findedItem = scripts.Find(x => x.Content.Contains(str));
            //if (findedItem != null) listObjects.Add(findedItem);
            try
            {
                List<ScriptItem> findedItems4 = ProcessIcon.lists.scripts.FindAll(x => x.Content.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (ScriptItem scr in findedItems4)
                {
                    listObjects.Add(scr);
                }
            }
            catch { }
            //***********
            //findedItem = files.Find(x => x.Name.Contains(str));
            //if (findedItem != null) listObjects.Add(findedItem);
            try
            {
                List<FileItem> findedItems5 = ProcessIcon.lists.files.FindAll(x => x.Name.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (FileItem fi in findedItems5)
                {
                    listObjects.Add(fi);
                }
            }
            catch { }
            //***********
            //***********
            //findedItem = files.Find(x => x.Name.Contains(str));
            //if (findedItem != null) listObjects.Add(findedItem);
            try
            {
                List<FileItem> findedItems5 = ProcessIcon.lists.files.FindAll(x => x.Guid.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (FileItem fi in findedItems5)
                {
                    listObjects.Add(fi);
                }
            }
            catch { }
            //***********                //findedItem = files.Find(x => x.ToString().Contains(str));
            //if (findedItem != null) listObjects.Add(findedItem);
            try
            {
                List<FileItem> findedItems6 = ProcessIcon.lists.files.FindAll(x => x.ToString().IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (FileItem fi in findedItems6)
                {
                    listObjects.Add(fi);
                }
            }
            catch { }
            //***********
            //findedItem = filesQuick.Find(x => x.Name.Contains(str));
            //if (findedItem != null) listObjects.Add(findedItem);
            try
            {
                List<FileItemQuick> findedItems7 = ProcessIcon.lists.filesQuick.FindAll(x => x.Name.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (FileItemQuick fi in findedItems7)
                {
                    listObjects.Add(fi);
                }
            }
            catch { }
            //***********
            //findedItem = filesQuick.Find(x => x.ToString().Contains(str));
            //if (findedItem != null) listObjects.Add(findedItem);
            try
            {
                List<FileItemQuick> findedItems8 = ProcessIcon.lists.filesQuick.FindAll(x => x.ToString().IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (FileItemQuick fi in findedItems8)
                {
                    listObjects.Add(fi);
                }
            }
            catch { }
            //***********
            //findedItem = universals.Find(x => x.Name.Contains(str));
            //if (findedItem != null) listObjects.Add(findedItem);
            try
            {
                List<UniversalItem> findedItems9 = ProcessIcon.lists.universals.FindAll(x => x.Name.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (UniversalItem ui in findedItems9)
                {
                    listObjects.Add(ui);
                }
            }
            catch { }
            //***********
            //findedItem = universals.Find(x => x.Name.Contains(str));
            //if (findedItem != null) listObjects.Add(findedItem);
            try
            {
                List<TaskItem> findedItems10 = ProcessIcon.lists.tasks.FindAll(x => x.Name.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (TaskItem ui in findedItems10)
                {
                    listObjects.Add(ui);
                }
            }
            catch { }
            //***********
            try
            {
                List<TaskItem> findedItems11 = ProcessIcon.lists.tasks.FindAll(x => x.Guid.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (TaskItem ui in findedItems11)
                {
                    listObjects.Add(ui);
                }
            }
            catch { }
            //***********
            foreach (UniversalItem ui in ProcessIcon.lists.universals)
            {
                try
                {
                    List<UniversalAtom> findedItemsAtom = ui.FindAll(x => x.Name.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                    foreach (UniversalAtom ua in findedItemsAtom)
                    {
                        listObjects.Add(ua);
                    }
                }
                catch { }
            }
            //***********
            try
            {
                List<ConnectionItem> findedItems = ProcessIcon.lists.connections.FindAll(x => x.Name.IndexOf(str, System.StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (ConnectionItem conn in findedItems)
                {
                    listObjects.Add(conn);
                }
            }
            catch { }
            if (listObjects.Count > 35)
            {
                listObjects.Clear();
                listObjects.Add(new StringBuilder("найдено более 35 объектов"));
            }
            return listObjects;
        }
        public static object GetSensorValue(String hotString)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + "" + hotString);
            }
            catch { }
            String cleanStr = "";
            object value = "";
            //Scripts scripts = new Scripts();
            if (hotString != null)
            {
                if (hotString.IndexOf("#COMMANDLINE", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = hotString.Replace("#COMMANDLINE", "");
                    string strCmdText;
                    strCmdText = "/C "+ cleanStr;
                    ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("CMD.exe", strCmdText);
                    Process proc = new Process();
                    procStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    procStartInfo.UseShellExecute = false;
                    procStartInfo.RedirectStandardOutput = true;
                    procStartInfo.CreateNoWindow = true;
                    proc.StartInfo = procStartInfo;
                    proc.Start();
                    //proc.WaitForExit(1000);
                    while(value.ToString().Length == 0)
                    {
                    value += proc.StandardOutput.ReadLine();
                    }
                    value += proc.StandardOutput.ReadLine();
                    value += proc.StandardOutput.ReadLine();
                    //Encoding trg = Encoding.GetEncoding(Console.OutputEncoding.CodePage);
                    //Encoding src = Encoding.UTF8;
                    //byte[] sourceBytes = src.GetBytes(value.ToString());
                    //byte[] resultBytes = Encoding.Convert(src, trg, sourceBytes);
                    //// Convert the new byte[] into a char[] and then into a string.
                    //char[] resultChars = new char[trg.GetCharCount(resultBytes, 0, resultBytes.Length)];
                    //trg.GetChars(resultBytes, 0, resultBytes.Length, resultChars, 0);
                    //value = new string(resultChars);
                    return value;
                }
                if (hotString.IndexOf("#SCRIPTRUN", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = hotString.Replace("#SCRIPTRUN", "");
                    ScriptItem script = ProcessIcon.lists.scripts.Find(x => x.Guid == cleanStr);
                    return ExecuteScript(script); ;
                }
                if (hotString.IndexOf("#RUNSCRIPT", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = hotString.Replace("#RUNSCRIPT", "");
                    ScriptItem script = ProcessIcon.lists.scripts.Find(x => x.Guid == cleanStr);
                    return ExecuteScript(script); ;
                }
                if (hotString.IndexOf("#RUNMINISCRIPT", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = hotString.Replace("#RUNMINISCRIPT", "");
                    ScriptItem script = ProcessIcon.lists.scripts.Find(x => x.Guid == cleanStr);
                    return ExecuteMiniScript(script.Content.ToString()); ;
                }
                if (hotString.IndexOf("#MINISCRIPTRUN", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = hotString.Replace("#MINISCRIPTRUN", "");
                    ScriptItem script = ProcessIcon.lists.scripts.Find(x => x.Guid == cleanStr);
                    return ExecuteMiniScript(script.Content.ToString()); ;
                }
                if (hotString.IndexOf("#PING", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = hotString.Replace("#PING", "");
                    return Ping(cleanStr);
                }
                return null;
            }
            return null;
        }
        public static void OpenTeamViewerConnection(ConnectionItem twObj)
        {
            string guidTeamViewer = Settings.Get("textBoxTeamViewerGUIDToOpenConnection", "");
            List<Object> findedObjs = FindObjectByGuid(guidTeamViewer);
            string cmdLine="";
            if (findedObjs.Count>0)
            {
                FileItem fileToOpen =  (FileItem)findedObjs[0];
                cmdLine+=" -i " + twObj.Adress.Replace(" ","");
                if (!(twObj.Password==null))
                {
                    if (twObj.Password.Length>0)
                        cmdLine+= " --Password " + twObj.Password;
                }
                ShellMenuProgramm(fileToOpen, cmdLine);
            }
        }
        public static object GetObjectByHotString(String stringToOpen)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name+" "+ stringToOpen);
            }
            catch { }

            if (stringToOpen != null)
            {
                //Notes notes = new Notes();
                //Scripts scripts = new Scripts();
                //Files files = new Files();
                //FilesQuick filesQuick = new FilesQuick();
                //Universals universals = new Universals();
                //Connections connections = new Connections();
                //Tasks tasks = new Tasks();

                String cleanStr;
                if (stringToOpen.IndexOf("#OPENWINDOW", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#OPENWINDOW", "");
                    cleanStr = cleanStr.Replace("(", "");
                    cleanStr = cleanStr.Replace(")", "");
                    switch (cleanStr)
                    {
                        case "MAIN":
                            return new FormMain();
                            break;
                        case "CONSOLE":
                            //return new FormConsole();
                            break;
                        case "SENSORS":
                            return new FormSensMon();
                            break;
                        case "MONITOR":
                            return new FormSysMon();
                            break;
                        case "FIND":
                            return cleanStr;
                    }
                    return null;
                }
                if (stringToOpen.IndexOf("#COMMANDLINE", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#COMMANDLINE", "");
                    System.Diagnostics.Process.Start("CMD.exe", "/C " + cleanStr);
                    return null;
                }
                if (stringToOpen.IndexOf("#SHELLEXECUTE", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#SHELLEXECUTE", "");
                    ShellExecute(0, "open", cleanStr, "", "", 5);
                    return null;
                }
                //if (stringToOpen.IndexOf("#COMMANDLINESENSOR", System.StringComparison.OrdinalIgnoreCase) >= 0)
                //{
                //    cleanStr = stringToOpen.Replace("#COMMANDLINESENSOR", "");
                //    Process p = new Process();
                //    p.StartInfo.RedirectStandardOutput = true;
                //    while (!p.HasExited)
                //    {
                //        value[currentLine] = p.StandardOutput.ReadToEnd();
                //    }

                //    System.Diagnostics.Process.Start("CMD.exe", "/C " + cleanStr);
                //    return null;
                //}
                if (stringToOpen.IndexOf("#NOTE", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#NOTE", "");
                    return ProcessIcon.lists.notes.Find(x => x.Guid == cleanStr);
                }
                if (stringToOpen.IndexOf("#SCRIPTRUN", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#SCRIPTRUN", "");
                    ScriptItem scriptFinded = ProcessIcon.lists.scripts.Find(x => x.Guid == cleanStr.Trim());
                    ExecuteScript(scriptFinded);
                    return null;
                }
                if (stringToOpen.IndexOf("#RUNSCRIPT", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#RUNSCRIPT", "");
                    ScriptItem scriptFinded = ProcessIcon.lists.scripts.Find(x => x.Guid == cleanStr.Trim());
                    ExecuteScript(scriptFinded);
                    return null;
                }
                if (stringToOpen.IndexOf("#MINISCRIPTRUN", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#MINISCRIPTRUN", "");
                    //ScriptItem script = ProcessIcon.lists.scripts.Find(x => x.Guid == cleanStr);
                    //ExecuteScript(script);
                    ExecuteMiniScript(cleanStr);
                    return null;
                }
                if (stringToOpen.IndexOf("#SCRIPT", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#SCRIPT", "");
                    return ProcessIcon.lists.scripts.Find(x => x.Guid == cleanStr);
                }
                if (stringToOpen.IndexOf("#FILEQUICK", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#FILEQUICK", "");
                    return ProcessIcon.lists.filesQuick.Find(x => x.Guid == cleanStr);
                }
                if (stringToOpen.IndexOf("#FILE", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#FILE", "");
                    return ProcessIcon.lists.files.Find(x => x.Guid == cleanStr);
                }
                if (stringToOpen.IndexOf("#CONNECTION", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#CONNECTION", "");
                    return ProcessIcon.lists.connections.Find(x => x.Guid == cleanStr);
                }
                if (stringToOpen.IndexOf("#UNIVERSAL", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#UNIVERSAL", "");
                    UniversalItem ui = ProcessIcon.lists.universals.Find(x => x.Guid == cleanStr);
                    if (ui == null)
                    {
                        foreach (UniversalItem unii in ProcessIcon.lists.universals)
                        {
                            foreach (UniversalAtom unia in unii)
                            {
                                return unia;
                            }
                        }
                    }
                    else
                    {
                        return ui;
                    }
                }
                if (stringToOpen.IndexOf("#TASK", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    cleanStr = stringToOpen.Replace("#TASK", "");
                    return ProcessIcon.lists.tasks.Find(x => x.Guid == cleanStr);
                }
            }
            return null;
            
        }
        public static void OpenObject(Object CurrentObjectNoteScriptEtc)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + "" + CurrentObjectNoteScriptEtc);
            }
            catch { }

            if (CurrentObjectNoteScriptEtc != null)
            {
                if (CurrentObjectNoteScriptEtc.GetType().Name.Substring(0,4)=="Form")
                //if (CurrentObjectNoteScriptEtc.GetType() == typeof(FormMain))
                {
                    Form openForm =  ((Form)CurrentObjectNoteScriptEtc);
                    openForm.Show();
                    openForm.Focus();
                }
                if (CurrentObjectNoteScriptEtc.GetType() == typeof(NoteItem))
                {
                    FormNotepad formNotepad = new FormNotepad();
                    formNotepad.OpenNote((NoteItem)CurrentObjectNoteScriptEtc);
                    formNotepad.Show();
                }
                if (CurrentObjectNoteScriptEtc.GetType() == typeof(ScriptItem))
                {
                    FormScriptEditor formScriptEditor = new FormScriptEditor();
                    formScriptEditor.OpenScript((ScriptItem)CurrentObjectNoteScriptEtc);
                    formScriptEditor.Show();
                }
                if (CurrentObjectNoteScriptEtc.GetType() == typeof(FileItem))
                {
                    ShellMenuProgramm((FileItem)CurrentObjectNoteScriptEtc);
                }
                if (CurrentObjectNoteScriptEtc.GetType() == typeof(FileItemQuick))
                {
                    ShellMenuProgrammQuick((FileItemQuick)CurrentObjectNoteScriptEtc);
                }
                if (CurrentObjectNoteScriptEtc.GetType() == typeof(ConnectionItem))
                {
                    OpenConnection((ConnectionItem)CurrentObjectNoteScriptEtc);
                }
                if (CurrentObjectNoteScriptEtc.GetType() == typeof(UniversalItem))
                {
                }
                if (CurrentObjectNoteScriptEtc.GetType() == typeof(TaskItem))
                {
                    FormTaskReminder formTaskReminder = new FormTaskReminder((TaskItem)CurrentObjectNoteScriptEtc);
                    formTaskReminder.Show();
                }
                if (CurrentObjectNoteScriptEtc.GetType() == typeof(String))
                {
                    switch((String)CurrentObjectNoteScriptEtc)
                    {
                        case "FIND":
                            ProcessIcon.timerQuickSearshHide.Enabled = true;
                            ProcessIcon.formQuickHint.Show();
                            ProcessIcon.formQuickHint.Height = 20;
                            ProcessIcon.formQuickHint.TopMost = true;
                            Point pos = new Point();
                            //pos.Y = Cursor.Position.Y - 34;
                            pos.Y = Screen.PrimaryScreen.Bounds.Height - 49;
                            pos.X = Screen.PrimaryScreen.Bounds.Width - ProcessIcon.formQuickHint.Width;
                            ProcessIcon.formQuickHint.Location = pos;
                            ProcessIcon.formQuickHint.Focus();
                            ProcessIcon.formQuickHint.timerSetFocusOnTextBox.Enabled = true;
                            ProcessIcon.formQuickHint.Activate();
                            break;
                    }
                }
            }
        }
        public static void ShellMenuProgramm(string guid, string commandLine)
        {
            try
            {
                ShellMenuProgramm((FileItem)FindObjectByGuid(guid)[0], commandLine);
            }
            catch (Exception ex)
            { Functions.Message(ex); }
        }
        public static void ShellMenuProgramm(FileItem fileToOpen,string commandLine)
        {
            //Files files = new Files();
            string path;
            string rootPath = Settings.GetString("textBoxRootDir");
            if (rootPath.Length > 0)
            {
                path = rootPath + "\\" + ProcessIcon.lists.files.Find(x => x.Guid == fileToOpen.Guid).PathFile;
            }
            else
            {
                path = Application.StartupPath.ToString() + "\\bin" + ProcessIcon.lists.files.Find(x => x.Guid == fileToOpen.Guid).PathFile;
            }
            try
            {
                ProcessCt process = new ProcessCt(path , commandLine);
                Thread th = new Thread(new ThreadStart(process.StartProcess));
                th.Start();
            }
            catch (Exception ex) { Functions.Message(ex); }

        }
        public static void ShellMenuProgramm(string guid)
        {
            try
            {
                ShellMenuProgramm((FileItem)FindObjectByGuid(guid)[0]);
            } catch (Exception ex)
            { Functions.Message(ex); }
        }
        public static void ShellMenuProgramm(FileItem fileToOpen)
        {
            //Files files = new Files();
            string path;
            string rootPath = Settings.GetString("textBoxRootDir");
            if (rootPath.Length > 0)
            {
                path = rootPath + "\\" + ProcessIcon.lists.files.Find(x => x.Guid == fileToOpen.Guid).PathFile;
            }
            else
            {
                path = Application.StartupPath.ToString() + "\\bin\\" + ProcessIcon.lists.files.Find(x => x.Guid == fileToOpen.Guid).PathFile;
            }
            try
            {
                ProcessCt process = new ProcessCt(path);
                Thread th = new Thread(new ThreadStart(process.StartProcess));
                th.Start();
            }
            catch (Exception ex) { Functions.Message(ex); }

        }
        public static void ShellMenuProgrammQuick(FileItemQuick fileQuickToOpen)
        {
            FilesQuick filesQuick = new FilesQuick();
            string path = filesQuick.Find(x => x.Guid == fileQuickToOpen.Guid).PathFile;
            Process.Start(path);
        }
        private static void OpenConnection(ConnectionItem connectionItemToOpen)
        {
            //Connections connections = new Connections();
            ProcessIcon.formConnection.Show();
            ProcessIcon.formConnection.AddTab(connectionItemToOpen);
        }

        public static string Base64Encode(string plainText)
        {
            if (plainText != null)
            {
                try
                {
                    var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
                    return System.Convert.ToBase64String(plainTextBytes).Replace("+", "PLUSSYMBOL");
                }
                catch
                {
                    return plainText;
                }
            }
            else
            {
                return "";
            }
        }
        public static string Base64Decode(string base64EncodedData)
        {
            if (base64EncodedData != null)
            {
                try
                {
                    var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData.Replace("PLUSSYMBOL", "+"));
                    return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                }
                catch
                {
                    return base64EncodedData;
                }
            }
            else
            {
                return "";
            }
        }
        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        public static void ShowBalloonToolTip(string text)
        {           
            notifyIcon.BalloonTipText = text;
            //notifyIcon.BalloonTipTitle = "Site connection";
            notifyIcon.Icon = Resources.about;
            notifyIcon.Visible = true;
            notifyIcon.ShowBalloonTip(3);
            //timer.Tick += timer_Tick;
            //timer.Interval = 3000;
            //timer.Enabled = true;
        }

        private static void timer_Tick(object sender, EventArgs e)
        {
            notifyIcon.Visible = false;
        }
        public static String ExecuteScript(String guid)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + "" + guid);
            }
            catch { }

            EngineScript scriptEngine;
            String value = "";
                scriptEngine = new EngineScript(ScriptLanguage.CSharp);
                //Scripts scripts = new Scripts();
                ProcessIcon.lists.scripts.ReadBase();
                ScriptItem script = ProcessIcon.lists.scripts.Find(x => x.Guid == guid);
                value = scriptEngine.ExecuteCode(script).ToString();
                return value;
        }
        public static String ExecuteScript(ScriptItem script)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + "" + script.ToString());
            }
            catch { }

            EngineScript scriptEngine;
            String value = "";
            scriptEngine = new EngineScript(ScriptLanguage.VBasic);
            value = scriptEngine.ExecuteCode(script).ToString();
            return value;
        }
        public static String HotstringExecute(String hotstring)
        {
            try
            {
                ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + "" + hotstring);
            }
            catch { }

            if (hotstring.Contains("#MINISCRIPTRUN"))
            {
                return ExecuteMiniScript(hotstring.Replace("#MINISCRIPTRUN", ""));
            }
            if (hotstring.Contains("#SCRIPTRUN") | hotstring.Contains("#RUNSCRIPT"))
            {
                return ExecuteScript(hotstring.Replace("#SCRIPTRUN", ""));
            }
            return "";
            
        }
        public static String ExecuteMiniScript(String script)
        {
            String output = null;
            bool returnExist = false;
            string lastUsing = "";
            string afterUsings = "";
            try
            {
            
            ScriptItem scriptMini = new ScriptItem();
            EngineScript scriptEngine = new EngineScript(ScriptLanguage.CSharp);
            string content = "";
            string[] splittedscript = script.Split(';');
            content += "using System;" + System.Environment.NewLine;
            //content += "System.Windows.Forms.dll;" + System.Environment.NewLine;
            if (script.Contains("using"))
            {
                foreach (string line in splittedscript)
                {
                    try
                    {
                        if (line.Substring(0, 5).ToLower().Contains("using"))
                        {
                            content += line + ";" + System.Environment.NewLine;
                            lastUsing = line;
                        }
                    }
                    catch { }
                }
                lastUsing = lastUsing.Replace("using", "").Trim();
                int lenUsings = script.IndexOf(lastUsing) + lastUsing.Length + 1;
                afterUsings = script.Substring(lenUsings, script.Length - lenUsings);
            }
            else
            {
                afterUsings = script;
            }
            content += "namespace ComputerToolkit" + System.Environment.NewLine;
            content += "{" + System.Environment.NewLine;
            content += "    public class MainClass : ComputerToolkit.Script" + System.Environment.NewLine;
            content += "    {" + System.Environment.NewLine;
            content += "        public MainClass(){}" + System.Environment.NewLine;
            content += "        public object Main()" + System.Environment.NewLine;
            content += "        {" + System.Environment.NewLine;
            content += afterUsings + System.Environment.NewLine;
            if (afterUsings.ToLower().Contains("return"))
                returnExist = true;
            //foreach (string line in splittedscript)
            //{
            //    try
            //    {
            //        if (!(line.Substring(0, 5).ToLower().Contains("using")))
            //        {
            //            content += line + ";" + System.Environment.NewLine;
            //            if (line.ToLower().Contains("return"))
            //                returnExist = true;
            //        }
            //    }
            //    catch 
            //    {
            //        content += line  + System.Environment.NewLine;
            //    }
            //}
            if (returnExist==false)
            content += "            return \"\";" + System.Environment.NewLine;
            content += "        }" + System.Environment.NewLine;
            content += "    }" + System.Environment.NewLine;
            content += "}" + System.Environment.NewLine;


            scriptMini.Guid = Guid.NewGuid().ToString();
            scriptMini.Name = "newscript";
            scriptMini.Content = content;
            if (Settings.Get("debug", false))
                File.WriteAllText("script.txt", content);
            output = scriptEngine.ExecuteCode(scriptMini).ToString();
            return output;
            }
            catch (Exception Ex)
            {
                Functions.Message(Ex);
            }
            return output;
        }

        public static string SaveFileDialog()
        {
            string fileName = "";
            Stream myStream = null;
            SaveFileDialog fi = new SaveFileDialog();
            fi.InitialDirectory = "c:\\";
            fi.Filter = "txt files (*.*)|*.*|All files (*.*)|*.*";
            fi.FilterIndex = 2;
            fi.RestoreDirectory = true;
            fi.Title = "Save to";
            if (fi.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = fi.OpenFile()) != null)
                    {
                        fileName = fi.FileName;
                        using (myStream)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return fileName;
        }
        public static string OpenFileDialog()
        {
            string fileName = "";
            Stream myStream = null;
            OpenFileDialog fi = new OpenFileDialog();
            fi.InitialDirectory = "c:\\";
            fi.Filter = "txt files (*.*)|*.*|All files (*.*)|*.*";
            fi.FilterIndex = 2;
            fi.RestoreDirectory = true;
            fi.Title = "Open";
            if (fi.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = fi.OpenFile()) != null)
                    {
                        fileName = fi.FileName;
                        using (myStream)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return fileName;
        }

    //public static void SaveObjectToJson(object obj, string fileName)
    //{
    //    string json = JsonConvert.SerializeObject(obj);
    //    using (StreamWriter sw = new StreamWriter(fileName))
    //    {
    //        sw.Write(json);
    //    }
    //}
    //public static void LoadObjectFromJson(object obj, string fileName)
    //{
    //    Files f;
    //    using (StreamReader sr = new StreamReader(fileName))
    //    {
    //        f = JsonConvert.DeserializeObject<Files>(sr.ReadToEnd());                
    //    }
    //    Files files = new Files();
    //    files = f;
    //}
        public static void SaveObjectToXML(string obj, string fileName)
        {
            string str = ToXML(obj);
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                sw.Write(str);
            }
        }
        public static string OpenTextFile(string fileName)
        {
            using (StreamReader sw = new StreamReader(fileName))
            {
                return sw.ReadToEnd();
            }
        }
        public static T FromXML<T>(string xml)
        {
            using (StringReader stringReader = new StringReader(xml))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
        }
        public static string ToXML<T>(T obj)
        {
            using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                xmlSerializer.Serialize(stringWriter, obj);
                return stringWriter.ToString();
            }
        }


        public static List<Method> GetAllMainMethods()
        {
            List<Method> listMethods = new List<Method>();
            List<Parameter> listParameters;
            Assembly a = Assembly.LoadWithPartialName("ComputerToolkit");
            Type[] types = a.GetTypes();
            foreach (Type type in types)
            {
                if (!type.IsPublic)
                {
                    continue;
                }

                //MemberInfo[] members = type.GetMembers(BindingFlags.Public
                //                                      | BindingFlags.Instance
                //                                      | BindingFlags.InvokeMethod);


                MethodInfo[] members = type.GetMethods(BindingFlags.Static | BindingFlags.Public);

                //MethodInfo[] members = type.GetMethods(BindingFlags.Public);

                foreach (MethodInfo member in members)
                {
                    
                    string[] paramsMethod = new string[member.GetParameters().Length];
                    string paramToadd="";
                    int idx=0;
                    foreach (ParameterInfo pParameter in member.GetParameters())
                    {
                        paramsMethod[pParameter.Position] = pParameter.Name + ";"+pParameter.ParameterType.Name;
                        idx++;
                    }
                    listParameters = new List<Parameter>();
                    for(int i=0;i<member.GetParameters().Length;i++)
                    {
                        string[] strArr=paramsMethod[i].Split(';');
                        listParameters.Add(new Parameter(strArr[0],strArr[1]));
                        //paramToadd+=paramsMethod[i];
                    }
                    listMethods.Add(new Method(type.Name , member.Name, member.ReturnType.ToString(), listParameters));
                }
            }
            return listMethods;
        }
        public static List<Method> GetAllGlobalMethods()
        {
            List<Method> listMethods = new List<Method>();
            List<Parameter> listParameters;
            Assembly a = Assembly.LoadWithPartialName("ComputerToolkit");
            Type[] types = a.GetTypes();
            foreach (Type type in types)
            {
                if (!type.IsPublic)
                {
                    continue;
                }



                //MethodInfo[] members = type.GetMethods(BindingFlags.Public
                //                                      | BindingFlags.Instance
                //                                      | BindingFlags.InvokeMethod);
                MethodInfo[] members = type.GetMethods(BindingFlags.Public| BindingFlags.Instance| BindingFlags.InvokeMethod);

                foreach (MethodInfo member in members)
                {
                    
                    string[] paramsMethod = new string[member.GetParameters().Length];
                    string paramToadd="";
                    int idx=0;
                    foreach (ParameterInfo pParameter in member.GetParameters())
                    {
                        paramsMethod[pParameter.Position] = pParameter.Name + ";"+pParameter.ParameterType.Name;
                        idx++;
                    }
                    listParameters = new List<Parameter>();
                    for(int i=0;i<member.GetParameters().Length;i++)
                    {
                        string[] strArr=paramsMethod[i].Split(';');
                        listParameters.Add(new Parameter(strArr[0],strArr[1]));
                        //paramToadd+=paramsMethod[i];
                    }
                    listMethods.Add(new Method(type.Name , member.Name, member.ReturnType.ToString(), listParameters));
                }

                MemberInfo[] members2 = type.GetMembers(BindingFlags.Public | BindingFlags.Instance | BindingFlags.InvokeMethod);
                listParameters = new List<Parameter>();
                foreach (MemberInfo member in members2)
                {

                    listMethods.Add(new Method(type.Name, member.Name, member.MemberType.ToString(), listParameters));
                }

                FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.InvokeMethod);

                foreach (FieldInfo field in fields)
                {

                    listMethods.Add(new Method(type.Name, field.Name, field.FieldType.ToString(), listParameters));
                }
            }
            return listMethods;
        }
        public static string GetParamName(System.Reflection.MethodInfo method, int index)
        {
            string retVal = string.Empty;

            if (method != null && method.GetParameters().Length > index)
                retVal = method.GetParameters()[index].Name;


            return retVal;
        }
        public static string GetDirInPath(String s)
        {
            int lastSlash = 0;
            for (int i = s.Length - 2; i > 0; i--)
            {
                if (s.Substring(i, 2).IndexOf("\\") > -1)
                {
                    lastSlash = i;
                    break;
                }
            }
            if (lastSlash != 0)
            {
                return s.Substring(0, lastSlash);
            }
            return s;
        }
        public static string GetFileNameInPath(String s)
        {
            String pz = GetDirInPath(s);
            return s.Substring(pz.Length + 1, (s.Length - (pz.Length + 1)));
        }
        public static string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }
        public static string ImageToBase64(Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }
        public static string IconToBase64(Icon icon)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                Image image = (Image)icon.ToBitmap();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }
        public static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }
        public static Icon Base64ToIcon(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Bitmap bitmap = (Bitmap)Image.FromStream(ms, true);
            Icon icon = System.Drawing.Icon.FromHandle(bitmap.GetHicon());
            return icon;
        }
    }
}
