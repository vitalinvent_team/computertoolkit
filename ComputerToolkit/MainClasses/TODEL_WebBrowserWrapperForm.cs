﻿//using Disk.SDK;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Windows.Forms;

//namespace ComputerToolkit
//{
//    class WebBrowserWrapperForm : IBrowser
//    {
//        private readonly WebBrowser browser;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="WebBrowserWrapper"/> class.
//        /// </summary>
//        /// <param name="browser">The browser.</param>
//        public WebBrowserWrapperForm(WebBrowser browser)
//        {
//            this.browser = browser;
//            this.browser.Navigating += this.BrowserOnNavigating;
//        }
//        /// <summary>
//        /// Occurs when browser is navigating to the url.
//        /// </summary>
//        /// <param name="sender">The sender.</param>
//        /// <param name="e">The <see cref="NavigatingCancelEventArgs"/> instance containing the event data.</param>

//        private void BrowserOnNavigating(object sender, WebBrowserNavigatingEventArgs e)
//        {
//            this.Navigating.Invoke(this, new GenericSdkEventArgs<string>(e.Url.ToString()));
//        }

//        /// <summary>
//        /// Navigates to the specified URL.
//        /// </summary>
//        /// <param name="url">The URL.</param>
//        public void Navigate(string url)
//        {
//            this.browser.Navigate(new Uri(url));
//        }

//        /// <summary>
//        /// Occurs just before navigation to a document.
//        /// </summary>
//        public event EventHandler<GenericSdkEventArgs<string>> Navigating;


//    }
//}
