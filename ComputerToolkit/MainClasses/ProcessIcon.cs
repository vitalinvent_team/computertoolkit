﻿using System.Threading;
using ComputerToolkit.Forms;
using ComputerToolkit.MainClasses;
using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;


namespace ComputerToolkit
{
    public class ProcessIcon : IDisposable
    {
        public static Lists lists = new Lists();
        public static FormQuickHint formQuickHint = new FormQuickHint();
        //public static FormQuickHintItem formQuickHintItem = new FormQuickHintItem();
        public static List<FormQuickHintItem> listFormQuickHintItem = new List<FormQuickHintItem>();
        public static int currentQuickIntem = 0;
        public static NotifyIcon notifyIcon;
        private ProcessesClass processesClass = new ProcessesClass();
        ContextMenuStrip menu = new ContextMenus().CreateMOnouseOverMenu();
        //private List<Timer> timersList = new List<Timer>();
        public static Timer timerTasks = new Timer();
        public static Timer timerQuickSearshHide = new Timer();
        private Timer timerCheckUpdates = new Timer();
        private Timer timerSynchronize = new Timer();
        //Tasks tasks = new Tasks();
        private bool updatesEnable = false;
        private bool googleEnable = false;
        private Thread googleThread;
        public static FormPlayer formPlayer;
        public static ContextMenuStrip cmtmnu = new ContextMenus().CreateMainMenu();
        public static FormConnections formConnection = new FormConnections();
        public static EngineExchange exchangeEngine = new EngineExchange();
        public static Logs logs = new Logs("logs");
        public ProcessIcon()
        {
            formPlayer = new FormPlayer();
            updatesEnable = Settings.Get("checkBoxCheckUpdate", false);
            googleEnable = Settings.Get("checkBoxGoogleEnable", false);
            notifyIcon = new NotifyIcon();
            timerQuickSearshHide.Interval = 1000;
            timerQuickSearshHide.Tick += timer_Tick;
            timerTasks.Interval = 10000;
            timerTasks.Enabled = Settings.Get("TasksEnabled", false);
            timerTasks.Tick += TimerTasks_Tick;
            timerCheckUpdates.Interval = int.Parse(Settings.Get("textBoxTimeToRefreshUpdateGoogle", "600")) * 1000 * 60;
            timerCheckUpdates.Enabled = true;
            timerCheckUpdates.Tick += timerCheckUpdates_Tick;
            timerSynchronize.Interval = int.Parse(Settings.Get("timerSynchronize", "1")) * 1000 * 60 * 60;
            timerSynchronize.Enabled = true;
            timerSynchronize.Tick += timerSynchronize_Tick;
            
        }

        private void timerSynchronize_Tick(object sender, EventArgs e)
        {
            //foreach(List<Object> list in ProcessIcon.lists)
            //{
            //    exchangeEngine.SynchronizeList(list);
            //}
            exchangeEngine.SynchronizeList(ProcessIcon.lists.connections);
            exchangeEngine.SynchronizeList(ProcessIcon.lists.tasks);
            exchangeEngine.SynchronizeList(ProcessIcon.lists.notes);
            exchangeEngine.SynchronizeList(ProcessIcon.lists.scripts);
            exchangeEngine.SynchronizeList(ProcessIcon.lists.tasks);
            exchangeEngine.SynchronizeList(ProcessIcon.lists.hotkeys);
            exchangeEngine.SynchronizeList(ProcessIcon.lists.sensors);
        }

        private void TimerTasks_Tick(object sender, EventArgs e)
        {
            //try
            //{
            //    ProcessIcon.logs.Add(System.Reflection.MethodBase.GetCurrentMethod().Name);
            //}
            //catch { }

            //timerTasks.Enabled = false;
            foreach (TaskItem task in ProcessIcon.lists.tasks)
            {
                try
                {

                    if (task.Enabled)
                    {
                        if (task.isReminder)
                        {
                            if (task.TimeToRun.Length > 0)
                            {
                                try
                                {
                                    //запуск напоминания на день и час и минута
                                    DateTime dateTime = DateTime.Parse(task.TimeToRun);
                                    DateTime LastTimeToRun = DateTime.Now;
                                    DateTime.TryParse(task.LastTimeToRun, out LastTimeToRun);
                                    if ((DateTime.Now.Day == dateTime.Day) & (DateTime.Now.Hour == dateTime.Hour) & (DateTime.Now.Minute == dateTime.Minute) & ((DateTime.Now - LastTimeToRun).TotalMinutes > 1))
                                    {
                                        Functions.Message(task);
                                        Functions.HotstringExecute(task.HotString);
                                        task.LastTimeToRun = DateTime.Now.ToString();
                                        //Functions.OpenObject(Functions.FindObjectByGuid(task.Guid));
                                    }
                                    //если пропущено напоминание
                                    if ((task.LastTimeToRun.Length==0) & (((DateTime.Now - DateTime.Parse(task.TimeToRun)).TotalMinutes > 1)))
                                    {
                                        Functions.Message(task);
                                        Functions.HotstringExecute(task.HotString);
                                        //task.LastTimeToRun = DateTime.Now.ToString();
                                    }
                                    //если пропущено отложенное напоминание
                                    if ((DateTime.Now > dateTime) & (dateTime > LastTimeToRun))
                                    {
                                        Functions.Message(task);
                                        Functions.HotstringExecute(task.HotString);
                                        //task.LastTimeToRun = DateTime.Now.ToString();
                                    }
                                }
                                catch (Exception ex)
                                { Functions.Message(ex); }

                            }
                        }
                        else
                        {
                            if (task.TimeToRun.Length > 0)
                            {
                                try
                                {
                                    TimeSpan timeToRun = TimeSpan.Parse(task.TimeToRun);
                                    if (task.Interval > 0)
                                    {
                                        //запуск либо в первый раз либо по периоду больше интервала
                                        if ((task.LastTimeToRun.Length==0) || (DateTime.Now.Subtract(DateTime.Parse(task.LastTimeToRun)).Minutes > task.Interval))
                                        {
                                            Functions.OpenObject(Functions.GetObjectByHotString(task.HotString));
                                            task.LastTimeToRun = DateTime.Now.ToString();
                                            ProcessIcon.lists.tasks.SaveBase();
                                        }
                                    }
                                    else
                                    {
                                        // запуск раз в день
                                        if ((DateTime.Now.Hour == timeToRun.Hours) & (DateTime.Now.Minute == timeToRun.Minutes) & ((DateTime.Now - DateTime.Parse(task.LastTimeToRun)).TotalMinutes > 1))
                                        {
                                            Functions.OpenObject(Functions.GetObjectByHotString(task.HotString));
                                            task.LastTimeToRun = DateTime.Now.ToString();
                                            ProcessIcon.lists.tasks.SaveBase();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                { Functions.Message(ex); }
                            }
                            else
                            {
                                Functions.OpenObject(Functions.GetObjectByHotString(task.HotString));
                            }
                        }

                    }



                }
                catch (Exception ex) { Functions.Message(ex); }
            }
        }

        private void timerCheckUpdates_Tick(object sender, EventArgs e)
        {
            try
            {
                if (updatesEnable)
                {
                    Updates updates = new Updates();                    
                    if (updates.VerifyUpdate(false))
                        timerCheckUpdates.Enabled = false;
                }
            }
            catch (Exception)
            {
            }
        }
        private void FillTimers()
        {
            //foreach (TaskItem task in ProcessIcon.lists.tasks)
            //{
            //    try
            //    {
            //        Timer timer = new Timer();
            //        timer.Interval = task.Interval;
            //        timer.Enabled = task.Enabled;
            //        timer.Tag = task.Guid;
            //        timer.Tick += new EventHandler(timerTask_tick);
            //        timersList.Add(timer);
            //    }
            //    catch (Exception ex) { Functions.Message(ex); }
            //}
        }
        //private void timerTask_tick(object sender, EventArgs e)
        //{
        //    Timer timer = (Timer)sender;
        //    TaskItem findedTask = ProcessIcon.lists.tasks.Find(x => x.Guid == timer.Tag);
        //    if (findedTask != null)
        //    {
        //        if (Settings.Get("TasksEnabled", false))
        //        {
        //            if (findedTask.Enabled)
        //            {
        //                if (findedTask.isReminder)
        //                {
        //                    if (findedTask.TimeToRun.Length > 0)
        //                    {
        //                        try
        //                        {
        //                            DateTime dateTime = DateTime.Parse(findedTask.TimeToRun);
        //                            if ((DateTime.Now.Day == dateTime.Day) & (DateTime.Now.Hour == dateTime.Hour))
        //                            {
        //                                Functions.Message(findedTask);
        //                                Functions.ExecuteMiniScript(findedTask.HotString);
        //                                //Functions.OpenObject(Functions.FindObjectByGuid(findedTask.Guid));
        //                            }
        //                        }
        //                        catch (Exception ex)
        //                        { Functions.Message(ex); }

        //                    }
        //                }
        //                else
        //                {
        //                    if (findedTask.TimeToRun.Length > 0)
        //                    {
        //                        try
        //                        {
        //                            TimeSpan timeToRun = TimeSpan.Parse(findedTask.TimeToRun);
        //                            if (((DateTime.Now.Day > timeToRun.Days)) &
        //                                    !(TimeSpan.Parse(findedTask.LastTimeToRun) == timeToRun))
        //                            {
        //                                Functions.OpenObject(Functions.GetObjectByHotString(findedTask.HotString));
        //                                findedTask.LastTimeToRun = DateTime.Now.ToString(); // DateTime.Now.Hour + ":" + DateTime.Now.Minute+":00";
        //                                ProcessIcon.lists.tasks.SaveBase();
        //                            }
        //                        }
        //                        catch (Exception ex)
        //                        { Functions.Message(ex); }
        //                    }
        //                    else
        //                    {
        //                        Functions.OpenObject(Functions.GetObjectByHotString(findedTask.HotString));
        //                    }
        //                }

        //                //Scripts scripts = new Scripts();
        //                //ScriptItem findedScript = scripts.Find(x => x.Guid == guidsToSplit[1]);
        //                //Functions.ExecuteScript(findedScript);
        //            }
        //        }
        //    }

        //}

        public void Display()
        {
            notifyIcon.MouseClick += new MouseEventHandler(ni_MouseClick);
            notifyIcon.Icon = ComputerToolkit.Properties.Resources.iconTray;
            notifyIcon.Text = "Computer toolkit v" + Assembly.GetEntryAssembly().GetName().Version;
            notifyIcon.Visible = true;
            ContextMenuStrip cmtmnu = new ContextMenus().CreateMainMenu();
            notifyIcon.ContextMenuStrip = cmtmnu;

            notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon.BalloonTipTitle = "Computer toolkit v" + Assembly.GetEntryAssembly().GetName().Version;


            if (Settings.Get("QuickChooser", "0") == "1")
            {
                Settings.Set("QuickChooser", "1");
                ContextMenus.formQuickMagnetickChooser.Show();
            }
            if (updatesEnable)
            {
                Updates updates = new Updates();
                Thread threadUpdate = new Thread(new ThreadStart(updates.VerifyUpdate));
            }
            
        }
        private void ni_MouseMove(object sender, MouseEventArgs e)
        {
        }
        public void Dispose()
        {
            notifyIcon.Dispose();
        }
        void ni_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                timerQuickSearshHide.Enabled = true;
                formQuickHint.Show();
                formQuickHint.Height = 20;
                formQuickHint.TopMost = true;
                Point pos = new Point();
                //pos.Y = Cursor.Position.Y - 34;
                pos.Y = Screen.PrimaryScreen.Bounds.Height - 49;
                pos.X = Screen.PrimaryScreen.Bounds.Width - formQuickHint.Width;
                formQuickHint.Location = pos;
                formQuickHint.Focus();
                formQuickHint.timerSetFocusOnTextBox.Enabled = true;
                formQuickHint.Activate();
                //menu.Show(Cursor.Position); menu quick files
            }
            if (e.Button == MouseButtons.Right)
            {
                //ERROR соединения обнуляются при втором вызове по правой кнопке мыши в трее на ярлыке
               cmtmnu = new ContextMenus().CreateMainMenu();
                notifyIcon.ContextMenuStrip = cmtmnu;
            }
        }
        public static void ShowQuickHintFormsList()
        {
            int YStartDraw = Screen.PrimaryScreen.Bounds.Height - 69;
            int XStartDraw = Screen.PrimaryScreen.Bounds.Width - formQuickHint.Width;
            int idx=0;
            foreach (FormQuickHintItem formQuickHintItem in listFormQuickHintItem)
            {
                Point point = new Point();
                point.Y = YStartDraw - idx * 21;
                point.X = XStartDraw;
                formQuickHintItem.Show();
                formQuickHintItem.Height = 20;
                formQuickHintItem.Location = point;
                idx++;
            }
        }
        public static void HideQuickHintFormsList()
        {
            foreach (FormQuickHintItem formQuickHintItem in listFormQuickHintItem)
            {
                formQuickHintItem.Hide();
            }
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            Control[] controls=formQuickHint.Controls.Find("textBoxQuickSearch",true);
            TextBox tbox=(TextBox) controls[0];
            if (tbox.Text.Length==0)
            {
            formQuickHint.Hide();
            timerQuickSearshHide.Enabled = false;
                }
        }

    }
}
