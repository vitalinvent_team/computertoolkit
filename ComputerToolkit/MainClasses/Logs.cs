﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit
{
    public  class Logs
    {
        private List<SettingsItem> logList = new List<SettingsItem>();
        private string logPath;
        private String _name;
        public Logs(String name)
        {
            _name = name;
            logPath = Application.StartupPath.ToString() + "\\" + _name + ".txt";
        }
        public  void Add(String val)
        {
            File.AppendAllText(logPath, string.Format("{0}-{1}{2}",DateTime.Now.ToString(), val.ToString(), Environment.NewLine));
        }
        public  void Add(String name, Boolean val)
        {
            File.AppendAllText(logPath, string.Format("{0}-{1}{2}", DateTime.Now.ToString(), val.ToString(), Environment.NewLine));
        }
        public  void Add(String name, int val)
        {
            File.AppendAllText(logPath, string.Format("{0}-{1}{2}", DateTime.Now.ToString(), val.ToString(), Environment.NewLine));
        }
        public void ClearAllLogs()
        {
            String[] clearLogs = new String[1] {""};
            try
            {
                File.WriteAllLines(logPath, clearLogs);
            }
            catch (Exception ex) { Functions.Message(ex); }
        }
        public  void SaveAllLogs(string[] logs)
        {
            try
            {
                if (logs.Length > 0)
                    File.WriteAllLines(logPath, logs);
            }
            catch (Exception ex) { Functions.Message(ex); }
        }
        public  string[] GetAllLogs()
        {
            string[] logs;
            try
            {
                logs = File.ReadAllLines(logPath);
                return logs;
            }
            catch (Exception ex) { logs = " ; ".Split(' '); Functions.Message(ex); return logs; }
        }
        public class SettingsItem
        {
            public String Name { get; set; }
            public String Value { get; set; }
            public SettingsItem()
            {
            }
            public SettingsItem(string name, string value)
            {
                this.Value = value;
                this.Name = name;
            }
        }
    }
}
