﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit
{
    public static class EngineDialogs
    {
        public static string[] OpenFileAsStringArray()
        {
            try
            {
                Stream myStream = null;
                System.Windows.Forms.OpenFileDialog fi = new System.Windows.Forms.OpenFileDialog();
                fi.InitialDirectory = Settings.Get("OpenFileAsStringArray","c:\\");
                fi.Filter = "txt files (*.*)|*.*|All files (*.*)|*.*";
                fi.FilterIndex = 2;
                fi.RestoreDirectory = true;
                String externRdp;
                if (fi.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        if ((myStream = fi.OpenFile()) != null)
                        {
                            Settings.Set("OpenFileAsStringArray", Path.GetDirectoryName(fi.FileName));
                            return File.ReadAllLines(fi.FileName);
                            //ParseEngine.ParseToList(fi.FileName, textBoxSeparator.Text);
                        }
                    }
                    catch (Exception ex)
                    {
                        Functions.Message(ex.Message);
                        return null;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Functions.Message(ex);
                return null;
            }
        }
        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }
        public static string ChooseDirectory()
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            folderDialog.SelectedPath = Settings.Get("ChooseDirectory", "c:\\");
            DialogResult result;
            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                Settings.Set("ChooseDirectory", folderDialog.SelectedPath);
                return folderDialog.SelectedPath;
            }
            else
            {
                return "c:\\";
            }
        }
        public static string ChooseFile()
        {
            OpenFileDialog folderDialog = new OpenFileDialog();
            folderDialog.FileName = Settings.Get("ChooseFile", "c:\\");
            DialogResult result;
            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                Settings.Set("ChooseDirectory", folderDialog.FileName);
                return folderDialog.FileName;
            }
            else
            {
                return "c:\\";
            }
        }

    }
}
