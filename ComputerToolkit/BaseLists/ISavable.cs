﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit
{
    interface ISavable
    {
        void ReadBase();
        void SaveBase();
    }
}
