﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class Hotkeys : List<HotkeyItem>, ISavable
    {
        private string appPath;

        [XmlElement("listGroups")]
        public List<string> listGroups = new List<string>();
        public Hotkeys()
        {
            appPath = Application.StartupPath.ToString();            
            ReadBase();
        }
        public void ReadBase()
        {
            try
            {
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                string xmlString = File.ReadAllText(fileName);
                List<HotkeyItem> hotkeys = xmlString.DeserializeFromStringXml<List<HotkeyItem>>();
                if (hotkeys != null)
                {
                    this.Clear();
                    foreach (HotkeyItem hotkey in hotkeys)
                    {
                        this.Add(hotkey);
                        if (hotkey.Group == null) hotkey.Group = "";
                        if (listGroups.FindAll(S => S.Equals(hotkey.Group.ToString())).Count == 0)
                            listGroups.Add(hotkey.Group);
                    }
                }
            }
            catch (Exception ex) { 
                Functions.Message(ex); 
            }
        }

        public void SaveBase()
        {
            try
            {
                string xmlString = EngineSerialization.SerializeToStringXml(this);
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                File.WriteAllText(fileName, xmlString);
            }
            catch (Exception ex) { Functions.Message(ex); }

        }
        //public void ReadBase(string filePath)
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(filePath, FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<HotkeyItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //}
        //public void ReadBase()
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_HOTKEYS", "ComputerToolkitHotkeys.bin"), FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<HotkeyItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //}
        //private void FillThisFromList(List<HotkeyItem> tempList)
        //{
        //    this.Clear();
        //    foreach (HotkeyItem connection in tempList)
        //    {
        //        this.Add(connection);
        //    }
        //    foreach (HotkeyItem item in tempList)
        //    {
        //        if (item.Date == null)
        //        {
        //            item.Date = DateTime.Now.ToString();
        //        }
        //    }
        //}
        //public void SaveBase()
        //{
        //    using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_HOTKEYS", "ComputerToolkitHotkeys.bin"), FileMode.Create))
        //    {
        //        var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //        bformatter.Serialize(stream, this);
        //    }
        //    try
        //    {
        //        string xmlString = EngineSerialization.SerializeToStringXml(this);
        //        string fileName = appPath + "\\" + this.GetType().Name + ".xml";
        //        File.WriteAllText(fileName, xmlString);
        //    }
        //    catch (Exception ex) { Functions.Message(ex.Message); }
        //}

    }
}
