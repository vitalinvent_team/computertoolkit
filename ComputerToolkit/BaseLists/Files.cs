﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class Files : List<FileItem>, ISavable
    {
        private string appPath;

        [XmlElement("listGroups")]
        public List<string> listGroups = new List<string>();
        public Files()
        {
            appPath = Application.StartupPath.ToString();            
            ReadBase();
        }
        public void ReadBase()
        {
            try
            {
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                string xmlString = File.ReadAllText(fileName);
                List<FileItem> files = xmlString.DeserializeFromStringXml<List<FileItem>>();
                if (files != null)
                {
                    this.Clear();
                    foreach (FileItem file in files)
                    {
                        this.Add(file);
                        if (file.Group == null) file.Group = "";
                        if (listGroups.FindAll(S => S.Equals(file.Group.ToString())).Count == 0)
                            listGroups.Add(file.Group);
                    }
                }

            }
            catch (Exception ex) { Functions.Message(ex); }
        }

        public void SaveBase()
        {
            try
            {
                string xmlString = this.SerializeToStringXml();//EngineSerialization.SerializeToStringXml(this);
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                File.WriteAllText(fileName, xmlString);
            }
            catch (Exception ex) { Functions.Message(ex); }

        }

        //public void ReadBase()
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_FILES", "ComputerToolkitFiles.bin"), FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        //            FillThisFromList((List<FileItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Functions.Message(ex);
        //    }
        //    FillGroups();
        //}
        //public void ReadBase(string filePath)
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(filePath, FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        //            FillThisFromList((List<FileItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Functions.Message(ex);
        //    }
        //    FillGroups();
        //}
        //private void FillThisFromList(List<FileItem> tempList)
        //{
        //    this.Clear();
        //    foreach (FileItem file in tempList)
        //    {
        //        if (file.Date == null)
        //        {
        //            file.Date = DateTime.Now.ToString();
        //        }
        //        this.Add(file);
        //    }
        //    foreach (FileItem connection in tempList)
        //    {
        //        connection.IconFileString = Functions.IconToBase64(connection.IconFile);
        //    }
        //}
        //public void SaveBase()
        //{
        //    CleanBase();

        //    try
        //    {

        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_FILES", "ComputerToolkitFiles.bin"), FileMode.Create))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            bformatter.Serialize(stream, this);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Functions.Message(ex);
        //    }
        //    try
        //    {
        //        string xmlString = EngineSerialization.SerializeToStringXml(this);
        //        string fileName = appPath + "\\" + this.GetType().Name + ".xml";
        //        File.WriteAllText(fileName, xmlString);
        //    }
        //    catch (Exception ex) { Functions.Message(ex.Message); }
        //}
        //private void FillGroups()
        //{
        //    foreach (FileItem file in this)
        //    {
        //        if (listGroups.FindAll(S => S.Contains(file.Group.ToString())).Count == 0)
        //            listGroups.Add(file.Group);
        //    }
        //}
        //private void CleanBase()
        //{
        //    foreach (FileItem file in this)
        //    {
        //        if (file.Group.IndexOf("\\") > -1)
        //        {
        //            file.Group = file.Group.Replace("\\", "");
        //        }

        //    }
        //}
    }
}
