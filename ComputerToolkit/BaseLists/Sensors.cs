﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class Sensors : List<SensorItem>, ISavable
    {
        private string appPath;

        [XmlElement("listGroups")]
        public List<string> listGroups = new List<string>();
        public Sensors()
        {
            appPath = Application.StartupPath.ToString();
            ReadBase();
        }
        public void ReadBase()
        {
            try
            {
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                string xmlString = File.ReadAllText(fileName);
                List<SensorItem> sensors = xmlString.DeserializeFromStringXml<List<SensorItem>>();
                if (sensors != null)
                {
                    this.Clear(); 
                    foreach (SensorItem sensor in sensors)
                    {
                        this.Add(sensor);
                        if (sensor.Group == null) sensor.Group = "";
                        if (listGroups.FindAll(S => S.Equals(sensor.Group.ToString())).Count == 0)
                            listGroups.Add(sensor.Group);
                    }
                }
            }
            catch (Exception ex) { Functions.Message(ex); }
        }

        public void SaveBase()
        {
            try
            {
                string xmlString = EngineSerialization.SerializeToStringXml(this);
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                File.WriteAllText(fileName, xmlString);
            }
            catch (Exception ex) { Functions.Message(ex); }
       }
            //public void ReadBase()
            //{
            //    try
            //    {
            //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_SENSORS", "ComputerToolkitSensors.bin"), FileMode.Open))
            //        {
            //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            //            FillThisFromList((List<SensorItem>)bformatter.Deserialize(stream));
            //        }
            //    }
            //    catch { }
            //}
            //public void ReadBase(string filePath)
            //{
            //    try
            //    {
            //        using (Stream stream = File.Open(filePath, FileMode.Open))
            //        {
            //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            //            FillThisFromList((List<SensorItem>)bformatter.Deserialize(stream));
            //        }
            //    }
            //    catch { }
            //}
            //private void FillThisFromList(List<SensorItem> tempList)
            //{
            //    this.Clear();
            //    foreach (SensorItem sensor in tempList)
            //    {
            //        this.Add(sensor);
            //    }
            //    foreach (SensorItem item in tempList)
            //    {
            //        if (item.Date == null)
            //        {
            //            item.Date = DateTime.Now.ToString();
            //        }
            //    }
            //}
            //public void SaveBase()
            //{
            //    using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_SENSORS", "ComputerToolkitSensors.bin"), FileMode.Create))
            //    {
            //        var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            //        bformatter.Serialize(stream, this);
            //    }
            //    try
            //    {
            //        string xmlString = EngineSerialization.SerializeToStringXml(this);
            //        string fileName = appPath + "\\" + this.GetType().Name + ".xml";
            //        File.WriteAllText(fileName, xmlString);
            //    }
            //    catch (Exception ex) { Functions.Message(ex.Message); }
            //}
        }
    }
