﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class Universals:List<UniversalItem>
    {
        private string appPath;

        [XmlElement("listGroups")]
        public List<string> listGroups = new List<string>();
        public Universals()
        {
            appPath = Application.StartupPath.ToString();            
            ReadBase();
        }

        public void ReadBase()
        {
            string fileName = appPath + "\\" + this.GetType().Name + ".xml";
            string xmlString = File.ReadAllText(fileName);
            List<UniversalItem> universals = xmlString.DeserializeFromStringXml<List<UniversalItem>>();
            if (universals != null)
            {
                this.Clear();
                foreach (UniversalItem universal in universals)
                {
                    this.Add(universal);
                    if (universal.Group == null) universal.Group = "";
                    if (listGroups.FindAll(S => S.Equals(universal.Group.ToString())).Count == 0)
                        listGroups.Add(universal.Group);
                }
            }
        }

        public void SaveBase()
        {
            try
            {
                string xmlString = EngineSerialization.SerializeToStringXml(this);
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                File.WriteAllText(fileName, xmlString);
            }
            catch (Exception ex) { Functions.Message(ex); }

        }
        //public void ReadBase(string filePath)
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(filePath, FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<UniversalItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //    FillGroups();
        //}
        //public void ReadBase()
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_UNIVERSALS", "ComputerToolkitUniversals.bin"), FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<UniversalItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //    FillGroups();
        //}
        //private void FillThisFromList(List<UniversalItem> tempList)
        //{
        //    this.Clear();
        //    foreach (UniversalItem connection in tempList)
        //    {
        //        if (connection.Guid.Length > 0)
        //            this.Add(connection);
        //    }
        //    foreach (UniversalItem item in tempList)
        //    {
        //        if (item.Date == null)
        //        {
        //            item.Date = DateTime.Now.ToString();
        //        }
        //    }
        //}
        //public void SaveBase()
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_UNIVERSALS", "ComputerToolkitUniversals.bin"), FileMode.Create))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            bformatter.Serialize(stream, this);
        //        }
        //    }
        //    catch { }
        //    try
        //    {
        //        string xmlString = EngineSerialization.SerializeToStringXml(this);
        //        string fileName = appPath + "\\" + this.GetType().Name + ".xml";
        //        File.WriteAllText(fileName, xmlString);
        //    }
        //    catch (Exception ex) { Functions.Message(ex.Message); }
        //}
        //private void FillGroups()
        //{
        //    try
        //    {
        //        foreach (UniversalItem universal in this)
        //        {
        //            if (listGroups.FindAll(S => S.Contains(universal.Group.ToString())).Count == 0)
        //                listGroups.Add(universal.Group);
        //        }
        //    }
        //    catch { }
        //}
    }

}
