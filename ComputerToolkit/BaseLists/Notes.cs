﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]
    public class Notes : List<NoteItem> , ISavable
    {
        private string appPath;

        [XmlElement("listGroups")]
        public List<string> listGroups = new List<string>();
        public Notes()
        {
            appPath = Application.StartupPath.ToString();            
            ReadBase();
        }
        public void ReadBase()
        {
            try
            {
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                string xmlString = File.ReadAllText(fileName);
                List<NoteItem> notes = xmlString.DeserializeFromStringXml<List<NoteItem>>();
                if (notes != null)
                {
                    this.Clear();
                    foreach (NoteItem note in notes)
                    {
                        this.Add(note);
                        if (note.Group == null) note.Group = "";
                        if (listGroups.FindAll(S => S.Equals(note.Group.ToString())).Count == 0)
                            listGroups.Add(note.Group);
                    }
                }
            }
            catch (Exception ex) { 
                Functions.Message(ex); 
            }
        }

        public void SaveBase()
        {
            try
            {
                string xmlString = EngineSerialization.SerializeToStringXml(this);
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                File.WriteAllText(fileName, xmlString);
            }
            catch (Exception ex) { Functions.Message(ex); }

        }
        //public void ReadBase()
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_NOTES", "ComputerToolkitNotes.bin"), FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<NoteItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //    FillGroups();
        //}
        //public void ReadBase(string filePath)
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(filePath, FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<NoteItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //    FillGroups();
        //}
        //private void FillThisFromList(List<NoteItem> tempList)
        //{
        //    this.Clear();
        //    foreach (NoteItem connection in tempList)
        //    {
        //        this.Add(connection);
        //    }
        //    foreach (NoteItem item in tempList)
        //    {
        //        if (item.Date == null)
        //        {
        //            item.Date = DateTime.Now.ToString();
        //        }
        //    }
        //}
        //public void SaveBase()
        //{
        //    CleanBase();
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_NOTES", "ComputerToolkitNotes.bin"), FileMode.Create))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            bformatter.Serialize(stream, this);
        //        }
        //    }
        //    catch { }
        //    try
        //    {
        //        string xmlString = EngineSerialization.SerializeToStringXml(this);
        //        string fileName = appPath + "\\" + this.GetType().Name + ".xml";
        //        File.WriteAllText(fileName, xmlString);
        //    }
        //    catch (Exception ex) { Functions.Message(ex.Message); }
                
        //}
        //private void FillGroups()
        //{
        //    try
        //    {
        //        foreach (NoteItem file in this)
        //        {
        //            if (listGroups.FindAll(S => S.Contains(file.Group.ToString())).Count == 0)
        //                listGroups.Add(file.Group);
        //        }
        //    }
        //    catch { }
        //}
        //private void CleanBase()
        //{
        //    try
        //    {
        //        foreach (NoteItem file in this)
        //        {
        //            if (file.Group.IndexOf("\\") > -1)
        //            {
        //                file.Group = file.Group.Replace("\\", "");
        //            }

        //        }
        //    }
        //    catch { }
        //}
    }
}
