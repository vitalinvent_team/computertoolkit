﻿using ComputerToolkit.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ComputerToolkit
{
    [Serializable]

    public class Connections : List<ConnectionItem>, ISavable
    {
        private string appPath;

        [XmlElement("listGroups")]
        public List<string> listGroups = new List<string>();
        public Connections()
        {
            appPath = Application.StartupPath.ToString();            
            ReadBase();
        }
        public void ReadBase()
        {
            ReadBase(File.ReadAllText(appPath + "\\" + this.GetType().Name + ".xml"));
        }
        public void ReadBase(String xmlString)
        {
            try
            {
                this.Clear();
                List<ConnectionItem> connections = xmlString.DeserializeFromStringXml<List<ConnectionItem>>();
                if (connections != null)
                {
                    this.Clear();
                    foreach (ConnectionItem connection in connections)
                    {
                        if (connection.Enabled)
                        {
                            this.Add(connection);
                            if (connection.Group == null) connection.Group = "";
                            if (listGroups.FindAll(S => S.Equals(connection.Group.ToString())).Count == 0)
                                listGroups.Add(connection.Group);
                        }
                    }
                }
            }
            catch (Exception ex) { Functions.Message(ex); }
        }

        public void SaveBase()
        {
            try
            {
                string xmlString = EngineSerialization.SerializeToStringXml(this);
                string fileName = appPath + "\\" + this.GetType().Name + ".xml";
                File.WriteAllText(fileName, xmlString);
            }
            catch (Exception ex) { Functions.Message(ex); }

        }

        public static explicit operator List<object>(Connections v)
        {
            throw new NotImplementedException();
        }


        //public void ReadBase()
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_CONNECTIONS", "ComputerToolkitConnections.bin"), FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<ConnectionItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //    CleanBase();
        //    FillGroups();
        //}
        //public void ReadBase(string filePath)
        //{
        //    try
        //    {
        //        using (Stream stream = File.Open(filePath, FileMode.Open))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            FillThisFromList((List<ConnectionItem>)bformatter.Deserialize(stream));
        //        }
        //    }
        //    catch { }
        //    CleanBase();
        //    FillGroups();
        //}
        //private void FillThisFromList(List<ConnectionItem> tempList)
        //{
        //    this.Clear();
        //    foreach (ConnectionItem connection in tempList)
        //    {
        //        this.Add(connection);
        //    }
        //    foreach (ConnectionItem item in tempList)
        //    {
        //        if (item.Date == null)
        //        {
        //            item.Date = DateTime.Now.ToString();
        //        }
        //    }
        //    foreach (ConnectionItem connection in tempList)
        //    {
        //        connection.IconConnectionString = Functions.IconToBase64(connection.IconConnection);
        //    }

        //}
        //public void SaveBase()
        //{
        //    CleanBase();
        //    try
        //    {
        //        using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_CONNECTIONS", "ComputerToolkitConnections.bin"), FileMode.Create))
        //        {
        //            var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //            bformatter.Serialize(stream, this);
        //        }

        //    }
        //    catch { }
        //    try
        //    {
        //        string xmlString = EngineSerialization.SerializeToStringXml(this);
        //        string fileName = appPath + "\\" + this.GetType().Name + ".xml";
        //        File.WriteAllText(fileName, xmlString);
        //    }
        //    catch (Exception ex) { Functions.Message(ex.Message); }
        //}
        //private void FillGroups()
        //{
        //    foreach (ConnectionItem file in this)
        //    {
        //        if (listGroups.FindAll(S => S.Contains(file.Group.ToString())).Count == 0)
        //            listGroups.Add(file.Group);
        //    }
        //}
        //private void CleanBase()
        //{
        //    foreach (ConnectionItem conn in this)
        //    {
        //        if (conn.Protocol.ToString().Contains("RDP"))
        //            conn.IconConnection = Resources.rdp;
        //        if (conn.Protocol.ToString().Contains("VNC"))
        //            conn.IconConnection = Resources.vnc;
        //        if (conn.Protocol.ToString().Contains("TEAMVIEWER"))
        //            conn.IconConnection = Resources.teamviewer;
        //        if (conn.Protocol.ToString().Contains("AMMY"))
        //            conn.IconConnection = Resources.ammy;
        //        if (conn.Group.IndexOf("\\") > -1)
        //        {
        //            conn.Group = conn.Group.Replace("\\", "");
        //        }

        //    }
        //}

    }
}
