﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace ComputerToolkit.Items
{
    [Serializable]
    public class Methods
    {
        private string appPath;
        public List<Method> mainList = new List<Method>();
        public List<Method> globalList = new List<Method>();
        public List<string> Members = new List<string>();
        public Methods()
        {
            appPath = Application.StartupPath.ToString();
        }

        public void RefrshAllMethods()
        {
            RefreshGlobalMethods();
            RefreshMainMethods();
        }
        public void RefreshMainMethods()
        {
        mainList = Functions.GetAllMainMethods();
            FillMembers();
            //SaveMainMethodsBase();
        }
        public void RefreshGlobalMethods()
        {
        globalList = Functions.GetAllGlobalMethods();
            //SaveGlobalMethodsBase();
        }
        private void FillThisFromListMain(List<Method> methods)
        {
            mainList.Clear();
            foreach (Method method in methods)
            {
                mainList.Add(method);
            }
        }
        private void FillThisFromListGlobal(List<Method> methods)
        {
            mainList.Clear();
            foreach (Method method in methods)
            {
                globalList.Add(method);
            }
        }
        public void ReadGlobalMethodsBase()
        {
            try
            {
                using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_METHODS_GLOBAL", "ComputerToolkitMethodsGlobal.bin"), FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                    FillThisFromListGlobal((List<Method>)bformatter.Deserialize(stream));
                }
            }
            catch { }
            FillMembers();
        }
        public void SaveGlobalMethodsBase()
        {
            //try
            //{
                using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_METHODS_GLOBAL", "ComputerToolkitMethodsGlobal.bin"), FileMode.Create))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                    bformatter.Serialize(stream, globalList);
                }
            //}
            //catch { }

        }
        public void ReadMainMethodsBase()
        {
            try
            {
                using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_METHODS_MAIN", "ComputerToolkitMethodsMain.bin"), FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                    FillThisFromListMain((List<Method>)bformatter.Deserialize(stream));
                }
            }
            catch { }
            FillMembers();
        }
        public void SaveMainMethodsBase()
        {
            try
            {
                using (Stream stream = File.Open(appPath + "\\" + Settings.Get("FILE_METHODS_MAIN", "ComputerToolkitMethodsMain.bin"), FileMode.Create))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                    bformatter.Serialize(stream, mainList);
                }
            }
            catch { }

        }
        private void FillMembers()
        {
            try
            {
                foreach (Method method in globalList)
                {
                    if (Members.FindAll(S => S.Contains(method.Member)).Count == 0)
                        Members.Add(method.Member);
                }
            }
            catch { }
        }
    }
    [Serializable]
    public class Method
    {
        public string Member { get; set; }
        public string Name { get; set; }
        public string ReturnType { get; set; }
        public List<Parameter> Parameters { get; set; }
        public Method(string member, string name, string returnType, List<Parameter> parameters =null)
        {
            this.Name = name;
            this.Member = member;
            this.ReturnType = returnType;
            this.Parameters = parameters;
        }

    }
    [Serializable]
    public class Parameter
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public Parameter(string name, string type)
        {
            this.Name = name;
            this.Type = type;
        }
    }

}
