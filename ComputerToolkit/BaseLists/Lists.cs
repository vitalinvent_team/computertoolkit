﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComputerToolkit
{
    public class Lists : List<Object>
    {

        //MAIN_LISTS***********************************************
        public Connections connections = null;
        public Files files = null;
        public FilesQuick filesQuick = null;
        public Hotkeys hotkeys = null;
        public Notes notes = null;
        public Plays plays = null;
        public Scripts scripts = null;
        public Sensors sensors = null;
        public Tasks tasks = null;
        public Universals universals = null;
                        //universals
        //MAIN_LISTS***********************************************
        public Lists()
        {
            connections = new Connections();
            this.Add(connections);
            files = new Files();
            this.Add(files);
            filesQuick = new FilesQuick();
            this.Add(filesQuick);
            hotkeys = new Hotkeys();
            this.Add(hotkeys);
            notes = new Notes();
            this.Add(notes);
            plays = new Plays();
            this.Add(plays);
            scripts = new Scripts();
            this.Add(scripts);
            sensors = new Sensors();
            this.Add(sensors);
            tasks = new Tasks();
            this.Add(tasks);
            universals = new Universals();
            this.Add(universals);

        }
        public void SaveBase(Object obj)
        {
            ((ISavable)obj).SaveBase();
        }
    }
}
